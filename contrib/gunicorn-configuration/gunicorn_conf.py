command = '/opt/ischool/ischool/virtenv/bin/gunicorn'
bind = '0.0.0.0:8000'
user = 'ischool'
group = 'ischool'
workers = 3
pythonpath = '/opt/ischool/ischool/virtenv/bin/python'
loglevel= 'debug'
errorlog='/tmp/gunicorn.log'
