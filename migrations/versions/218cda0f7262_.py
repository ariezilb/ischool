"""
Add ondelete cascades to small_group_members

Revision ID: 218cda0f7262
Revises: 55108771204c
Create Date: 2015-04-12 00:56:02.780030

"""

# revision identifiers, used by Alembic.
revision = '218cda0f7262'
down_revision = '55108771204c'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_constraint("small_group_members_ibfk_1", "small_group_members", "foreignkey")
    op.create_foreign_key("small_group_members_ibfk_1", "small_group_members", "small_groups", ["small_group_id"], ["id"], ondelete="CASCADE")
    op.drop_constraint("small_group_members_ibfk_2", "small_group_members", "foreignkey")
    op.create_foreign_key("small_group_members_ibfk_2", "small_group_members", "people", ["person_id"], ["id"], ondelete="CASCADE")

def downgrade():
    op.drop_constraint("small_group_members_ibfk_1", "small_group_members", "foreignkey")
    op.create_foreign_key("small_group_members_ibfk_1", "small_group_members", "small_groups", ["small_group_id"], ["id"])
    op.drop_constraint("small_group_members_ibfk_2", "small_group_members", "foreignkey")
    op.create_foreign_key("small_group_members_ibfk_2", "small_group_members", "people", ["person_id"], ["id"])
