# Migrations!

So how do we do migrations? Simple:

1) Make changes to the model
2) python manage.py db migrate
3) Edit and review the migrate script to make sure it caught everything
4) To update the db in other places, just run "python manage.py db upgrade"

Easy!
