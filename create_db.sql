CREATE DATABASE school_db character set utf8 collate utf8_unicode_ci;
CREATE USER 'ischool'@'localhost' IDENTIFIED BY '1234';
GRANT ALL PRIVILEGES ON school_db.* TO 'ischool'@'localhost' WITH GRANT OPTION;
