from setuptools import setup, find_packages

version = "0.0.1"


setup(
    name='ischool',
    version=version,
    url='https://bitbucket.org/ariezilb/ischool',
    author='iSchool Software Foundation',
    description='iSchool',
    packages=find_packages()
)
