# Ubuntu Prerequisites:

    sudo apt-get install python2.7
    sudo apt-get install python2.7-dev
    sudo apt-get install python-pip
    sudo apt-get install mysql-server libmysqlclient-dev
    sudo service mysql start

# Mac Prerequisites:

Download mysql from: http://dev.mysql.com/downloads/mysql/

Change Path to something like this (depends on the version you've downloaded)

    export PATH=$PATH:/usr/local/mysql-5.5.20-osx10.6-x86_64/bin
    export DYLD_LIBRARY_PATH="/usr/local/mysql-5.6.23-osx10.8-x86_64/lib/"

    /usr/local/mysql/support-files/mysql.server start

Install SequelPro

# Windows Prerequisites:

Install ActivePython from: http://www.activestate.com/activepython (don't use regular python, installing pip is a nightmare)
Make sure to install 32 bit python so that pre-compiled binaries will work.

Install MySQL Community Server

Add "C:\Program Fi...\MySQL\MySQL Server 5.6\bin" directory to path.

Install MySQL service (in new cmd with updated path):

    mysqld --install

Start the MySQL service through services.msc

# General Instructions:

## Setup Virtualenv (most people don't bother with this on windows)

sudo pip install virtualenv

virtualenv virtenv

### [NIX]

    source virtenv/bin/activate

## On all platforms, continue here:

    pip install -r requirements.txt

If installation of mysql failed on windows: install pre-compiled mysql python from: https://pypi.python.org/pypi/MySQL-python/1.2.5

### Compass

To install compass we need ruby. To install ruby properly,
we will use rvm, which is similar to python's virtualenv
The recommened way to do so is like this (it's only slightly scary...):

#### Linux Only:

    gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3

#### Linux & Mac:

    \curl -sSL https://get.rvm.io | bash -s stable --ruby
    source ~/.rvm/scripts/rvm

#### MAC - Add your user to rvm:

    sudo dseditgroup -o edit -a root -t user rvm

#### Windows:

Install ruby from http://rubyinstaller.org/downloads/

SSL certificate error can be solved with this: https://gist.github.com/luislavena/f064211759ee0f806c88

#### All platforms

    gem install compass

Run compass to generate css files

    compass compile ischool/site/static

### Create database:

    mysql -u root < create_db.sql

Populate the database

    python manage.py reset
    python manage.py mock_db

Ready to go!

    python manage.py debug
