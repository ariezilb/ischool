from functools import wraps
from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
import flask

from ischool.site import create_app
from ischool.site import mock_data as mocks
from ischool.site.extensions import db

app = create_app()
manager = Manager(app)

migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)

def dangerous_operation(f):
    @wraps(f)
    def inner(*args, **kwargs):
        print "==============================================="
        print "== WARNING: THIS WILL DESTROY THE CURRENT DB =="
        print "==============================================="
        confirm = raw_input("Are you sure you want to continue? (y\N) ").lower()
        if confirm == 'y':
            f(*args, **kwargs)
        else:
            print "Not running command"
    return inner

@manager.command
def mock_db():
    mocks.ready()

@manager.command
@dangerous_operation
def drop_db():
    db.drop_all()

@manager.command
@dangerous_operation
def reset():
    print "Dropping db..."
    db.drop_all()
    print "Creating db..."
    db.create_all()

@manager.command
def debug():
    app.run('0.0.0.0', debug=True)

if __name__ == "__main__":
    manager.run()
