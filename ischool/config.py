import os
from logging.config import dictConfig

class IschoolConfig(object):

    PROJECT = "ischool"

    DEBUG = False
    TESTING = False
    JSONIFY_PRETTYPRINT_REGULAR = False

    SECRET_KEY = "secret key"

    STORAGE_PATH = os.path.join(os.getcwd(), "storage")
    LOG_FOLDER = os.path.join(STORAGE_PATH, "logs")
    UPLOAD_FOLDER = os.path.join(STORAGE_PATH, "uploads")

    SQLALCHEMY_DATABASE_URI = "mysql+mysqldb://ischool:1234@127.0.0.1:3306/school_db?charset=utf8"
    SQLALCHEMY_POOL_RECYCLE = 3600

    TEMPLATE_FOLDER = os.path.join(os.path.dirname(__file__), "site/templates")
    STATIC_FOLDER = os.path.join(os.path.dirname(__file__), "site/static")

    if not os.path.exists(UPLOAD_FOLDER):
        os.makedirs(UPLOAD_FOLDER)

    if not os.path.exists(LOG_FOLDER):
        os.makedirs(LOG_FOLDER)

LOG_CONFIG = {
    "version": 1,
    "formatters": {
        "standard": {
            "format": "%(asctime)s [%(levelname)s] %(name)s: %(message)s"
        },
    },
    "handlers": {
        "log_rotate": {
            "class": "logging.handlers.RotatingFileHandler",
            "formatter": "standard",
            "filename": os.path.join(IschoolConfig.LOG_FOLDER, "ischool.log"),
            "maxBytes": 102400,
            "backupCount": 3,
            "level": "INFO",
        },
        "stream": {
            "class": "logging.StreamHandler",
            "formatter": "standard",
            "level": "INFO"
        }
    },
    "loggers": {
        "ischool-logger": {
            "handlers": ["log_rotate"],
            "level": "INFO",
        },

        "werkzeug": {
            "handlers": ["log_rotate", "stream"],
            "level": "INFO",
        }

    }
}

dictConfig(LOG_CONFIG)
