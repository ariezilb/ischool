#coding: utf8

from sqlalchemy.orm import relationship
from ischool.site.utils import sqlalchemy
from ischool.site.extensions import db
from ischool.site.utils.sqlalchemy import PkColumn
from .small_groups import SmallGroup
from ischool.models.serialization import SerializableModel
import datetime

cuts_phases_table = db.Table('cuts_phases', db.Model.metadata,
                               db.Column('cut_id', db.Integer, db.ForeignKey('cuts.id')),
                               db.Column('phase_id', db.Integer, db.ForeignKey('phases.id')),
)

class Cut(db.Model, sqlalchemy.Model, SerializableModel):

    __tablename__ = "cuts"

    id = PkColumn()

    small_group_id = db.Column(db.Integer, db.ForeignKey('small_groups.id'))
    SmallGroup.cuts = db.relationship('Cut', backref='small_group', cascade='all')

    cutter_id = db.Column(db.Integer, db.ForeignKey('people.id'))
    cutter = db.relationship('Person', foreign_keys=[cutter_id])

    comment = db.Column(db.Text)
    timestamp = db.Column(db.DateTime(), nullable=False, default=datetime.datetime.now)

    phases = db.relation('Phase', secondary=cuts_phases_table, backref='cuts')

