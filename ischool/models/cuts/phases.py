#coding: utf8

from sqlalchemy.orm import relationship
from ischool.site.utils import sqlalchemy
from ischool.site.extensions import db
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.models.instances import LessonInstance
from ischool.models.serialization import SerializableModel


class Phase(db.Model, sqlalchemy.Model, SerializableModel):

    __tablename__ = "phases"

    id = PkColumn()

    instance_id = db.Column(db.Integer, db.ForeignKey('instances.id'))
    LessonInstance.phases = db.relationship('Phase', backref='instance', cascade='all')

    name = db.Column(db.String(64), nullable=False)
    description = db.Column(db.Text, nullable=False)
    order = db.Column(db.Integer, nullable=False, default=0)

