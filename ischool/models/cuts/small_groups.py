#coding: utf8

from sqlalchemy.orm import relationship
from ischool.site.utils import sqlalchemy
from ischool.site.extensions import db
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.models.instances import LessonInstance
from ischool.models.serialization import SerializableModel

small_group_members_table = db.Table('small_group_members', db.Model.metadata,
                               db.Column('small_group_id', db.Integer, db.ForeignKey('small_groups.id', ondelete="CASCADE"), primary_key=True),
                               db.Column('person_id', db.Integer, db.ForeignKey('people.id', ondelete="CASCADE"), primary_key=True),
)

class SmallGroup(db.Model, sqlalchemy.Model, SerializableModel):

    __tablename__ = "small_groups"

    id = PkColumn()

    instance_id = db.Column(db.Integer, db.ForeignKey('instances.id'))
    LessonInstance.small_groups = db.relationship('SmallGroup', backref='instance', cascade='all')

    room_id = db.Column(db.Integer, db.ForeignKey('rooms.id'), nullable=True)
    room = db.relationship('Room', foreign_keys=[room_id])

    members = db.relation('Person', secondary=small_group_members_table, backref='small_groups')
