# coding=utf-8
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.hybrid import hybrid_property
from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
from ischool.models.courses import Course
from ischool.models.exercise.assignment import Assignment
from ischool.models.serialization import SerializableModel
from ischool.site.flask_restless import helpers


class Array(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'arrays'
    api_name = 'array'

    id = PkColumn()
    name = db.Column(db.String(64), nullable=False)
    course_id = db.Column(db.Integer, db.ForeignKey('courses.id'))
    manager_id = db.Column(db.Integer, db.ForeignKey('people.id'))
    track_id = db.Column(db.Integer, db.ForeignKey('tracks.id'))

    instances = db.relationship('LessonInstance', backref='array', cascade='all')
    groups = db.relationship('Group', backref='array', cascade='all')
    units = db.relationship('EducationUnit', backref='array', cascade='all')
    files_usages = db.relationship('FileUsage', backref='array', cascade='all')
    files = association_proxy('files_usages', 'file_revision')

    is_archived = db.Column(db.Boolean, default=False)

    def __init__(self, name, manager_id=None, course=None, course_id=None, groups=None, track_id=None):
        # TODO Does this init make any sense? Why both course and course_id?
        self.name = name
        if manager_id is not None:
            self.manager_id = manager_id
        if course_id is not None:
            course = Course.query.get_or_404(course_id)
        if course is not None:
            self.course = course
        if groups is not None:
            self.groups = groups
        if track_id is not None:
            self.track_id = track_id

    def get_all_bizur_people(self):
        people = set()
        for inst in self.instances:
            people.update(inst.bizur_people())

        return people

    def all_teachers(self):
        return filter(lambda t: t.is_needs_verifying(self.course), self.get_all_bizur_people())

    def get_all_dates(self, group=None):
        instances = self.instances
        if group:
            instances = filter(lambda inst: group == inst.group, instances)
        placements = sum(map(lambda inst: inst.get_real_placements(), instances), [])
        placements = filter(lambda p: p.start_time, placements)
        return list(set(map(lambda p: p.start_time.date(), placements)))

    @property
    def orphan_instances(self):
        return filter(lambda inst: inst.unit is None, self.instances)

    def get_course(self):
        return self.course

    def jsonify(self):
        retval = {}
        for elm in ['id', 'name', 'course_id', 'manager_id', 'track_id']:
            retval[elm] = getattr(self, elm)

        return retval
