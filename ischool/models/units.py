# coding=utf-8
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.hybrid import hybrid_property
from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
from ischool.models.courses import Course
from ischool.models.groups import Group
from ischool.models.arrays import Array
from ischool.models.exercise.assignment import Assignment
from ischool.models.serialization import SerializableModel
from ischool.site.flask_restless import helpers

class EducationUnit(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'edu_units'
    api_name = 'edu-unit'

    id = PkColumn()
    name = db.Column(db.String(128), default=u'יחידת לימוד חדשה')
    array_id = db.Column(db.Integer, db.ForeignKey('arrays.id'))
    _order = db.Column(db.Integer, default=0)
    group_id = db.Column(db.Integer, db.ForeignKey('groups.id'))

    instances = db.relation('LessonInstance', backref='unit', cascade="all,delete,delete-orphan")

    @hybrid_property
    def order(self):
        return self._order

    @order.setter
    def order(self, value):
        if isinstance(value, basestring):
            if value.startswith('+'):
                for i in xrange(int(value[1:])):
                    next = filter(lambda unit2: unit2.order == self._order + 1,
                                  self.array.units)

                    if len(next) > 0:
                        # Just need to re-order
                        next[0]._order -= 1
                        self._order += 1
                    else:
                        # Reached end, bug
                        raise ValueError("Trying to move a unit outbounds")
                return
            elif value.startswith('-'):
                for i in xrange(int(value[1:])):
                    for i in xrange(int(value[1:])):
                        prev = filter(lambda unit2: unit2.order == self._order - 1,
                                      self.array.units)

                    if len(prev) > 0:
                        # Just need to re-order
                        prev[0]._order += 1
                        self._order -= 1
                    else:
                        # Reached end, bug
                        raise ValueError("Trying to move a unit outbounds")
                return
            else:
                value = int(value)
        self._order = value

    def __init__(self, name=None, array=None, array_id=None, order=None, group=None, group_id=None):
        if name is not None:
            self.name = name

        if array_id is not None:
            array = Array.query.get(array_id)
        if array is not None:
            self.array = array

        if group_id is not None:
            group = Group.query.get(group_id)
        if group is not None:
            self.group = group

        if order is None:
            order = max(map(lambda unit: unit.order or 0, self.array.units)) + 1
        self.order = order

    @property
    def total_hours(self):
        return sum(map(lambda inst: inst.hours, self.instances))

    @property
    def total_suggested_hours(self):
        return sum(map(lambda inst: inst.suggested_hours, self.instances))

    @property
    def total_luz_hours(self):
        return sum(map(lambda inst: inst.luz_hours, self.instances))

    def jsonify(self, property_name=None):
        retval = {}
        for elm in ['id', 'name', 'array_id', 'order']:
            retval[elm] = getattr(self, elm)
        return retval

    def clone(self, deep=True):
        new_unit = EducationUnit(name=self.name, array=self.array, order=self.order, group_id=self.group_id)

        if deep:
            for inst in self.instances:
                new_ints = inst.clone()
                new_ints.unit = new_unit
                new_unit.instances.append(new_ints)

        return new_unit
