# coding=utf-8
from hashlib import sha512
from os import urandom
from sqlalchemy.ext.hybrid import hybrid_property
from ischool.site.extensions import db
from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.models.people import Person
from ischool.models.serialization import SerializableModel


class LoginError(ValueError):
    def __init__(self, msg):
        self.msg = msg


class IncorrectUsernameError(LoginError):
    pass


class IncorrectPasswordError(LoginError):
    pass


class User(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'users'
    api_name = 'user'

    # BUG: flask_restless doesn't work well with PUT requests for string primary keys, that's why we need an id
    id = PkColumn()
    username = db.Column(db.String(40), unique=True)
    person_id = db.Column(db.Integer, db.ForeignKey('people.id'), nullable=False)
    password_hash = db.Column(db.LargeBinary(64))
    password_salt = db.Column(db.LargeBinary(16))

    SALT_LENGTH = 32

    def __init__(self, username, person_id=None, password=None):
        self.username = username
        if person_id is not None:
            self.person = Person.query.get_or_404(person_id)

        self.password_salt = urandom(User.SALT_LENGTH)

        if password is not None:
            self.password = password

    @hybrid_property
    def password(self):
        # This is only here for the setter method
        return None

    @password.setter
    def password(self, val):
        self.password_hash = User.generate_hash(val, self.password_salt)


    def change_password(self, val):
        self.password_hash = User.generate_hash(val, self.password_salt)
        db.session.add(self)
        db.session.commit()

    def get_course(self):
        return self.person.get_course()

    @staticmethod
    def generate_hash(password, salt):
        engine = sha512(password)
        engine.update(salt)
        for _ in xrange(50000):
            engine = sha512(engine.digest())
        return engine.digest()

    @classmethod
    def connect(cls, username, password):
        """

        :raises ValueError (IncorrectUsernameError / IncorrectPasswordError) in case of wrong username/password
        :return: User object if parameters are correct
        """
        user = cls.query.filter(User.username == username).first()
        if user is None:
            raise IncorrectUsernameError(u'משתמש לא קיים במערכת')
        if User.generate_hash(password, user.password_salt) != user.password_hash:
            raise IncorrectPasswordError(u'סיסמה שגויה')

        return user

    def jsonify(self):
        retval = {}
        for elm in ['username', 'person_id']:
            retval[elm] = getattr(self, elm)

        return retval
