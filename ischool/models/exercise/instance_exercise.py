# coding=utf-8
from sqlalchemy.orm import backref

from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
from ischool.models.serialization import SerializableModel


class InstanceExercise(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'instance_exercises'
    api_name = 'inst_ex'

    id = PkColumn()

    order = db.Column(db.Integer, default=1)
    is_filler = db.Column(db.Boolean, default=False)

    instance_id = db.Column(db.Integer, db.ForeignKey('instances.id'), nullable=False)
    exercise_id = db.Column(db.Integer, db.ForeignKey('exercises.id'), nullable=False)

    instance = db.relationship('LessonInstance', backref=backref('instance_exercises',
                                                                 order_by=db.asc('instance_exercises.order'), cascade='delete'))

    def get_course(self):
        return self.instance.get_course()

    def clone(self):
        return InstanceExercise(order=self.order, is_filler=self.is_filler, instance_id=self.instance_id, exercise_id=self.exercise_id)
