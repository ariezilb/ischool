# coding=utf-8
from datetime import datetime

from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
from ischool.models.serialization import SerializableModel

class SubmissionLock(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'submission_lock'

    id = PkColumn()

    locked_at = db.Column(db.DateTime, default=lambda: datetime.now())

    locker_id = db.Column(db.Integer, db.ForeignKey('people.id'), nullable=False)
    locker = db.relationship('Person')
