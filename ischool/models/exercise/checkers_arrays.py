# coding=utf-8

from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
from ischool.models.serialization import SerializableModel
from ischool.site.flask_restless import helpers
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import func, UniqueConstraint


class CheckersArrays(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'checkers_arrays'

    __table_args__ = (
        UniqueConstraint('array_id', 'checker_id'),
    )

    api_name = 'car'

    id = PkColumn()

    array_id = db.Column(db.Integer, db.ForeignKey('arrays.id'), nullable=False)
    array = db.relationship('Array', foreign_keys=[array_id])

    checker_id = db.Column(db.Integer, db.ForeignKey('people.id'), nullable=False)
    checker = db.relationship('Person', foreign_keys=[checker_id])
