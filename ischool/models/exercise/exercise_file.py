# coding=utf-8

from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
# from ischool.models.files import FileUsage
from ischool.models.serialization import SerializableModel
from sqlalchemy.ext.hybrid import hybrid_property


class ExerciseFile(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'exercise_files'
    api_name = 'ex_file'
    id = PkColumn()

    file_type = db.Column(db.Enum('master', 'student_copy', 'staff_copy', 'patbas', 'autocheck', 'student_attachment',
                                  'staff_attachment', 'other', name='file_type'), nullable=False)

    file_usage_id = db.Column(db.Integer, db.ForeignKey('file_usages.id'), nullable=False)
    file_usage = db.relationship('FileUsage', backref='exercise_file')

    exercise_id = db.Column(db.Integer, db.ForeignKey('exercises.id'), nullable=False)

    @hybrid_property
    def file_name(self):
        return self.file_usage.file_revision.file_full_name()

    @hybrid_property
    def file_link(self):
        return self.file_usage.file_revision.file_url()

    def file_id(self):
        return self.file_usage.file_revision.id

    def hebrew_file_type(self):
        if self.file_type == 'master':
            return u'עותק מקור'
        elif self.file_type == 'student_copy':
            return u'עותק חניך'
        elif self.file_type == 'staff_copy':
            return u'עותק סגל'
        elif self.file_type == 'patbas':
            return u'פתב"ס'
        elif self.file_type == 'autocheck':
            return u'בודק אוטומטי'
        elif self.file_type == 'student_attachment':
            return u'נספחים לחניך'
        elif self.file_type == 'staff_attachment':
            return u'נספחים לסגל'
        elif self.file_type == 'other':
            return u'אחר - %s' % self.file_name
        else:
            return ''
