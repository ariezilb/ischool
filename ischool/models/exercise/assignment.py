# coding=utf-8

import datetime
from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
from ischool.models.serialization import SerializableModel
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import func, UniqueConstraint


class Assignment(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'assignments'

    __table_args__ = (
        UniqueConstraint('exercise_id', 'person_id'),
    )
    api_name = 'as'

    id = PkColumn()

    IN_PROGRESS = 'in_progress'
    SUBMITTED = 'submitted'
    CHECKING = 'checking'
    RESEND = 'resend'
    DONE = 'done'
    state = db.Column(db.Enum(IN_PROGRESS, SUBMITTED, CHECKING, RESEND, DONE, name='state'),
                      nullable=False,
                      default='in_progress')

    given_time = db.Column(db.DateTime, default=datetime.datetime.now)
    exercise_id = db.Column(db.Integer, db.ForeignKey('exercises.id', ondelete="CASCADE"), nullable=False)
    exercise = db.relationship('Exercise', foreign_keys=[exercise_id])
    person_id = db.Column(db.Integer, db.ForeignKey('people.id'), nullable=False)
    person = db.relationship('Person', foreign_keys=[person_id])
    is_read = db.Column(db.Boolean, default=True)

    submissions = db.relationship('Submission', backref='assignment')

    def jsonify(self):
        retval = {}
        for elm in ['id', 'state', 'exercise_id', 'person_id', 'version']:
            retval[elm] = getattr(self, elm)

        return retval

    @hybrid_property
    def version(self):
        return max(1, len(self.submissions))

    @hybrid_property
    def state_for_student(self):
        if self.state == 'checking':
            return 'submitted'
        return self.state

    @state_for_student.setter
    def state_for_student(self, value):
        """
        In one of the PUT requests someone is trying to set this
        variable, which caused an exception. An empty setter fixes
        the problem, a better solution is to find the PUT request
        and fix it.
        """
        pass

    @version.expression
    def version(cls):
        return func.max(1, func.count(cls.submissions))

    @property
    def hebrew_state(self):
        if self.state == 'in_progress':
            return u'בתהליך'
        elif self.state == 'submitted':
            return u'נשלח'
        elif self.state == 'checking':
            return u'בבדיקה'
        elif self.state == 'resend':
            return u'נסה שוב'
        elif self.state == 'done':
            return u'הושלם'

    @property
    def hebrew_state_for_student(self):
        if self.state == 'in_progress':
            return u'בתהליך'
        elif self.state == 'submitted':
            return u'נשלח'
        elif self.state == 'checking':
            return u'נשלח'
        elif self.state == 'resend':
            return u'נסה שוב'
        elif self.state == 'done':
            return u'הושלם'

    @property
    def state_priority(self):
        prioritized = ['resend', 'in_progress', 'submitted', 'checking', 'done']
        return dict(zip(prioritized, range(len(prioritized))))[self.state]

    def last_submission(self):
        if len(self.submissions) > 0:
            return self.submissions[-1]

    def last_change_time(self):
        last_submission = self.last_submission()
        if last_submission is not None:
            return last_submission.created_at
        return self.given_time
