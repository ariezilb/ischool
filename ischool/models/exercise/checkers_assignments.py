# coding=utf-8

from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
from ischool.models.serialization import SerializableModel
from ischool.site.flask_restless import helpers
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import func, UniqueConstraint


class CheckersAssignments(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'checkers_assignments'

    __table_args__ = (
        UniqueConstraint('exercise_id', 'checker_id', 'student_id'),
    )

    api_name = 'cas'

    id = PkColumn()

    exercise_id = db.Column(db.Integer, db.ForeignKey('exercises.id'), nullable=False)
    exercise = db.relationship('Exercise', foreign_keys=[exercise_id])

    checker_id = db.Column(db.Integer, db.ForeignKey('people.id'), nullable=False)
    checker = db.relationship('Person', foreign_keys=[checker_id])

    student_id = db.Column(db.Integer, db.ForeignKey('people.id'), nullable=False)
    student = db.relationship('Person', foreign_keys=[student_id])

    #def checkers(self):
    #    exercise_ids = [ex.id for ex in self.checkers_assignments]
    #    return [helpers.to_dict(x) for x in list(CheckersAssignments.query.filter(CheckersAssignment.exercise_id.in_(exercise_ids)))]

