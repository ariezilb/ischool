# coding=utf-8

from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
from ischool.models.serialization import SerializableModel
from sqlalchemy.ext.hybrid import hybrid_property


class Exercise(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'exercises'
    api_name = 'ex'

    id = PkColumn()
    name = db.Column(db.String(128), nullable=False)
    submit_type = db.Column(db.Enum('files', 'ok', 'svn', name='submit_type'), nullable=False, default='files')
    course_specific = db.Column(db.Boolean, default=False)
    comments = db.Column(db.String(2048), nullable=False, default="")

    array_id = db.Column(db.Integer, db.ForeignKey('arrays.id'), nullable=False)
    array = db.relationship('Array', backref='exercises')
    instance_exercise = db.relationship('InstanceExercise', backref='exercise', cascade='all, delete-orphan')

    files = db.relationship('ExerciseFile', backref='exercise', cascade='all, delete-orphan')

    @hybrid_property
    def hebrew_submit_type(self):
        if self.submit_type == 'files':
            return u'קבצים'
        elif self.submit_type == 'ok':
            return u'אישור'
        elif self.submit_type == 'svn':
            return u'svn'
        else:
            return ''

    def jsonify(self, property_name=None):
        if property_name is not None:
            # Use default implementation
            return None

        retval = {}
        for elm in ['id', 'name', 'submit_type', 'course_specific', 'comments', 'array_id']:
            # TODO: files & instance_exercise aren't serialized because .jsonify() is not implemented
            retval[elm] = getattr(self, elm)

        return retval
