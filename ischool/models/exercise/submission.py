# coding=utf-8
from datetime import datetime

from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
from ischool.models.serialization import SerializableModel


class Submission(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'submissions'
    api_name = 'sub-ok'

    id = PkColumn()

    created_at = db.Column(db.DateTime, default=lambda: datetime.now())

    assignment_id = db.Column(db.Integer, db.ForeignKey('assignments.id', ondelete="CASCADE"), nullable=False)
    #assignment = db.relationship('Assignment') -- Already exists as a backref in the Assignment model

    review_id = db.Column(db.Integer, db.ForeignKey('reviews.id'))
    review = db.relationship('Review', uselist=False) # One-to-one relation.

    lock_id = db.Column(db.Integer, db.ForeignKey('submission_lock.id'))
    lock = db.relationship('SubmissionLock', uselist=False, cascade="all,delete-orphan", single_parent=True) # One-to-one relation.

    __mapper_args__ = {
        'polymorphic_identity': 'ok',
        'with_polymorphic': '*'
    }

class SvnPathSubmission(Submission):
    __tablename__ = 'submission_svn_paths'
    api_name = 'sub-svn'

    id = PkColumn()
    submission_id = db.Column(db.Integer, db.ForeignKey('submissions.id'), nullable=False)
    svn_path = db.Column(db.String(750), nullable=False)

    __mapper_args__ = {
        'polymorphic_identity': 'svn'
    }


class FileSubmission(Submission):
    __tablename__ = 'submission_files'
    api_name = 'sub-file'

    id = PkColumn()
    submission_id = db.Column(db.Integer, db.ForeignKey('submissions.id'), nullable=False)
    file_usage_id = db.Column(db.Integer, db.ForeignKey('file_usages.id'), nullable=False)
    file_usage = db.relationship('FileUsage', backref='submission')

    __mapper_args__ = {
        'polymorphic_identity': 'file'
    }

'''
class FileReviewSubmission(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'submission_review_files'
    api_name = 'sub-rev_file'

    id = PkColumn()
    submission__id = db.Column(db.Integer, db.ForeignKey('submissions.id'), nullable=False)
    file__usage_id = db.Column(db.Integer, db.ForeignKey('file_usages.id'), nullable=False)
    file_usage = db.relationship('FileUsage', backref='submission_review')

'''
