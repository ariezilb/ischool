# coding=utf-8
from datetime import datetime

from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
from ischool.models.serialization import SerializableModel

class Review(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'reviews'

    id = PkColumn()

    reviewed_at = db.Column(db.DateTime, default=lambda: datetime.now())

    reviewer_id = db.Column(db.Integer, db.ForeignKey('people.id'))
    reviewer = db.relationship('Person')

    comment_for_staff = db.Column(db.Text)
    comment_for_student = db.Column(db.Text)

    file_id = db.Column(db.Integer, db.ForeignKey('file_usages.id'), nullable=True)
    file = db.relationship('FileUsage')
