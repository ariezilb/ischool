# coding=utf-8

from datetime import datetime
from shutil import move
from uuid import uuid4

from flask import safe_join
from flask.helpers import url_for
from sqlalchemy.ext.hybrid import hybrid_property

from ischool.config import IschoolConfig as config
from ischool.site.extensions import db
from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.models.serialization import SerializableModel

class File(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'files'
    api_name = 'file'

    id = PkColumn()
    name = db.Column(db.String(64), nullable=False)
    type = db.Column(db.String(32), nullable=False)

    revisions = db.relationship('FileRevision',
                                backref=db.backref('file', lazy='joined'),
                                order_by=db.desc('file_revisions.timestamp'),
                                cascade='all')

    def __init__(self, name, type, creator, data_file):
        self.name = name
        self.type = type
        rev = FileRevision(data_file, creator)
        self.revisions.append(rev)

    def get_course(self):
        """
        A file might be linked to many courses
        """
        return None

    @hybrid_property
    def last_revision(self):
        if len(self.revisions) == 1:
            return self.revisions[0]

        return FileRevision.query.with_parent(self)\
            .order_by(db.desc(FileRevision.timestamp)).first()

    @hybrid_property
    def full_name(self):
        return '%s.%s' % (self.name, self.type)

    def jsonify(self):
        retval = {}
        for elm in ['id', 'name', 'type', 'full_name']:
            retval[elm] = getattr(self, elm)

        return retval


class FileRevision(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'file_revisions'
    api_name = 'file_rev'

    id = PkColumn(db.String(32), auto_increment=False,
                  default=lambda: uuid4().hex)
    file_id = db.Column(db.Integer, db.ForeignKey('files.id'))
    timestamp = db.Column(db.DateTime, default=lambda: datetime.now())
    creator_id = db.Column(db.Integer, db.ForeignKey('people.id'))
    creator = db.relation('Person')
    usages = db.relationship('FileUsage', backref=db.backref('file_revision'),
                             cascade='all')

    def __init__(self, data_file, creator):
        self.id = uuid4().hex
        self.creator = creator
        data_path = safe_join(config.STORAGE_PATH, self.get_path())
        move(data_file, data_path)

    def is_latest(self):
        return self == self.file.last_revision or not self.file.last_revision

    def get_path(self):
        # this mustn't start with a slash or a 404 error will be thrown when
        # calling "safe_join"
        return self.id

    def get_course(self):
        """
        A file might be linked to many courses
        """
        return None

    def file_name(self):
        return self.file.name

    def file_type(self):
        return self.file.type

    def file_full_name(self):
        return self.file.full_name

    def file_url(self, name=None, down=True):
        if not name:
            name = self.file_full_name()

        if not down:
            return url_for('ischool.file_get', rev_id=self.id, slug=name, _external=True, down=down)
        else:
            return url_for('ischool.file_get', rev_id=self.id, slug=name, _external=True)

    def jsonify(self):
        retval = {}
        for elm in ['id', 'file_id', 'timestamp', 'creator_id', 'file_name', 'file_type', 'file_full_name',
                    'file_url']:
            retval[elm] = getattr(self, elm)

        return retval


class FileUsage(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'file_usages'
    api_name = 'usage'

    id = PkColumn()
    file_revision_id = db.Column(db.String(32), db.ForeignKey('file_revisions.id'))

    array_file_id = db.Column(db.Integer, db.ForeignKey('arrays.id'))
    array_file = db.relationship('Array', foreign_keys=[array_file_id])
    inst_run_id = db.Column(db.Integer, db.ForeignKey('instances.id'))
    inst_run = db.relationship('LessonInstance', backref='run_files',
                               primaryjoin='FileUsage.inst_run_id == LessonInstance.id')
    inst_fu_id = db.Column(db.Integer, db.ForeignKey('instances.id'))
    inst_fu = db.relationship('LessonInstance', backref='fu_files',
                              primaryjoin='FileUsage.inst_fu_id == LessonInstance.id')

    def file_name(self):
        return self.file_revision.file_name()

    def file_type(self):
        return self.file_revision.file_type()

    def file_full_name(self):
        return self.file_revision.file_full_name()

    def file_url(self):
        return self.file_revision.file_url()

    def __init__(self, file_revision):
        self.file_revision = file_revision

    def get_linked_object(self):
        if self.array_file is not None:
            return self.array_file
        elif self.exercise_file is not None:
            return self.exercise_file
        return None

    def get_course(self):
        linked_obj = self.get_linked_object()
        if linked_obj is None:
            return None
        if not hasattr(linked_obj, 'get_course'):
            return None
        return linked_obj.get_course()
