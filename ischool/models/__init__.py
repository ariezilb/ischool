from .people import *
from .courses import *
from .arrays import *
from .units import *
from .instances import *
from .groups import *
from .files import *
from .placements import *
from .exercise import *
from .exercise.assignment import *
from .exercise.submission import *
from .points import *
from .users import *
from .rooms import *
from .medida import *
from .medida.criteria import *
from .medida.exercise_criterion import *
from .medida.scores import *
from .meetings import *
from .meetings.meetings import *
from .cuts import *

current_locals = locals()
