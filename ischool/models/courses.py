# coding=utf-8
from datetime import datetime, timedelta
from sqlalchemy.orm import backref

from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
from ischool.models.serialization import SerializableModel


class Course(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'courses'
    api_name = 'course'

    id = PkColumn()
    name = db.Column(db.String(40), nullable=False)
    eng_name = db.Column(db.String(40), unique=True)
    order = db.Column(db.Integer, nullable=False, default=0)
    start_date = db.Column(db.DateTime, nullable=False)

    arrays = db.relation('Array', backref='course', cascade='all')
    tracks = db.relation('Track', backref='course', cascade='all')
    rooms = db.relation('Room', backref='course', cascade='all')

    def __init__(self, name=u'קורס חדש', eng_name=u'new_course', order=None, start_date=None):
        self.name = name
        self.eng_name = eng_name
        if order is None:
            order = max(Course.query.all(), key=lambda c: c.order).order + 1
        self.order = order
        if start_date is None:
            start_date = datetime.now()
        self.start_date = start_date

    def get_course(self):
        return self

    def get_week_num(self, date):
        """
        :type date datetime
        """
        date = date.date()  # throw away the time of day (if any)
        course_week_start = (self.start_date - timedelta(self.start_date.isoweekday() % 7)).date()
        return (date - course_week_start).days / 7 + 1

    def get_week_time(self, week_num):
        """
        :type week_num int
        """
        course_week_start = (self.start_date - timedelta(self.start_date.isoweekday() % 7)).date()
        week_start = course_week_start + timedelta(7 * (week_num - 1))
        return week_start, week_start + timedelta(days=6)

    def get_active_arrays(self):
        return filter(lambda arr: not arr.is_archived, self.arrays)

    def get_archived_arrays(self):
        return filter(lambda arr: arr.is_archived, self.arrays)

    def jsonify(self):
        retval = {}
        for elm in ['id', 'name', 'eng_name', 'order', 'start_date']:
            retval[elm] = getattr(self, elm)

        return retval
