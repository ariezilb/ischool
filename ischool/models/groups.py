# coding=utf-8
from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
from ischool.models.arrays import Array
from ischool.models.serialization import SerializableModel

group_members_table = db.Table('group_members', db.Model.metadata,
                               db.Column('group_id', db.Integer, db.ForeignKey('groups.id')),
                               db.Column('person_id', db.Integer, db.ForeignKey('people.id')),
)


class Group(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'groups'
    api_name = 'group'

    id = PkColumn()
    name = db.Column(db.String(48), nullable=False)
    level = db.Column(db.Text)
    array_id = db.Column(db.Integer, db.ForeignKey('arrays.id'))

    manager_id = db.Column(db.Integer, db.ForeignKey('people.id'))
    manager = db.relation('Person')

    members = db.relation('Person', secondary=group_members_table, backref='groups')

    units = db.relation('EducationUnit', backref='group', cascade="all,delete-orphan")

    def __init__(self, name=u'new group', level=u'ללא רמה', manager_id=None, array_id=None, base_group_id=None):
        """
        Create a new group.
        :type name: unicode
        """
        self.name = name
        self.level = level
        self.manager_id = manager_id
        if array_id is not None:
            self.array = Array.query.get_or_404(array_id)

        if base_group_id is not None:
            other_group = Group.query.filter_by(id=base_group_id, array_id=self.array.id).first_or_404()

            for unit in other_group.array.units:
                if unit.group_id == base_group_id:
                    new_unit = unit.clone()
                    new_unit.group = self
                    self.array.units.append(new_unit)
                    self.units.append(new_unit)

    def get_course(self):
        return self.array.course

    def jsonify(self):
        retval = {}
        for elm in ['id', 'name', 'array_id']:
            retval[elm] = getattr(self, elm)

        return retval
