# coding=utf-8

from ischool.site.extensions import db
from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.models.serialization import SerializableModel
from sqlalchemy.ext.hybrid import hybrid_property
from flask import abort

class Point(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'points'
    api_name = 'point'

    id = PkColumn()

    room_id = db.Column(db.Integer, db.ForeignKey('rooms.id'), nullable=True)
    room = db.relationship('Room', foreign_keys=[room_id])

    type = db.Column(db.Enum('bathroom', 'exercise', 'general', name='type'),
                      nullable=False,
                      default='general')

    student_status = db.Column(db.Enum('active', 'hidden', name='student_status'), nullable=False, default='active')
    _staff_status = db.Column('_staff_status', db.Enum('open', 'handling', 'closed', 'saved', 'approved', name='staff_status'), nullable=False, default='open')

    person_id = db.Column(db.Integer, db.ForeignKey('people.id'), nullable=False)
    person = db.relationship('Person', foreign_keys=[person_id], backref='points')

    assignment_id = db.Column(db.Integer, db.ForeignKey('assignments.id'), nullable=True)
    assignment = db.relationship('Assignment', foreign_keys=[assignment_id], backref='points')

    messages = db.relationship('Message', backref='point', cascade='all')

    @hybrid_property
    def staff_status(self):
        return self._staff_status

    @staff_status.setter
    def staff_status(self, value):
        if value == 'handling' and self._staff_status == 'handling':
                # Point is locked
                abort(403, 'Point %d is locked for editing!' % self.id)
        self._staff_status = value

    def has_messages(self):
        return len(self.messages) > 0

    def first_message(self):
        return self.messages[0]

    def last_message(self):
        return self.messages[-1]

    def unread_messages(self):
        return filter(lambda m: m.read_at is None, self.messages)

    def unread_messages_for_person(self, person):
        return filter(lambda m: m.person_id != person.id, self.unread_messages())

    def is_open(self):
        return self.student_status == 'active'

    def exercise_name(self):
        if self.assignment_id:
            return self.assignment.exercise.name

    def array_id(self):
        if self.assignment_id:
            return self.assignment.exercise.array.id

    def title(self):
        if self.type == 'bathroom':
            return u'שירותים'
        elif self.type == 'exercise':
            return u'תרגיל'
            # return self.exercise.name
        else:
            return u'הודעה מ-%s' % self.first_message().created_at.strftime("%d/%m %H:%M")
