from datetime import datetime
from ischool.site.extensions import db
from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.models.serialization import SerializableModel

class Message(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'messages'
    api_name = 'message'

    id = PkColumn()

    text = db.Column(db.String(4096), nullable=False)
    urgent = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime, default=lambda: datetime.now())
    read_at = db.Column(db.DateTime, nullable=True)
    remind_in = db.Column(db.DateTime, default=lambda: datetime.now())

    point_id = db.Column(db.Integer, db.ForeignKey('points.id'), nullable=False)

    person_id = db.Column(db.Integer, db.ForeignKey('people.id'), nullable=False)
    person = db.relationship('Person', foreign_keys=[person_id], backref='messages')

    def mark_read(self):
        self.read_at = datetime.now()

    def jsonify(self):
        retval = {}
        for elm in ['id', 'text', 'urgent', 'created_at', 'read_at', 'remind_in', 'point_id', 'person_id']:
            retval[elm] = getattr(self, elm)

        return retval
