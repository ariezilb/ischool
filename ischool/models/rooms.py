# coding=utf-8
from ischool.site.extensions import db
from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.models.serialization import SerializableModel

class Room(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'rooms'
    api_name = 'room'

    id = PkColumn()
    course_id = db.Column(db.Integer, db.ForeignKey('courses.id'))

    # TODO: add student in charge, number of computers (& types?)
    name = db.Column(db.String(40), nullable=False)
    eng_name = db.Column(db.String(40), nullable=False)

    def __init__(self, name, eng_name):
        self.name = name
        self.eng_name = eng_name
