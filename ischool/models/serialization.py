class SerializableModel(object):
    def jsonify(self):
        raise NotImplementedError()


def jsonify_object(obj, property_name, val=None, cls=None, **params):
    return {
        'objId': obj.id if hasattr(obj, 'id') else -1,
        'objType': obj.api_name if hasattr(obj, 'api_name') else cls.api_name,
        'prop': property_name,
        'value': val if val is not None else getattr(obj, property_name),
        'params': params,
    }
