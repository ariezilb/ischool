#coding: utf8

from sqlalchemy.orm import relationship
from ischool.site.utils import sqlalchemy
from ischool.site.extensions import db
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.models.arrays import Array
from ischool.models.serialization import SerializableModel


class Criterion(db.Model, sqlalchemy.Model, SerializableModel):

    __tablename__ = "criteria"

    id = PkColumn()

    name = db.Column(db.String(64))
    # Descriptions of 1,3,5 scores.
    low_score_description = db.Column(db.Text)
    middle_score_description = db.Column(db.Text)
    high_score_description = db.Column(db.Text)

    array_id = db.Column(db.Integer, db.ForeignKey('arrays.id'))

    Array.criteria = db.relationship('Criterion', backref='array', cascade='all')
    exercises = db.relationship('Exercise', secondary="exercises_criteria", backref="criteria")
