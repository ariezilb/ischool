# coding=utf-8

from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
from ischool.models.medida.criteria import Criterion
from ischool.models.serialization import SerializableModel
from sqlalchemy import UniqueConstraint


class ExerciseCriterion(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'exercises_criteria'

    __table_args__ = (
        UniqueConstraint('exercise_id', 'criterion_id'),
    )

    id = PkColumn()

    criterion_id = db.Column(db.Integer, db.ForeignKey('criteria.id'), nullable=False)
    exercise_id = db.Column(db.Integer, db.ForeignKey('exercises.id'), nullable=False)
