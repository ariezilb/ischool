from ischool.site.utils import sqlalchemy
from ischool.site.extensions import db
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.models.exercise.submission import Submission
from ischool.models.medida.criteria import Criterion
from ischool.models import Person
from ischool.models.cuts.cuts import Cut
from ischool.models.serialization import SerializableModel


class SubmissionScore(db.Model, sqlalchemy.Model, SerializableModel):

    __tablename__ = "submissions_scores"

    id = PkColumn()

    score = db.Column(db.Integer)
    comment = db.Column(db.Text)

    submission_id = db.Column(db.Integer, db.ForeignKey('submissions.id', ondelete="CASCADE"))
    criterion_id = db.Column(db.Integer, db.ForeignKey('criteria.id'))

    Submission.scores = db.relationship('SubmissionScore', backref='submission', cascade="all, delete-orphan")
    Criterion.submissions_scores = db.relationship('SubmissionScore', backref='criterion')


class CutScore(db.Model, sqlalchemy.Model, SerializableModel):

    __tablename__ = "cuts_scores"

    id = PkColumn()

    score = db.Column(db.Integer)
    comment = db.Column(db.Text)

    person_id = db.Column(db.Integer, db.ForeignKey('people.id'))
    Person.cuts_scores = db.relationship('CutScore', backref='person')

    cut_id = db.Column(db.Integer, db.ForeignKey('cuts.id'))
    criterion_id = db.Column(db.Integer, db.ForeignKey('criteria.id'))

    Cut.scores = db.relationship('CutScore', backref='cut')
    Criterion.cuts_scores = db.relationship('CutScore', backref='criterion')
