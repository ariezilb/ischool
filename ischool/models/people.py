# coding=utf-8
from sqlalchemy.ext.hybrid import hybrid_property
from ischool.models.courses import Course
from ischool.models.serialization import SerializableModel, jsonify_object
from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db

db.Model.__table_args__ = {'mysql_engine': 'InnoDB'}


class Person(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'people'
    api_name = 'person'

    id = PkColumn()
    name = db.Column(db.String(100))
    service_type = db.Column(db.Enum('conscript', 'keva', 'reserve', 'citizen', 'kid', 'other', name='service_type'),
                             nullable=False)
    gender = db.Column(db.Enum('male', 'female', name='gender'), nullable=False, default='male')
    main_course_id = db.Column(db.Integer, db.ForeignKey('courses.id'))
    army_personal_num = db.Column(db.Integer, nullable=True)
    identity_num = db.Column(db.Integer, nullable=True)
    details = db.Column(db.Text, nullable=True)

    role = db.Column(db.Enum('student', 'staff', 'track_leader', 'course_chief', 'teacher', 'checker',
                             name='role'), nullable=True)

    # This field should not be here - Student should be a whole different object than person, they shouldn't be in
    # the same table
    course_number = db.Column(db.Integer)

    managing_arrays = db.relation('Array', backref='manager')
    track = db.relation('Track', backref='track_leader')
    tutor_id = db.Column(db.Integer, db.ForeignKey('people.id'), nullable=True)
    tutor = db.relation('Person', backref="students", remote_side=[id])

    main_course = db.relation('Course')
    user = db.relation('User', uselist=False, backref='person', cascade='all')
    contact_details = db.relation('ContactDetail', backref='person', cascade='all')

    assignments = db.relation('Assignment', backref='Assignment', cascade='all', lazy='joined')

    def __init__(self, name=u'איש חדש', gender='male',
                 service_type='conscript', main_course_id=None, role=None):
        self.name = name
        self.gender = gender
        self.service_type = service_type
        self.role = role
        if main_course_id is not None:
            self.main_course = Course.query.get_or_404(main_course_id)

    def is_needs_verifying(self, course):
        """
        :type course: Course
        """
        if self.service_type in ['reserve', 'citizen']:
            return True
        if self.service_type in ['conscript', 'keva']:
            return not self.is_staff(course)
        return False

    def is_staff(self, course=None):
        if course and course.id != self.main_course_id:
            return False
        return self.role in ('staff', 'track_leader', 'course_chief')

    def is_checker(self):
        return self.role in ('checker')

    def is_or_was_staff(self):
        return self.is_staff()

    @staticmethod
    def get_by_name(name):
        """
        Returns the person by the specified name.
        :type name: unicode
        """
        return Person.query.filter(Person.name == name).first()

    @staticmethod
    def get_by_id(person_id):
        """
        Returns the person by the specified name.
        :type name: unicode
        """
        return Person.query.filter(Person.id == person_id).first()

    def get_course(self):
        """
        People are not course-bound
        """
        return self.main_course
        #return None

    def hebrew_gender(self):
        return u'זכר' if self.gender == 'male' else u'נקבה'

    def jsonify(self, property_name=None):
        if property_name == 'gender':
            return [jsonify_object(self, property_name, self.gender),
                    jsonify_object(self, 'heb-gender', self.hebrew_gender())]
        if property_name == 'main_course_id':
            return [jsonify_object(self, property_name, self.main_course_id),
                    jsonify_object(self, 'main-course-name', self.main_course.name)]
        if property_name is not None:
            # Use default implementation
            return None

        retval = {}
        for elm in ['id', 'name', 'service_type',
                    'gender', 'main_course_id', 'army_personal_num',
                    'identity_num', 'main_course', 'user', 'contact_details', 'role', 'course_number']:
            retval[elm] = getattr(self, elm)

        if self.tutor is not None:
            retval['tutor'] = self.tutor.name

        if len(self.students) > 0:
            retval['students'] = [{'name': s.name, 'user': s.user.username if s.user is not None else '', 'course_number': s.course_number} for s in self.students]

        return retval


class ContactDetail(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'contact_details'
    api_name = 'contact'

    id = PkColumn()
    person_id = db.Column(db.Integer, db.ForeignKey('people.id'), nullable=False)
    type = db.Column(
        db.Enum('cellphone', 'home_phone', 'office_phone', 'green_phone', 'extension', 'matkali', 'red_phone',
                'address', 'voip', name='contact_detail_type'), nullable=False)
    value = db.Column(db.String(64))

    def __init__(self, detail_type='cellphone', value=u'...', person=None):
        """
        :type detail_type: unicode
        :type value: unicode
        :type person: Person
        """
        self.type = detail_type
        self.value = value
        if person:
            self.person = person

    def hebrew_type(self):
        if self.type == 'cellphone':
            return u'טלפון נייד'
        if self.type == 'home_phone':
            return u'בית'
        if self.type == 'office_phone':
            return u'משרד'
        if self.type == 'green_phone':
            return u'טלפון ירוק'
        if self.type == 'extension':
            return u'שלוחה'
        if self.type == 'matkali':
            return u'טלפון מטכ"לי'
        if self.type == 'red_phone':
            return u'טלפון אדום'
        if self.type == 'address':
            return u'כתובת'
        if self.type == 'voip':
            return u'VoIP'
        return u'ICQ'

    def get_course(self):
        return self.person.get_course()

    def jsonify(self, property_name=None):

        retval = {}
        for elm in ['id', 'person_id', 'type', 'value']:
            retval[elm] = getattr(self, elm)

        retval['hebrew_type'] = self.hebrew_type()

        return retval


def get_staff_dropdown():
    people = Person.query.filter(Person.role != "student")
    pl = "{"
    for p in people:
        pl += "'" + str(p.id) + "':'" + p.name + "',"
    pl = pl[:-1] + "}"
    return pl


def get_people_by_start(name_start):
    if name_start.isdigit():
        people = Person.query.filter(Person.course_number == int(name_start))
    else:
        people = Person.query.filter(Person.name.startswith(name_start))
    people_info = []
    for person in people:
        name = person.name
        if person.role == u"student":
            name = (u"%d•"%person.course_number) + name
        people_info.append({"name": name, "id": person.id, "type": "person"})
    return people_info
