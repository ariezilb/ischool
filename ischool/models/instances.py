# coding=utf-8
from datetime import datetime
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import backref
from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.extensions import db
from ischool.models.units import EducationUnit
from ischool.models.arrays import Array
from ischool.models.placements import RealPlacement
from ischool.models.serialization import SerializableModel, jsonify_object

class LessonInstance(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'instances'
    api_name = 'inst'

    id = PkColumn()
    based_on_instance_id = db.Column(db.Integer, db.ForeignKey('instances.id'))
    array_id = db.Column(db.Integer, db.ForeignKey('arrays.id'), nullable=False)
    unit_id = db.Column(db.Integer, db.ForeignKey('edu_units.id'))
    name = db.Column(db.String(64))
    type = db.Column(db.Enum('self-work', 'self-learn', 'targil', 'lesson', 'cut', 'project', 'tamak', name='instance_type'), default='lesson')
    teacher = db.Column(db.String(64))
    hours = db.Column(db.Float, nullable=False, default=0)
    suggested_hours = db.Column(db.Float, nullable=False, default=0)
    _order = db.Column(db.Integer)
    comments = db.Column(db.String(2048), nullable=False)

    tasks = db.relationship('InstanceTask', backref='instance', cascade='all', lazy='joined')
    based_instances = db.relationship('LessonInstance',
                                      backref=backref('based_on_instance', foreign_keys=[based_on_instance_id], remote_side=[id]))
    placements = db.relationship('AbstractPlacement', backref='instance', cascade='all', lazy='joined')

    # TODO: this feature is temporary, and as every temporary feature it's probably here for the long run.
    # TODO: However, if you ever change it - it comes to replace the outsiders from organization confirmation.
    marked_as_verified = db.Column(db.Boolean, default=False)

    def __init__(self, name=u'מופע חדש', type='lesson', hours=0, suggested_hours=0, array=None, unit=None,
                 order=None, comments='', array_id=None, unit_id=None, group_id=None, teacher=None):
        """
        :type name: unicode
        :type type: unicode
        :type hours: float
        :type suggested_hours: float
        :type array: ischool.models.Array
        :type unit: EducationUnit
        """
        self.name = name
        self.type = type
        self.hours = hours
        self.suggested_hours = suggested_hours
        self.comments = comments
        self.teacher = teacher
        self.unit_id = unit.id if unit else unit_id
        self.array_id = array.id if array else array_id

        self.unit = EducationUnit.query.get_or_404(self.unit_id)
        self.array = Array.query.get_or_404(self.array_id)

        if array and order is None:
            if unit:
                order = max(map(lambda inst: inst.order or 0, unit.instances)) + 1
            else:
                order = max(map(lambda inst: inst.order or 0, array.orphan_instances)) + 1

        if order is not None:
            self.order = order
        elif self.unit and self.unit.instances:
            # Pick the largest order and add 1 to it
            self.order = (max(self.unit.instances,
                              key=lambda inst: inst.order).order or 0) + 1
        elif self.array and self.array.units:
            # Pick the largest order and add 1 to it
            self.order = max([max(unit.instances, key=lambda inst: inst.order).order or 0 for unit in self.array.units if unit.group == group_id]) + 1
        else:
            print '-- no unit or array'
            self.order = 1

    def hebrew_type(self):
        if self.type == 'self-work':
            return u'ע"ע'
        elif self.type == 'self-learn':
            return u'ל"ע'
        elif self.type == 'targil':
            return u'תרגיל'
        elif self.type == 'lesson':
            return u'שיעור'
        elif self.type == 'cut':
            return u'חיתוך'
        elif self.type == 'project':
            return u'פרויקט'
        elif self.type == 'tamak':
            return u'תמ"כ'
        else:
            return 'WTF @_@'

    def get_real_placements(self):
        return filter(lambda p: isinstance(p, RealPlacement), self.placements)

    def luz_hours(self):
        return float(sum(map(lambda p: p.duration, self.get_real_placements())))

    def bizur_people(self):
        people = set()
        for placement in self.placements:
            people.update(placement.bizur_people)

        return people

    @property
    def is_needs_verifying(self):
        for placement in self.placements:
            if placement.is_needs_verifying():
                return True
        return False

    @property
    def is_all_placements_verified(self):
        return all([placement.is_all_verified() for placement in self.placements])

    def get_bizurs(self):
        return sum(map(lambda p: p.bizurs, self.placements), [])

    def get_unverified_bizurs(self):
        """
        Returns all bizurs that need to be verified
        :rtype: list
        """
        return filter(lambda b: b.is_needs_verifying, self.get_bizurs())

    def get_unfinished_tasks(self):
        return filter(lambda t: not t.is_done, self.tasks)

    def get_course(self):
        return self.array.get_course()

    def start_time(self):
        l = list(self.get_real_placements())
        l.sort(key=lambda p: p.start_time)
        if len(l):
            return l[0].start_time
        return None

    def get_all_followups(self, except_instances=None):
        """
        A method returning all follow up documents of the instance, and all of it's based lessons hierarchy.
        The method returns a dictionary from a file to the lesson it was written about.
        """

        if except_instances is None:
            except_instances = set()

        # an array of tuples - (fu_file, fu_file's lesson)
        fus = [] + map(lambda f: (f, self), self.fu_files)
        visited_lessons = except_instances.union([self])

        predecessor = self.based_on_instance
        if predecessor and predecessor != self:
            visited_lessons.update([predecessor])
            fus += predecessor.get_all_followups(except_instances=visited_lessons)

        if self.based_instances:
            for successor in self.based_instances:
                if successor not in visited_lessons:
                    visited_lessons.update([successor])
                    fus += successor.get_all_followups(except_instances=visited_lessons)

        return dict(fus)

    def weeknum(self):
        start_time = self.start_time()
        if not start_time:
            return None

        return self.get_course().get_week_num(start_time)

    def clone(self):
        new_inst = LessonInstance(name=self.name, type=self.type, hours=self.hours, suggested_hours=self.suggested_hours,
                              unit=self.unit, array=self.array, order=self.order, teacher=self.teacher)
        new_inst.based_on_instance = self
        for task in self.tasks:
            if not task.is_done or task.is_recurring:
                new_inst.tasks.append(task.clone())

        for inst_ex in self.instance_exercises:
            new_inst_ex = inst_ex.clone()
            new_inst_ex.instance = new_inst
            db.session.add(new_inst_ex)

        # TODO: restore this logic when restoring placements & bizurs
        #if for_group:
        #    for placement in self.placements:
        #        new_inst.placements.append(PlannedPlacement(for_group))

        return new_inst

    @hybrid_property
    def order(self):
        return self._order

    @order.setter
    def order(self, value):
        if isinstance(value, basestring):
            if value.startswith('+'):
                for i in xrange(int(value[1:])):
                    if self.unit is not None:
                        unit_instances = self.unit.instances
                    else:
                        unit_instances = self.array.orphan_instances

                    next = filter(lambda inst2: inst2._order == self._order + 1,
                                  unit_instances)

                    if len(next) > 0:
                        # Just need to re-order
                        next[0]._order -= 1
                        self._order += 1
                    else:
                        # Reached end, move to next unit

                        if self.unit is not None:
                            # Move it to next existing unit (or None)
                            new_unit = EducationUnit.query.filter_by(array=self.array,
                                                                     _order=self.unit._order + 1).first()
                            if new_unit is not None:
                                new_unit_instances = new_unit.instances
                            else:
                                new_unit_instances = self.array.orphan_instances
                            for inner_inst in new_unit_instances:
                                inner_inst._order += 1
                        else:
                            # WTF, segfault
                            raise ValueError("Trying to move outbounds an instance without a unit")

                        self._order = 1
                        self.unit = new_unit
                return
            elif value.startswith('-'):
                for i in xrange(int(value[1:])):
                    if self.unit is not None:
                        unit_instances = self.unit.instances
                    else:
                        unit_instances = self.array.orphan_instances

                    prev = filter(lambda inst2: inst2._order == self._order - 1,
                                  unit_instances)
                    if len(prev) > 0:
                        # Just need to re-order
                        prev[0]._order += 1
                        self._order -= 1
                    else:
                        # Reached beginning, move to prev unit

                        for inner_inst in unit_instances:
                            inner_inst._order -= 1

                        if self.unit is not None:
                            # Move it to previous existing unit
                            new_unit = EducationUnit.query.filter_by(array=self.array,
                                                                     _order=self.unit._order - 1).first()
                        else:
                            # Move it to last existing unit
                            new_unit = max(EducationUnit.query.filter(EducationUnit.array == self.array).all(),
                                           key=lambda x: x.order)

                        if len(new_unit.instances) > 0:
                            max_order = max(new_unit.instances, key=lambda x: x._order)._order
                        else:
                            max_order = 0

                        self._order = max_order + 1
                        self.unit = new_unit
                return
            else:
                value = int(value)
        self._order = value

    def _default_jsonify(self):
        retval = {}
        for elm in ['id', 'name', 'array_id', 'unit_id', 'type', 'hours', 'suggested_hours', 'luz_hours', 'order',
                    'comments', 'hebrew_type', 'start_time']:
            retval[elm] = getattr(self, elm)
        return retval

    def jsonify(self, property_name=None):
        if property_name == 'preferred':

            if self.unit is not None:
                instances = self.unit.instances
            else:
                instances = self.array.orphan_instances

            return [jsonify_object(self, property_name, self.suggested_hours),
                    jsonify_object(self.unit, 'total-preferred', LessonInstance.sum_suggested_hours(instances),
                                   cls=EducationUnit)]
        elif property_name == 'hours':
            if self.unit is not None:
                instances = self.unit.instances
            else:
                instances = self.array.orphan_instances

            return [jsonify_object(self, property_name, self.hours),
                    jsonify_object(self.unit, 'total-allocated', LessonInstance.sum_instances_hours(instances),
                                   cls=EducationUnit)]
        elif property_name == 'type':
            return [jsonify_object(self, property_name, self.type),
                    jsonify_object(self, 'heb-type', self.hebrew_type())]
        elif property_name == 'order':
            return [jsonify_object(x, property_name, x.order) for x in self.array.instances]
        elif property_name == 'add-person-bizur-to-first-placement':
            return [jsonify_object(self, property_name, True)]
        elif property_name == '@detailed':
            retval = self._default_jsonify()

            retval['bizur_people'] = list(self.bizur_people())
            retval['tasks'] = list(self.tasks)
            retval['placements'] = list(self.placements)

            return retval
        elif property_name is not None:
            return None

        return self._default_jsonify()


    @staticmethod
    def sum_instances_hours(instances):
        return sum(map(lambda inst: inst.hours, instances))

    @staticmethod
    def sum_suggested_hours(instances):
        return sum(map(lambda inst: inst.suggested_hours, instances))


class InstanceTask(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'instance_tasks'
    api_name = 'inst-task'

    priorities = ['severe', 'important', 'normal', 'low', 'nice_to_have', 'note']

    id = PkColumn()
    instance_id = db.Column(db.Integer, db.ForeignKey('instances.id'), nullable=False)
    text = db.Column(db.String(255), nullable=False)
    priority = db.Column(db.Enum(*priorities, name='priority'), default='normal',
                         nullable=False)
    date_issued = db.Column(db.DateTime, nullable=False)
    date_completed = db.Column(db.DateTime, nullable=True, default=None)
    is_recurring = db.Column(db.Boolean, nullable=False, default=False)

    def __init__(self, text, date_issued=None, date_completed=None):
        """
        :type text: unicode
        :type date_issued: datetime
        :type date_completed: datetime
        """
        self.text = text
        self.date_issued = datetime.now() if date_issued is None else date_issued
        self.date_completed = date_completed

    @hybrid_property
    def is_done(self):
        return self.date_completed is not None

    @is_done.setter
    def is_done(self, value):
        if not value:
            self.date_completed = None
        elif not self.date_completed:
            self.date_completed = datetime.now()

    def get_course(self):
        return self.instance.get_course()

    def clone(self):
        task = InstanceTask(self.text, self.date_issued, self.date_completed)
        task.is_recurring = self.is_recurring
        return task

    @property
    def priority_as_number(self):
        return InstanceTask.priorities.index(self.priority) + 1

    def jsonify(self):
        retval = {}
        for elm in ['id', 'instance_id', 'text', 'priority', 'date_issued', 'date_completed', 'is_recurring',
                    'is_done', 'priority_as_number']:
            retval[elm] = getattr(self, elm)
        return retval
