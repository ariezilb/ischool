# coding=utf-8
from datetime import timedelta
from sqlalchemy.ext.associationproxy import association_proxy
from ischool.site.extensions import db
from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.models.serialization import SerializableModel


placement_rooms_table = db.Table('reality_rooms', db.Model.metadata,
                                 db.Column('reality_id', db.Integer, db.ForeignKey('reality.id')),
                                 db.Column('room_id', db.Integer, db.ForeignKey('rooms.id')))


class AbstractPlacement(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'placements'
    api_name = 'abs-place'

    id = PkColumn()
    instance_id = db.Column(db.Integer, db.ForeignKey('instances.id'), nullable=False)
    bizur_people = association_proxy('bizurs', 'person')
    placement_type = db.Column(db.String(20))

    def __init__(self, bizur_people):
        """
        :param list bizur_people: The people that are bizured in the placement
        """
        if bizur_people:
            for person in bizur_people:
                self.bizur_people.append(person)

    def is_needs_verifying(self):
        for bizur in self.bizurs:
            if bizur.person.is_needs_verifying(self.instance.get_course()):
                return True
        return False

    def is_all_verified(self):
        """
        Returns whether all teachers for the placement were verified
        :rtype : bool
        """
        return all([not bizur.is_needs_verifying for bizur in self.bizurs])

    def is_verifiable(self):
        return True

    def get_course(self):
        return self.instance.get_course()

    def get_bizurs_by_person_id(self, id):
        return filter(lambda b: str(b.person.id) == str(id), self.bizurs)

    def get_bizur_by_person_id(self, id):
        return self.get_bizurs_by_person_id(id)[0]

    def jsonify(self):
        retval = {}
        for elm in ['id', 'instance_id', 'placement_type', 'is_needs_verifying', 'is_all_verified']:
            retval[elm] = getattr(self, elm)
        retval['bizur_people'] = list(self.bizur_people)
        return retval

    __mapper_args__ = {
        'polymorphic_on': placement_type,
        'polymorphic_identity': 'abstract'
    }


class RealPlacement(AbstractPlacement):
    __tablename__ = 'reality'
    api_name = 'placement'

    id = db.Column(db.Integer, db.ForeignKey('placements.id'), primary_key=True)
    start_time = db.Column(db.DateTime, nullable=True)
    duration = db.Column(db.Float, nullable=False)
    display_name = db.Column(db.String(48))
    is_break = db.Column(db.Boolean, default=False)
    is_locked = db.Column(db.Boolean, default=False)
    rooms = db.relationship('Room', secondary=placement_rooms_table, backref='placements', lazy='joined')

    def __init__(self, start_time, duration, display_name=None, bizur_people=None):
        """
        Constructs the placement

        :param datetime start_time: The date and time of the start of the placement
        :param duration: The length in hours for this placement
        :param display_name: The name to display in the schedule. Leave empty for the instance's name
        :param bizur_people: The people that are bizured in the placement
        :type duration: float
        :type display_name: str
        :type bizur_people: list
        """
        AbstractPlacement.__init__(self, bizur_people)
        self.start_time = start_time
        self.duration = duration
        self.display_name = display_name

    @property
    def actual_duration(self):
        return timedelta(hours=float(self.duration))

    @actual_duration.setter
    def actual_duration(self, delta):
        self.duration = float(delta.total_seconds()) / 60 / 60

    def jsonify(self, property_name=None):
        if property_name is not None:
            return None

        retval = super(RealPlacement, self).jsonify(property_name)
        for elm in ['start_time', 'display_name', 'display_name', 'is_break', 'is_locked']:
            retval[elm] = getattr(self, elm)
        retval['duration'] = float(self.duration)
        return retval

    __mapper_args__ = {
        'polymorphic_identity': 'real',
    }


class PlannedPlacement(AbstractPlacement):
    __tablename__ = 'planned_placements'
    api_name = 'planned-placement'

    id = db.Column(db.Integer, db.ForeignKey('placements.id'), primary_key=True)
    week = db.Column(db.Integer, nullable=True)

    def __init__(self, week=None, bizur_people=None):
        """
        Constructs the planned placement
        :param list bizur_people: The people that are bizured to this placement
        """
        AbstractPlacement.__init__(self, bizur_people)
        self.week = week

    def jsonify(self, property_name=None):
        if property_name is not None:
            return None

        retval = super(PlannedPlacement, self).jsonify(property_name)
        for elm in ['week']:
            retval[elm] = getattr(self, elm)
        return retval

    __mapper_args__ = {
        'polymorphic_identity': 'planned'
    }


class Bizur(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'bizurs'
    api_name = 'bizur'

    placement_id = db.Column(db.Integer, db.ForeignKey('placements.id'), primary_key=True)
    person_id = db.Column(db.Integer, db.ForeignKey('people.id'), primary_key=True)
    is_verified = db.Column(db.Boolean, default=False)

    placement = db.relationship('AbstractPlacement',
                                backref=db.backref('bizurs', cascade='all', lazy='joined'), lazy='joined')
    person = db.relationship('Person', backref=db.backref('bizurs', cascade='all'), lazy='joined')

    def __init__(self, person=None, placement=None, is_verified=False):
        self.person = person
        self.placement = placement
        self.is_verified = is_verified

    def get_course(self):
        return self.placement.get_course()

    def is_verifiable(self):
        return self.person.is_needs_verifying(self.get_course())

    @property
    def is_needs_verifying(self):
        """
        Returns if this Bizur should be verified as it is currently not, and the person related
        to the Bizur should be verified with.
        """
        return (not self.is_verified) and (self.person.is_needs_verifying(self.get_course()))

    def jsonify(self, property_name=None):
        if property_name is not None:
            return None

        retval = {}
        for elm in ['placement_id', 'person_id', 'is_verified', 'is_needs_verifying']:
            retval[elm] = getattr(self, elm)
        return retval
