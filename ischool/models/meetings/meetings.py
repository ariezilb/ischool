from ischool.site.utils import sqlalchemy
from ischool.site.extensions import db
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.models.serialization import SerializableModel
import datetime


class Meeting(db.Model, sqlalchemy.Model, SerializableModel):

    __tablename__ = "meetings"

    id = PkColumn()
    title = db.Column(db.String(100))
    agenda = db.Column(db.Text)
    summary = db.Column(db.Text)
    short_summary = db.Column(db.Text)
    student_id = db.Column(db.Integer, db.ForeignKey('people.id'))
    student = db.relationship('Person', foreign_keys=[student_id], backref='student_meetings')
    staff_id = db.Column(db.Integer, db.ForeignKey('people.id'))
    staff = db.relationship('Person', foreign_keys=[staff_id], backref='staff_meetings')
    scheduled_time = db.Column(db.DateTime(), nullable=True)
    last_updated = db.Column(db.DateTime(), nullable=False, default=datetime.datetime.now)

