from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import aliased
from sqlalchemy import func

from ischool.site.extensions import db
from ischool.site.utils import sqlalchemy
from ischool.site.utils.sqlalchemy import PkColumn
from ischool.site.flask_restless import helpers
from ischool.models import people
from ischool.models import Person
from ischool.models.serialization import SerializableModel

from flask import jsonify
import datetime
import re

hashtag_tweets_table = db.Table('hashtag_tweets', db.Model.metadata,
                               db.Column('hashtag_id', db.Integer, db.ForeignKey('hashtags.id')),
                               db.Column('tweet_id', db.Integer, db.ForeignKey('tweets.id')),
)

people_tweets_table = db.Table('people_tweets', db.Model.metadata,
                               db.Column('person_id', db.Integer, db.ForeignKey('people.id')),
                               db.Column('tweet_id', db.Integer, db.ForeignKey('tweets.id')),
)

class Tweet(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'tweets'
    api_name = 'tweet'

    id = PkColumn()
    content = db.Column(db.Text(), nullable=False)
    timestamp = db.Column(db.DateTime(), nullable=False, default=datetime.datetime.now)

    poster_id = db.Column(db.Integer, db.ForeignKey('people.id'), nullable=False)

    handled = db.Column(db.Boolean(), nullable=False)

    hashtags = db.relation('Hashtag', secondary=hashtag_tweets_table, backref='tweets')

    people = db.relation('Person', secondary=people_tweets_table, backref='tweets')

    def __init__(self, tweet, mentions, poster_id):
        self.hashtags, self.people, tweet = parse_tweet(tweet, mentions)
        self.content = tweet
        self.poster_id = poster_id
        self.handled = False
        db.session.add(self)
        db.session.commit()


    @staticmethod
    def get_by_id(tweet_id):
        return Tweet.query.filter(Tweet.id == tweet_id).first()


class Hashtag(db.Model, sqlalchemy.Model, SerializableModel):
    __tablename__ = 'hashtags'
    api_name = 'hashtag'

    id = PkColumn()
    name = db.Column(db.String(64), nullable=False)

    def __init__(self, name):
        self.name = name
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_by_name(name):
        return Hashtag.query.filter(Hashtag.name == name).first()


    @staticmethod
    def get_by_id(hashtag_id):
        return Hashtag.query.filter(Hashtag.id == hashtag_id).first()

    def jsonify(self, property_name=None):

        retval = {}
        for elm in ['id', 'name']:
            retval[elm] = getattr(self, elm)

        return retval


def parse_tweet(tweet, mentions):

    hashtags = []
    people_list = []
    for mention in mentions:
        if mention["type"] == "person":
            people_list.append(people.Person.get_by_id(mention["id"]))
        if mention["type"] == "hashtag":
            if mention["id"] < 0:
                hashtag = Hashtag.get_by_name(mention["name"])
                if hashtag is None:
                    hashtag = Hashtag(mention["name"])
                tweet = tweet.replace("@[hashtag:%d]"%mention["id"], "@[hashtag:%d]"%hashtag.id)
                hashtags.append(hashtag)
            else:
                hashtags.append(Hashtag.get_by_id(mention["id"]))
    return hashtags, people_list, tweet


def get_person_for_tweet(person_id):
    person = people.Person.get_by_id(person_id)
    return {"id":person_id, "name":person.name, "role":person.role, "course_number":person.course_number}


def parse_tweets(tweets, regex, cls):
    """
    Extract set of items from regex in tweet
    """
    ids = set()
    for tweet in tweets:
        ids.update(re.compile(regex).findall(tweet.content))
    result = cls.query.filter(cls.id.in_(ids))
    return result

def get_hashtags_and_people_for_tweets(tweets):
    people = parse_tweets(tweets, "@\[person:(\d*)\]", Person)
    hashtags = parse_tweets(tweets, "@\[hashtag:(\d*)\]", Hashtag)

    people_info = [{"id": str(person.id), "name": person.name,
        "role": person.role, "course_number": person.course_number}
        for person in people]
    hashtags_info = [{"id": str(h.id), "name": h.name} for h in hashtags]

    return hashtags_info, people_info

def format_tweets_for_client(tweets):
    tweets_content = []
    hashtags_info, people_info = get_hashtags_and_people_for_tweets(tweets)
    for tweet in tweets:
        poster_name = people.Person.query.filter(people.Person.id == tweet.poster_id)[0].name
        tweets_content.append({"content": tweet.content, "hashtags": hashtags_info, "people":people_info,
                               "poster": poster_name, "timestamp": tweet.timestamp.isoformat(),
                               "id": tweet.id, "handled": tweet.handled})
    return tweets_content

def get_last_tweets(tweet_count):
    tweets = list(Tweet.query.order_by("id desc").limit(tweet_count))
    return format_tweets_for_client(tweets)

def get_tweets_between(id_first, id_last):
    tweets = list(Tweet.query.order_by("id desc").filter(id_first <= Tweet.id).filter(Tweet.id < id_last))
    return format_tweets_for_client(tweets)

def get_tweets_since(id):
    tweets = list(Tweet.query.order_by("id desc").filter(Tweet.id > id))
    return format_tweets_for_client(tweets)


def get_tags_since(id):
    tags = list(Hashtag.query.order_by("id desc").filter(Hashtag.id > id))
    return [tag.jsonify() for tag in tags]


def get_all_tags():
    return [tag.jsonify() for tag in Hashtag.query.all()]

def get_all_tweets():
    return format_tweets_for_client(Tweet.query.order_by("id desc"))


def get_hashtags_by_start(hashtag_start):
    tags = Hashtag.query.filter(Hashtag.name.startswith(hashtag_start))
    return [({"name": tag.name, "id": tag.id, "type": "hashtag"}) for tag in tags]

def get_hashtags_by_contains(hashtag_contains):
    tags = Hashtag.query.filter(Hashtag.name.contains(hashtag_contains))
    return [({"name": tag.name, "id": tag.id, "type": "hashtag"}) for tag in tags]


def get_tweets_filtered(hashtag_ids, people_ids, filterHandled):
    query = Tweet.query.order_by("id desc")
    query = filter_tweets(query, hashtag_ids, people_ids, filterHandled)
    return format_tweets_for_client(query.all())

def get_hashtag_cloud(hashtag_ids = [], people_ids = [], filterHandled = False):
    query = Tweet.query.session.query(func.count(Tweet.id), Hashtag.id, Hashtag.name).join(Tweet.hashtags).group_by(Hashtag.id)
    query = filter_tweets(query, hashtag_ids, people_ids, filterHandled)
    return [{'weight': count, 'link': id, 'text': name} for count, id, name in query.all()]


def filter_tweets(query, hashtag_ids, people_ids, filterHandled):
    for id in hashtag_ids:
        t = aliased(hashtag_tweets_table)
        query = query.join(t, t.c['tweet_id'] == Tweet.id)
        query = query.filter(t.c['hashtag_id'] == id)

    for id in people_ids:
        t = aliased(people_tweets_table)
        query = query.join(t, t.c['tweet_id'] == Tweet.id)
        query = query.filter(t.c['person_id'] == id)

    if filterHandled:
        query = query.filter(Tweet.handled == False)

    return query
