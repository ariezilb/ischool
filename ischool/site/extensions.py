from flask.ext.sqlalchemy import SQLAlchemy
db = SQLAlchemy()

from . import flask_restless
api_manager = flask_restless.APIManager()
