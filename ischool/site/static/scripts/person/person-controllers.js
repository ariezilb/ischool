/**
 * User: user1
 * Date: 08/09/13
 * Time: 17:19
 */

function PersonPageCtrl($scope, $http, Restangular) {
    $scope.courses = {};
    $http.get("/api/people/all_staff").then(function (result) {
      $scope.all_tutors = result.data;
    });

    Restangular.all('courses').getList().then(function(courses) {
        $scope.courses = {};

        _(courses).each(function(course) {
            $scope.courses[course.id] = course.name;
        });
    });

    Restangular.one('people', $scope.personId).get().then(function(person) {
        $scope.person = person;
        $scope.tutorId = person.tutor_id;
    });

    $scope.removeContact = function(person, contact) {
        Restangular.one('contact_details', contact.id).remove().then(function() {
            person.contact_details.splice(person.contact_details.indexOf(contact), 1);
        });
    };

    $scope.addContact = function(person) {
        person.contact_details.push({});
        person.put().then(function(person) {
            $scope.person = person;
        })
    };

    $scope.setTutor = function(tutor_id) {
        $scope.person.tutor_id = tutor_id;
        $scope.person.put();
    };

    $scope.savePersonDetails = function(data) {
        $scope.person.details = data.bodyElement.innerHTML;
        $scope.person.put();
        $scope.$apply(); // This hack is used because we run outside the scope of the controller.
    };

    $scope.openAddRole = function() {
        $scope.addRoleOpen = true;
        $scope.newRole = "student";
        $scope.newCourse = _($scope.courses).keys()[0];
    };


    tinymce.init({
        selector: "div.editable",
        inline: true,
        language: 'he_IL',
        directionality : 'rtl',
        menubar: true,
        statusbar: false,
        extended_valid_elements: '-p[style|class]',
        plugins: [
            "advlist autolink lists link charmap preview",
            "visualblocks save table ",
            "table contextmenu paste directionality"
        ],
        toolbar: "save | bold italic underline | alignleft alignright | undo | bullist numlist | link emoticons",
        save_onsavecallback: function(data) {
            $scope.savePersonDetails(data);
        }
    });
}

function PersonListCtrl($scope, Restangular, $modal, $filter, $http) {
    $scope.course = window.env.course;

    $scope.modes = [
        {text: $scope.course.name, filter: {main_course_id: $scope.course.id}},
        {text: 'ללא קורס', filter: {main_course_id: '!'}},
        {text: 'כל האנשים', filter: {}}
    ];

    $http.get('/api/people/all').then(function (result) {
        $scope.people = result.data;
        $scope.roles = _.uniq(_.compact(_.map($scope.people, function (p){ return p.role })));
    })

    $scope.searchedRole = "כל התפקידים";
    $scope.filterByRole = function(person) {
        var expectedRole = document.getElementById("rolesearch").value;
        return (undefined == $scope.roles[expectedRole]) || (person.role == $scope.roles[expectedRole]);
    }

    $scope.filterText = "";

    $scope.peopleViewFilter = function(person) {
        // If the filter is less then 3 digit number, search by course number only // TODO: maybe add phone-number too.
        if ($.isNumeric($scope.filterText) && $scope.filterText.length <= 3) {
            if (person.course_number == $scope.filterText)
                return true;
            if (person.students) {
                return _(person.students).any(function (s) {
                    return s.course_number == $scope.filterText;
                });
            }
        }
        else { // Textual search in the data.
            var translatedFilterText = TranslateByKeyboard($scope.filterText);
            return $filter('filter')([person], $scope.currentMode.filter).length == 1 && $scope.filterByRole(person) && ($filter('filter')([person], $scope.filterText).length == 1 || (translatedFilterText && $filter('filter')([person], translatedFilterText).length == 1));
        }
    };

    $scope.areAllPeopleSelected = function() {
        return $scope.multiselection.getSelectedIds().length == $scope.multiselection.getAllIds().length;
    }

    $scope.togglePeopleSelectAll = function() {
        var shouldSelectAllPeople = document.getElementById("shouldSelectAllPeopleCheckBox").checked && !($scope.areAllPeopleSelected());
        _.map($scope.multiselection.getAllIds(), function (p) { $scope.multiselection.getDomElementById(p).checked = shouldSelectAllPeople; });
    };

    $scope.setPersonListMode = function(mode) {
        $scope.currentMode = mode;
    };

    $scope.setPersonListMode($scope.modes[0]);

    $scope.openNewPersonModal = function() {
        $modal.open({
            templateUrl: "newPersonModal.html",
            controller: NewPersonModalCtrl,
            resolve: {
                mainCourseId: function() { return $scope.course.id; }
            }
        }).result.then(function(person) {
            $scope.people.push(person);
        });
    };

    $scope.deletePerson = function(person) {
        if (!confirm("למחוק את " + person.name + "?")) {
            return;
        }
        Restangular.one('people', person.id).remove().then(function() {
            $scope.people.splice($scope.people.indexOf(person), 1);
        });
    };

    $scope.openUserEditingModal = function (person) {
        var modalInstance = $modal.open({
          templateUrl: 'userEditingModal.html',
          controller: EditUserModalCtrl,
          resolve: {
            parentScope: function() {
                return $scope;
            },
            person: function() {
                return person;
            }
          }
        });

        modalInstance.result.then(function(form) {
            // onConfirm
        }, function () {
            // onDismiss
        });
    };

    // Handle multiselection of people.
    $scope.multiselection = {
        ids: {}, // internal. used to store the ng-models of the checkboxes

        getAllIds: function () {
            return _($scope.people).filter($scope.peopleViewFilter).map(function (p) { return p.id; });
        },
        getDomElementById: function(person_id) {
            return document.getElementById("personCheckBox" + person_id);
        },
        isPersonChecked: function(person_id) {
            var person_dom_element = $scope.multiselection.getDomElementById(person_id);
            return (person_dom_element != undefined && person_dom_element.checked);
        },
        getSelectedIds: function () {
            return _($scope.multiselection.getAllIds()).filter(function (i) { return $scope.multiselection.isPersonChecked(i);});
        },
        getSelectedPeople: function () {
            return _($scope.multiselection.getSelectedIds()).map(function (id) {
                return _($scope.people).where({id: parseInt(id)})[0];
            });
        },
        clearAll: function () {
            _.map($scope.multiselection.getAllIds(), $scope.multiselection.clear);
        },
        clear: function (id) {
            $scope.multiselection.getDomElementById(id).checked = false;
        }
    };

    $scope.deleteSelectedPeople = function () {
        // sanity check.
        if ($scope.multiselection.getSelectedIds().length == 0) {
            alert('שגיאה: לא נבחרו אנשים');
            return;
        }

        // Get the names of the selected people.
        var names = _($scope.multiselection.getSelectedPeople()).map(function(p) {return p.name}).join(', ');

        if (!confirm('האם אתה בטוח שברצונך למחוק ' + $scope.multiselection.getSelectedIds().length + ' אנשים?\n' + names)) return;

        // For each selected person, delete it and update the view.
        var numOfDeleted = 0, failToDelete = [], totalNum = $scope.multiselection.getSelectedIds().length;
        function handleDone() {
            // Check if done.
            if (totalNum == numOfDeleted + failToDelete.length) {
                var failToDeleteMessage = '\nנכשלו: ' + _(failToDelete).map(function(id) {return _($scope.people).where({id: parseInt(id)})[0].name}).join(', ');
                if (failToDelete.length == 0)
                    failToDeleteMessage = '';
                alert(numOfDeleted + ' נמחקו בהצלחה.' + failToDeleteMessage);
            }
        }
        _.each($scope.multiselection.getSelectedIds(), function (id) {
            // Delete the current person.
            Restangular.one('people', id).remove().then(function() {
                $scope.people = _($scope.people).reject(function (p) {return p.id == id;});
                numOfDeleted++;
                handleDone();
            }, function () {
                failToDelete.append(id);
                handleDone();
            });
        });
    };

    $scope.openMultipleUsersEditingModal = function () {
        if ($scope.multiselection.getSelectedIds().length == 0) {
            alert('שגיאה: לא נבחרו אנשים');
            return;
        }

        var modalInstance = $modal.open({
          templateUrl: 'muiltpleUsersEditingModal.html',
          controller: MultipleUsersEditingModalCtrl,
          resolve: {
            parentScope: function() {
                return $scope;
            },
            people: function() {
                return $scope.multiselection.getSelectedPeople();
            }
          }
        });

        modalInstance.result.then(function(form) {
            // onConfirm
        }, function () {
            // onDismiss
        });
    };

    $scope.openMultipleCourseEditingModal = function () {
        alert('Not implemented yet');
        // TODO: open the multiple course editing modal.

        if ($scope.multiselection.getSelectedIds().length == 0) {
            alert('שגיאה: לא נבחרו אנשים');
            return;
        }
    };
}

function NewPersonModalCtrl($scope, mainCourseId, $modalInstance, Restangular) {
    $scope.fields = {
        name: "",
        courseId: mainCourseId.toString(),
        role: "student"
    };

    Restangular.all('courses').getList().then(function(courses) {
        $scope.courses = {};

        _.each(courses, function(course) {
            $scope.courses[course.id] = course.name;
        });
    });

    $scope.save = function() {
        var data = {
            name: $scope.fields.name,
            main_course_id: $scope.fields.courseId,
            role: $scope.fields.role
        };
        Restangular.all('people').post(data).then(function(person) {
            $modalInstance.close(person);
        });
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
}

function EditUserModalCtrl($scope, parentScope, person, $modalInstance, Restangular, $http) {
    $scope.person = person;
    $scope.fields = {
        password: "",
        confirmPassword: ""
    };
    if ($scope.person.user) {
        $scope.fields.username = person.user.username;
    }

    // Bootstrap alerts
    $scope.localAlerts = [];
    $scope.addAlert = function(msg, type) {
        $scope.localAlerts.push({msg: msg, type: type || 'error'});
    };
    $scope.closeAlert = function(index) {
        $scope.localAlerts.splice(index, 1);
    };

    $scope.updateUser = function() {
        if (!$scope.fields.username) {
            $scope.addAlert("חובה למלא שם משתמש");
            return;
        }

        if (!$scope.person.user) {
            if (!$scope.fields.password || $scope.fields.password.length == 0) {
                $scope.addAlert("חובה להכניס סיסמה למשתמש חדש");
                return;
            }
        }

        if ($scope.fields.password !== $scope.fields.confirmPassword) {
            $scope.addAlert("הסיסמה לא תואמת לאימות");
            return;
        }

        var data = $scope.person.user || {};
        data.username = $scope.fields.username;
        if ($scope.fields.password && $scope.fields.password.length) {
            data.password = $scope.fields.password;
        }

        $http.put('/api/people/' + $scope.person.id + '/user', data).then(function(result) {
            parentScope.addAlert("המשתמש נשמר בהצלחה", 'success');
            $scope.person.user = result.data;
            $modalInstance.close(this);
        });
    };

    $scope.removeUser = function() {
        if (!confirm("למחוק את " + $scope.person.user.username + "?")) return;

        Restangular.one('users', $scope.person.user.id).remove().then(function() {
            delete $scope.person.user;
            $scope.addAlert("המשתמש נמחק בהצלחה", 'success');
        });
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
}

function MultipleUsersEditingModalCtrl($scope, parentScope, people, $modalInstance, Restangular, $http) {
    $scope.people = people;
    $scope.fields = {
        password: "",
        confirmPassword: "",
        usernames: {}
    };
    _($scope.people).each(function (p) {
        if (p.user)
            $scope.fields.usernames[p.id] = p.user.username;
        else
            $scope.fields.usernames[p.id] = '';
    });

    // Bootstrap alerts
    $scope.localAlerts = [];
    $scope.addAlert = function(msg, type) {
        $scope.localAlerts.push({msg: msg, type: type || 'error'});
    };
    $scope.closeAlert = function(index) {
        $scope.localAlerts.splice(index, 1);
    };

    $scope.updateUsers = function() {
        if (!_($scope.fields.usernames).all(function (name) {return name})) {
            $scope.addAlert("חובה למלא שמות משתמשים");
            return;
        }

        if (_($scope.people).any(function (person) {return !person.user;})) {
            if (!$scope.fields.password || $scope.fields.password.length == 0) {
                $scope.addAlert("חובה להכניס סיסמה למשתמש חדש");
                return;
            }

            if ($scope.fields.password !== $scope.fields.confirmPassword) {
                $scope.addAlert("הסיסמה לא תואמת לאימות");
                return;
            }
        }

        var numOfUpdatedPeople = 0;
        _($scope.people).each(function (person) {
            var data = {};
            data.username = $scope.fields.usernames[person.id];
            if ($scope.fields.password && $scope.fields.password.length) {
                data.password = $scope.fields.password;
            }

            $http.put('/api/people/' + person.id + '/user', data).then(function(result) {
                person.user = result.data;
                parentScope.addAlert("המשתמש " + person.user.username + " נשמר בהצלחה", 'success');

                numOfUpdatedPeople++;
                if (numOfUpdatedPeople == $scope.people.length)
                    $modalInstance.close(this);
            });
        });
    };

    $scope.removeUsers = function() {
        var users = _.chain($scope.people)
                     .filter(function (p) {return p.user;})
                     .map(function (p) {return p.user})
                     .value();

        var usernames = _(users).map(function (u) {return u.username})
                         .join(', ');

        if (users.length == 0) {
            alert('לאנשים שנבחרו אין משתמשים');
            return;
        }

        if (!confirm('למחוק ' + users.length + ' משתמשים?\n' + usernames)) return;

        // For each user, delete it and update the view.
        var numOfDeleted = 0, failToDelete = [], totalNum = users.length;
        function handleDone() {
            // Check if done.
            if (totalNum == numOfDeleted + failToDelete.length) {
                var failToDeleteMessage = '\nנכשלו: ' + _.map(failToDelete, function(u) {return u.username}).join(', ');
                if (failToDelete.length == 0)
                    failToDeleteMessage = '';
                alert(numOfDeleted + ' נמחקו בהצלחה.' + failToDeleteMessage);
            }
        }
        _.each(users, function (u) {
            // Delete the current user.
            Restangular.one('users', u.id).remove().then(function() {
                delete _.chain($scope.people)
                        .where({user: u})
                        .first()
                        .value().user;

                numOfDeleted++;
                handleDone();
            }, function () {
                failToDelete.append(u);
                handleDone();
            });
        });
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
}


