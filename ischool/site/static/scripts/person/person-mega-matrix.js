function MegaMatrixCtrl($scope, $controller, $http) {
    function initialize() {
        $scope.selectedGroups = {};
    }

    initialize();

    // Inherit from PersonGroupsCtrl.
    $controller('PersonGroupsCtrl', {$scope: $scope, $http: $http});
}
