function PersonExerciseCtrl($scope, Restangular, $http) {
   Restangular.one('course',$scope.course).all('arrays').getList().then(function(arrays) {
        $scope.arrays = arrays;
        $scope.selectedArray = $scope.arrays[0];
        $scope.groups = [];
        $scope.instances = [];
        $scope.exercises = [];
        $scope.assignments = {};

       for (var i=0;i < arrays.length;i++) {
           var arr_id = arrays[i].id
            Restangular.one('arrays',arr_id).getList('groups').then(function(groups) {
                $scope.groups = $scope.groups.concat(groups);
            });

            Restangular.one('arrays',arr_id).getList('instances').then(function(instances) {
                $scope.instances = $scope.instances.concat(instances);
                $scope.instancesByUnits = _.groupBy($scope.instances, "unit_id");
                $scope.units = _.pluck($scope.instances, "unit");
                $scope.units = _.values(_.object(_.pluck($scope.units, "id"), $scope.units));
            });

            Restangular.one('arrays',arr_id).getList('exercises').then(function(exercises) {
                $scope.exercises = $scope.exercises.concat(exercises);
            });
            /*
            Restangular.one('matrix_array', arr_id).get().then(function(matrixArray) {
            var assignmentsByExerciseId = _.groupBy(matrixArray.assignments, "exercise_id");
            _.keys(assignmentsByExerciseId).forEach(function(key) {
                $scope.assignments[key] = _.object(_.pluck(assignmentsByExerciseId[key], "person_id"),
                    assignmentsByExerciseId[key]);
                });
            });
            */
           $http.get('/api/assignments/' + arr_id).then(function(result) {
		_.each(result.data.assignment, function(value, key) {$scope.assignments[key] = value;});
           });

        }
   });

    $scope.instanceHasExercises = function(instance) {
        return instance.instance_exercises.length > 0;
    };

    $scope.getExercisesOfInstance = function(instance) {
        // Notice that we're getting the relevant exercises according to the many2many object - instance_exercise
        // We're also sorting the exercises according to the order in the instance_exercise
        if ($scope.exercises) {
            var ids = _.pluck(instance.instance_exercises, 'exercise_id');
            var exercises = _.filter($scope.exercises, function(exercise) {
                return _.contains(ids, exercise.id);
            });
            return _.sortBy(exercises, function(exercise) {
                return _.find(exercise.instance_exercise, function(ie) {
                    return ie.instance_id == instance.id;
                }).order;
            });
        }
        return [];
    };

    $scope.linkForExercise = function(exercise) {
        return "arrays/" + $scope.selectedArray.id + "/exercises/" + exercise.id;
    };

     $scope.unitInArray = function(unit) {
        return unit.array_id == $scope.selectedArray.id;
    };
    $scope.GroupIncludesUser = function(group) {
        var detailed_group = _.filter($scope.groups,function(groupIter) {return groupIter.id == group.id})[0];
        var members = detailed_group.members;
        return _.filter(members,function(member) {return member.id == $scope.person.id}).length > 0;
    };

    $scope.instanceIsForUser = function(instance) {
        return $scope.GroupIncludesUser(instance.group);
    };

    $scope.unitHasRelevantInstances = function(unit) {
        var instances = $scope.instancesByUnits[unit.id];
        return _.filter(_.filter(instances,$scope.instanceIsForUser), $scope.instanceHasExercises).length > 0;
    };
}

function PersonScoresCtrl($scope, $modal, Restangular, $http) {


    $scope.initializeArrays = function(personId) {
        Restangular.one('arrays',$scope.course).all('arrays').getList().then(function(arrays) {
            $scope.arrays = arrays;
        })
    };

    $scope.initialize = function(personId) {
        $scope.personId = personId;
        $scope.initializeArrays(personId);
    };

    $scope.updateSelectedArrayUrl = function() {
        $scope.selectedArrayUrl = "/people/" + $scope.personId + "/scores/array/" + $scope.selectedArray;
    };

    $scope.openExerciseScoresModal = function (submissionId, submissionNumber) {
        var modalInstance = $modal.open({
            templateUrl: 'exerciseScoresModal.html',
            controller: ExerciseScoresModalCtrl,
            resolve: {
                parentScope: function () {
                    return $scope;
                },
                submissionId: function () {
                    return submissionId;
                },
                submissionNumber: function () {
                    return submissionNumber;
                }
            }
        });

        modalInstance.result.then(function (form) {
            // onConfirm
        }, function () {
            // onDismiss
        });
    };
}



function ExerciseScoresModalCtrl($scope, parentScope, $modalInstance, Restangular, $http, submissionId, submissionNumber) {

    $scope.submissionNumber = submissionNumber;
    $http.get("/api/scores/submission/" + submissionId).then(function(result) {
        $scope.scores = result.data.exercise_scores;
        $scope.exerciseName = result.data.exercise_name;
    })

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}
