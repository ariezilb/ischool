mod.filter('translateRole', function() {
    return function(role) {
        switch (role) {
            case 'student': return "חניך/כה";
            case 'staff': return "סגל";
            case 'track_leader': return "מפקד/ת מגמה";
            case 'course_chief': return "מפקד/ת קורס";
            case 'teacher': return "מורה";
            case 'checker': return "בודק/ת תרגילים";
            default: return role;
        }
    }
});
