function PersonGroupsCtrl($scope, $http) {
    $http.get("/api/people/" + $scope.personId + "/groups").then(function(result) {
        var groups = result.data;
        $scope.groups = _(groups).groupBy('array_id');
        $scope.arrays = [];
        var arraysIds = [];
        _(_(groups).pluck('array')).each(function(item) {
            if (!_(arraysIds).contains(item.id)) {
                $scope.arrays.push(item);
                arraysIds.push(item.id);
            }
        });
    });

    $http.get("/api/people/" + $scope.personId + "/assignments").then(function(result) {
        var assignments = result.data;
        $scope.assignments = _(assignments).groupBy(function(g) { return g.exercise.array_id; });
    });

    $scope.exercisesToShow = {
        done: true,
        resend: true,
        in_progress: true,
        submitted: true
    };
    $scope.showByState = function(elm) {
        return $scope.exercisesToShow[elm.state];
     };
}
