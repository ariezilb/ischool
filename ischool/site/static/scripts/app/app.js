/**
 * User: user1
 * Date: 08/09/13
 * Time: 17:19
 */


// Initalize app
var app = angular.module('iSchool', ['appFilters', 'appServices',
    'inlineEditable', 'powerTip', 'fileUpload',
    'ui.bootstrap', 'restangular', 'ui.select2', 'ui.tinymce'
]);

app.run(function() {
   moment.lang('he');
});

app.run(['$rootScope', function($rootScope) {
    $rootScope.isHebrew = function(text) {
        if (!text || text.length == 0) return true;

        var hebrewLetters = _.filter(text.split(""), function(letter) {
            return letter >= 'א' && letter <= 'ת';
        }).length;

        return hebrewLetters >= text.length / 2.0;
    };
}]);

app.config(['$sceProvider', function($sceProvider) {
    // Turn off SCE
    $sceProvider.enabled(false);
}]);

app.config(['$interpolateProvider', function($interpolateProvider) {
        // Change interpolation from {{ binding }} to {@ binding @} (to support jinja)
        $interpolateProvider.startSymbol('{@');
        $interpolateProvider.endSymbol('@}');
    }]).
    config(['RestangularProvider', function(RestangularProvider) {
        RestangularProvider.setBaseUrl('/api');

        // Add request headers - X-Requested-With, to identify XHR requests
        RestangularProvider.setDefaultHeaders({'X-Requested-With': "XMLHttpRequest"});

        // Add single=true to .one() requests
        RestangularProvider.setFullRequestInterceptor(function(element, operation, route, url, headers, params) {
            if (operation === "get") {
                // Request only single object
                if (params == undefined) {
                    params = {};
                }
                if (params.q == undefined) {
                    params.q = {};
                }
                params.q.single = true;
            }
            return {
                element: element,
                params: params,
                headers: headers
            };
        });

        // Extract response (get only the .objects information for lists)
        RestangularProvider.setResponseExtractor(function(response, operation, what, url) {
            var newResponse;
            if (operation === "getList") {
                newResponse = response.objects;
                delete response.objects;
                newResponse.metadata = response;
            } else if (operation === "get") {
                // TODO: save this as metadata
                delete response.headers;
                newResponse = response;
            } else {
                newResponse = response;
            }

            return newResponse;
        });

        // Configure parentless requests
        // FIXME: This list of objects won't ever have parent, THIS IS BAD. fix it ASAP.
        RestangularProvider.setParentless(['groups', 'units', 'instances', 'arrays']);
    }]);



// Some convenience functions

/**
 * Returns select course's id if any, otherwise returns default course id
 * @param scope
 * @returns {*}
 */
function getCourseId(scope) {
    return scope.selectedCourse ? scope.selectedCourse.id : scope.course.id;
}

/**
 * Creates Restangularized object from regular object
 * @param Restangular - The Restangular object as received from Dependency Injector
 * @param obj - Raw object to extend
 * @param apiPath - /api/api_path, e.g. "people" or "arrays"
 * @returns {*}
 */
function createRestangularObject(Restangular, obj, apiPath) {
    if (obj == null) {
        return null;
    }
    return _.extend(Restangular.one(apiPath, obj.id), obj);
}

/**
 * Create string for localStorage/sessionStorage
 * @param name The thing that we want to store
 * @param props Extra details about that thing
 * @returns {string}
 */
function createStorageString(name, props) {
    var storageStr = name + "@";
    _(props).each(function(prop, key) {
        storageStr += key + ":" + prop + ";";
    });
    return storageStr;
}

app.directive('ngTimeago', function($rootScope) {
    $rootScope.current_mode = 0;
    var modes = [
        function (time) {
            return moment(time).fromNow();
        },
        function (time) {
            return moment(time).format("[ב] H:mm:ss");
        },
        function (time) {
            return moment(time).format("[ב] DD/MM/YY, H:mm");
        },
    ]
    return {
        restrict: 'E',
        scope: {
            time: '=time',
        },
        template: '<small><abbr title="{@ title @}" ng-click="change_mode()">{@ text @}</abbr></small>',
        link: function (scope, elem, attrs) {
            function update() {
                scope.title = moment(scope.time).format("[יום] dddd, Do [ב]MMMM YYYY, H:mm:ss");
                if (scope.time)
                    scope.text = modes[$rootScope.current_mode](scope.time);
                else
                    scope.text = 'לפני \u221e זמן'; // Infinity.
            }
            scope.change_mode = function () {
                $rootScope.current_mode = ($rootScope.current_mode + 1) % modes.length;
            }

            $rootScope.$watch('current_mode', update);
            scope.$watch('time', update);
        }
    };
});
