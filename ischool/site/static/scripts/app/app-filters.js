/**
 * User: user1
 * Date: 08/09/13
 * Time: 17:19
 */

var mod = angular.module('appFilters', []);

mod.filter('safe', ['$sce', function($sce){
    return function(text) {
        if (!$sce.isEnabled()) {
            return text;
        }
        return $sce.trustAsHtml(text);
    };
}]);

mod.filter('groupBy', function() {
	return function(obj, value) {
	    return _.groupBy(obj, value);
	};
});

mod.filter('personalize_message', function() {
	return function(person, maleMessage, femaleMessage) {
        if (!person) {
            return maleMessage;
        }

        if (person.person) {
            // Catchy!
            person = person.person;
        }

         if (person.gender == 'male') {
            return maleMessage;
        }
        else {
            return femaleMessage;
        }
	};
});

mod.filter('person_short_name', function() {
	return function(person, uniqueInGroup) {
        if (uniqueInGroup == null) {
            return person.first_name;
        }

        var sameFirstName = _.filter(uniqueInGroup, function(p) {
            return p != person && p.first_name == person.first_name;
        });

        if (sameFirstName.length == 0) {
            return person.first_name;
        }

        var lastNameLetter = person.last_name.slice(0);
        var sameLastLetter = _.filter(sameFirstName, function(p) {
            return p.last_name.slice(0) == lastNameLetter
        });

        if (sameLastLetter.length == 0) {
            return person.first_name + " " + lastNameLetter;
        }

        return person.name;
	};
});

mod.filter('course_link', function() {
	return function(course, showIcon) {
        if (!course) return;
        return "<a href='/courses/" + course.eng_name.toLowerCase() + "' class='course-link "
            + course.eng_name.toLowerCase() + " " + (!!showIcon ? "icon" : "")
            + "'>" + course.name + "</a>";
	};
});

mod.filter('person', function() {
	return function(person, name) {
        if (!person) return;
        name = name || person.name;
        return name;
	};
});

mod.filter('person_link', function() {
	return function(person, name) {
        if (!person) return;
        name = name || person.name;
        return "<a href='/people/" + person.id + "'>" + name + "</a>";
	};
});

mod.filter('person_link_icon', function() {
	return function(person_id) {
        return "<a class='icon-info-sign' href='/people/" + person_id + "'></a>";
	};
});

mod.filter('array_link', function() {
	return function(array, name) {
        if (!array) return;
        name = name || array.name;
        return "<a href='/arrays/" + array.id + "'>" + name + "</a>";
	};
});

mod.filter('class_name', function() {
	return function(cls) {
        if (cls == null) {
            return "";
        }
        return cls.replace(/[^a-zA-Z\-]+/, "-");
	};
});

mod.filter('short_float', function() {
	return function(num) {
        var newNum = new String(num).replace(/0+$/, '').replace(/\.$/, '');
        if (newNum == '') {
            return '0';
        }
        return newNum;
	};
});

mod.filter('truncate', function() {
	return function(text, maxChars) {
        if (text === undefined) {
            return "(ריק)";
        }
        if (!maxChars || text.length <= maxChars) {
            return text;
        }
        return text.substr(0, maxChars) + '…';
	};
});


mod.filter('smartFilter', function() {
	return function(array, predicate) {
        if (!array) return;
        var retArr = _.clone(array);
        for (var i = 0; i < array.length; ++i) {
            var obj = array[i];
            _.each(predicate, function(val, key) {
                if (obj[key] != val) {
                    retArr = _.without(retArr, obj);
                }
            });
        }
        return retArr;
	};
});
