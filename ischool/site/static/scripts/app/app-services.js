/**
 * User: user1
 * Date: 08/09/13
 * Time: 17:19
 */

var appServices = angular.module('appServices', ['ngSanitize']);

/**
 * The `AutoSyncr` service allows for automaticly sync data from the server, by fetching only the new data from it.
 *
 * @param {*} message Message to be logged.
 */
appServices.service('AutoSyncr', ['$timeout', '$http', '$q', '$document', function($timeout, $http, $q, $document) {
	this.new = function(interval, url) {
    	var degerred = $q.defer(),
    		last_fetced_ids,
    		timeout_promise,
    		callbacks = {'success': $.Callbacks(), 'new_data': $.Callbacks(), 'fail': $.Callbacks()},
    		un_idle_events = 'mousemove keydown DOMMouseScroll mousewheel mousedown touchstart',
    		is_idle = false,
    		idle_timeout_promise,
    		IDLE_TIMEOUT = 42 * 60 * 1000; // 42 minutes.

    	if (interval && !$.isNumeric(interval)) {
    		url = interval;
    		interval = undefined;
    	}

    	$document.find('body').on(un_idle_events, function () {
    		is_idle = false;
    	});

    	function check_for_idle() {
    		if (!is_idle) {
    			is_idle = true;
	    		if (idle_timeout_promise)
	    			$timeout.cancel(idle_timeout_promise);

				idle_timeout_promise = $timeout(function () {
					if (timeout_promise) {
						alert('This page is idle for more then ' + (IDLE_TIMEOUT / 60 / 1000) + ' minutes. \nAutomatic syncronization is paused. \nPress OK to unpause. \n\nPlease close this tab if it has no use.');
					}
				}, IDLE_TIMEOUT);
			}
    	}

		function fetch() {
			check_for_idle();

			var tmp_url = url;
			if (last_fetced_ids)
				tmp_url += "?since_ids=" + JSON.stringify(last_fetced_ids);

			$http.get(tmp_url).success(function (data) {
				callbacks.success.fire();
				if (data) {
					last_fetced_ids = data.newest_ids;
					callbacks.new_data.fire(data.new_data);
				}
		    	timeout_promise = $timeout(fetch, interval);
		    }).error(function () {
		    	callbacks.fail.fire();
		    	timeout_promise = $timeout(fetch, interval);
		    });
		}

		function clear_timeout() {
			if (timeout_promise) {
				$timeout.cancel(timeout_promise);
				timeout_promise = undefined;
			}
		}

		var returned_promise = {
			start: function(_interval, _url) {
				if (_interval && !$.isNumeric(_interval)) {
		    		_url = _interval;
		    		_interval = undefined;
		    	}
		    	if (_interval) interval = _interval;
		    	if (_url) url = _url;

				clear_timeout();
				last_fetced_ids = undefined;
				fetch();

				return returned_promise;
			},
			stop: function() {
				clear_timeout();

				return returned_promise;
			},
		};

		_(callbacks).each(function (callback_list, fn_name) {
			returned_promise[fn_name] = function(fn) {
				callback_list.add(fn);
				return returned_promise;
			};
		});

		return returned_promise;
	};
}]);
