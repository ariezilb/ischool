/**
 * Created with PyCharm.
 * User: user1
 * Date: 12/09/13
 * Time: 15:34
 *
 * Enables in-place changes to models, that sends an update request for each element change.
 * Use ngEditableExpression for different evaluation for get & set
 * <doc:example>
 *     <section ng-editable-model="product">
 *         <h1 ng-editable="product.title"></h1>
 *         <h2 ng-editable="product.price" ng-editable-expression="product.price | currency"></h2>
 *     </section>
 * </doc:example>
 *
 */

angular.module('inlineEditable', ['restangular']).
    directive('ngEditable', function($parse) {
        return {
            restrict: 'A', // only activate on element attribute

            link: function(scope, element, attrs) {
                var ngEditableGet = $parse(attrs.ngEditable),
                    ngEditableSet = ngEditableGet.assign;

                if (attrs.ngEditableExpression) {
                    ngEditableGet = $parse(attrs.ngEditableExpression);
                }

                var getObj, setObj;
                if (attrs.ngEditableObject) {
                    // Editable Object is specifically defined
                    getObj = $parse(attrs.ngEditableObject);
                    setObj = getObj.assign;
                } else {
                    getObj = function() {
                        if (scope.$ngEditableModel) {
                            // There's a parent Editable Model
                            return scope.$ngEditableModel(scope);
                        }

                        // None of the above, use ngEditable as ngModel
                        return ngEditableGet(scope);
                    };

                    setObj = function(_, val) {
                        if (scope.$ngEditableModel) {
                            // There's a parent Editable Model
                            return scope.$ngEditableModel.assign(scope, val);
                        }

                        // None of the above, use ngEditable as ngModel
                        return ngEditableGet.assign(scope, val);
                    };
                }

                // Create Editable instance, depends on editable type
                var editableElement;
                if (attrs.numberType) {
                    editableElement = new ischool.editing.EditableNumeric(element);
                } else if (attrs.selectType) {
                    editableElement = new ischool.editing.EditableChoice(element,
                        function() {
                            return scope.$eval(attrs.selectType);
                        });
                } else if (attrs.checkType) {
                    console.error('check type is not yet supported');
                    return;
                } else { // defaults to textType
                    editableElement = new ischool.editing.EditableString(element);
                }

                // Each time the scope refreshes, refresh current value
                scope.$watch(function() {
                    element.html(editableElement.getValue(ngEditableGet(scope)));
                });

                // Listen for change events to enable binding
                element.html(ngEditableGet(scope) || '').addClass('editable');
                element.bind('editable.end', function(e, data) {
                    scope.$apply(function() { read(data.val, data.callback) });
                });

                // Start editing on click
                element.bind('click', function(e) {
                    editableElement.edit(e);
                });

                // Write data to the model & save the resource
                function read(newValue, callback) {
                    var oldValue = ngEditableGet(scope);
                    // The editabletext element is the one created by auto-grow-input.js
                    //newValue = newValue || element.find('editabletext').html();

                    if (oldValue == newValue) {
                        if (callback)
                            callback();
                        return;
                    }

                    ngEditableSet(scope, newValue);
                    var obj = getObj(scope);
                    if (obj != null) {
                        console.log("Saving", newValue, "(using .put() method)");
                        obj.put().then(function(newObj) {
                            setObj(scope, newObj);
                            if (callback) // Call the callback async, because we are inside the $apply function.
                                setTimeout(callback, 0);
                        });
                    }
                }
            }
        };
    }).
    directive('ngEditableModel', function($parse) {
        return {
            restrict: 'A', // only activate on element attribute

            link: function(scope, element, attrs) {
                scope.$ngEditableModel = $parse(attrs.ngEditableModel);
            }
        };
    });




if (!window['ischool']) window.ischool = {};
if (!ischool['editing']) ischool.editing = {};

ischool.editing.all_editables = [];

$(document).on('click', function() {
    $.each(ischool.editing.all_editables, function() { if (this) { this.blur(); } });
});

/**
 * An abstract class for state-having editable elements.
 * These elements have an editing states, and a normal state (i.e. by entering
 * the 'edit' state, normal text can turn into an input text box, and then go
 * back to normal)
 * @type {ischool.editing.Editable}
 */
ischool.editing.Editable = Base.extend({

    // constant for the editing state.
    EDITING_STATE: 'editing',

    // constant for state value when not editing.
    NORMAL_STATE: '',

    // The current component's state
    state: '',

    /**
     * @param el The editable element (clicking on it will start the editing)
     * @param {Function} onclick The function to call when the element was clicked. Doesn't receive any arguments.
     * @param {Function} onblur The function to call when stopping editing. Receives an argument saying whether the
     * editing was stopped for rolling back (i.e. if the user pressed escape)
     */
    constructor: function(el, onclick, onblur) {
        this.el = $(el);
        el.addClass('editable'); // so we'll have some hover css for it
        this._onclick = onclick;
        this._onblur = onblur;

        ischool.editing.all_editables.push(this);
    },

    /** Enters editing state */
    edit: function(e) {

        if (this.state == this.EDITING_STATE) {
            e.stopPropagation();
            return;
        }

        //Close all other editable.
        ischool.editing.Editable.closeOthers(this, function () {
            this.state = this.EDITING_STATE;
            this._onclick();
        });

        e.stopPropagation();
    },

    /**
     * Leaves editing state
     * @param isRollback Whether the changes made should not be saved
     * @param {function} callback The callback is called when blur is done.
     */
    blur: function(isRollback, callback) {
        if (this.state == this.NORMAL_STATE) {
            if (callback)
                callback()
            return;
        }
        this._onblur(isRollback, callback);
        this.state = this.NORMAL_STATE;
    }

}, { // STATIC MEMBERS

    /**
     * Closes all editable components except for the passed object.
     * Used so (hopefully) only one element will be editable simultaneously.
     * @param {ischool.editing.Editable} context The element that close all the others.
     * @param {function} callback The callback is called when closeing is done.
     */
    closeOthers: function(context, callback) {
        var num_of_waiting_callbacks = _(ischool.editing.all_editables).without(context).length;
        function check_callbacks_num() {
            if (num_of_waiting_callbacks == 0) {
                callback.apply(context);
            }
        }
        check_callbacks_num();
        // Close all but the context.
        $(_(ischool.editing.all_editables).without(context)).each(function() {
            this.blur(false, function () {
                num_of_waiting_callbacks--;
                check_callbacks_num();
            });
        });
    },

    /**
     * Returns an Editable-element-compatible callback (that gets a new value, success and fail callbacks)
     * that calls API.update.singleField.
     * @returns {Function} A callback that receives (newValue, fn) and calls fn(newValue)
     */
    updateFieldCallback: function() {
        return function(newValue, fn) {
            fn(newValue);
        }
    }
});

/**
 * An editable subclass that handles editing simple string values.
 * @type {Function}
 */
ischool.editing.EditableString = ischool.editing.Editable.extend({

    /**
     * The value in the element before the editing, to be rolled back into.
     */
    originalValue: '',

    /** The input element */
    txt: null,

    /** The comfort zone to pass to the autoGrowInput */
    txtComfortZone: 9,

    /**
     * @param {jQuery} el The element to edit
     * @param {Function} onchange A function to call to commit a change.
     */
    constructor: function(el, onchange) {
        this.base(el, this.startEdit, this.endEdit);

        this._onchange = onchange || function(newValue, fn) { fn(newValue) };
    },

    /**
     * Returns the transformed value of origVal, should be overridden (default implementation returns origVal)
     * @param {string} origVal The original value
     * @returns {*} Exact same value
     */
    getValue: function(origVal) {
        return origVal;
    },

    /**
     * Returns the initial value from the element.
     * @returns {string}
     */
    extractOriginalValue: function() {
        return this.el.html();
    },

    /**
     * Returns a value from the textbox's text
     * @param text
     * @returns {string}
     */
    extractValueFromText: function(text) {
        return text;
    },

    /**
     * Serializes a value to the text box
     * @param val The value to serialize
     * @returns {string}
     */
    valueToText: function(val) {
        return String(val);
    },

    /**
     * Renders a new value into the editable element
     * @param {jQuery} container The containing element (usually this.el)
     * @param val The value to render
     */
    renderValue: function(container, val) {
        container.html(val);
    },

    /**
     * Extra customization for the text box
     */
    setupInput: $.noop,

    /**
     * Moves the element into the 'editing' state
     */
    startEdit: function() {
        var val = this.originalValue = this.extractOriginalValue();
        var me = this;

        this.el.empty();
        var textValue = this.valueToText(val);

        var newInput = '<input type="text"/>';
        if (this.el.data('input-type') == "wysiwyg") {
            // TODO: add wysiwyg component
            newInput = '<textarea></textarea>';
        }

        var el = this.el;

        this.txt = $(newInput).val(textValue)
            .appendTo(this.el)
            .keydown(function(e) {
                if (e.keyCode == 27) {
                    // esc - rollback
                    me.blur(true);
                }
                if (el.data('input-type') != "wysiwyg") {
                    if (e.keyCode == 13) {
                        // enter - commit
                        me.blur(false);
                    }
                }
            }).addClass('editable-input').css({
                'font-family' : this.el.css('font-family'),
                'font-size' : this.el.css('font-size'),
                'font-weight' : this.el.css('font-weight'),
                'color' : this.el.css('color')
            });
        if (this.el.data('input-type') != "wysiwyg") {
            this.txt.autoGrowInput({ comfortZone: this.txtComfortZone, minWidth: 5 });
        }
        this.setupInput(this.txt);
        this.txt.keyup();
        this.txt.focus();
    },

    /**
     * Ends the editing.
     * @param isRollback Whether to rollback
     * @param {function} callback The callback is called when endEdit is done.
     */
    endEdit: function(isRollback, callback) {
        if (isRollback) {
            this.rollback();
            if (callback)
                callback();
        } else {
            this.performUpdate(callback);
        }
    },

    /**
     * Performs the update (calls the onChange function)
     * @param {function} callback The callback is called when performUpdate is done.
     */
    performUpdate: function(callback) {
        var newVal = this.extractValueFromText(this.txt.val());
        this.el.trigger('editable.end', {val: newVal, callback: callback});
    },

    /**
     * destroys the text box (for after an update was made or cancelled)
     */
    removeInput: function() {
        if (this.txt) {
            this.txt.remove();
        }
        this.txt = null;
    },

    /**
     * Called after the update successfully completed
     * @param newVal
     */
    complete: function(newVal) {
        try {
            this.removeInput();
            this.renderValue(this.el, newVal);
        } catch (e) {
            this.rollback();
        }
    },

    /**
     * Called when an update was aborted. Return the element to the way it was before.
     */
    rollback: function() {
        this.removeInput();
        this.renderValue(this.el, this.originalValue)
    }
});

/**
 * A type of an EditableString, that only accepts numbers.
 * @type {Function}
 */
ischool.editing.EditableNumeric = ischool.editing.EditableString.extend({

    /**
     * Whether the browser supports <input type=number>
     */
    supportsNumberInput: function() {
        return $('<input type="number"/>')[0].type == 'number';
    }(),

    constructor: function() {
        this.base.apply(this, arguments);

        // input[type=number] needs more width to fit the up/down arrows
        if (this.supportsNumberInput) {
            this.txtComfortZone = 25;
        }
    },

    /**
     * Returns the initial value from the element.
     * @returns {number}
     */
    extractOriginalValue: function() {
        return Number(this.el.html());
    },

    /**
     * Returns a value from the textbox's text
     * @param text
     * @returns {number}
     */
    extractValueFromText: function(text) {
        return Number(text);
    },

    /**
     * Sets the input with type=number (if supported) and binds the key input otherwise
     * @param {jQuery} input
     */
    setupInput: function(input) {
        if (this.supportsNumberInput) {
            input[0].setAttribute('type', 'number');
            input[0].setAttribute('min', 0);
            input[0].setAttribute('step', 0.5);
        } else {
            //var re = /^\d+(\.\d+)?$/;
            input.keypress(function(e) {
                if (e.which != /* dot: */ 46 && e.which != /* misc keys: */ 0
                    && e.which != /* backspace: */ 8
                    && (e.which < 46 || e.which > 57)) {
                    return false;
                }
            });
        }
    }
});

/**
 * An editable that offers a choice between several options
 * @type {Function}
 */
ischool.editing.EditableChoice = ischool.editing.Editable.extend({

    /**
     * The dictionary of {value: display_name} containing the choices for the editor.
     * @type {Object}
     */
    opts: null,

    /**
     * The select element
     * @type {jQuery}
     */
    input: null,

    /**
     * @param {jQuery} el The editable element
     * @param {Object} opts The options to show in the select box
     * @param {Function} onChange A function to call to update the values
     */
    constructor: function(el, opts, onChange) {
        this.base(el, this.startEdit, this.endEdit);

        this.opts = opts;
        this.onchange = onChange || function(newValue, fn) { fn(newValue) };
    },

    /**
     * Gets options list
     * @returns {Object} Options
     */
    getOptions: function() {
        if (_.isFunction(this.opts)) {
            return this.opts();
        }
        return this.opts;
    },


    /**
     * Returns the transformed value of origVal (from received opts)
     * @param {string} origVal The original value
     * @returns {*} opts[origVal]
     */
    getValue: function(origVal) {
        if (origVal) {
            return this.getOptions()[origVal];
        }
        return null;
    },

    /**
     * Called when entering editing mode.
     */
    startEdit: function() {
        this.originalValue = this.el.html();
        this.el.empty();
        this.input = $('<select/>')
            .appendTo(this.el)
            .keydown(function(e) {
                if (e.keyCode == 27) {
                    this.blur(true); // esc - rollback
                }
                if (e.keyCode == 13) {
                    this.blur(false); // enter - apply
                }
            }.bind(this)).addClass("ischool-dropdown");

        $.each(this.getOptions(), function(value, name) {
            var opt = $('<option/>').val(value).html(name).appendTo(this.input);
            if (name == this.originalValue) {
                opt.attr('selected', true);
            }
        }.bind(this));
    },

    /**
     * Ends the editing.
     * @param isRollback Whether to rollback
     * @param {function} callback The callback is called when endEdit is done.
     */
    endEdit: function(isRollback, callback) {
        if (isRollback) {
            this.rollback();
            if (callback)
                callback();
        } else {
            this.performUpdate(callback);
        }
    },

    /**
     * destroys the text box (for after an update was made or cancelled)
     */
    removeInput: function() {
        if (this.input) {
            this.input.remove();
        }
        this.input = null;
    },

    /**
     * Performs the update (calls the onChange function)
     * @param {function} callback The callback is called when performUpdate is done.
     */
    performUpdate: function(callback) {
        var newValue = this.input.val();
        var newDisplay = this.input.find('option:selected').get(0).text;

        if (newDisplay == this.originalValue) {
            this.blur(true);
            if (callback)
                callback();
            return null;
        }

        this.el.trigger('editable.end', {val: newValue, callback: callback});
    },

    /**
     * Called after the update successfully completed
     */
    complete: function() {
        this.el.html(this.getOptions()[this.input.val()]);
        this.removeInput();
    },

    /**
     * Called when an update was aborted. Return the element to the way it was before.
     */
    rollback: function() {
        this.removeInput();
        this.el.html(this.originalValue);
    }
});
