/**
 * Created with PyCharm.
 * User: user2
 * Date: 15/10/13
 * Time: 19:15
 *
 * Enables automatic file uploading for elements
 * with the ng-file-upload tag.
 *
 * Usage:
 *  <button class="auto-file-upload [with-filename?] file-type template docx" ng-file-upload="http://upload-path/">
 *      Add New...
 *  </button>
 *
 * WHEN ADDING NEW MODULES BE SURE TO INCLUDE'EM
 * IN THE MAIN APP SCRIPT FILE (app.js).
 *
 */

angular.module(/* the module's name, for reference in the app.js file */ 'fileUpload', []).
    directive(/* camel-case version of the html tag attribute */ 'ngFileUpload', function($parse, $log) {
        return {
            restrict: 'A', // A is for Attributes (html elements attributes, that is)

            link: function(scope, element, attrs) {
                element.addClass('file-type').wrap('<div class="file-upload-button-container"/>');
                var input = $('<input name="file" type="file">').insertBefore(element);

                var withFilename = (attrs.withFilename !== undefined);

                input.change(function() {
                    var file = input[0].files[0];
                    var name = file.name;
                    var size = file.size;
                    var type = file.type;

                    var formData = new FormData();
                    formData.append('file', file);

                    $.ajax({
                        url: attrs['ngFileUpload'] + (withFilename ? file.name : ''),
                        type: 'PUT',
                        // Escaping and unescaping in order to support all languages
                        // Note: "unescape" is considered deprecated, but currently have no real replacement for
                        // our intentions
                        headers: { 'X_FILENAME': unescape(encodeURIComponent(file.name)) },
                        // Form data
                        data: formData,
                        // Options to tell JQuery not to process data or worry about content-type
                        cache: false,
                        contentType: false,
                        processData: false,
                        error: function(data, subject, message) { alert(message); },
                        success: function(data) {
                            // Do everything with $apply so the angular digest loop would occur
                            scope.$apply(function () {
                                // Add file to files list if any
                                if (attrs.ngFilesList) {
                                    $parse(attrs.ngFilesList)(scope).push(data);
                                }
                            });
                        }
                    }, 'json');
                });
            }
        };
    });
