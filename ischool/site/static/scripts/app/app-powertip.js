/**
 * Created with PyCharm.
 * User: user1
 * Date: 12/09/13
 * Time: 15:34
 *
 * Enables jquery-powerTip tags
 * Usage:
 * <section ng-powertip>
        Some text...
        <ng-actions>
            <ul class="ltr">
                <li>
                    Item1
                </li>
                <li>
                    Item2
                </li>
                <li>
                    Item3
                </li>
            </ul>
        </ng-actions>
    </section>
 */

angular.module('powerTip', []).
    directive('ngPowertip', function($parse, $log) {
        return {
            restrict: 'A', // only activate on element attribute

            link: function(scope, element, attrs) {
                var actions = element.find('ng-actions,[ng-actions]');
                if (actions.size() == 0) {
                    $log.error("Could not find ngActions for powerTip");
                    return;
                }

                var config = (attrs['ngPowertip'] ? $parse(attrs['ngPowertip'])() :
                {
                    placement: 'e',
                    mouseOnToPopup: true,
                    smartPlacement: true,
                    fadeInTime: 100
                });

                element.data('powertipjq', actions.detach());
                element.powerTip(config);
            }
        };
    });
