/**
 * User: user1
 * Date: 08/09/13
 * Time: 17:19
 */

function AppCtrl($scope, Restangular) {
    $scope.course = createRestangularObject(Restangular, window.env.course, 'courses');

    $scope.alerts = [];
    $scope.addAlert = function(msg, type) {
        $scope.alerts.push({msg: msg, type: type || 'error'});
    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };
}

function DefaultPageCtrl($scope) {
    // Default page control, if no other one uses it
}

function PersonHeaderCtrl($scope, Restangular) {
    $scope.person = createRestangularObject(Restangular, window.env.person, 'people');
}

function OmniSearchCtrl($scope, $http) {
    $scope.onSelect = function ($item, $model, $label) {
        window.location.href = $model.__url__;
    };

    $scope.performSearch = function(query) {
        if (query.length < 2) {
            return [];
        }
        return $http.get("/api/search/" + encodeURIComponent(query), {
            params: {people: true, arrays: true, exercises: true}
        }).then(function (result) {
          var results = [];
           _.forEach(result.data.people, function(item){
               item.__type__ = 'person';
               item.__url__ = '/people/' + item.id;
               results.push(item);
          });
          _.forEach(result.data.arrays, function(item){
              item.__type__ = 'array';
              item.__url__ = '/arrays/' + item.id;
              results.push(item);
          });
          _.forEach(result.data.exercises, function(item){
              item.__type__ = 'exercise';
              item.__url__ = '/arrays/' + item.array_id + '/exercise/' + item.id;
              results.push(item);
          });
          return results;
        });
    }
}

function ErrorBoxModalCtrl($scope, $modalInstance, response) {
    $scope.response = response;

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

/**
 * Set error handler
 */
app.run(function(Restangular, $modal) {
    // App should be initialized, app = angular.module('iSchool')
    Restangular.setErrorInterceptor(function(response) {
            if (response.status == 400 && response.data.custom) {
                return true;
            }

            console.log("Error:", response);
            if (response.status != 0) {
                var modalInstance = $modal.open({
                    templateUrl: '/static/htmls/partials/error-box-modal.html',
                    controller: ErrorBoxModalCtrl,
                    resolve: {
                        response: function() {
                            return response;
                        }
                    }
                });

                modalInstance.result.then(function(form) {
                    // onConfirm
                }, function () {
                    // onDismiss
                });
            }

        return false;
        });
});
