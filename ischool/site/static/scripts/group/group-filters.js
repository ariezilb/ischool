/**
 * User: user1
 * Date: 08/09/13
 * Time: 17:19
 */

mod.filter('has_member', function() {
	return function(members, student) {
	    return _.any(_.map(members, function(member) {
            return student.id == member.id;
        }));
	};
});
