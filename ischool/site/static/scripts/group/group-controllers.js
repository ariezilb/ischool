/**
 * User: user1
 * Date: 08/09/13
 * Time: 17:19
 */

function GroupPageCtrl($scope, Restangular, $http) {
    $http.get("/api/people/all_staff").then(function (result) {
      $scope.all_staff = result.data;
    });

    function setGroup(group) {
        $scope.group = group;
        $scope.members = {};
        $scope.managerId = group.manager_id;
        _.each(group.members, function(member) {
            $scope.members[member.id] = member;
        });
        // The typeahead of the student to add to the group.
        $scope.addedStudent = undefined;
    }

    $scope.group = Restangular.one('groups', $scope.groupId).get();

    $scope.group.then(function(group) {
        // Data binding (e.g. ngEditable) works in oddly when using one() method, this piece of code solves it
        setGroup(group);
        // TODO: replace with /api/search for better performance (done in points/manage)
        $http.get("/api/arrays/all-students/" + $scope.group.array.id).then(function(result) {
            $scope.allStudents = result.data;
        });

        $scope.setManager = function(managerId) {
            $scope.group.manager_id = managerId;
            $scope.group.put();
        };

        $scope.changeStudentMembership = function(student) {
            if ($scope.members[student.id]) {
                // Remove from group members
                group.members = _.filter(group.members, function(member) { return member.id != student.id });
                // Delete the group from the student's groups in allStudents.
                _.each($scope.allStudents, function(currentStudent) {
                    if (currentStudent.id == student.id) {
                        currentStudent.groups = _.filter(currentStudent.groups, function(group) {
                            return group.id != $scope.group.id;
                        })
                    }
                });
            } else {
                // Add to group members
                group.members.push({id: student.id});
                // Add the group to student's group in allStudents.
                _.each($scope.allStudents, function(currentStudent) {
                    if (currentStudent.id == student.id) {
                        // TODO: find why we send the group's name. remove if possible.
                        currentStudent.groups.push({'id': $scope.group.id, 'name': $scope.group.name})
                    }
                });
            }
            group.put().then(setGroup);
        };

        $scope.goBack = function(group) {
            window.history.back();
        }

        $scope.removeGroup = function(group) {
            if (!confirm("פעולה זו תמחק את הקבוצה ואת כל המופעים שלה, האם אתה בטוח?")) return;
            group.remove().then(function() {
                window.location = "/arrays/" + group.array.id;
            });
        };

        $scope.notMember = function(student) {
            console.log(student);
            // Checks if the student is in the group.
            return _.filter($scope.group.members, function(member) {
                return member.id == student.id;
            }).length == 0;
        };

        /**
        * This functions get a student and returns a string comprised of the student's name
        * and the groups that he studies in (in the specific array) if any exists.
        */
        $scope.nameWithGroups = function(student) {
            var student = _.filter($scope.allStudents, function(currentStudent) {
                return currentStudent.id == student.id;
            })[0];
            if (_.any(student.groups)) {
                var group_names = [];
                _.each(student.groups, function(group) {
                    group_names.push(group.name);
                });
                return student.name + " (" + group_names.join(", ") + ")";
            }
            return student.name;
        }
    });
}
