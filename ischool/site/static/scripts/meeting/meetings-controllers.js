/**
 * Created by aviad on 25/01/14.
 */


function MeetingListCtrl($scope, $modal, $http) {

    $scope.meetings = $http.get("/api/meetings/student/" + $scope.personId).then(function (result) {
        return result.data;
    });

    $scope.meetingListMode = true;
    $scope.meetingEditMode = false;

    $scope.updateMeetingsList = function () {
        $scope.meetings = $http.get("/api/meetings/student/" + $scope.personId).then(function (result) {
            return result.data;
        });
    }

    $scope.viewMeeting = function (meeting) {
        $scope.selectedMeeting = meeting;
        $scope.meetingListMode = false;
        $scope.meetingViewMode = true;
    }

    $scope.editMeeting = function (meeting) {
        $scope.selectedMeeting = meeting;
        $scope.meetingEditMode = true;
        $scope.meetingListMode = false;
        $scope.meetingViewMode = false;
    }

     $scope.createMeeting = function () {
        $scope.selectedMeeting = undefined;
        $scope.meetingEditMode = true;
        $scope.meetingListMode = false;
        $scope.meetingViewMode = false;
    }

    $scope.viewList = function (meeting) {
        $scope.selectedMeeting = 0;
        $scope.meetingListMode = true;
        $scope.meetingViewMode = false;
    }

    $scope.tinymceOptions = {
        language: 'he_IL',
        directionality : 'rtl',
        menubar: false,
        statusbar: false,
        plugins: "directionality",
        toolbar: "bold italic underline | alignleft alignright | undo redo | bullist numlist | ltr rtl"
    };


    $scope.openDeleteMeetingModal = function (meeting) {
        var modalInstance = $modal.open({
            templateUrl: 'deleteMeetingModal.html',
            controller: DeleteMeetingModalCtrl,
            resolve: {
                parentScope: function () {
                    return $scope;
                },
                meeting: function () {
                    return meeting;
                }
            }
        });
    }

    function DeleteMeetingModalCtrl($scope, parentScope, $modalInstance, Restangular, $http, meeting) {
        $scope.meeting = meeting;
        $scope.meetingId = meeting.id;
        $scope.meetingTitle = meeting.title;

        $scope.deleteMeeting = function() {
            $http.delete("/api/meetings/" + $scope.meetingId).then(function(result) {
                $modalInstance.close(this);
                location.reload();
            });
        }
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

}
