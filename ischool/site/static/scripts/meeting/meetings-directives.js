app.directive('meetingModify', ['$http', function($http) {
    return {
        scope: {
            selectedMeeting: '=',
            parent: '='
        },
        templateUrl: '/static/htmls/partials/person-meetings-modify.html',
        link: function(scope, element) {
            scope.$watch('selectedMeeting', function() {
                if (scope.selectedMeeting) {
                    scope.meeting_title = scope.selectedMeeting.title;
                    scope.meeting_agenda = scope.selectedMeeting.agenda;
                    scope.meeting_summary = scope.selectedMeeting.summary;
                    scope.meeting_short_summary = scope.selectedMeeting.short_summary;
                    scope.meeting_scheduled_time = scope.selectedMeeting.scheduled_time;
                } else {
                    scope.meeting_title = scope.meeting_agenda = scope.meeting_summary =
                        scope.meeting_short_summary = scope.meeting_scheduled_time = undefined;
                }
            });

            scope.manageMeeting = function() {
                var meetingPromise;
                if (scope.selectedMeeting == undefined) {
                    meetingPromise = $http.post("/api/meetings/student/" + scope.parent.personId, {
                        meeting_title: this.meeting_title,
                        meeting_agenda: this.meeting_agenda,
                        meeting_summary: this.meeting_summary,
                        meeting_short_summary: this.meeting_short_summary,
                        meeting_scheduled_time: this.meeting_scheduled_time
                    });
                } else {
                    meetingPromise = $http.put("/api/meetings/" + scope.selectedMeeting.id, {
                        meeting_title: this.meeting_title,
                        meeting_agenda: this.meeting_agenda,
                        meeting_summary: this.meeting_summary,
                        meeting_short_summary: this.meeting_short_summary,
                        meeting_scheduled_time: this.meeting_scheduled_time
                    });
                }
                meetingPromise.then(function (result) {
                    scope.parent.updateMeetingsList();
                    scope.viewList();
                });
            }

            scope.cancelEdit = function() {
                scope.parent.meetingEditMode = false;
                scope.parent.meetingViewMode = true;
            };

            scope.viewList = function() {
                scope.parent.meetingEditMode = false;
                scope.parent.meetingListMode = true;
            };
        }

    };

}]);
