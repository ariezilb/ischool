/**
 * User: user1
 * Date: 08/09/13
 * Time: 17:19
 */

function sum(iterable) {
    return _.reduce(iterable, function(memo, num) { return memo + parseFloat(num) }, 0);
}

mod.filter('total_hours', function() {
	return function(instances) {
	    return sum(_.pluck(instances, 'hours'));
	};
});

mod.filter('total_suggested_hours', function() {
	return function(instances) {
	    return sum(_.pluck(instances, 'suggested_hours'));
	};
});

mod.filter('total_luz_hours', function() {
	return function(instances) {
	    return sum(_.pluck(instances, 'luz_hours'));
	};
});

mod.filter('unfinished_tasks', function() {
	return function(tasks) {
	    return _.filter(tasks, function(t) {
            return !t.is_done;
        });
	};
});

mod.filter('is_needs_verifying', function() {
	return function(placements) {
	    return _.some(placements, function(p) {
            return p.is_needs_verifying;
        });
	};
});

mod.filter('is_all_verified', function() {
	return function(placements) {
	    return _.every(placements, function(p) {
            return p.is_all_verified;
        });
	};
});

mod.filter('is_first_instance', function() {
	return function(instance, units) {
        if (!units || units.length == 0) {
            return true;
        }
        units = _.sortBy(units, function(unit) {
            return unit.order;
        });
        var instancesTest = _.filter(_.first(units).instances, function(inst) {
            return inst.id == instance.id;
        });
	    if (instancesTest.length == 0) {
            return false;
        }

        instances = _.sortBy(_.first(units).instances, function(instance) {
            return instance.order;
        });

        return _.first(instances).id == instance.id;
	};
});

mod.filter('is_last_instance', function() {
	return function(instance, instances) {
        // Instance should be last, and be unit-less
	    if (instance.unit_id != null) {
            return false;
        }

        var parentlessInstances = _.sortBy(_.filter(instances, function(inst) {
            return inst.unit_id == null;
        }), function(inst) {
            return inst.order;
        });

        return _.last(parentlessInstances).id == instance.id;
	};
});

mod.filter('file_url', function() {
    return function() {
        return 'TODO'
    }
});

mod.filter('get_inst_time_desc', function() {
    return function(instance) {
        if (!instance.start_time)
            return "";

        var days_names = ['א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ש'];

        return "יום " + days_names[new Date(instance.start_time).getDay()] + "', " +
            instance.luz_hours + " שעות";
    }
});

mod.filter('sortPriorities', function() {
    return function(priorities) {
        return _.sortBy(priorities, function(priority) { return priority.priority });
    };
});
