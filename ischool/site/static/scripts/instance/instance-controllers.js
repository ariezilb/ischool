/**
 * User: user1
 * Date: 08/09/13
 * Time: 17:19
 */

function InstanceListCtrl($scope, Restangular, $http, $modal) {
    $scope.groups = [];
    $scope.instances = [];
    $scope.instances_by_weeks = {};

    // Add listener when array is ready
    $scope.$watch("array", function(array) {
        var arrayBase = Restangular.one('arrays', array.id);

        arrayBase.getList("units").then(function(units) {
            $scope.units = units;
            $scope.createNewUnit = function() {
                Restangular.all('units').post({array_id: array.id, group_id: $scope.selected.group.id}).then(function(unit) {
                    $scope.units.push(unit);
                });
            };

            $scope.isInGroup = function(unit) {
                return $scope.selected.group && unit.group_id == $scope.selected.group.id;
            };

            $scope.reorderUnit = function(unit, delta) {
                var newOrder = unit.order + delta;

                // If it hasn't been moved outside of the list
                if (newOrder > 0 && newOrder <= $scope.units.length) {
                    var unitToSwap = _.findWhere($scope.units, {order: newOrder});
                    unitToSwap.order = unit.order;
                    unit.order = newOrder;
                    unitToSwap.put();
                    unit.put();
                }
            };

            $scope.removeUnit = function(unit) {
                if (!confirm("האם אתה בטוח?\nפעולה זו תמחק את כל המופעים ביחידה.")) {
                    return;
                }
                unit.remove().then(function() {
                    $scope.units.splice($scope.units.indexOf(unit), 1);

                    // TODO: all instances of unit should now be orphan, so a new request is not needed
                    $scope.changeGroup($scope.selected.group);
                });
            };
        });

        $scope.changeGroup = function(group) {
            if (group.id ==null) {
                $scope.addAlert("לא נבחרה קבוצה");
                return;
            }

            $scope.selected.group = group;

            localStorage.setItem(createStorageString("group", {arrayId: array.id}), group.id);

            Restangular.all('instances').getList({q: {filters: [
                {name: 'array_id', op: 'eq', val: array.id},
            ]}}).then(function(instances) {
                instances = _(instances).filter(function (inst) {
                    return inst.unit.group_id == group.id;
                })
                $scope.createNewInstance = function(unit) {
                    Restangular.all('instances').post({
                        array_id: array.id,
                        unit_id: unit.id,
                    }).then(function(instance) {
                        instances.push(instance);
                    });
                };

                $scope.moveInstances = function (instances, newUnitId, prevOrder) {
                    var instancesInNewUnit = _($scope.instances).filter(function(i) {return i.unit.id == newUnitId;});

                    // Determine the newOrder.
                    var newOrder;
                    if (prevOrder === undefined) {
                        newOrder = _(instancesInNewUnit).min(function(i) {
                            return i.order;
                        }).order;
                    } else {
                        newOrder = prevOrder + 1;
                    }

                    // Shift the order of all the instances after the newOrder.
                    _(instancesInNewUnit).chain()
                    .filter(function(i) {
                        return i.order >= newOrder;
                    }).each(function(i) {
                        i.order += instances.length;
                        $scope.updateInstance(i);
                    });

                    /*
                     It is important to instance's new order after other instances have been shifted,
                     to avoid bugs when the instance is moved up in the same unit.
                     */
                    // Update the instances to be in the new unit and place them in the new order.
                    _(instances).each(function (instance, index) {
                        instance.unit_id = newUnitId;
                        instance.order = newOrder + index;
                        $scope.updateInstance(instance);
                    });
                }

                $scope.removeInstance = function(instance) {
                    instance.remove().then(function() {
                        $scope.changeGroup(group);
                    });
                };

                $scope.updateInstance = function(instance) {
                    return instance.put();
                };

                // TODO: this is temporary function, replace with actual verification for older people when luz sync is ready
                $scope.toggleVerification = function(instance) {
                    instance.marked_as_verified = !instance.marked_as_verified;
                    $scope.updateInstance(instance);
                };

                $scope.toggleTaskDone = function(task, instance) {
                    task.is_done = !task.is_done;
                    $scope.updateInstance(instance);
                };

                $scope.removeTask = function(task, instance) {
                    var restTask = createRestangularObject(Restangular, task, 'tasks');
                    restTask.remove().then(function() {
                        instance.tasks.splice(instance.tasks.indexOf(task), 1);
                    });
                };

                $scope.addTask = function(taskText, instance, eventArgs) {
                    if (eventArgs.keyCode == 10 || eventArgs.keyCode == 13) {
                        instance.tasks.push({
                            text: taskText
                        });
                        $scope.updateInstance(instance).then(function () {
                            $scope.changeGroup(group);
                        });
                    }
                };

                $scope.instances = instances;
                $scope.instances_by_weeks = _.groupBy(instances, 'weeknum');

                setTimeout(function () {
                    var selectedClass = 'selected',
                        sortableListSelector = '.array-plan',
                        sortableItemsSelector = '.instance-row';

                    $(sortableListSelector).multisortable({
                        connectWith: sortableListSelector,
                        items: sortableItemsSelector,
                        cancel: 'td:not(:first-child)',
                        beforeStop: function (event, ui) {
                            // This is an hack. it makes the stop event be only when the element is dragged into his source list.
                            ui.item[0].isTransferToOtherList = false;
                        },
                        receive: function (event, ui) {
                            ui.item[0].isTransferToOtherList = true;
                            onDropInstance(this, event, ui);
                        },
                        stop: function (event, ui) {
                            if (ui.item[0].isTransferToOtherList == false) {
                                onDropInstance(this, event, ui);
                            }
                        },
                    })
                }, 0);

                function onDropInstance(listElement, event, ui) {
                    instances = _(ui.items).map(function (item) {
                        return _($scope.instances).where({
                            id: $(item).data('inst-id')
                        })[0];
                    });

                    targetUnitId = $(listElement).data('unit-id');

                    prevInstanceElement = ui.items.first().prev();
                    prevOrder = prevInstanceElement.data('inst-order');

                    $scope.moveInstances(instances, targetUnitId, prevOrder);
                    $scope.$apply(); // This hack is used because we run outside the scope of the controller.
                }
            });
        };


        Restangular.one('arrays', array.id).getList('groups').then(function(groups) {
            $scope.groups = groups;
            $scope.createGroup = function () {
                var modalInstance = $modal.open({
                    templateUrl: 'createGroupModal.html',
                    controller: CreateGroupModalCtrl,
                    resolve: {
                        parentScope: function () {
                            return $scope;
                        },
                        array: function() {
                            return array;
                        },
                        groups: function() {
                            return groups;
                        }
                    }
                });

                modalInstance.result.then(function(form) {
                    // onConfirm
                }, function () {
                    // onDismiss
                });
            };

            var newSelectedGroup = {group_id: null},
                preselected = localStorage.getItem(createStorageString("group", {arrayId: array.id}));
            for (var i = 0; preselected !== undefined && i < groups.length; i++) {
                if (preselected == groups[i].id) {
                    newSelectedGroup = groups[i];
                    break;
                }
            }

            $scope.changeGroup(newSelectedGroup);
        });
    });

    $scope.prioritiesSet = {
        severe: {priority: 1, style: {color: '#ec6767'}},
        important: {priority: 2, style: {color: '#ec9c67'}},
        normal: {priority: 5, style: {color: '#f3d585'}},
        low: {priority: 10, style: {color: '#8cec67'}},
        nice_to_have: {priority: 20, style: {color: '#67d1ec'}},
        note: {priority: 1000, style: {color: '#cdcdcd'}}
    };

    $scope.priorityAsNumber = function(task) {
        return $scope.prioritiesSet[task.priority].priority;
    };

    $scope.removeInstanceFile = function(doc, inst) {
        $http.delete('/instances/' + inst.id + '/files/' + doc.id, {
        }).success(function() {
            var index_of_instance = $scope.instances.indexOf(inst);
            var index_of_file = $scope.instances[index_of_instance].run_files.indexOf(doc);
            $scope.instances[index_of_instance].run_files.splice(index_of_file);
        });
    };

    $scope.removeInstanceFU = function(doc, inst) {
        $http.delete('/instances/' + inst.id + '/fus/' + doc.id, {
        }).success(function() {
            var index_of_instance = $scope.instances.indexOf(inst);
            var index_of_file = $scope.instances[index_of_instance].fu_files.indexOf(doc);
            $scope.instances[index_of_instance].fu_files.splice(index_of_file);
        });
    };

    $scope.translateInstanceType = function(type) {
        switch(type) {
            case 'self-work': return 'ע״ע';
            case 'lesson': return 'שיעור';
            case 'cut': return 'חיתוך';
            case 'self-learn': return 'ל״ע';
            case 'targil': return 'תרגיל';
            case 'project': return 'פרויקט';
            case 'tamak': return 'תמ״כ';
            default: console.error("Unknown type : ", type);
                return 'לא ידוע';
        }
    };
}

function CreateGroupModalCtrl($scope, parentScope, groups, array, $modalInstance, Restangular) {
    $scope.groups = groups;

    function addGroup(groupToDuplicate) {
        Restangular.all('groups').post({
            array_id: array.id,
            base_group_id: groupToDuplicate ? groupToDuplicate.id : null,
            name: groupToDuplicate ? groupToDuplicate.name + " - העתק" : null
        }).then(function(group) {
            groups.push(group);

            parentScope.addAlert("הקבוצה נוצרה בהצלחה", 'success');

            // Go to group page
            window.location = '/groups/' + group.id;
        });
    }

        function newGroup(newGroupName) {
        Restangular.all('groups').post({
            array_id: array.id,
            base_group_id:  null,
            name: newGroupName
        }).then(function(group) {
            groups.push(group);

            parentScope.addAlert("הקבוצה נוצרה בהצלחה", 'success');

            // Go to group page
            window.location = '/groups/' + group.id;
        });
    }
    $scope.createNewGroup = function(group_name) {
        return newGroup(group_name)
    };

    $scope.duplicateGroup = function(group) {
        return addGroup(group);
    };


    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}
