var update_header = function() {
  var currentdate = new Date();
  if (currentdate.getHours() < 17 && currentdate.getHours() >= 6) {
    $("#floating-page-header").css('background-image', 'url(/static/images/header_day.jpg?' + Date.now() + ')');
  }
  if (currentdate.getHours() == 17) {
    $("#floating-page-header").css('background-image', 'url(/static/images/header_sunset.jpg?' + Date.now() + ')');
  }
  if (currentdate.getHours() > 17 || currentdate.getHours()  < 6) {
    $("#floating-page-header").css('background-image', 'url(/static/images/header_night.jpg?' + Date.now() + ')');
  }
}

update_header();
// Automatically update the header once an hour
setInterval(update_header, 1000 * 60 * 60);
