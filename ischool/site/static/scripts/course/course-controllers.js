/**
 * User: user1
 * Date: 08/09/13
 * Time: 17:19
 */

function CourseListCtrl($scope, Restangular) {
    var courseBase = Restangular.all('courses');
    $scope.courses = courseBase.getList();

    $scope.createNewCourse = function() {
        courseBase.post().then(function(course) {
            window.location = "/courses/" + course.eng_name;
        });
    };
}
