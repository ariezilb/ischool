function MatrixCtrl($scope, $element, Restangular, $location, $http, $timeout, AutoSyncr) {
    $scope.show_checkers = false;
    $scope.highlighted_checker_id = false;
    $scope.checkers_in_array = [];
    $scope.checkers = {};
    $scope.new_checker = false;
    $scope.flatten_assignments = [];
    $scope.assignments = {};
    $scope.selected.group = {id: null};
    $scope.openUntil = function(instance, exercise) {
        var exId = exercise.id;

        // Enable unchecking
        if (exId == $scope.openUntil[instance.id]) {
            exId = 0;
        }
        sessionStorage.setItem(createStorageString("autoOpen", {arrayId: $scope.arrayId, instId: instance.id}), exId);
        $scope.openUntil[instance.id] = exId;
    };
    $scope.openUntil.loadStorage = function(instances) {
        _(instances).each(function(instance) {
            $scope.openUntil[instance.id] = sessionStorage.getItem(
                createStorageString("autoOpen", {arrayId: $scope.arrayId, instId: instance.id}));
        });
    };

    $scope.focused_assignment = $location.search()['assignment'];

    $http.get('/api/checkers_in_array/' + $scope.arrayId).then(function (result) {
        $scope.checkers_in_array = result.data.checkers;
    });

    $scope.toggle_checker_highlight = function (checker) {
        if ($scope.highlighted_checker_id == checker)
            $scope.highlighted_checker_id = false;
        else
            $scope.highlighted_checker_id = checker;
    };

    $scope.add_new_checker = function () {
        if ($scope.new_checker) {
            $http.put('/api/checker/' + $scope.new_checker + '/in_array/' + $scope.arrayId).then(function () {
                if (!_($scope.checkers_in_array).contains($scope.new_checker))
                    $scope.checkers_in_array.push($scope.new_checker);

                $scope.new_checker = false;
            }, function () {
                alert('There was an error. Please refreash the page');
            });
        }
    };

    $scope.remove_checker = function (checker_id) {
        $http.delete('/api/checker/' + checker_id + '/in_array/' + $scope.arrayId).then(function () {
            $scope.checkers_in_array = _($scope.checkers_in_array).without(checker_id);
            if ($scope.highlighted_checker_id == checker_id)
                $scope.highlighted_checker_id = false;
        });
    };

    $http.get('/api/people/role/checker').then(function (result) {
        _(result.data).each(function (checker) {
            $scope.checkers[checker.id] = checker;
        });
    });

    (function () {
        // Loads the $scope.checkers_assignments.
        // The structure:
        //      $scope.checkers_assignments.data[exercise_id][student_id] ==> list of checkers that assigned to the exercise of the student.

        var self = $scope.checkers_assignments = {data: {}};
        var last_checker_assignment_id = 0;
        self.fetchFromServer = function () {

            if (last_checker_assignment_id === undefined || last_checker_assignment_id === null)
                last_checker_assignment_id = 0;

            // NOTE: This code makes use in jQuery.ajax instead of $http in order to prevent $apply after each request.
            $.ajax({url: '/api/checkers_assignments/' + $scope.arrayId + '/' + last_checker_assignment_id}).done(function(result) {
                if (result.last_checker_assignment_id != last_checker_assignment_id) {
                    self.data = {};
                    _(result.checker_assignments).each(function (i) {
                        if (!self.data[i.exercise_id])
                            self.data[i.exercise_id] = {};

                        if (!self.data[i.exercise_id][i.student_id])
                            self.data[i.exercise_id][i.student_id] = [];

                        self.data[i.exercise_id][i.student_id].push(i.checker_id);
                    });
                    last_checker_assignment_id = result.last_checker_assignment_id;

                    console.log('New checkers-assignments fetched');
                    // Rerender angular because there is a new data.
                    $scope.$apply();
                }
            }).fail(function () {
                console.error('Fail to check for new checkers-assignments');
                debugger;
            });
        };
        self.has_checker = function (exercise_id, student_id) {
            // Return true if there is a checker for the exercise of the student that is in checkers_in_array.
            return _(self.data).has(exercise_id)
                && _(self.data[exercise_id]).has(student_id)
                && _(self.data[exercise_id][student_id]).any(function (checker_id) {
                    return _($scope.checkers_in_array).contains(checker_id);
                });
        };
        self.is_checker_of = function (exercise_id, student_id, checker_id) {
            // Return true if the checker is assigned to the exercise of the student.
            return _(self.data).has(exercise_id)
                && _(self.data[exercise_id]).has(student_id)
                && _(self.data[exercise_id][student_id]).contains(checker_id);
        };
        self.toggle_checker = function (exercise_id, student_id, checker_id) {
            self.set_checker(exercise_id, student_id, checker_id, !self.is_checker_of(exercise_id, student_id, checker_id));
        };
        self.set_checker = function (exercise_id, student_id, checker_id, value) {
            if (!self.data[exercise_id])
                self.data[exercise_id] = {};

            if (!self.data[exercise_id][student_id])
                self.data[exercise_id][student_id] = [];

            if (!value) {
                // Unassigne the checker in the DB.
                $http.delete('/api/checkers_assignments/' + exercise_id + '/' + student_id + '/' + checker_id).then(function () {
                    // Success

                    // Remove the checker from the assigned checker list.
                    self.data[exercise_id][student_id] = _(self.data[exercise_id][student_id]).without(checker_id);
                }, function () {
                    // Fail
                    // TODO: Alert the error.
                    debugger;
                });
            }
            else {
                // Assigne the checker in the DB.
                $http.put('/api/checkers_assignments/' + exercise_id + '/' + student_id + '/' + checker_id).then(function () {
                    // Success

                    // Add the checker to the assigned checker list.
                    self.data[exercise_id][student_id].push(checker_id);
                }, function () {
                    // Fail
                    // TODO: Alert the error.
                    debugger;
                });
            }
        };
        self.set_checker_to_current_group = function (exercise_id, checker_id, value) {
            _($scope.selected.group.members).each(function (s) {
                self.set_checker(exercise_id, s.id, checker_id, value);
            });
        };
        self.fetchFromServer();
    })();

    function autoOpenExercises() {
        if ($scope.assignments) {
            _($scope.instances).each(function(instance) {
                if (!instance.sorted_exercises) {
                    // No exercises...
                    return;
                }

                var selectedExId = $scope.openUntil[instance.id];
                if (!selectedExId || selectedExId == "0") {
                    // This instance isn't marked for auto-opening
                    return;
                }

                _($scope.selected.group.members).each(function(person) {
                    // Validate that exercise isn't already open
                    if ($scope.assignments[selectedExId] && $scope.assignments[selectedExId][person.id]) {
                        return;
                    }

                    // Find first finished exercise
                    var exercises = _(instance.sorted_exercises).clone().reverse();
                    var firstFound = null, unfinishedExercises = 0;
                    _(exercises).each(function(ex) {
                        if (firstFound) {
                            return;
                        }


                        if (!$scope.assignments[ex.id] || !$scope.assignments[ex.id][person.id]) {
                            // Exercise is not opened yet, check next
                            return;
                        }
                        var state = $scope.assignments[ex.id][person.id].state;
                        if (state == 'submitted' || state == 'done' || state == 'checking') {
                            firstFound = ex;
                        } else {
                            // There are one or some open exercises!
                            ++unfinishedExercises;
                        }
                    });

                    if (unfinishedExercises > 0) {
                        return;
                    }

                    var nextEx;
                    if (firstFound == null) {
                        // Don't open first exercise automatically
                        return;
                    } else {
                        // Open next one (previous one because of .reverse())
                        nextEx = exercises[_(exercises).indexOf(firstFound) - 1];
                    }
                    console.log("AutoOpen: Opening ", nextEx.name, " to ", person.name);

                    $scope.addExercise(nextEx, person);
                });
            });
        }
    }

    $scope.getAssignment = function(exercise_id, person_id) {
        return _($scope.assignments).where({exercise_id: exercise_id, person_id: person_id})[0];
    }

    function show_new_submission_notification(ass) {
        // Open Notification about the new submission.
        var student_name = _($scope.selected.group.members).where({id: ass.person_id})[0].name;
        var exercise_name = _($scope.exercises).where({id: ass.exercise_id})[0].name;
        var noty = new Notification(student_name, {body: "סיימתי את '" + exercise_name + "' ב" + $scope.array.name});

        // On click, set the focus to this window, and this assignment.
        noty.onclick = function() {
            $scope.$apply(function() {
                $scope.focused_assignment = ass.id;
                window.focus();
            });
        };
    }

    function update_assignments(new_assignments) {
        $scope.flatten_assignments = _(new_assignments).chain()
                                               .union($scope.flatten_assignments)
                                               .uniq(function (assignments) { return assignments.id; }) // NOTE: in case of duplication, uniq keeps the first one he saw. practicly it is the newer version of the cut (because the new_cuts is before).
                                               .value()
        $scope.assignments = {};
        _($scope.flatten_assignments).each(function (ass) {
            $scope.assignments[ass.exercise_id] = $scope.assignments[ass.exercise_id] || {}
            $scope.assignments[ass.exercise_id][ass.person_id] = ass;
        });
    }

    function check_for_new_submissions(new_assignments) {
        _(new_assignments).each(function (ass) {
            // If the assignment has changed from non-submitted to submitted
            if (ass.state == 'submitted' &&
                $scope.assignments[ass.exercise_id] &&
                $scope.assignments[ass.exercise_id][ass.person_id] &&
                $scope.assignments[ass.exercise_id][ass.person_id].state != 'submitted') {
                    show_new_submission_notification(ass);
                }
        });
    }

    $scope.last_fetch_time = undefined;
    var RELOAD_TIMEOUT = 10 * 1000, // Every 10 seconds.
        assignments_auto_syncr = AutoSyncr.new(RELOAD_TIMEOUT)
        .success(function () {
            $scope.last_fetch_time = new Date();

            $scope.checkers_assignments.fetchFromServer(); // TODO: implement the checkers via other AutoSyncr.
        })
        .new_data(function (new_data) {
            check_for_new_submissions(new_data.assignments);

            update_assignments(new_data.assignments);

            // Auto-open if needed
            autoOpenExercises();
        })
        .fail(function () {
            $scope.checkers_assignments.fetchFromServer(); // TODO: implement the checkers via other AutoSyncr.

            console.error('Fail to fetch new assignments');
            debugger;
        });

    $scope.changeGroup = function(group) {
        if (group.id != null) {
            $location.search('group', group.id);
            localStorage.setItem(createStorageString("group", {arrayId: $scope.arrayId}), group.id);
            assignments_auto_syncr.start('/api/assignments/' + $scope.arrayId + '/of/' + group.id + '/auto_sync');
            document.title = $scope.arrayName + " - " + group.name;
        }

        $scope.selected.group = group;
    };

    /** This function centers the matrix on the given student.
     *  This is done by calculating the index of the student,
     *  the width of a single student, and the width of the window and
     *  using maths to place the middle of the current view in the
     *  appropriate location.
     */
    $scope.focusOnSelectedStudent = function() {
        if ($scope.selected.student) {
            // 1. Find selected student index in our matrix.
            var groupMemberIds = $scope.selected.group.members.map(function(member) { return member.course_number; });
            groupMemberIds = groupMemberIds.sort(function(a, b) { return a - b; });
            var selectedMemberIndex = groupMemberIds.indexOf($scope.selected.student.course_number);

            // 2. Find the width of one student block in our matrix
            var studentsElmsHtmlCollection = $element[0].getElementsByClassName('students');
            // Convert the HTMLCollection to an array.
            var studentsElms = Array.prototype.slice.call(studentsElmsHtmlCollection);
            var studentsElm = studentsElms.filter(function(elm) { return elm.children.length > 1; } );

            if (studentsElm.length != 1) {
                throw 'Fail to find students element in matrix.';
            }

            studentsElm = studentsElm[0];
            var studentBlockWidth = $(studentsElm.children[0]).width();

            // 3. Highlight selected student
            studentsElm.children[selectedMemberIndex].style.backgroundColor = "yellow";

            // 4. Scroll to center on student
            // We need to scroll from left, so we inverse our indices.
            selectedMemberIndex = groupMemberIds.length - selectedMemberIndex - 1;

            var elementToScroll = $element[0].getElementsByClassName('instances-container')[0];
            elementToScroll.scrollLeft = (selectedMemberIndex + 0.5) * studentBlockWidth - $(elementToScroll).width() / 2;
        }
    };

    Restangular.one('arrays', $scope.arrayId).getList('groups').then(function(groups) {
        $scope.groups = groups;
        var preselected = $location.search()['group'];

        if (!preselected) {
            preselected = localStorage.getItem(createStorageString("group", {arrayId: $scope.arrayId}));
        }

        if ($scope.selected && $scope.selected.group && $scope.selected.group.id) {
            preselected = $scope.selected.group.id;
        }

        var newSelectedGroup = _(groups).find(function(group) { return preselected == group.id });
        $scope.changeGroup(newSelectedGroup || {id: null});
    });

    $http.get('/api/2.0/arrays/' + $scope.arrayId + '/instances').then(function(result) {
        var instances = result.data;
        $scope.instancesByUnits = _.groupBy(instances, "unit_id");

        $scope.units = _(instances).pluck("unit");
        $scope.units = _($scope.units).uniq(function(unit) {return unit.id; });

        $scope.instances = instances;
        $scope.openUntil.loadStorage(instances);

        // Fold the matrix on click.
        setTimeout(function () {
            $('.matrix-section').on('click', '.first-col-container .instance>.first-col', function () {
                $(this).siblings('.exercise').slideToggle('fast');
                $('.instances-container .exercises').eq(
                    $(this).parent().index(".first-col-container .instance")
                ).slideToggle('fast');
            }).on('mouseenter', '.matrix-assignment', function () {
                $('[data-student-id=' + $(this).data('student-id') + ']:not(.matrix-assignment), [data-exercise-id=' + $(this).data('exercise-id') + ']:not(.matrix-assignment)').addClass('highlight');
            }).on('mouseleave', '.matrix-assignment', function () {
                $('[data-student-id=' + $(this).data('student-id') + ']:not(.matrix-assignment), [data-exercise-id=' + $(this).data('exercise-id') + ']:not(.matrix-assignment)').removeClass('highlight');
            });
        }, 0);

        $scope.focusOnSelectedStudent();
    });

    Restangular.one('arrays', $scope.arrayId).getList('exercises').then(function(exercises) {
        $scope.exercises = exercises;
    });

    $scope.isAssignmentRemovable = function(exercise, member) {
        if (!$scope.assignments || !$scope.assignments[exercise.id] || !$scope.assignments[exercise.id][member.id]) {
            return false;
        }
        var as = $scope.assignments[exercise.id][member.id];
        return as.version == 1 && as.state == 'in_progress';
    };

    $scope.unitInGroup = function(unit) {
        return unit.group_id == $scope.selected.group.id;
    };

    $scope.instanceHasExercises = function(instance) {
        return instance.instance_exercises.length > 0;
    };

    $scope.linkForExercise = function(exercise) {
        return "/arrays/" + $scope.arrayId + "/exercise/" + exercise.id;
    };

    $scope.getExercisesOfInstance = function(instance) {
        // Notice that we're getting the relevant exercises according to the many2many object - instance_exercise
        // We're also sorting the exercises according to the order in the instance_exercise
        if ($scope.exercises) {
            var ids = _.pluck(instance.instance_exercises, 'exercise_id');
            var exercises = _.filter($scope.exercises, function(exercise) {
                return _.contains(ids, exercise.id);
            });

            instance.sorted_exercises =  _.sortBy(exercises, function(exercise) {
                return _.find(exercise.instance_exercise, function(ie) {
                    return ie.instance_id == instance.id;
                }).order;
            });
            return instance.sorted_exercises;
        }
        return [];
    };

    $scope.addExercise = function(exercise, member) {
        API.create.assignment.fromScratch(exercise.id, member.id).thenCall(function(data) {
            $scope.$apply(function() {
                if (!$scope.assignments[exercise.id]) {
                    $scope.assignments[exercise.id] = {};
                }
                $scope.assignments[exercise.id][member.id] = data;
            });
        });
    };

    $scope.addExerciseToAll = function(exercise) {
        var member_ids = [];
        $scope.selected.group.members.forEach(function(member) {
            if (!$scope.assignments[exercise.id] || !$scope.assignments[exercise.id][member.id]) {
                member_ids.push(member.id);
            }
        });

        API.create.assignment.bulk(exercise.id, member_ids).thenCall(function(data) {
            $scope.$apply(function() {
                if (!$scope.assignments[exercise.id]) {
                    $scope.assignments[exercise.id] = {};
                }
                data.results.forEach(function(assignment) {
                    $scope.assignments[exercise.id][assignment.person_id] = assignment;
                });
            });
        });

    };

    $scope.removeAssignment = function(exercise, member) {
        API.delete.assignment($scope.assignments[exercise.id][member.id].id).thenCall(function() {
            $scope.$apply(function() {
                delete $scope.assignments[exercise.id][member.id];
            });
        });
    };

    $scope.unitHasRelevantInstances = function(unit) {
        var instances = $scope.instancesByUnits[unit.id];
        return _(instances).filter($scope.instanceHasExercises).length > 0;
    };

    // Request for desktop notifications
    (function () {
        if (!window.Notification || !Notification.permission) {
            // Request the user to use newer browser.
            $scope.addAlert('הדפדפן שלך אינו תומך ב-Desktop Notification. אנא שדרג לגרסה חדשה יותר על מנת להנות מכל יכולות המערכת.');
            return;
          }

          if (Notification.permission === 'denied') {
            // Request the user to remove as from the notification's blocked list
            $scope.addAlert('<b>הדפדפן שלך חוסם Desktop Notification מאתר זה.</b> <br />  \
                             אנא פעל על פי השלבים הבאים על מנת לאפשר לו זאת: <br />           \
                             <ol style="list-style: decimal;">                               \
                                <li>לחץ בפינה הימנית למעלה על הכפתור "Chrome Menu"</li>      \
                                <li>בחר "Settings"</li>                                \
                                <li>לחץ "Show advanced settings"</li>                  \
                                <li>תחת "Privacy" לחץ על "Content settings"</li>       \
                                <li>תחת "Notification" לחץ על "Manage exceptions"</li> \
                                <li>מצא את השורה בה ה-Hostname pattern הוא "' + location.protocol + '//' + location.host + '"</li> \
                                <li>לחץ על ה-X בשורה זו</li>                              \
                                <li>טען מחדש את העמוד על ידי לחיצה על F5</li>               \
                             </ol>                                                    \
                             ');
            return;
          }

          if (Notification.permission !== "granted") {
            // Request permission to use Desktop Notifications from the user.
            // Place it in the click event handler because 34 <= Chrome_Version <= 37
            // only suppoorts notification permission requests in response to user actions.
            window.addEventListener('click', function() {
                Notification.requestPermission(function (status) {
                    if (Notification.permission !== status) {
                        Notification.permission = status;
                    }
                });
            });
          }
    })();
}
