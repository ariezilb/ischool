function StaticMatrixCtrl($scope, $element, $controller, $http) {
    $scope.initialize = function(_array, _group, _student) {
        $scope.arrayId = _array.id;

        // MatrixCtrl sets $scope.selected.group to an empty group. So we need to set it here to prevent that action from failing.
        $scope.selected = { 'group': _group };
        // Inherit from MatrixCtrl.
        $controller('MatrixCtrl', {$scope: $scope, $http: $http, $element: $element});

        // And we set it again here for it to have the value we want it to have.
        $scope.selected = { 'group': _group, 'student' : _student };
    }
}
