function getExerciseCriteria($http, exercise_id, $scope) {
    $http.get("/api/criteria/exercise/" + exercise_id).then(function (result) {
        $scope.criteria = result.data;
        $scope.selectedArraysCriteria = {};
        _.each($scope.criteria.taken_from_array, function(criterion) {$scope.selectedArraysCriteria[criterion.id] = true;});
    });
}

function ManageExerciseCtrl($scope, $modal, Restangular) {
    Restangular.one('arrays', $scope.arrayId).get().then(function (array) {
        $scope.array = array;
    });

    Restangular.one('arrays', $scope.arrayId).all('exercises').getList().then(function(exercises) {
        _(exercises).each(function (ex) {
            ex.num_of_criteria = ex.criteria.length;
        });

        $scope.exercises = exercises;
        // Hide all the toggled after thay rendered.
        setTimeout(function () {
            $('.toggled').slideToggle(0);
        }, 0);
    });

    $('[ng-controller=ManageExerciseCtrl]').on('click', '.toggle', function () {
        $(this).toggleClass('closed opened').parents('.exercise').find('.toggled').slideToggle();
    });

    $scope.hasStudentCopy = function (exercise) {
        return _(exercise.files).any(function (f) {return f.file_type == 'student_copy'});
    }

    $scope.submit_type_icon = function (exercise) {
        if (exercise.submit_type == 'files')
            return 'page_copy';
        else if (exercise.submit_type == 'ok')
            return 'thumb_up';
        else if (exercise.submit_type == 'svn')
            return 'link';
    }

    $scope.hebrew_file_type = function (file) {
        if (file.file_type == 'master')
            return 'עותק מקור';
        else if (file.file_type == 'student_copy')
            return 'עותק חניך';
        else if (file.file_type == 'staff_copy')
            return 'עותק סגל';
        else if (file.file_type == 'patbas')
            return 'פתב"ס';
        else if (file.file_type == 'autocheck')
            return 'בודק אוטומטי';
        else if (file.file_type == 'student_attachment')
            return 'נספחים לחניך';
        else if (file.file_type == 'staff_attachment')
            return 'נספחים לסגל';
        else if (file.file_type == 'other')
            return 'אחר';
        else
            return '';
    }

    $scope.openManageExerciseCriteriaModal = function (exercise) {

        var modalInstance = $modal.open({
            templateUrl: 'manageExerciseCriteriaModal.html',
            controller: manageExerciseCriteriaModalCtrl,
            resolve: {
                parentScope: function () {
                    return $scope;
                },
                exercise: function() {
                    return exercise;
                }
            }
        });

        modalInstance.result.then(function (form) {
            // onConfirm
        }, function () {
            // onDismiss
        });
    };

    $scope.openManageExerciseModal = function (modifiedArray, exerciseId) {
        var modalInstance = $modal.open({
            templateUrl: 'manageExerciseModal.html',
            controller: ManageExerciseModalCtrl,
            resolve: {
                parentScope: function () {
                    return $scope;
                },
                modifiedArray: function () {
                    return modifiedArray;
                },
                exerciseId: function() {
                    return exerciseId;
                }
            }
        });

        modalInstance.result.then(function (form) {
            // onConfirm
        }, function () {
            // onDismiss
        });
    };

    $scope.openDeleteExerciseModal = function (modifiedArray, exerciseId) {
        var modalInstance = $modal.open({
            templateUrl: 'deleteExerciseCriterionModal.html',
            controller: ManageExerciseModalCtrl,
            resolve: {
                parentScope: function () {
                    return $scope;
                },
                modifiedArray: function () {
                    return modifiedArray;
                },
                exerciseId: function() {
                    return exerciseId;
                }
            }
        });

        modalInstance.result.then(function (form) {
            // onConfirm
        }, function () {
            // onDismiss
        });
    };
}

function ManageExerciseModalCtrl($scope, parentScope, $modalInstance, Restangular, $http, modifiedArray, exerciseId) {
    $scope.updateCriteriaList = function() {
        if ($scope.modifiedExercise != null) {
            getExerciseCriteria($http, exerciseId, $scope);
        } else {
            $scope.criteria = {};
            $scope.criteria.taken_from_array = modifiedArray.criteria;
        }
    }

    $scope.array = modifiedArray;
    $scope.arrayId = modifiedArray.id;
    $scope.modifiedExercise = _.find(modifiedArray.exercises, function(exercise) {return exercise.id == exerciseId});
    $scope.submitTypes = [{ "value": "files", "text": "קבצים" }, { "value": "svn", "text": "SVN" }, { "value": "ok", "text": "אישור" }];
    $scope.submitType = $scope.submitTypes[0].value;
    $scope.courseSpecific = false;
    $scope.selectedArraysCriteria = {};
    $scope.exerciseName = "";
    if ($scope.modifiedExercise != null) {
        $scope.exerciseName = $scope.modifiedExercise.name;
        $scope.submitType = $scope.modifiedExercise.submit_type;
        $scope.courseSpecific = $scope.modifiedExercise.course_specific;
    }
    $scope.updateCriteriaList();

    $scope.manageExercise = function() {
        if ($scope.modifiedExercise == null) {
            exercisePromise = $http.post("/api/exercise", {
                array_id: $scope.arrayId,
                name: this.exerciseName,
                course_specific: this.courseSpecific,
                submit_type: this.submitType,
                selected_array_criteria: this.selectedArraysCriteria
            });
        } else {
            exercisePromise = $http.put("/api/exercise/" + exerciseId, {
                name: this.exerciseName,
                course_specific: this.courseSpecific,
                submit_type: this.submitType,
                selected_array_criteria: $scope.selectedArraysCriteria
            });
        }
        exercisePromise.then(function(result) {
                $modalInstance.close(this);
                location.reload();
        });
    }

    $scope.deleteExercise = function() {
        $http.delete("/exercise/delete/" + exerciseId).then(function(result) {
            $modalInstance.close(this);
            location.reload();
        });
    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

function ManageExerciseCriterionCtrl($scope, $modal) {

    $scope.openManageExerciseCriterionModal = function (modifiedExercise, modifiedCriterion) {
        var modalInstance = $modal.open({
            templateUrl: 'manageExerciseCriterionModal.html',
            controller: manageExerciseCriterionModalCtrl,
            resolve: {
                parentScope: function () {
                    return $scope;
                },
                modifiedExercise: function () {
                    return modifiedExercise;
                },
                modifiedCriterion: function() {
                    return modifiedCriterion;
                }
            }
        });

        modalInstance.result.then(function (form) {
            // onConfirm
        }, function () {
            // onDismiss
        });
    };
}


function manageExerciseCriterionModalCtrl($scope, parentScope, $modalInstance, Restangular, $http, modifiedExercise, modifiedCriterion) {
    $scope.modifiedExercise = modifiedExercise;
    $scope.modifiedCriterion = modifiedCriterion;
    if (modifiedCriterion != null) {
        $scope.criterionId = modifiedCriterion.id;
        $scope.criterionName = modifiedCriterion.name;
        $scope.lowScoreDescription = modifiedCriterion.low_score_description;
        $scope.middleScoreDescription = modifiedCriterion.middle_score_description;
        $scope.highScoreDescription = modifiedCriterion.high_score_description;
    }

    $scope.manageCriterion = function() {
        var criterionPromise;
        if (modifiedCriterion == null) {
            criterionPromise = $http.post("/api/criteria/exercise/" + modifiedExercise.id, {
                criterion_name: this.criterionName,
                low_score_description: this.lowScoreDescription,
                middle_score_description: this.middleScoreDescription,
                high_score_description: this.highScoreDescription
            });
        } else {
            criterionPromise = $http.put("/api/criteria/" + $scope.criterionId, {
                criterion_name: this.criterionName,
                low_score_description: this.lowScoreDescription,
                middle_score_description: this.middleScoreDescription,
                high_score_description: this.highScoreDescription
            });
        }
        criterionPromise.then(function (result) {
            parentScope.updateCriteriaList();
            $modalInstance.close(this);
        });
    }

    $scope.deleteCriterion = function() {
        var criterionPromise = $http.delete("/api/criteria/" + $scope.criterionId);
        criterionPromise.then(function (result) {
            // update criteria list of exercise
            $modalInstance.close(this);
        });
    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

function manageExerciseCriteriaModalCtrl($scope, parentScope, $modalInstance, $http, exercise) {
    $scope.exercise = exercise;

    $scope.updateCriteriaList = function () {
        getExerciseCriteria($http, exercise.id, $scope);
    };
    $scope.updateCriteriaList();

    $scope.save = function() {
        $http.put("/api/exercise/" + exercise.id, {
            name: exercise.name,
            course_specific: exercise.course_specific,
            submit_type: exercise.submit_type,
            selected_array_criteria: $scope.selectedArraysCriteria
        })
        .then(function(result) {
            exercise.num_of_criteria = $scope.criteria.specific_to_exercise.length + _($scope.selectedArraysCriteria).filter(function (s) {return s;}).length;
            $modalInstance.close(this);
        });
    }
}
