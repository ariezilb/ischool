// TODO: remove base2 from here

/**
 * Start up the base2 framework
 */
Base = {};
eval(base2.namespace);
base2.JavaScript.Array2(Array.prototype);
base2.JavaScript.Date2(Date.prototype);
base2.JavaScript.Function2(Function.prototype);
base2.JavaScript.String2(String.prototype);


// stretch the page main part
$(function() {

    function str2num(str) {
        return Number(str.replace(/\D/g, ''));
    }

    var page_main = $('.page-main');
    var sidebar = $('aside.sidebar');

    var winHeight = Math.max(document.body.clientHeight, window.innerHeight),
        top = page_main.offset().top,
        paddings = str2num(page_main.css('padding-top')) + str2num(page_main.css('padding-bottom'));
    page_main[0].style.minHeight = Math.max(winHeight - top, sidebar.height()) - paddings - 1 + 'px';

    // Sidebar toggle!
    var chevron = $('i#sidebar-toggler');
    chevron.click(function(e) {
        // Should we expand or collapse?
        if (chevron.hasClass('icon-chevron-right')) {
            // Collapse
            sidebar.css('width', '1px').hide('slow', function() {
                page_main.css('margin-right', '5px');
            });
        } else {
            // Expand
            page_main.css('margin-right', '');
            sidebar.css('width', '').show('slow');
        }

        chevron.toggleClass('icon-chevron-right icon-chevron-left');
    });
});

Editor = Base.extend({

    /** The inner editing component. ActiveX if in IE, an embedded object otherwise. */
    _component: null,

    constructor: function() {
        if (window['ActiveXObject']) {
            this._component = new ActiveXObject('SharePoint.OpenDocuments.2');
        } else {
            var pluginUrl = '/static/npOpenDocuments.' + ($.browser['webkit'] ? 'crx' : 'xpi');
            this._component = $('<embed type="application/x-sharepoint" pluginspage="' + pluginUrl + '" />').appendTo($(document.body))[0];
        }
    },

    openForView: function(url) {
        this._component.ViewDocument2(window, url, '');
    },

    openForEdit: function(url) {
        this._component.EditDocument2(window, url, '');
    }
}, {
    _instance: null,
    get: function () {
        if (!Editor._instance) {
            Editor._instance = new Editor();
        }
        return Editor._instance;
    }
});

$(document).click(function(e) {
    var target = $(e.target);
    var asLink = target.filter('a[href*="/files/"][href*=.],a.file-link');
    if (e.button == 0 && asLink.length > 0) {
        try {
            Editor.get().openForView(asLink.attr('href'));
            e.preventDefault();
        } catch(exception) {
            console.error("Error:", exception, ", falling to normal redirection");
        }
    }
});

$(function() {
    $('button.auto-file-upload').each(function() {
        var btn = $(this);

        btn.wrap('<div class="file-upload-button-container"/>');
        var input = $('<input name="file" type="file">').insertBefore(btn);

        input.change(function() {
            var file = input[0].files[0];
            var name = file.name;
            var size = file.size;
            var type = file.type;

            var formData = new FormData();
            formData.append('file', file);

            $.ajax({
                url: btn.data('path') + (btn.hasClass('with-filename') ? file.name : ''),
                type: 'PUT',
                headers: { 'X_FILENAME': file.name },
                // Form data
                data: formData,
                // Options to tell JQuery not to process data or worry about content-type
                cache: false,
                contentType: false,
                processData: false,
                error: function(data, subject, message) { alert(message); },
                success: function(data) {
                    $('<li class="file-type ' + type + '"><a href="' + data.url +
                          '">' + name + '</a></li>').insertBefore(btn);
                }
            }, 'json');
        });
    });
});

TranslateByKeyboard = (function () {
    var map = {
        't': 'א',
        'c': 'ב',
        'd': 'ג',
        's': 'ד',
        'v': 'ה',
        'u': 'ו',
        'z': 'ז',
        'j': 'ח',
        'y': 'ט',
        'h': 'י',
        'f': 'כ',
        'k': 'ל',
        'n': 'מ',
        'b': 'נ',
        'x': 'ס',
        'g': 'ע',
        'p': 'פ',
        'm': 'צ',
        'e': 'ק',
        'r': 'ר',
        'a': 'ש',
        ',': 'ת',
        'l': 'ך',
        'o': 'ם',
        'i': 'ן',
        ';': 'ף',
        '.': 'ץ',
    };
    return function (text) {
        return _(text).map(function (c) {
            return map[c] || c;
        }).join('');
    };
})();
