function ArrayExerciseScheduleCtrl($scope, Restangular) {
    Restangular.one('arrays', $scope.arrayId).getList('groups').then(function(groups) {
        $scope.groups = groups;

        var newSelectedGroup = 0,
                preselected = localStorage.getItem(createStorageString("group", {arrayId: $scope.array.id}));
            for (var i = 0; preselected !== undefined && i < groups.length; i++) {
                if (preselected == groups[i].id) {
                    newSelectedGroup = i;
                    break;
                }
            }

            $scope.changeGroup(groups[newSelectedGroup] || {group_id: null});

    });

    $scope.changeGroup = function(group) {

        localStorage.setItem(createStorageString("group", {arrayId: $scope.array.id}), group.id);
        $scope.selected.group = group;
    }

    Restangular.one('arrays', $scope.arrayId).getList('units').then(function(units) {
        $scope.units = units;
    });

    Restangular.one('arrays', $scope.arrayId).getList('instances').then(function(instances) {
        $scope.instancesByUnit = _.groupBy(instances, "unit_id");
    });

    Restangular.one('arrays', $scope.arrayId).getList('exercises').then(function(exercises) {
        $scope.exercises = _.object(_.pluck(exercises, "id"), exercises);
    });

    $scope.isInGroup = function(unit) {
        return unit.group_id == $scope.selected.group.id;
    };
}

app.directive('arrayExerciseUnitTable', function($http) {
    return {
        templateUrl: "/static/htmls/array-exercise-unit-table.directive.html",
        replace: true,
        controller: function($scope) {
            $scope.$watch("instancesByUnit[unit.id]", function(instances) {
                $scope.instancesWithExercises = [];
                if (instances) {
                    var orderedInstances = _(instances).sortBy('order');
                    _(orderedInstances).each(function(instance) {
                        $scope.instancesWithExercises.push({
                            type: 'instance',
                            instance: instance
                        });
                        instance.instance_exercises.forEach(function(ie) {
                            if (ie.id) {
                                $scope.instancesWithExercises.push({
                                    type: 'instance_exercise',
                                    ie: ie,
                                    instance: instance
                                });
                            } else {
                                $scope.instancesWithExercises.push({
                                    type: 'new_instance_exercise',
                                    ie: ie,
                                    instance: instance
                                });
                            }
                        });
                    });
                }
            }, true);

            $scope.getRowTemplateUrl = function(object) {
                if (object.type == "instance") {
                    return "/static/htmls/array-exercise-schedule-row-instance.html";
                } else if (object.type == "instance_exercise") {
                    return "/static/htmls/array-exercise-schedule-row-exercise.html";
                } else if (object.type == "new_instance_exercise") {
                    return "/static/htmls/array-exercise-schedule-row-new-exercise.html";
                } else {
                    console.error("unknown object type for object: ", object);
                    return "";
                }
            };

            $scope.isSupportExercises = function(instance) {
                return instance && (instance.type == 'tamak' || instance.type == 'self-work' ||
                    instance.type == 'self-learn' || instance.type == 'targil' || instance.type == 'project');
            };

            $scope.translateInstanceType = function(type) {
                switch(type) {
                    case 'self-work': return 'ע״ע';
                    case 'self-learn': return 'ל"ע';
                    case 'lesson': return 'שיעור';
                    case 'cut': return 'חיתוך';
                    case 'project': return 'פרויקט';
                    case 'tamak': return 'תמ״כ';
                    case 'targil': return 'תרגיל';
                    default: console.error("Unknown type : ", type);
                        return 'לא ידוע';
                }
            };

            $scope.addExercise = function(instance) {
                var newInstanceExercise = {
                    instance_id: instance.id,
                    order: instance.instance_exercises.length + 1
                };
                instance.instance_exercises.push(newInstanceExercise);
            };

            $scope.submitNewInstanceExercise = function(obj) {
                var url = "/arrays/" + $scope.arrayId +
                    "/exercise/" + obj.instance.id + "/" + obj.ie.exercise_id + "/schedule";
                $http.post(url).success(function(data) {
                    // We need to delete the "new" type from the list and add a real instance excercise object
                    $scope.deleteInstanceExercise(obj);
                    obj.instance.instance_exercises.push(data);
                });
            };

            $scope.deleteInstanceExercise = function(obj) {
                if (obj.ie.id) {
                    // TODO Check the return value to see it was really deleted
                    API.delete.exerciseInstance(obj.ie.id);
                }
                obj.instance.instance_exercises = _.reject(obj.instance.instance_exercises, function(ie) {return ie == obj.ie;});
                reorderInstanceExercises(obj.instance);
            };

            $scope.isLastRealInstanceExercise = function(obj) {
                var realInstanceExercises = _.filter(obj.instance.instance_exercises, function(ie) {return ie.id != undefined;}).length;
                return obj.ie.order >= realInstanceExercises;
            };

            function reorderInstanceExercises(instance) {
                for (var i = 0; i < instance.instance_exercises.length; i++) {
                    var ie = instance.instance_exercises[i];
                    if (ie.id && ie.order != (i + 1)) {
                        ie.order = i + 1;
                        var url = "/arrays/" + $scope.arrayId + "/instance_exercise/" + ie.id;
                        $http.put(url, ie);
                    }
                }
            }

            $scope.movePriorityUp = function(obj) {
                obj.instance.instance_exercises[obj.ie.order - 1] = obj.instance.instance_exercises[obj.ie.order - 2];
                obj.instance.instance_exercises[obj.ie.order - 2] = obj.ie;
                reorderInstanceExercises(obj.instance);
            };

            $scope.movePriorityDown = function(obj) {
                obj.instance.instance_exercises[obj.ie.order - 1] = obj.instance.instance_exercises[obj.ie.order];
                obj.instance.instance_exercises[obj.ie.order] = obj.ie;
                reorderInstanceExercises(obj.instance);
            };
        }
    };
});
