/**
 * User: user1
 * Date: 08/09/13
 * Time: 17:19
 */

function updateArrayCriteria($http, $scope, array_id) {
    $http.get("/api/criteria/array/" + array_id).then(function (result) {
        $scope.criteria = result.data;
    });
}

function ArrayListCtrl($scope, $http) {
    $http.get("/api/tracks").then(function (result) {
        $scope.tracks = result.data;
        var id = parseInt(localStorage.getItem("track"));
        $scope.firstTrack = _($scope.tracks).where({id: id})[0];
    });

    $http.get("/api/arrays").then(function (result) {
        $scope.arrays = result.data;
    });

    $scope.isFirstTrack = function(track) {
        return track == $scope.firstTrack;
    };

    $scope.setArrayIsArchived = function(arr, is_archived) {
        $http.put("/api/arrays/is_archived", {arr_id: arr.id, is_archived: is_archived}).then(function (result) {
            arr.is_archived = is_archived;
        });
    };

    $scope.updateTrack = function(track) {
        localStorage.setItem('track', track.id);
    }
    $scope.setCurrentMode = function (mode) {
        $scope.currentMode = mode;
    };
    $scope.setCurrentMode($scope.modes['active']);
}

function ArrayPageCtrl($scope, $http) {
    $http.get("/api/arrays/" + $scope.arrayId).then(function (result) {
        $scope.array = result.data;
        $http.get('/api/tracks/' + $scope.array.track_id).then(function (result) {
            $scope.track = result.data;
        })
    });

    $scope.selected = {
        group: null,
    };
}


function ManageArrayCtrl($scope, $modal) {

    $scope.openManageArrayModal = function (modifiedArray) {
        var modalInstance = $modal.open({
            templateUrl: 'manageArrayModal.html',
            controller: ManageArrayModalCtrl,
            resolve: {
                parentScope: function () {
                    return $scope;
                },
                modifiedArray: function () {
                    return modifiedArray;
                }
            }
        });

        modalInstance.result.then(function (form) {
            // onConfirm
        }, function () {
            // onDismiss
        });
    };
}

function ManageArrayModalCtrl($scope, parentScope, $modalInstance, Restangular, $http, modifiedArray) {
    $http.get("/api/tracks").then(function (result) {
        $scope.all_tracks =  result.data;
    });
    $http.get("/api/people/all_staff").then(function (result) {
        $scope.all_managers = result.data;
    });
    $scope.criteria = [];
    $scope.modifiedArray = modifiedArray;
    if (modifiedArray != null) {
        $scope.managerId = modifiedArray.manager.id;
        $scope.trackId = modifiedArray.track_id;
        $scope.array_name = modifiedArray.name;
        updateArrayCriteria($http, $scope, modifiedArray.id);
    }

    $scope.manageArray = function () {
        var arrayPromise;
        if (modifiedArray == null) {
            arrayPromise = Restangular.all("arrays").post({
                name: this.array_name,
                course_id: getCourseId(parentScope),
                track_id: this.trackId,
                manager_id: this.managerId
            });
        } else {
            arrayPromise = $http.put("/api/arrays/" + modifiedArray.id, {
                name: this.array_name,
                course_id: getCourseId(parentScope),
                track_id: this.trackId,
                manager_id: this.managerId
            });
        }
        arrayPromise.then(function (result) {
                $modalInstance.close(this);
                if (modifiedArray != null) {
                    window.location = "/arrays/" + modifiedArray.id;
                } else {
                    window.location = "/arrays/" + result.id;
                }
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

}

function ManageArrayCriterionCtrl($scope, $modal) {

    $scope.openManageArrayCriterionModal = function (modifiedArray, modifiedCriterion) {
        var modalInstance = $modal.open({
            templateUrl: 'manageArrayCriterionModal.html',
            controller: ManageArrayCriterionModalCtrl,
            resolve: {
                parentScope: function () {
                    return $scope;
                },
                modifiedArray: function () {
                    return modifiedArray;
                },
                modifiedCriterion: function() {
                    return modifiedCriterion;
                }
            }
        });

        modalInstance.result.then(function (form) {
            // onConfirm
        }, function () {
            // onDismiss
        });
    };

    $scope.openDeleteArrayCriterionModal = function (modifiedArray, modifiedCriterion) {
        var modalInstance = $modal.open({
            templateUrl: 'deleteArrayCriterionModal.html',
            controller: ManageArrayCriterionModalCtrl,
            resolve: {
                parentScope: function () {
                    return $scope;
                },
                modifiedArray: function () {
                    return modifiedArray;
                },
                modifiedCriterion: function() {
                    return modifiedCriterion;
                }
            }
        });

        modalInstance.result.then(function (form) {
            // onConfirm
        }, function () {
            // onDismiss
        });
    };
}


function ManageArrayCriterionModalCtrl($scope, parentScope, $modalInstance, $http, modifiedArray, modifiedCriterion) {
    $scope.modifiedArray = modifiedArray;
    $scope.arrayId = modifiedArray.id;
    $scope.modifiedCriterion = modifiedCriterion;
    if (modifiedCriterion != null) {
        $scope.criterionId = modifiedCriterion.id;
        $scope.criterionName = modifiedCriterion.name;
        $scope.lowScoreDescription = modifiedCriterion.low_score_description;
        $scope.middleScoreDescription = modifiedCriterion.middle_score_description;
        $scope.highScoreDescription = modifiedCriterion.high_score_description;
    }

    $scope.manageCriterion = function() {
        var criterionPromise;
        if (modifiedCriterion == null) {
            criterionPromise = $http.post("/api/criteria/array/" + $scope.arrayId, {
                criterion_name: this.criterionName,
                low_score_description: this.lowScoreDescription,
                middle_score_description: this.middleScoreDescription,
                high_score_description: this.highScoreDescription
            });
        } else {
            criterionPromise = $http.put("/api/criteria/" + $scope.criterionId, {
                criterion_name: this.criterionName,
                low_score_description: this.lowScoreDescription,
                middle_score_description: this.middleScoreDescription,
                high_score_description: this.highScoreDescription
            });
        }
        criterionPromise.then(function (result) {
            updateArrayCriteria($http, parentScope, modifiedArray.id);
            $modalInstance.close(this);
        });
    }

    $scope.deleteCriterion = function() {
        var criterionPromise = $http.delete("/api/criteria/" + $scope.criterionId);
        criterionPromise.then(function (result) {
            updateArrayCriteria($http, parentScope, modifiedArray.id);
            $modalInstance.close(this);
        });
    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}
