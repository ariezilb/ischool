function CutsRunCtrl($scope, Restangular, $http, $timeout, AutoSyncr) {
    var REFRESH_INTERVAL = 15 * 1000;
    $scope.selected_small_group = undefined;

    $http.get("/api/people/all_staff").then(function (result) {
        $scope.staff = result.data;
    });

    $scope.$watch('instance_id', function () {
        Restangular.all('phases').getList({q: {filters: [
            {name: 'instance_id', op: 'eq', val: $scope.instance_id},
        ]}}).then(function (phases) {
            $scope.phases = _(phases).sortBy(function (phase) { return phase.order; });
        });

        $http.get('/api/instances/' + $scope.instance_id + '/exercises').success(function (data) {
            $scope.exercises = data.exercises;
        });

        $http.get('/api/cuts/' + $scope.instance_id + "/rooms").then(function(result) {
            $scope.rooms = result.data.rooms;
        });
    });

    _.mixin({ // TODO: replace with _.max after underscore.js fixed that issus: https://github.com/jashkenas/underscore/issues/1656
        max_string: function (obj, iterator, context) {
            var result = {computed : '', value: ''};
            _(obj).each(function(value, index, list) {
                var computed = iterator ? iterator.call(context, value, index, list) : value;
                computed >= result.computed && (result = {value : value, computed : computed});
            });
            return result.value;
        }
    });

    $scope.last_fetch_time = undefined;
    var last_cuts_auto_syncr = AutoSyncr.new(REFRESH_INTERVAL)
        .success(function () {
            $scope.last_fetch_time = new Date();
        })
        .new_data(function (data) {
            $scope.flatten_assignments = union_assignments($scope.flatten_assignments, data.assignments);

            $scope.assignments = {};
            _($scope.flatten_assignments).each(function (ass) {
                $scope.assignments[ass.exercise.id] = $scope.assignments[ass.exercise.id] || {}
                $scope.assignments[ass.exercise.id][ass.person_id] = ass;
            });

            _($scope.small_groups).each(function (sg) {
                // Append the new cuts of the current small-group sorted by id.
                sg.cuts = union_cuts(sg.id, sg.cuts, data.cuts);
                sg.newest_cut = _(sg.cuts).max_string(function (cut) { return cut.timestamp; }); // TODO: implement using _.max after underscore.js fixed that issus: https://github.com/jashkenas/underscore/issues/1656

                sg.timeline = preper_timeline(sg);
            });
        })
        .fail(function () {
            console.error('Fail to fetch last cuts');
        });

    $scope.$watch('selectedRoom', function () {
        if ($scope.selectedRoom) {
            Restangular.all('small_groups').getList({q: {filters: [
                {name: 'instance_id', op: 'eq', val: $scope.instance_id},
                {name: 'room_id', op: 'eq', val: $scope.selectedRoom.id},
            ]}}).then(function (small_groups) {
                $scope.small_groups = small_groups;

                last_cuts_auto_syncr.start('/api/cuts/' + $scope.instance_id + '/room/' + $scope.selectedRoom.id + '/auto_sync');
            });
        }
    });

    function preper_timeline(sg) {
        var timeline = _(sg.cuts).map(function (cut) {
            return {
                type: 'cut',
                time: cut.timestamp,
                cut: cut,
            };
        });
        _($scope.flatten_assignments).each(function (ass) {
            var student = _(sg.members).where({id: ass.person_id});
            if (student.length) {
                student = student[0];
                ass.student = student;
                timeline.push({
                    type: 'got_assignment',
                    time: ass.given_time,
                    ass: ass,
                });
                _(ass.submissions).each(function (sub, index) {
                    sub.version = index + 1;
                    if (index != ass.submissions.length - 1) { // If this isn't the last submission, it was resend.
                        sub.state = 'resend';
                    } else {
                        sub.state = ass.state;
                    }
                    sub.student = student;
                    sub.exercise = ass.exercise;
                    timeline.push({
                        type: 'submit',
                        time: sub.created_at,
                        ass: ass,
                        sub: sub,
                    });
                    if (sub.reviewed_at)
                        timeline.push({
                            type: 'review',
                            time: sub.reviewed_at,
                            ass: ass,
                            sub: sub,
                        });
                });
            }
        });

        return _(timeline).sortBy(function (t) { return -t.time });
    }

    function union_cuts(sg_id, cuts, new_cuts) {
        return _(new_cuts).chain()
                          .where({small_group_id: sg_id})
                          .union(cuts || [])
                          .uniq(function (cut) { return cut.id; }) // NOTE: in case of duplication, uniq keeps the first one he saw. practicly it is the newer version of the cut (because the new_cuts is before).
                          .value()
    }

    function union_assignments(assignments, new_assignments) {
        return _(new_assignments).chain()
                          .union(assignments || [])
                          .uniq(function (assignments) { return assignments.id; }) // NOTE: in case of duplication, uniq keeps the first one he saw. practicly it is the newer version of the cut (because the new_cuts is before).
                          .value()
    }

    $scope.addAssignment = function (ex_id, member_id) {
        API.create.assignment.fromScratch(ex_id, member_id).thenCall(function(data) {
            $scope.$apply(function() {
                $scope.assignments[ex_id] = $scope.assignments[ex_id] || {};
                $scope.assignments[ex_id][member_id] = data;
            });
        });
    };

    $scope.isAssignmentRemovable = function(exercise_id, member_id) {
        if (!$scope.assignments || !$scope.assignments[exercise_id] || !$scope.assignments[exercise_id][member_id]) {
            return false;
        }
        var as = $scope.assignments[exercise_id][member_id];
        return as.version == 1 && as.state == 'in_progress';
    };

    $scope.removeAssignment = function(exercise_id, member_id) {
        API.delete.assignment($scope.assignments[exercise_id][member_id].id).thenCall(function() {
            $scope.$apply(function() {
                delete $scope.assignments[exercise_id][member_id];
            });
        });
    };

    $scope.isPhaseDone = function (phase_id, done_phases) {
        return _(done_phases).contains(phase_id);
    };

    $scope.focus_on_small_group = function (sg) {
        $scope.new_cut_form_is_shown = false;
        if (sg) {
            $scope.selected_small_group = sg;

            // Set focus on the second page.
            $('.page').removeClass('focus').eq(1).addClass('focus');
        }
        else {
            // Set focus on the first page.
            $('.page').removeClass('focus').eq(0).addClass('focus');
        }
    };

    $scope.show_new_cut_form = function () {
        $scope.new_cut_form_is_shown = true;

        var phases = _($scope.phases).chain()
                                     .pluck('id')
                                     .object(_($scope.phases).chain()
                                                              .size()
                                                              .range()
                                                              .map(function () {return false;})
                                                              .value())
                                     .value();

        if ($scope.selected_small_group.last_cut)
            _($scope.selected_small_group.last_cut.phases).each(function (phase) {
                phases[phase] = true;
            });

        $scope.new_cut = {
            comment: '',
            cutter: $scope.person.id, // Initialize the new cut's cutter to be the connected person.
            timestamp: new Date(),
            phases: phases,
        };

        tinymce.get('new_cut_comment').setContent(''); // BUGFIX: clear the tinymce content.
    };

    $scope.save_new_cut_button_active = true;
    $scope.save_new_cut = function () {
        $scope.save_new_cut_button_active = false;
        var sg = $scope.selected_small_group;
        $scope.new_cut.timestamp_serialized = $scope.new_cut.timestamp.getTime() / 1000;
        $http.post('/api/cuts/small_group/' + sg.id , $scope.new_cut).success(function (data) {
            sg.cuts = union_cuts(sg.id, sg.cuts, [data.new_cut]);
            $scope.new_cut_form_is_shown = false;
            $scope.save_new_cut_button_active = true;
        }).error(function () {
            console.error('Fail to save new cut.');
            // TODO: handle error.
            $scope.save_new_cut_button_active = true;
        });
    };

    $scope.tinymceOptions = {
        language: 'he_IL',
        directionality : 'rtl',
        menubar: false,
        statusbar: false,
        plugins: "directionality",
        toolbar: "bold italic underline | alignleft alignright | undo redo | bullist numlist | ltr rtl"
    };
}

function CutsPlanCtrl($scope, Restangular, $http) {
    $scope.num_of_people = 2;

    $scope.$watch('instance_id', function () {
        Restangular.all('phases').getList({q: {filters: [
            {name: 'instance_id', op: 'eq', val: $scope.instance_id},
        ]}}).then(function (phases) {
            $scope.phases = phases;
        });

        Restangular.all('rooms').getList().then(function (rooms) {
            $scope.rooms = rooms;
            setTimeout(function () {
                $('.room').droppable({
                    scope: 'small-groups',
                    hoverClass: "ui-state-hover",
                    drop: function (event, ui) {
                        var droppable = this;
                        $scope.$apply(function () {
                            // Move the dragged small-group to the dropped room.
                            var scope = angular.element(ui.draggable).scope(),
                            sg = scope.sg,
                            src_room = scope.room,
                            dest_room = angular.element(droppable).scope().room;

                            if (src_room.id != dest_room.id) {
                                sg.room_id = dest_room.id;
                                sg.put();
                            }
                            setTimeout(setGroupDraggable);
                        });
                    },
                });
            });
        });

        $scope.reload();
    });

    $scope.reload = function() {
        function setGroupDraggable() {
            $('.small-group').draggable({
                cursor: 'move',
                scope: 'small-groups',
                revert: 'invalid',
            }).animate({left: 0, top: 0});
            setDroppable();
            setDraggable();
        }

        function setDroppable() {
            $('.small-group, .new-small-group').droppable({
                hoverClass: "ui-state-hover",
                scope: 'members',
                tolerance: 'pointer',
                drop: function(event, ui) {
                    var droppable = this,
                        new_sg = $(droppable).hasClass('new-small-group');
                    $scope.$apply(function () {
                        // Move the dragged member to the dropped small-group.
                        var scope = angular.element(ui.draggable).scope(),
                        member = scope.member,
                        src_sg = scope.sg,
                        dest_sg = angular.element(droppable).scope().sg;
                        // If we move the member to other sg
                        if (new_sg || !src_sg || src_sg.id != dest_sg.id) {
                            if (src_sg) {
                                src_sg.members = _(src_sg.members).without(member);

                                // If the src group is empty, delete it.
                                if (src_sg.members.length == 0) {
                                    src_sg.remove().then(function () {
                                        $scope.small_groups = _($scope.small_groups).without(scope.sg);
                                    });
                                }
                                else {
                                    // Commit the changes in the src_sg.
                                    src_sg.put();
                                }
                            }

                            // Commit the changes in the dest_sg.
                            if (new_sg) {
                                Restangular.all('small_groups')
                                    .post({
                                        room_id: angular.element(droppable).scope().room.id,
                                        instance_id: $scope.instance_id,
                                    }).then(function (sg) {
                                        $scope.small_groups.push(sg);
                                        sg.members.push(member);
                                        sg.put().then(function () {
                                            setTimeout(setGroupDraggable);
                                        });
                                    });
                            }
                            else {
                                dest_sg.members.push(member);
                                dest_sg.put();
                            }
                        }

                        setTimeout(setDraggable);
                    });
                  },
              });
        }

        function setDraggable() {
            $('.member').draggable({
                cursor: 'move',
                scope: 'members',
                revert: 'invalid',
            }).animate({left: 0, top: 0});
        }

        $http.get('/api/cuts/' + $scope.instance_id + '/members').then(function (result) {
            $scope.members = result.data.members;
        });

        Restangular.all('small_groups').getList({q: {filters: [
            {name: 'instance_id', op: 'eq', val: $scope.instance_id},
        ]}}).then(function (small_groups) {
            $scope.small_groups = small_groups;

            setTimeout(function () {
                setGroupDraggable();

                $('.not_placed').droppable({
                    hoverClass: "ui-state-hover",
                    scope: 'members',
                    tolerance: 'pointer',
                    drop: function(event, ui) {
                        var droppable = this;
                        $scope.$apply(function () {
                            // Move the dragged member to the dropped small-group.
                            var scope = angular.element(ui.draggable).scope(),
                            member = scope.member,
                            src_sg = scope.sg;
                            if (src_sg) {
                                src_sg.members = _(src_sg.members).without(member);

                                // If the src group is empty, delete it.
                                if (src_sg.members.length == 0) {
                                    src_sg.remove().then(function () {
                                        $scope.small_groups = _($scope.small_groups).without(src_sg);
                                    });
                                }
                                else {
                                    // Commit the changes in the src_sg.
                                    src_sg.put();
                                }
                            }
                            setTimeout(setDraggable);
                        });
                    }
                });
            });
        });
    }

    $scope.isFirst = function (item, list) {
        return _(list).min(function (i) {
            return i.order;
        }).id == item.id;
    };

    $scope.isLast = function (item, list) {
        return _(list).max(function (i) {
            return i.order;
        }).id == item.id;
    };

    $scope.removePhase = function (phase) {
        phase.remove().then(function () {
            $scope.phases = _($scope.phases).without(phase);
        });
    };

    $scope.reorderPhase = function (phase, change) {
        var sorted_phases = _($scope.phases).sortBy(function (p) {
            return p.order;
        }),
        index = _(sorted_phases).chain().map(function (p) {return p.order}).sortedIndex(phase.order).value(),
        tmp;

        // Swap the order values.
        tmp = sorted_phases[index + change].order;
        sorted_phases[index + change].order = phase.order;
        phase.order = tmp;

        // Commit the changes.
        sorted_phases[index + change].put();
        phase.put();
    };

    $scope.addNewPhase = function () {
        Restangular.all('phases').post({
            name: '<שם השלב>',
            description: '<תיאור השלב>',
            instance_id: $scope.instance_id,
            order: _($scope.phases).max(function (i) {
                return i.order;
            }).order + 1,
        }).then(function (phase) {
            $scope.phases.push(phase);
        })
    };

    $scope.isNotPlaced = function (member) {
        return _($scope.small_groups).all(function (sg) {
            return _(sg.members).where({id: member.id}).length == 0;
        });
    };

    $scope.randomizeRooms = function() {
        var data = {
            rooms:  _.pluck(_($scope.rooms).where({selected: true}), 'id'),
            num_of_people: $scope.num_of_people,
        };
        $http.post('/api/cuts/' + $scope.instance_id + "/randomize" , data).success(function (data) {
            $scope.reload();
        });
    };
}
