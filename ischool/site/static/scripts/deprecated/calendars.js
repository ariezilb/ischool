
Calendar = function(month, daysToMark, firstWeeknum) {
    this.startDate = month;
    this.monthId = month.getMonth();
    this.daysToMark = daysToMark;
    this.firstWeeknum = firstWeeknum;

    this.calculateWeekdays();
    this.daysInWeek = this.lastWeekday - this.firstWeekday + 1;
    this.calculateBoundaries();
};

Calendar.prototype = {

    /** The first day of the week */
    defaultFirstDay: 1,

    /** The minimum number of week days to show (potentially more if there were marked days later in the week) */
    minWeekDays: 5,

    shouldShow: function(date) {
        var weekday = date.getDay() + 1;
        return (weekday >= this.firstWeekday && weekday <= this.lastWeekday);
    },

    calculateBoundaries: function() {
        var date = new Date(this.startDate);

        while(!this.shouldShow(date) && date.getMonth() == this.monthId){
            date.setDate(date.getDate() + 1);
            // if we skip first week, increment weeknum
            if (date.getDay() == 0) {
                this.firstWeeknum++;
            }
        }
        // if we start in mid-week, backtrack to the week start.
        date.setDate(date.getDate() - (date.getDay() + 1 - this.firstWeekday));
        this.firstDate = date;

        this.lastDate = new Date(this.startDate.getFullYear(), this.monthId + 1, 0);
        var diff = this.lastWeekday - (this.lastDate.getDay() + 1);
        this.lastDate.setDate(this.lastDate.getDate() + diff);
        this.lastDate = this.lastDate;
    },

    calculateWeekdays: function() {
        this.firstWeekday = this.defaultFirstDay;
        this.lastWeekday = this.firstWeekday + this.minWeekDays - 1;
        this.lastWeekday = Math.max(this.lastWeekday, this.getLargestWeekday(this.daysToMark));
    },

    /**
     * Returns the (1-based) largest weekday for the list of days in the month.
     * @param days The array of important days we must show.
     */
    getLargestWeekday: function(days) {
        var max = 0;
        var cal = this;
        $(days).each(function() {
            var date = new Date(new Date(cal.startDate).setDate(Number(this)));
            var weekday = date.getDay() + 1;
            max = Math.max(max, weekday);
        });
        return max;
    }
};

/**
 * SuperCalendar is a class that holds multiple calendars and shares info between them
 * @constructor
 */
SuperCalendar = function(listEl) {
    this.listEl = listEl;
    this.parseList();
};

SuperCalendar.prototype = {

    listEl: null,

    markedDays: null,

    calendars: null,

    isMultiYear: false,

    parseList: function() {
        var me = this;
        var calendars = [];
        $(this.listEl).children('li.template').each(function() { calendars.push(me.getCalendarFromEl(this)); });

        calendars.sort(function(cal1, cal2) {
            var cal1_sort = cal1.startDate.getFullYear() + '-' + cal1.startDate.getMonth();
            var cal2_sort = cal2.startDate.getFullYear() + '-' + cal2.startDate.getMonth();
            return (cal1_sort < cal2_sort) ? -1 : (cal1_sort > cal2_sort) ? 1 : 0
        });

        if (calendars.length) {
            this.isMultiYear = (calendars[0].startDate.getFullYear() != calendars[calendars.length - 1].startDate.getFullYear());
        }

        this.calendars = calendars;
    },

    getCalendarFromEl: function(li) {
        li = $(li);

        var firstWeekNum = li.data('week-num');

        var year = Number(li.data('month').substr(0, 4));
        var month = Number(li.data('month').substr(5, 2));
        var startDate = new Date(year, month - 1);

        // we add "" because if there's only one number it would evaluate it
        // and we want it to stay a string so we can split it without worries.
        var daysToMark = "" + li.data('mark');
        daysToMark = daysToMark.split(',');

        li.remove();
        var me = this;

        $(daysToMark).each(function() {
            me.addDayMark(year, month, Number(this));
        });

        return new Calendar(startDate, daysToMark, firstWeekNum);
    },

    addDayMark: function(year, month, day) {
        if (!this.markedDays) this.markedDays = {};
        if (!this.markedDays[year]) this.markedDays[year] = {};
        if (!this.markedDays[year][month]) this.markedDays[year][month] = {};
        this.markedDays[year][month][day] = true;
    },

    isDayMarked: function(date) {
        var year = date.getFullYear(),
            month = date.getMonth() + 1,
            day = date.getDate();
        return !!(this.markedDays[year] && this.markedDays[year][month] && this.markedDays[year][month][day]);
    }
};

$(function() {

    var MONTHS = ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר'];
    var DAY_NAMES = ['א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ש'];

    function addMainTableHeader(table, cal, isMultiYear) {
        var title = MONTHS[cal.monthId];
        if (isMultiYear) {
            title += " " + cal.startDate.getFullYear()
        }

        var thead = $('<thead>').appendTo(table);
        $('<tr><th colspan="' + cal.daysInWeek + '" class="title">' + title + '</th></tr>').appendTo(thead);
        var daysTr = $('<tr>').appendTo(thead);
        for (var i = cal.firstWeekday; i <= cal.lastWeekday; i++) {
            $('<th>' + DAY_NAMES[i - 1] + '</th>').appendTo(daysTr);
        }
    }

    var calsEl = $('ol.calendars');
    var cals = new SuperCalendar(calsEl);
    $(cals.calendars).each(function() {
        var cal = this;

        var mainTable = $('<table>').addClass('body');
        var weeksTable = $('<table>').addClass('weeknums');

        addMainTableHeader(mainTable, cal, cals.isMultiYear);
        $('<thead><tr><td>שבוע</td></tr></thead>').appendTo(weeksTable);

        var lastWeeknum = cal.firstWeeknum;
        var calendarBody = $('<tbody>').appendTo(mainTable);
        var weeknumsBody = $('<tbody>').appendTo(weeksTable);
        for (var d = cal.firstDate; d <= cal.lastDate; d.setDate(d.getDate() + 1)) {
            if (!cal.shouldShow(d)) continue;

            var tr;
            if (d.getDay() + 1 == cal.firstWeekday) {
                tr = $('<tr>').appendTo(calendarBody);
                $('<tr><td><div>' + lastWeeknum++ + '</div></td></tr>').appendTo(weeknumsBody);
            }

            $('<td>' + d.getDate() + '</td>')
                .toggleClass('marked', cals.isDayMarked(d))
                .toggleClass('dimmed', d.getMonth() != cal.monthId).appendTo(tr);
        }

        var li = $('<li>').addClass('calendar');
        weeksTable.appendTo(li);
        mainTable.appendTo(li);
        li.appendTo(calsEl);
    });
});
