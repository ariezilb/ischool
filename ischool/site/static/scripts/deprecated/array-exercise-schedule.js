Scheduler = Base.extend({
    constructor: function(el){
        this.$el = $(el);
        this.exercisesEl = this.$el.nextUntil('.schedule-exercise-form');
        this.formEl = this.$el.nextAll('.schedule-exercise-form:first');

        this.ui = {
            toggle: this.$el.find('.schedule-exercise-toggle'),
            selectBox: this.formEl.find('select'),
            submit: this.formEl.find('.btn')
        };

        if (this.ui.toggle.length == 0)
            return;

        var that = this;

        this.ui.toggle.click(function(){that.toggle()});
        this.ui.submit.click(function(){that.submit()});
        this.exercisesEl.find('.up,.down').click(function(ev){
            that.move(ev.target);
        });
        this.exercisesEl.find('.del').click(function(ev){
            ev.preventDefault();
            that.delete(ev.target);
        });

        this.updateArrows();
    },

    updateArrows: function(){
        this.exercisesEl.find('.disable').removeClass('disable');
        this.$el.nextUntil('.schedule-exercise-form').first().find('.up').addClass('disable');
        this.$el.nextUntil('.schedule-exercise-form').last().find('.down').addClass('disable');
    },

    toggle: function(){
        this.formEl.toggle();
    },

    delete: function(el){
      el = $(el);
      API.delete.exerciseInstance(el.data('id'))/*.thenCall(function() {
          el.closest('.exercise_instance').remove();
          // Need to reorder (better done using pushUpdates and data-ids)
      });*/
          .thenReload();
    },

    move: function(arrowEl){
      arrowEl = $(arrowEl);
      if (arrowEl.hasClass('disabled')) return;
      var up = arrowEl.hasClass('up');
      var exEl = arrowEl.closest('.exercise_instance');
      var modifier = up ? -1: 1;
      var url = arrowEl.parent('.order').data('url');
      var that = this;
      jQuery.post(url, {modifier: modifier}, function(response){
        var otherId = response.otherId;
        var otherEl = that.exercisesEl.filter('[data-id=' + otherId + ']');
          otherEl.detach();
        if (up){
            otherEl.insertAfter(exEl);
        } else {
            otherEl.insertBefore(exEl);
        }
        that.updateArrows();
        // FIXME: updateArrows does not update order as well, should invoke pushUpdates() with appropriate values

      });
    },

    submit: function(){
        var ex_id = this.ui.selectBox.val();
        var path = this.formEl.data('target');

        if (ex_id == 'new'){
            self.location.href = 'plan';
            return;
        }

        jQuery.post(path, {ex_id: ex_id}, function(){
            window.location.href = window.location.href
        });
    }

});

$('.inst').each(function(){
    var el = $(this);
    new Scheduler(el);
});
