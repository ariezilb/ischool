function MatrixCtrl($scope, Restangular, $http) {

    $http.get("/api/students").then(function(result) {
        $scope.students = result.data
    });


    $http.get('/api/get_checker_exercises').then(function(result) {
        $scope.exercises = result.data;
    });

    $http.get('/api/assignments_checker').then(function(result) {
        $scope.assignments = result.data;
    });


}

