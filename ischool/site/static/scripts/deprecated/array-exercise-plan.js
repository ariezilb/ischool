ToggledFiles = Base.extend({
    constructor: function(el){
        this.$el = $(el);
        this.toggleEl = this.$el.find('.toggle');
        this.listenTo = this.$el.find('.toggle');
        this.toggledEls = this.$el.find('.toggled');
        var that = this;
        this.listenTo.click(function(){
            that.toggle();
        });
        this.close();
    },

    close: function(){
        this.toggle(true);
    },

    toggle: function(shouldClose){
        var isOpening = this.toggleEl.hasClass('closed');
        if (shouldClose) isOpening = false;
        this.toggleEl.removeClass('open closed');
        if (isOpening){
            this.toggleEl.addClass('open');
            this.toggledEls.show();
        } else {
            this.toggleEl.addClass('closed');
            this.toggledEls.hide();
        }
    }
});

ToggleForm = Base.extend({
    constructor: function(toggle, toggled){
        this.toggleEl = $(toggle);
        this.toggledEl = $(toggled);
        this.toggledEl.hide();
        var that = this;
        this.toggleEl.click(function(){
            that.toggle()
        })
    },

    toggle: function(){
        var toHide = this.toggledEl.is(':visible');
        if (toHide){
            this.toggledEl.hide();
        } else {
            this.toggledEl.show();
        }
    }
});

$('.exercise').each(function(){
    new ToggledFiles(this);
});
