API = {
    update: {
        singleField: function(objId, objType, prop, newValue, onSuccess, onFail, disablePush) {

            onFail = onFail || API._defaultFailureCallback;
            onSuccess = onSuccess || $.noop;

            var baseData = {objId: objId, objType: objType, prop: prop, value: newValue};

            $.ajax({
                url: '/api/update',
                type: 'POST',
                data: JSON.stringify([baseData]),
                contentType: 'application/json',
                dataType: 'json',
                success: function(data) {
                    var result = $(data).filter(function(i, obj) {
                        return obj.objId == objId && obj.objType == objType && obj.prop == prop
                    });

                    if (result.length) {
                        onSuccess(result[0].value);
                    } else {
                        onFail(null, 'no response', 'נראה שהשרת לא ביצע את הפעולה, השינוי ככה"נ לא התבצע');
                    }

                    if (!disablePush) {
                        API.pushUpdates(data);
                    }
                },
                error: onFail
            });
        }
    },

    create: {

        task: {
            fromScratch: function(instance_id, text, onFail) {
                var url = '/instances/' + instance_id + '/tasks';
                return API.create._putRequest(url, {text: text}, onFail);
            }
        },

        assignment: {
            fromScratch: function(exercise_id, member_id, onFail) {
                var url = '/exercises/' + exercise_id + '/member/' + member_id;
                return API.create._putRequest(url, {}, onFail);
            },
            bulk: function(exercise_id, member_ids, onFail) {
                var url = '/exercises/' + exercise_id + '/members/' + member_ids;
                return API.create._putRequest(url, {}, onFail);
            }
        },

        _putRequest: function(url, data, onFail, type) {
            return API._request('PUT', url, data, onFail)
        }
    },

    delete: {
        assignment: function(assignment_id, onFail) {
            var url = '/assignments/' + assignment_id;
            return API._request('DELETE', url, {}, onFail);
        },

        task: function(taskId, onFail) {
            var url = '/tasks/' + taskId;
            return API._request('DELETE', url, {}, onFail);
        },

        point: function(point_id, onFail) {
            var url = '/points/' + point_id;
            return API._request('DELETE', url, {}, onFail);
        },

        exerciseInstance: function(exInstId, onFail) {
            var url = '/exercises/instances/' + exInstId;
            return API._request('DELETE', url, {}, onFail);
        }
    },

    assignment: {
        startChecking: function(assignment_id, onFail) {
            var url = '/assignments/' + assignment_id + '/start-checking';
            return API._request('POST', url, {}, onFail);
        },

        stopChecking: function(assignment_id, onFail) {
            var url = '/assignments/' + assignment_id + '/stop-checking';
            return API._request('POST', url, {}, onFail);
        },

        submitReview: function(assignment_id, state, comment_for_staff, comment_for_student, onFail) {
            var url = '/assignments/' + assignment_id + '/submit-review';
            return API._request('POST', url, {
                'state': state,
                'comment_for_staff': comment_for_staff,
                'comment_for_student': comment_for_student
            }, onFail);
        }
    },

    login: {
        send: function(username, password, onFail) {
            var url = '/login';
            return API._request('POST', url, {
                'username': username,
                'password': password
            }, onFail);
        }
    },

    extraPushListeners: [],

    extraElementListeners: [],

    pushUpdates: function(updates) {

        $(updates).each(function() {
            var objId = this.objId,
                objType = this.objType,
                prop = this.prop,
                value = this.value;

            $('[data-is-updatable="true"]').filter(function() {
                var el = $(this);
                if (el.data('obj-id') != objId || el.data('type') != objType || el.data('prop') != prop){
                    return false;
                }

                return true;
            }).each(function() {
                    var el = this;
                    if (el['updateValue'] && typeof(el['updateValue']) == 'function') {
                        el['updateValue'](value);
                    } else {
                        // if the element is a checkbox and the value is boolean
                        if (el.tagName.toLowerCase() == 'input'
                            && $(el).attr('type') == 'checkbox'
                            && value === !!value /* typeof(value) == "boolean" */) {
                            el.checked = value;
                        } else {
                            $(el).html(value);
                        }
                    }
                });

            $.each(API.extraElementListeners, function() {
                var listener = this;
                $(listener.selector).filter(function() {
                    return listener.matcherFn.call(this, objId, objType, prop);
                }).each(function(){
                        listener.func.call(this, value);
                    })
            });
        });

        $(document).trigger("ischool.update");
    },

    _request: function(type, url, data, onFail) {
        var onSuccess = $.noop;
        onFail = onFail || API._defaultFailureCallback;

        $.ajax({
            url: url,
            type: type,
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            success: function(data) {
                if (data) {
                    onSuccess(data);
                } else {
                    onFail(null, 'no response', 'בעיית שרת :(');
                }
            },
            error: onFail
        });

        return {
            thenNavigate: function() {
                onSuccess = function(data) {
                    if (data['url']) {
                        location.href = data.url;
                    } else {
                        onFail(null, 'no url', 'בעיית שרת, לא נתקבלה כתובת :(');
                    }
                };
            },
            thenCall: function(callback) {
                onSuccess = function(data) {
                    callback(data, arguments);
                    if (data['updates']) {
                        API.pushUpdates(data.updates);
                    }
                }
            },
            thenReload: function() {
                onSuccess = function() {
                    location.reload();
                }
            }
        }
    },

    /**
     * For when we want to register elements that don't have the data attributes, so we provide a
     *  matcher function that receives the objId, type, and prop and returns true if the element
     *  matches them.
     * @param selector
     * @param matcherFn
     * @param func
     */
    registerElementForPushUpdates: function(selector, matcherFn, func) {
        // in case a jQuery object is passed instead of a selector.
        if (selector['selector']) {
            selector = selector.selector;
        }
        if (matcherFn == null) {
            matcherFn = function(objId, type, prop) {
                var el = $(this);
                return el.data('obj-id') == objId && el.data('type') == type && prop == 'type';
            };
        }
        API.extraElementListeners.push({selector: selector, matcherFn: matcherFn, func: func})
    },

    _defaultFailureCallback: function(data, subject, message) {
        try {
            data = $.parseJSON(data.responseText)
        } catch (SyntaxError) {
            console.warn("Data is not JSON");
        }
        console.error(subject, message, data);

        var msg;
        if (data['error'] != null) {
            var error = data['error'];
            if (data['msg']) {
                error = data['msg'];
            }
            if (data['subject']) {
                subject = data['subject'];
            }
            msg = $("<p/>").addClass("error").html(error);
        } else {
            msg = $("<p/>").html(message);
        }

        new ischool.dialogs.ErrorBox(subject, msg).show();
    }
};
