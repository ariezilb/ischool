var resetNeeded = false;
var tweetHandled = 0;
var deletedTweets = [];

function mytimeago_interval()
{
    elements = $("abbr.mytimeago")
    for (var i = 0; i < elements.length; ++i)
    {
        elements[i].innerText = moment(elements[i].getAttribute("time")).fromNow();
    }
}


function makeTweet(tweet)
{
    if (filterHandled)
        if (tweet.handled)
            return "";
    var people_info = tweet.people;
    var hashtags_info = tweet.hashtags;
    var raw_content = tweet.content;
    for (var i = 0; i < people_info.length; ++i)
    {
        var person = people_info[i];
        var name = person.name;
        if (person.role == "student")
        {
            //name += " <" + person.course_number + ">";
            name = "" + person.course_number + "&bull;" + name;
        }
        var regex = new RegExp("@\\[person:" + person.id + "\\]", 'g');
        tweet.content = tweet.content.replace(regex,
            "<a class='person_link'  href='/people/" + person.id + "'>" + name +"</a>");
        raw_content = raw_content.replace(regex,
            "@" + name);
    }

    for (var i = 0; i < hashtags_info.length; ++i)
    {
        var hashtag = hashtags_info[i];
        var link = "onclick=\"addTagFilter(this)\"";
        //var link = "onclick=\"onTag(" + hashtag.id + ",\"" + hashtag.name + "\")\">";
        //tweet.content = tweet.content.replace(new RegExp("@\\[hashtag:" + hashtag.id + "\\]", 'g'),
        //    "<a class='hashtag_link' href='/twitter/hashtags/" + hashtag.id + "'>" + hashtag.name +"</a>");
        var regex = new RegExp("@\\[hashtag:" + hashtag.id + "\\]", 'g');
        tweet.content = tweet.content.replace(regex,
            "<a class='hashtag_link' " + link + " id=" + hashtag.id + ">" + hashtag.name +"</a>");
        raw_content = raw_content.replace(regex,
            '#' + hashtag.name);
    }

    var posterColor = tweet.poster.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0).toString(16);
    posterColor = ("000000" + posterColor);
    posterColor = posterColor.substr(posterColor.length-6 , 6);
    var chkd = "";
    var stl = "";
    if (tweet.handled)
    {
        chkd = "checked"
        stl = "background:lightgray;";
    }
    return "<div style='word-wrap:break-word; clear:both; border-bottom:1px dotted gray;" + stl + "'>" +
            "<a onclick=\"deleteTweet(" + tweet.id + ", '" + encodeURI(raw_content).replace(/'/g, "\\'") + "')\">X</a>" +
            "<input type='checkbox' " + chkd  +" style='float:left' onclick='handleTweet(this)' value='" + tweet.id + "'/>" +
            "<span style='color:#"
            + posterColor + "'><abbr dir='ltr' class='mytimeago' time='" + tweet.timestamp + "' style='color:gray; float:left'>" +
            tweet.timestamp + "/</abbr>" + tweet.poster + ":  </span>" + tweet.content +
            "</div>"
}

function handleTweet(chkbox)
{
    var handled_str = "False";
    if (chkbox.checked)
        var handled_str = "True";

    $.ajax(
    {
        url: '/api/tweets/handled/' + chkbox.getAttribute("value") + '/' + handled_str,
        dataType: "json",
        contentType: "application/json",
        success: function(data)
        {
            resetNeeded = true;
            tweetHandled = chkbox.getAttribute("value");
        }
    });

    chkbox.checked = ! chkbox.checked;
    return false;
}

function deleteTweet(id, content)
{
    content = decodeURI(content);
    if (confirm('האם אתה בטוח שברצונך למחוק את: ' + content)) {
        $.ajax({
            url: '/api/tweets/' + id,
            type: 'DELETE',
            success: function(result) {
                resetNeeded = true;
                deletedTweets.push(id);
                alert('הtweet נמחק בהצלחה');
            },
            // TODO: handle the error.
        });
    }

    return false;
}

function createFormattedFilters(filters, filterHandled) {
    var filter = "";
    for (var i = 0; i < filters.length; ++i)
    {
        if (filters[i].type == "hashtag")
            filter += filters[i].id + ",";
    }
    filter += "@";
    for (var i = 0; i < filters.length; ++i)
    {
        if (filters[i].type == "person")
            filter += filters[i].id + ",";
    }
    if (filterHandled)
    {
        filter += "@True";
    }
    else
    {
        filter += "@Flase";
    }
    return filter;
};

var tweetsFilters = [];
var filterHandled = false;
var filtersUpToDate = true;

function TimelineListCtrl($scope, $http)
{
    $scope.formattedFilters = "";
    $scope.AvailableTags = [];
    $scope.CurHTML = "";

    $scope.MessagesDiv = $("#MessagesDiv")[0];
    $scope.InitialTweetCount = 10;

    $scope.LatestTweets = [];
    $scope.AutocompleteTags = [];

    $scope.Updating = false;

    $scope.needToLoadMore = false;
    $scope.needToLoadAll = false;
    $scope.allLoaded = false;



    $scope.UpdateTweets = function()
    {
        if ($scope.Updating)
            return;
        $scope.Updating = true;

        var filtered = tweetsFilters.length > 0;

        var req = "/twitter/initialize/" + $scope.InitialTweetCount;
        if ($scope.LatestTweets.length != 0)
        {
            req = "/twitter/tweets_since/" + $scope.LatestTweets[0].id;
        }
        if (filtered)
        {
            if (filtersUpToDate) return;
            var filter = createFormattedFilters(tweetsFilters, filterHandled);
            req = "/twitter/pull_filtered/" + filter;
        }
        if ($scope.needToLoadAll)
        {
            req = "/twitter/load_all/";
        }
        if ($scope.needToLoadMore)
        {
            req = "/twitter/load_more/" + $scope.LatestTweets[$scope.LatestTweets.length-1].id + "/20";
        }
        $http.get(req).then(function(result)
        {
            var ld = false;
            if ($scope.needToLoadAll)
            {
                ld = true;
                $scope.LatestTweets = result.data.tweets;
                resetNeeded = true;
                $scope.needToLoadAll = false;
                $scope.allLoaded = true;
                if (filtered)
                {
                    $scope.Updating = false;
                    return;
                }
            }
            if ($scope.needToLoadMore)
            {
                ld = true;
                var newTweets = result.data.tweets;
                $scope.LatestTweets = $scope.LatestTweets.concat(newTweets);
                resetNeeded = true;
                $scope.needToLoadMore = false;
                if (filtered)
                {
                    $scope.Updating = false;
                    return;
                }
            }

            if (filtered)
            {
                var newTweets = result.data.tweets;
                if (newTweets == null)
                {
                    $scope.Updating = false;
                    return;
                }
                var newShit = "";
                for (var i = 0; i < newTweets.length; ++i)
                {
                    newShit += makeTweet(newTweets[i]);
                }

                $($scope.MessagesDiv).html(newShit );

                mytimeago_interval();
                $scope.Updating = false;
                return;
            }

            if (!ld)
            {
                var newTweets = result.data.tweets;
                $scope.LatestTweets = newTweets.concat($scope.LatestTweets );
            }
            if (resetNeeded)
            {
                $scope.LatestTweets = _($scope.LatestTweets).filter(function (tweet) {
                    return !_(deletedTweets).contains(tweet.id);
                });
                deletedTweets = [];

                var newShit = "";
                for (var i = 0; i < $scope.LatestTweets.length; ++i)
                {
                    if ($scope.LatestTweets[i].id == tweetHandled)
                        $scope.LatestTweets[i].handled = !$scope.LatestTweets[i].handled;
                    newShit += makeTweet($scope.LatestTweets[i]);
                }
                $scope.CurHTML = newShit;
                $($scope.MessagesDiv).html($scope.CurHTML);
                resetNeeded = false;
            }
            else
            {
                var newShit = "";
                for (var i = 0; i < newTweets.length; ++i)
                {
                    newShit += makeTweet(newTweets[i]);
                }
                $scope.CurHTML = newShit + $scope.CurHTML;
                $($scope.MessagesDiv).html($scope.CurHTML);
            }

            mytimeago_interval();
            $scope.Updating = false;
        }, function(result)
        {
            $scope.Updating = false;
        });
    }
    $scope.UpdateTweets();

    function scope_UpdateTweets()
    {
        $scope.$apply($scope.UpdateTweets);
    }
    window.setInterval(scope_UpdateTweets.bind(this), 1000);
    window.setInterval(mytimeago_interval, 60000);

    $scope.loadAll = function()
    {
        $scope.needToLoadAll = true;
    }

    $scope.loadMore = function()
    {
        $scope.needToLoadMore = true;
    }
}

function FilterCtrl($scope, $http)
{
    $scope.filters = [];

    $scope.build_tags_cloud = function(){
        $http.get("/api/tags_cloud/" + $scope.formattedFilters).then(function(result) {
            // Clear the prev cloud and create the new one.
            $("#cloud").html('').jQCloud(result.data);
        });
    }

    onTag = onTag.bind($scope)

    filtersChanged = filtersChanged.bind($scope)

    addTagFilter = addTagFilter.bind($scope);

    $scope.remove_filter = function(tag_id){
        for (var i = 0; i < $scope.filters.length; ++i)
        {
            if ($scope.filters[i].id == tag_id)
            {
                $scope.filters.remove($scope.filters[i]);

                break;
            }

        }
        for (var i = 0; i < tweetsFilters.length; ++i)
        {
            if (tweetsFilters[i].id == tag_id)
            {
                tweetsFilters.remove(tweetsFilters[i]);
                filtersUpToDate = false;
                filtersChanged();
                return;
            }

        }
    }

    $scope.init = function(filter)
    {
        $scope.filters.push(filter);
        tweetsFilters.push(filter);
        filtersUpToDate = false;
        filtersChanged();
    };

    filtersChanged();
}

function onTag(tag_id, tag_name){
    $scope = this;

    for (var i = 0; i < $scope.filters.length; ++i)
    {
        if ($scope.filters[i].id == tag_id)
            return;
    }
    var filter = {id:tag_id, name:tag_name, type:"hashtag"};

    $scope.filters.push(filter);
    tweetsFilters.push(filter);
    filtersUpToDate = false;
    filtersChanged();
}

function filtersChanged(filters)
{
    $scope = this;

    if (filters && filters[0]) {
        var filter = filters[0];

        var is_new_filter = true;
        for (var i = 0; i < $scope.filters.length; ++i)
        {
            if ($scope.filters[i].id == filter.id)
                is_new_filter = false;
        }

        if (is_new_filter) {
            $scope.filters.push(filter);
            tweetsFilters.push(filter);
            filtersUpToDate = false;
        }
    }

    $scope.formattedFilters = createFormattedFilters($scope.filters, false);
    $scope.build_tags_cloud();
}

function setCheckedFilter(chkbox)
{
    filterHandled = chkbox.checked;
    resetNeeded = true;
    filtersUpToDate = false;
}

function addTagFilter(a)
{
    $scope = this;
    onTag(a.getAttribute("id"), a.innerText);
}
