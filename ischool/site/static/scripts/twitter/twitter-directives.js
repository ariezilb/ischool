var filterStatic = true;

app.directive('ngTwitterbox', function($http)
{
    return {
        restrict: 'A',
        link: function($scope, $elem, $attrs)
        {
            function sendTweet(tweet, mentions)
            {
                var msg = tweet;
                $scope.$apply(function()
                {
                    $http.post('/twitter/tweet', {
                        title: 'tweet',
                        message: msg,
                        mentions: mentions
                        }).success(function(data)
                        {
                            console.log("Tweet sent");
                        });
                });
            }

            $elem.MentionsInput = $($elem).mentionsInput({onDataRequest: function(mode, query, triggerChar, callback)
            {
                if (triggerChar == "@")
                {
                    $.ajax(
                    {
                        url: '/api/people/begins_with/' + query.toLocaleLowerCase(),
                        dataType: "json",
                        contentType: "application/json",
                        success: function(data)
                        {
                            callback.call(this, data.people);
                        }
                    });
                }
                else if (triggerChar == "#")
                {
                    $.ajax(
                    {
                        url: '/api/hashtags/contains/' + query.toLocaleLowerCase(),
                        dataType: "json",
                        contentType: "application/json",
                        success: function(data)
                        {
                            var found = false;
                            for (var i = 0; i < data.hashtags.length; ++i)
                            {
                                if (data.hashtags[i].name == query)
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found)
                                data.hashtags.unshift({id:Math.floor((Math.random()*10000000)+1)*-1, name:query, type:"hashtag"});
                            callback.call(this, data.hashtags);
                        }
                    });
                }
            },
            showAvatars: false,
            minChars: 1});

            $elem.bind("keypress", function(e)
            {
                if (e.which === 13) // enter pressed
                {
                    $elem.MentionsInput[0].instance.val(function(tweet)
                    {
                        if (tweet.trim() == "")
                            return false;
                        $elem.MentionsInput[0].instance.getMentions(function(mentions)
                        {
                            if (filterStatic)
                            {
                                var prefix = ""
                                for (var i = 0; i < tweetsFilters.length; ++i)
                                {
                                    mentions.push(tweetsFilters[i]);
                                    prefix += "@[" + tweetsFilters[i].type + ":" + tweetsFilters[i].id + "]";
                                    if (i != (tweetsFilters.length - 1))
                                        prefix += ", ";
                                }
                                prefix = "(" + prefix  + "): ";
                                tweet = prefix + tweet;
                            }
                            sendTweet(tweet, mentions);
                        });
                    });

                    $elem.MentionsInput[0].instance.reset();
                    return false;
                }
            });
        }

    }
});

function loadFilters(filters, $http)
{
    req = "/twitter/pull_filters/" + filter;
    $http.get(req).then(function(result)
    {
        var newTweets = result.data.tweets;
        var newShit = "";
        for (var i = 0; i < newTweets.length; ++i)
        {
            newShit += makeTweet(newTweets[i]);
        }

        $($scope.MessagesDiv).html(newShit );

        return;
    });
}

app.directive('ngFilterbox', function($http)
{
    return {
        restrict: 'A',
        link: function($scope, $elem, $attrs)
        {
            var initFilters = $attrs.initialfilter;

            var filters = []

            $elem.MentionsInput = $($elem).mentionsInput({onDataRequest: function(mode, query, triggerChar, callback)
            {
                if (triggerChar == "@")
                {
                    $.ajax(
                    {
                        url: '/api/people/begins_with/' + query.toLocaleLowerCase(),
                        dataType: "json",
                        contentType: "application/json",
                        success: function(data)
                        {
                            callback.call(this, data.people);
                        }
                    });
                }
                else if (triggerChar == "#")
                {
                    $.ajax(
                    {
                        url: '/api/hashtags/begins_with/' + query.toLocaleLowerCase(),
                        dataType: "json",
                        contentType: "application/json",
                        success: function(data)
                        {
                            callback.call(this, data.hashtags);
                        }
                    });
                }
            },
            showAvatars: false,
            minChars: 1,
            addedSuffix: ", "});

            /*
            $elem.bind("keydown", function(e)
            {
                var LEFT = 37, RIGHT = 39;
                switch (e.keyCode) {
                    case LEFT:
                    case RIGHT:
                        // TODO: make this use CTRL+SHIFT
                }
            });
            */

            $elem.bind("keyup", function(e)
            {
                $elem.MentionsInput[0].instance.getMentions(function(mentions)
                {
                    var changed = false;
                    for (var i = 0; i < mentions.length; ++i)
                    {
                        var found = false;
                        for (var j = 0; j < filters.length; ++j)
                        {
                            if (filters[j].id == mentions[i].id)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            changed = true;
                            filters.push({id:mentions[i].id, name:mentions[i].name, type: mentions[i].type });
                        }
                    }

                    for (var i = 0; i < filters.length; ++i)
                    {
                        var found = false;
                        for (var j = 0; j < mentions.length; ++j)
                        {
                            if (filters[i].id == mentions[j].id)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            changed = true;
                            filters.removeAt(i)
                        }
                    }

                    if (changed)
                    {
                        window[$attrs.onfilterschange](filters);
                        $elem.MentionsInput[0].instance.reset();
                    }
                });
            });
        }
    }
});

function setFiltersStatic(chkbox)
{
    filterStatic = chkbox.checked;
}
