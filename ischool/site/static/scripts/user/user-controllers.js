/**
 * User: user1
 * Date: 08/09/13
 * Time: 17:19
 */

function LoginCtrl($scope, Restangular) {
    // $scope.people = User.query({personId: $scope.mode});

    $scope.performLogin = function() {
        $scope.error = "";

        // FIXME: Using autocomplete, angular won't recognize the validity of the form.

        Restangular.all('login').post({username: $scope.username,
                                             password: $scope.password}).then(function() {
            // Success
            $scope.error = "ההתחברות בוצעה בהצלחה, אתה מועבר כעת";
            window.location.href = "/";
        }, function(response) {
            // This is the status the server sends upon predictable failure
            $scope.error = response.data.message;
        });
    };
}

function ChangePassCtrl($scope, Restangular, $http) {
    // $scope.people = User.query({personId: $scope.mode});

    $scope.performChange = function() {
        $scope.error = "";

        if ($scope.new_password != $scope.valid_new_password){
            $scope.error = "הסיסמה לא תואמת לאימות";
            return;
        };

        $http.get('/api/getUserName/' + $scope.personId).then(function(result) {
            $scope.user_name =  result.data;

            $http.post('/api/changePassword', {username: $scope.user_name,
                                                 password: $scope.password,newPassword: $scope.new_password}).then(function() {
                // Success
                $scope.error = "הסיסא שונתה!";
                window.history.back();
            }, function(response) {
                // This is the status the server sends upon predictable failure
                $scope.error = response.data.message;
            });
        });
    };
}
