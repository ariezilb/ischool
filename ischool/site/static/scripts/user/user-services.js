/**
 * User: user1
 * Date: 08/09/13
 * Time: 17:19
 */

appServices.factory('User', function($resource){
	return $resource('/login', {}, {
		connect: {method:'POST', params: {}}
	});
});
