function ScoreVisualizationCtrl($scope, $http) {
	// input
	/*
		$scope.mode.array_id;
		$scope.mode.exercise_id;
		$scope.mode.compare_to;
		$scope.mode.which_score_to_use;
	*/

	// output
	/*
		$scope.isLoading;
		$scope.group;
		$scope.mode.array_id;
		$scope.mode.exercise_id;
		$scope.mode.compare_to;
		$scope.mode.which_score_to_use;
	*/

	var student_id, exercises_only;

	$scope.mode = $scope.mode || {};
	$scope.mode.compare_to = $scope.mode.compare_to || 'compare_to_group';
	$scope.mode.which_score_to_use = $scope.mode.which_score_to_use || 'all_the_scores';
	$scope.mode.exercise_id = $scope.mode.exercise_id || 'all';

	// This is an HACK to make mode be ng-editable
	$scope.mode.put = function () {
		// Return fake promise object.
		return {
			then: function (callback) {}
		};
	};

	$scope.mousehoverAttr = undefined;

	$scope.initialize = function (student_id_, exercises_only_) {
		// If exercises_only is true, the controller will not be able to interact with arrays at all.
		// Use this param in order to increas performance.

		student_id = student_id_;
		exercises_only = exercises_only_;

		if (!exercises_only) {
			$http.get('/api/arrays_of_student/' + student_id).then(function (result) {
				$scope.arrays = result.data;

				// Init the selected array with the first array
				if (!$scope.mode.array_id) {
					$scope.mode.array_id = _($scope.arrays).keys()[0];
				}
			});
		}

		// Fetch after any change in $scope.mode.
		$scope.$watch('mode.compare_to', fetchData);
		$scope.$watch('mode.which_score_to_use', fetchData);
		if (exercises_only) {
			$scope.$watch('mode.exercise_id', fetchData);
		}
		else {
			$scope.$watch('mode.exercise_id', updateGraph);
			$scope.$watch('mode.array_id', fetchData);
		}

		$scope.scoresSeries = [
			{
				key: 'q4_avg',
				stat_src_key: 'compare_to_statistics',
				text: 'ממוצע ברבעון העליון',
				backgroundColor: 'red',
				shadow: false,
				alphaArea: 0.1,
				alpha: 0.1,
				lineColor: 'red',
				aspect: 'area',
			},
			{
				key: 'q3_avg',
				stat_src_key: 'compare_to_statistics',
				text: 'ממוצע ברבעון השלישי',
				backgroundColor: 'red',
				shadow: false,
				alphaArea: 0.1,
				alpha: 0.1,
				lineColor: 'red',
				aspect: 'area',
			},
			{
		   		key: 'q2_avg',
		   		stat_src_key: 'compare_to_statistics',
		   		text: 'ממוצע ברבעון השני',
		   		backgroundColor: 'red',
		   		shadow: false,
		   		alphaArea: 0.1,
		   		alpha: 0.1,
		   		lineColor: 'red',
				aspect: 'area',
		   	},
			{
		   		key: 'q1_avg',
		   		stat_src_key: 'compare_to_statistics',
		   		text: 'ממוצע ברבעון התחתון',
		   		backgroundColor: 'red',
		   		shadow: false,
		   		alphaArea: 0.1,
		   		alpha: 0.1,
		   		lineColor: 'red',
				aspect: 'area',
		   	},
		   	{
				key: 'avg',
				stat_src_key: 'student_statistics',
				text: 'ציון החניך',
				lineColor: '#a7da47',
				aspect: 'line',
			},
		];
	}

	$scope.$on('zingchart_node_mouseover', function (e, data) {
		$scope.mousehoverAttr = {name: data.scaletext, description: data['data-description']};
	});

	$scope.$on('zingchart_node_mouseout', function (e, data) {
		$scope.mousehoverAttr = undefined;
	});

	function fetchData() {
		var url = '/api/scores_statistics/' + student_id + '/' + $scope.mode.compare_to + '/' + $scope.mode.which_score_to_use;
		if (exercises_only) {
			if (!$scope.mode.exercise_id)
				return;
			url += '/exercise/' + $scope.mode.exercise_id;
		}
		else {
			if (!$scope.mode.array_id)
				return;
			url += '/array/' + $scope.mode.array_id;
		}

		$scope.isLoading = true;
		$http.get(url).then(function (result) {
			$scope.isLoading = false;

			$scope.group = result.data.group;
			$scope.exercises = objFromPairs(_(result.data.exercises).map(function (v, id) {return [id, v.name]}));
			$scope.data = result.data;

			updateGraph();
		});
	};

	function updateGraph() {
		if (!$scope.data) return;

		var statistics;

		if (exercises_only) {
			statistics = $scope.data.statistics;
		}
		else {
			// If the data doesn't contains the exercise_id, reset it.
			if (!_($scope.data.exercises).has($scope.mode.exercise_id))
				$scope.mode.exercise_id = 'all';

			statistics = $scope.data.exercises[$scope.mode.exercise_id];
		}

		$scope.scoresData = _($scope.scoresSeries).map(function (series) {
			return objFromPairs(_(statistics[series.stat_src_key]).map(function (v, k) {return [k, v[series.key]]}))
		});

		$scope.scoresAttrs = {
			keys: _(statistics.compare_to_statistics).keys(),
			labels: _(statistics.compare_to_statistics).map(function (stat, criterion_id) {
				return $scope.data.criteria[criterion_id].name;
			}),
			descriptions: _(statistics.compare_to_statistics).map(function (stat, criterion_id) {
				return $scope.data.criteria[criterion_id].description;
			}),
		};
	}
}

function objFromPairs(pairs) {
	var obj = {};
	_(pairs).each(function (pair) {
		obj[pair[0]] = pair[1];
	})
	return obj;
}
