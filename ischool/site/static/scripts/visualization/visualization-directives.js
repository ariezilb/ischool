app.directive('ngVisualizationRadar', function($http) {
	var uniq_id_index = 0,
		UNIQ_ID_PREFIX = 'visualizationRadar_';
    return {
        restrict: 'E',
        scope: {
            attributes: '=radarAttributes',
            data: '=radarData',
            series: '=radarSeries',
        },
        template: '<div></div>',
        link: function (scope, elem, attrs) {
        	var id, prev_attribute_length = 0,
        		is_initialized = false;

        	function init() {
        		if (is_initialized || !scope.data)
        			return;

        		is_initialized = true;
        		id = UNIQ_ID_PREFIX + (uniq_id_index++);
    			elem.children().attr('id', id);

    			var attributes = prepareAttributes(),
    				configuration = {
						type: 'radar',
						plot:{
							animation: {
								speed: 0.3,
								method: 'ANIMATION_ELASTIC_EASE_OUT',
							}, // TODO: from attribute.
							dataDescription: attributes.descriptions,
						},
						'scale-k': {
							values: attributes.labels,
						},
						'scale-v': {
							values: '0:5', // TODO: from attribute.
						},
						series: _(prepareData(attributes)).map(function (data, i) {
							var res = {
								text: '<ללא שם>',
							};

							if (scope.series && scope.series[i])
								_(res).extend(scope.series[i]);

							res.values = data;

							delete res.key;
							delete res.stat_src_key;

							return res;
						}),
						tooltip: {
							text: '<u style="font-size: 1.2em">%t ב%kt:</u><br /><br /><b style="font-size: 1.5em">%v</b>',
						}, // TODO: from attribute.
						legend: {}, // TODO: from attribute.
					};

        		zingchart.render({
					id : id,
					height : 450, // TODO: from attribute.
					width : 600, // TODO: from attribute.
					data : configuration,
				});
        	}

        	function prepareAttributes() {
				var attr = scope.attributes || _.keys(scope.data[0]);
				if (typeof attr[0] == 'string') {
					attr = {keys: attr, labels: attr};
				}
				return attr;
        	}

        	function prepareData(attributes) {
				return _(scope.data).map(function (item) {
					return _(item).chain().pick(attributes.keys).values().value();
				});
        	}

        	var is_update_needed = false;

        	function update() {
        		init();
        		if (!is_initialized || is_update_needed)
        			return;

				is_update_needed = true;
    			setTimeout(function () {
    				is_update_needed = false;

	        		var attributes = prepareAttributes();

	        		for (var i = 0; i < prev_attribute_length; i++) {
						zingchart.exec(id, 'removescalevalue', {
							scale : 'scale-k',
							update: false,
						});
					}

					_(attributes.labels).each(function (attr) {
						zingchart.exec(id, 'addscalevalue', {
							scale : 'scale-k',
							value : attr,
							update: false,
						});
					});

					// Update the descriptions of the attributes.
					zingchart.exec(id, 'modifyplot', {
						data : {
							dataDescription: attributes.descriptions,
						},
						update: false,
					});

					zingchart.exec(id, 'setseriesvalues', {
						values : prepareData(attributes),
					});

					prev_attribute_length = attributes.labels.length;
				}, 0);
			}

            scope.$watch('attributes', update, true);
            scope.$watch('data', update, true);

            init();
        }
    };
});

// Proxy the events of zingchart to angular events.
_([
// Animation Events
'animation_end',
'animation_start',
'animation_step',
// Data Manipulation Events
'modify',
'node_add',
'node_remove',
'plot_add',
'plot_modify',
'plot_remove',
'reload',
'setdata',
// Export Events
'data_export',
'image_save',
'print',
// Feed Events
'feed_clear',
'feed_interval_modify',
'feed_start',
'feed_stop',
// Global Events
'click',
'complete',
// 'dataparse', // FIXME: proxy this event couse an exception.
'dataready',
'guide_mousemove',
'load',
'menu_item_click',
'resize',
// Graph Events
'gcomplete',
'gload',
// History Events
'history_back',
'history_forward',
// Interactive Events
'node_deselect',
'node_select',
'plot_deselect',
'plot_select',
// Legend Events
'legend_item_click',
'legend_marker_click',
// Node Events
'node_click',
'node_doubleclick',
'node_mouseout',
'node_mouseover',
// Object Events
'label_click',
'label_mouseout',
'label_mouseover',
'legend_marker_click',
'shape_click',
'shape_mouseout',
'shape_mouseover',
// Plot Events
'plot_click',
'plot_doubleclick',
'plot_mouseout',
'plot_mouseover',
// Toggle Events
'about_hide',
'about_show',
'bugreport_hide',
'bugreport_show',
'dimension_change',
'legend_hide',
'legend_maximize',
'legend_minimize',
'legend_show',
'lens_hide',
'lens_show',
'plot_hide',
'plot_show',
'source_hide',
'source_show',
// Zoom Events
'zoom']).each(function (event_name) {
	zingchart[event_name] = function(e) {
		var $scope = angular.element('#' + e.id).scope();
		$scope.$apply(function () {
			$scope.$emit('zingchart_' + event_name, e);
		});
	};
});
