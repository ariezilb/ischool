app.controller('PointsManagingCtrl', ['$scope', '$location', '$http', 'Restangular', 'Points', function($scope, $location, $http, Restangular, Points) {
    $http.get("/api/2.0/people").then(function(result) {
        $scope.people = _.object(_.pluck(result.data, 'id'), result.data);
        console.log($scope.people);
    });
    Restangular.all('rooms').getList().then(function(rooms) {
        $scope.rooms = _.object(_.pluck(rooms, 'id'), rooms);

        // Try to load room from url/storage
        var roomId = parseInt($location.search()['room']);
        if (!roomId) {
            roomId = parseInt(localStorage.getItem("room"));
        }
        var room = _($scope.rooms).findWhere({id: roomId});
        if (room != null) {
            $scope.saveRoom(room);
        }
    });

    $scope.saveRoom = function (room) {
        var roomId = 0;
        if (room != null) {
            roomId = room.id;
        }
        localStorage.setItem("room", roomId);
        $location.search('room', roomId);
        $scope.roomFilter = room;
    };

    $scope.staffStatusFilter = function(point) {
        if ($scope.staffStatus == 'pending') {
            return point.staff_status == 'open' || point.staff_status == 'handling';
        } else {
            return point.staff_status == $scope.staffStatus;
        }
    };

    $scope.filterByRoom = function (point) {
        if ($scope.roomFilter) {
            return point.room_id == $scope.roomFilter.id;
        }
        return true;
    };

    $scope.filterByStudent = function (point) {
        if ($scope.studentFilter) {
            return point.person_id == $scope.studentFilter.id;
        }
        return true;
    };

    $scope.init = function(courseId, staffStatus) {
        $scope.courseId = courseId;
        $scope.staffStatus = staffStatus;

        Points.newestPoints(0, $scope.staffStatus);
        var handle = Points.$scope.$watch('points', function(points) {
            $scope.points = points;
        });
        $scope.$on('$destroy', handle);

        window.setInterval(function() {
            Points.newerPoints();
            $scope.$apply();
        }, 3000);
    };

    $scope.nextNewestPoints = function() {
        if ($scope.roomFilter) {
            var newRoomFilter = angular.copy(_($scope.rooms).findWhere({id: parseInt($scope.roomFilter.id)}));

            // It seems the room id in $scope.rooms is sometimes a string and sometimes
            // an integer. This hacky fix makes the code run well.
            if (newRoomFilter == undefined) {
                newRoomFilter = angular.copy(_($scope.rooms).findWhere({id: $scope.roomFilter.id.toString()}));
            }
            $scope.roomFilter = newRoomFilter;

            $scope.saveRoom($scope.roomFilter);
        }
        if ($scope.roomFilter && $scope.roomFilter.id) {
            Points.nextNewestPoints($scope.roomFilter.id);
        } else {
            Points.nextNewestPoints();
        }
    };

    $scope.tinymceReplyOptions = {
        language: 'he_IL',
        directionality : 'rtl',
        menubar: true,
        statusbar: true,
        plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify  | ltr rtl",
        toolbar2: "bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons",
    };

    $scope.lockAndAnswer = function(point) {
        point.staff_status = 'handling';
        point.put().then(function(result) {
            $scope.selectedPoint = point;
            $scope.creatingStaffPoint = false;
        }, function(error) {});
    };

    $scope.approveBathroom = function(point) {
        $http({
            method: 'POST',
            url: '/points/' + point.id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param({message: 'באפשרותך לצאת לשירותים ולחזור'})
        }).then(function(response) {
                point.staff_status = 'approved';
            }, function() {
            $scope.selectedPoint = point;
            alert('התגובה נכשלה');
        });
    };

    $scope.viewPoint = function(point) {
        $scope.selectedPoint = point;
        $scope.creatingStaffPoint = false;
    };

    $scope.hebrew_staff_status = function(point) {
        if (point.staff_status == 'open') {
            return 'פתוחה';
        } else if (point.staff_status == 'handling') {
            return 'בטיפול';
        } else if (point.staff_status == 'closed') {
            return 'סגורה';
        } else if (point.staff_status == 'saved') {
            return 'שמורה';
        }
        return '';
    };

    $scope.savePoint = function(point) {
        point.staff_status = 'saved';
        point.put();
    };

    $scope.reopenPoint = function(point) {
        point.staff_status = 'open';
        point.put();
    };

    $scope.trashPoint = function(point) {
        if (confirm('האם למחוק? ההודעה תימחק לאלתר ולא תופיע במסך החניך.')) {
            point.remove();
            var index = _.indexOf($scope.points, point);
            if (index >= 0) {
                $scope.points.splice(index, 1);
            }
        }
    };

    $scope.closePoint = function(point) {
        if (confirm('האם לסגור? ההודעה תמשיך להופיע במסך החניך.')) {
           point.staff_status = 'closed';
           point.put();
        }
    };

    $scope.unlockPoint = function(point) {
        if (confirm('באמת לשחרר מנעילה?')) {
            point.staff_status = 'open';
            point.put();
        }
    };

    $scope.staffPoint = function() {
        $scope.selectedPoint = undefined;
        $scope.creatingStaffPoint = true;
    };


    $scope.searchStudent = function(query) {
        if (query.length < 2) {
            return [];
        }
        return $http.get("/api/search/" + encodeURIComponent(query), {
            params: {people: true}
        }).then(function (result) {
          var results = [];
           _.forEach(result.data.people, function(item){
               item.__type__ = 'person';
               item.__url__ = '/people/' + item.id;
               results.push(item);
          });
          return results;
        });
    }
    $scope.selectFilteredStudent = function($item) {
        $scope.studentFilter = $item;
        if ($item) {
            $scope.addedStudent = $item.name;
        } else {
            $scope.addedStudent = null;
        }
    }

}]);
