app.factory('Points', ['$rootScope', 'Restangular', function($rootScope, Restangular) {
    var $scope = $rootScope.$new();
    var fetched = false;
    var oldestMessageId;
    var relevantStaffStatus = "";


    function uniqAndSortPoints(newPoints, oldPoints) {
        mergedPoints = [];
        if (newPoints) {
            mergedPoints = newPoints;
        }
        if (oldPoints) {
            mergedPoints = mergedPoints.concat(oldPoints);
        }
        return _(_(mergedPoints).uniq(function(point) {return point.id})).sortBy(function(point) {
            return -_(point.messages).max(function(message) {return message.id}).id;
        });
    }

    function newestPoints(roomId, staffStatus) {
        relevantStaffStatus = staffStatus;
        $.get('/latest_points/' + relevantStaffStatus + '/10').then(function (result) {
            points_ids = JSON.parse(result);
            console.log(typeof points_ids);
            if (points_ids.length > 0) {
                Restangular.all('points').getList({q: {filters:[{name:'id', op:'in', val:points_ids}]}}).then(function(points) {
                    $scope.points = uniqAndSortPoints(points);
                    oldestMessageId = _($scope.points[$scope.points.length - 1].messages).min(function(message) {return message.id});
                })
            }
        });
    }

    return {
        $scope: $scope,
        fetch: function() {
            if (fetched) return;

            fetched = true;
            Restangular.all('points').getList().then(function(points) {
                $scope.points = points;
            });
        },

        refetch: function() {
            fetched = true;
            Restangular.all('points').getList().then(function(points) {
                $scope.points = points;
            });
        },

        newestPoints: newestPoints,

        nextNewestPoints: function(roomId) {
            if ($scope.points) {
                var request_data = {};
                if (roomId) {
                    request_data['room_id'] = roomId;
                    var roomPoints = _($scope.points).filter(function(point) {return point.room_id == roomId});
                    if (roomPoints.length > 0) {
                        request_data['oldest_message_id'] = _(roomPoints[roomPoints.length - 1].messages).min(function(message) {return message.id}).id;
                    }
                } else {
                    oldestMessageId = _($scope.points[$scope.points.length - 1].messages).min(function(message) {return message.id}).id;
                    request_data['oldest_message_id'] = oldestMessageId;
                }

                $.get('/latest_points/' + relevantStaffStatus + '/10', request_data).then(function(result) {
                    var points_ids = JSON.parse(result);
                    if (points_ids.length > 0) {
                        Restangular.all('points').getList({q: {filters:[{name:'id', op:'in', val:points_ids}]}}).then(function(points) {
                            $scope.points = uniqAndSortPoints(points, $scope.points);
                            if (!roomId) {
                                var oldestPointId = points_ids[points_ids.length - 1];
                                var oldestPoint = _($scope.points).filter(function(point) {return point.id == oldestPointId})[0];
                                oldestMessageId = _(oldestPoint.messages).min(function(message) {return message.id}).id;
                            }
                        })
                    }
                })
            }
        },

        newerPoints: function() {
            if ($scope.points) {
                var newestMessageId = _($scope.points[0].messages).max(function(message) {return message.id}).id;
                $.get('/newer_points/' + relevantStaffStatus + '/' + newestMessageId).then(function(result) {
                    var points_ids = JSON.parse(result);
                    if (points_ids.length > 0) {
                        Restangular.all('points').getList({q: {filters:[{name:'id', op:'in', val:points_ids}]}}).then(function(points) {
                            $scope.points = uniqAndSortPoints(points, $scope.points);
                        })
                    }
                });
            } else {
                newestPoints(0, relevantStaffStatus);
            }
        },

        totalPointsCount: function() {
            return $scope.points && $scope.points.length;
        },

        pendingPoints: function() {
            return _.filter($scope.points, function(point) {
                return point.staff_status == 'open' || point.staff_status == 'handling';
            });
        },

        closedPoints: function() {
            return _.where($scope.points, {staff_status: 'closed'});
        }
    };
}]);

app.service('$studentEventService', function() {
    var handlers = {};

    return {
        on: function(name, handler) {
            if (handlers[name] === undefined)
                handlers[name] = [];

            handlers[name].push(handler);
        },
        emit: function (name, data) {
            _(handlers[name]).each(function (handler) {
                handler.apply(data.valForThis, data.params);
            });
        }
    }
});
