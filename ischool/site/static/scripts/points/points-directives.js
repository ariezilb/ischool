app.directive('pointReply', ['$http', function($http) {
    return {
        scope: {
            selectedPoint: '=',
            people: '='
        },
        templateUrl: '/static/htmls/partials/point-reply.html',
        link: function(scope, element) {
            scope.reply = function() {
                var point = scope.selectedPoint;
                scope.selectedPoint = undefined;
                $http({
                    method: 'POST',
                    url: '/points/' + point.id + '/message',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param({text: scope.replyText})
                }).then(function(result) {
                        point.messages.push(result.data);
                        point.staff_status = 'closed';
                        point.student_status = 'active';
                        point.put().then(function() {},
                        function(error) {
                            scope.selectedPoint = point;
                            alert('התגובה נכשלה');
                        });
                    }, function() {
                        scope.selectedPoint = point;
                                alert('התגובה נכשלה');
                    });
            };

            scope.unlock = function() {
                scope.selectedPoint.staff_status = 'open';
                scope.selectedPoint.put();
                scope.hide();
            };

            scope.hide = function() {
                scope.selectedPoint = undefined;
            };
        }
    };
}]);

app.directive('staffPoint', ['Restangular', '$http', function(Restangular, $http) {
    return {
        scope: {
            people: '=',
            enabled: '=',
            courseId: '=',
            rooms: '='
        },
        templateUrl: '/static/htmls/partials/staff-point.html',
        link: function(scope) {
            scope.peopleList = function() {
                return _.values(scope.people);
            };

            scope.selectedStudents = Array();

            scope.isStudent = function(person) {
                return person.role == 'student';
            };

            scope.notSelected = function(student) {
                var id = student.id
                var filtered = _.filter(scope.selectedStudents, function(student) {
                    return student.id == id
                });
                return _.size(filtered) == 0;
            };

            scope.isNotValid = function() {
                if (scope.sending|| _.size(scope.selectedStudents)  == 0) {
                    return true;
                }
                return false;
            };
            scope.addStudent = function(student) {
                scope.selectedStudents.push(student);
            };

            scope.removeStudent = function(student) {
                scope.selectedStudents = _.without(scope.selectedStudents, student);

            };

            Restangular.all('arrays').getList().then(function (arrays) {
                scope.arrays = arrays;
            });

            scope.updateGroups = function(array) {
                scope.groups = array.groups;
            };

            scope.setStudentsToGroup = function() {
                $http.get("/api/groups/all-students/" + scope.selected.group.id).then(function(result) {
                    scope.selectedStudents = result.data;
                });
            };

            scope.createPoint = function() {
                scope.sending = true;

                var room_id = null;
                if (scope.$parent.roomFilter)
                    room_id = scope.$parent.roomFilter.id;

                $http.post('/api/points/new_staff_point', {
                    type: 'general',
                    message_text: scope.pointText,
                    room_id: room_id,
                    student_ids: _(scope.selectedStudents).pluck('id')
                }).then(function () {
                    scope.sending = false;
                    scope.enabled = false;
                    scope.pointText = '';
                    scope.selectedStudents=[];
                }, function () {
                    alert('שליחת ההודעה נכשלה');
                    scope.sending = false;
                });
            };

            scope.hide = function() {
                scope.enabled = false;
            };

            scope.$watch('enabled', function() {
                scope.point = {};
            });
        }
    };
}]);
