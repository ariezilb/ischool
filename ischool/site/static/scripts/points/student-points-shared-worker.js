var connections = [];

console.log('new worker start:', connections);

self.addEventListener("connect", function (e) {
	var port = e.ports[0];
	console.log('new connection');

	port.addEventListener("message", function (e) {
		var data = JSON.parse(e.data);
		console.log('got message:', data);
		if (data === 'close') {
			connections = _(connections).without(port);
		}
		else if (data.page === 'popup') {
			connections.push(port);
			port.data = data;
			port.page = data.page;
			port.popup_point_id = data.point_id;
		}
		else if (data.page === 'regular') {
			connections.push(port);
			port.data = data;
			port.page = data.page;
		}
		else {
			console.error('unknown message received:', data);
		}
	}, false);

	port.start();

}, false);

function handleResponse(unread_points) {
	console.log('handleResponse:', 'unread_points:', unread_points, 'connections:', connections);
	if (unread_points.length > 0 && connections.length > 0) {

		// Filter out the popup that are already opened.
		// TODO: make an alert inside them.
		var need_popup_points = _(unread_points).filter(function (point_id) {
			return _(connections).where({page: 'popup', popup_point_id: point_id}).length == 0;
		});
		console.log('handleResponse:', 'unread_points:', unread_points, 'connections:', connections, 'need_popup_points:', need_popup_points);

		if (need_popup_points.length > 0)
			connections[0].postMessage(JSON.stringify({points: need_popup_points, cmd: 'open_popups'}));
	}
}

function checkOfUnreadMessages() {
	var request = new XMLHttpRequest();
	request.onreadystatechange = function() {
	    if (request.readyState == 4 && request.status == 200) {
	        handleResponse(JSON.parse(request.responseText));
	    }
	}

	request.open("GET", "/api/unread_points/", true);
	request.send();
}

importScripts('/static/scripts/vendor/underscore.js');

setInterval(checkOfUnreadMessages, 30*1000);

