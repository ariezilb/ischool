var initHash = window.location.hash.replace(/[\\\/#]/g, '');
var isInitHashUsed = false;
function getSelectedFromInitHash() {
    try {
        var both_regex = /(\d+)_(\d+)/;
        var splited = both_regex.exec(initHash);
        if (splited) {
            return {array: parseInt(splited[1]), assignment: parseInt(splited[2])};
        }
        return {array: parseInt(initHash), assignment: -1};
    } catch(err) {

    }
    return {array: -1, assignment: -1};
}

function uniqAndSortPoints(newPoints, oldPoints) {
    mergedPoints = [];
    if (newPoints) {
        mergedPoints = newPoints;
    }
    if (oldPoints) {
        mergedPoints = mergedPoints.concat(oldPoints);
    }
    return _(_(mergedPoints).uniq(function(point) {return point.id})).sortBy(function(point) {
        return -_(point.messages).max(function(message) {return message.id}).id;
    });
}

function state_to_hebrew(state) {
    if (state == 'in_progress')
        return 'טרם הוגש';
    if (state == 'submitted')
        return 'נשלח';
    if (state == 'resend')
        return 'נסה שוב';
    if (state == 'done')
        return 'הושלם';
}

function startSharedWorker(connections_msg) {
    /*var shared_worker = new SharedWorker("/static/scripts/points/student-points-shared-worker.js");

    shared_worker.port.addEventListener("message", function(e) {
        var data = JSON.parse(e.data);
        if (data.cmd === 'open_popups') {
            _(data.points).each(function (point_id) {
                window.open('/my_points/' + point_id);
            });
        }
    }, false);

    shared_worker.port.start();

    // post a message to the shared web worker
    shared_worker.port.postMessage(JSON.stringify(connections_msg));

    $(window).on('beforeunload', function() {
        shared_worker.port.postMessage(JSON.stringify("close"));
    });*/
}

function hideSidebar() {
    $('i#sidebar-toggler').click();
    $('i#sidebar-toggler').hide();
}

app.controller('StudentPointsGlobalCtrl', ['$scope', '$http', 'Restangular', '$modal', '$studentEventService', '$location', function($scope, $http, Restangular, $modal, $studentEventService, $location) {
    startSharedWorker({page: 'regular'});

    $studentEventService.on('selectedAssignmentChange', function (new_id) {
        $scope.selectedAssignmentId = new_id;
    })

    $http.get('/api/assignments/arrays').then(function (result) {
        $scope.my_arrays = result.data;
        $scope.selectedArrayId = getSelectedFromInitHash().array;
        $scope.selectArrayChange($scope.selectedArrayId);
    });

    $scope.selectArrayChange = function (selectedArrayId) {
        $location.hash(selectedArrayId);
        $studentEventService.emit('selectedArrayChange', {params: [selectedArrayId]});
    };

    Restangular.all('rooms').getList().then(function(rooms) {
        $scope.rooms = rooms;

        // TODO: implment using underscore.
        var selectedRoom = undefined,
        preselected = sessionStorage.getItem("room");
        for (var i = 0; preselected !== undefined && i < rooms.length; i++) {
            if (preselected == rooms[i].id) {
                selectedRoom = i;
                break;
            }
        }

        $scope.newPointRoom = rooms[selectedRoom] || undefined;
        $scope.chooseRoom($scope.newPointRoom);
    });

    $scope.chooseRoom = function(room) {
        if (!room) {
            return;
        }
        sessionStorage.setItem("room", room.id);

        $studentEventService.emit('selectedRoomChange', {params: [room.id]});
    }

    function CreatePointModalCtrl($scope, parentScope, $modalInstance) {
      parentScope.activeModalInstance = $modalInstance;

     $scope.tinymceOptions = {
        language: 'he_IL',
        directionality : 'rtl',
        menubar: false,
        statusbar: false,
        plugins: "directionality",
        toolbar: "bold italic underline | alignleft alignright | undo redo | bullist numlist | ltr rtl"
    };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.newPoint = parentScope.newPoint;
      $scope.assignments = parentScope.assignments;
      $scope.createPoint = parentScope.createPoint;
    }

    function newPointWithType(type) {
        $scope.selectedPoint = undefined;
        if ($scope.newPoint) {
            $scope.newPoint = undefined;
        } else {
            $scope.newPoint = {
            type: type,
            title: 'הודעה',
            messages: [{text: null}]
            };
            var modalInstance = $modal.open({
              templateUrl: 'newPointForm.html',
              controller: CreatePointModalCtrl,
              resolve: {
                parentScope: function () {
                  return $scope;
                },
                assignmentId: function () {
                    return $scope.selectedAssignmentId;
                },
              }
            });

            modalInstance.result.then(function(form) {

            }, function () {
                $scope.newPoint = undefined;
            });
        }
    }

    $scope.newTechnicalPoint = function() {
        newPointWithType('general');
    };

    $scope.newExercisePoint = function() {
        if ($scope.selectedAssignmentId && $scope.selectedArrayId &&
            $scope.selectedAssignmentId >= 0 && $scope.selectedArrayId >= 0) {
            newPointWithType('exercise');
        }
        else {
            alert('בחר בבקשה מקצוע ותרגיל טרם שליחת ההודעה');
        }
    };

    $scope.createPoint = function() {
        $scope.creatingPoint = true;
        if ($scope.newPoint.type == 'exercise') {
            $scope.newPoint.assignment_id = $scope.selectedAssignmentId;
        }
        $scope.newPoint.room_id = $scope.newPointRoom.id;
        $http.post('/points', $scope.newPoint).then(function() {
            if ($scope.activeModalInstance) {
                $scope.activeModalInstance.close();
                $scope.activeModalInstance = undefined;
            }
            $scope.newPoint = undefined;
            $scope.childScope = undefined;
            $scope.creatingPoint = false;
        }, function() {
            alert('שגיאה: ההודעה לא נשלחה');
            $scope.creatingPoint = false;
        });

    };
}]);

/* TODO: Clean the unneccesary parts */
app.controller('StudentPointsCtrl', ['$scope', '$http', 'Restangular', function($scope, $http, Restangular) {
    hideSidebar();
    $scope.selected_array = -1;
    $http.get('/api/assignments/arrays').then(function (result) {
        $scope.my_arrays = result.data;
    });

    $scope.selectArrayChange = function (arrayId) {
        $scope.selected_array = arrayId;
        $scope.pageId = 1;
        $scope.points = [];
        getPaginatedPoints($scope.pageId);
    }

    function getPaginatedPoints(pageId) {
        $http.get('/api/points/paginated/' + $scope.selected_array + '/' + pageId)
            .then(function (result) {
            $scope.showingAll = result.data.length == 0;
            $scope.points.push.apply($scope.points, result.data);
        });
    }

    $scope.pageId = 1;
    $scope.points = [];
    getPaginatedPoints($scope.pageId);

    $scope.showMore = function() {
        $scope.pageId += 1;
        getPaginatedPoints($scope.pageId);
    };

    $scope.tinymceReplyOptions = {
        language: 'he_IL',
        directionality : 'rtl',
        menubar: false,
        statusbar: false,
        plugins: "directionality",
        toolbar: "bold italic underline | alignleft alignright | undo redo | bullist numlist | ltr rtl"
    };


    /*function latestSegelMessage(point) {
        var latest = _.chain(point.messages)
            .reject(function(message) { return message.person_id == $scope.personId; })
            .pluck('created_at')
            .map(function(created_date) { return moment(created_date); })
            .max()
            .value();
        if (latest === -Infinity) return null;
        return latest;
    }*/


    $scope.selectPoint = function(point) {
        //askForNotificationPermission();
        $http.post('/points/mark_last_message_read', {
            point_id: point.id
        }).then(function(response) {
            point.read_at = new Date(response.data.read_at);
            //updateUnseenAnsweredPointsCount($scope);
        });
        $scope.selectedPoint = point;
        $scope.newPoint = undefined;
    };

    $scope.$watch('selectedPoint.id', function() {
        $scope.replyText = undefined;
    });

    $scope.sendReply = function() {
        $http({
            method: 'POST',
            url: '/points/' + $scope.selectedPoint.id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param({message: $scope.replyText,
                            room: $scope.newPointRoom.id})
        }).then(function(response) {
            $scope.replyText = undefined;
            $scope.addAlert("ההודעה נשלחה בהצלחה", 'success');
        }, function() {
            //notify('שגיאה: התגובה נכשלה');
            $scope.replyText = undefined;
        });
    };

    $scope.lastMessageDate = function(point) {
        return _.max(_.map(_.pluck(point.messages, 'created_at'), function(created_date) {return moment(created_date); }))
    };

    $scope.closePoint = function(point, e) {
        e.stopPropagation();

        if (confirm('להסיר הודעה?')) {
            point.removing = true;
            $http.delete('/points/' + point.id).then(function() {

            }, function() {
                //notify('שגיאה: ההסרה נכשלה');
                point.removing = false;
            });
        }
    };

    $scope.appliedClass = function(point) {
        if ($scope.selectedPoint == point) {
            return 'selected';
        }
        var lastMessage = point.messages[point.messages.length - 1];
        if (lastMessage.read_at == null && lastMessage.person_id != $scope.personId) {
            return 'unread';
        }
    };

    /*$scope.maxPointsToDisplay = 10;
    $scope.pointsFilter = {student_status: 'active', type: '!bathroom'};*/
    $scope.pointsFilter = {};

    $scope.$watch('points', function() {
        if (!$scope.points) return;
        $scope.hasHiddenPoints = $scope.points.length > $scope.maxPointsToDisplay
            || _.findWhere($scope.points, {student_status: 'hidden'})
    });

    /*$scope.displayAll = function() {
        $scope.pointsFilter = {type: '!bathroom'};
        $scope.maxPointsToDisplay = 1000000;
        $scope.showingAll = true;
    };*/

    //updatePoints();
    //setInterval(updatePoints, 20*1000);
}]);



app.controller('StudentPointPopupCtrl', ['$scope', '$http', function($scope, $http) {
    startSharedWorker({page: 'popup', point_id: $scope.pointId});

    $scope.remindIn = 300;

    $http.get('/api/points/' + $scope.pointId).then(function (result) {
        $scope.point = result.data;
    });

    $scope.markAsReadAndClose = function () {
        $http.post('/api/points/mark_point_read', {point_id: $scope.pointId}).then(function () {
            window.close();
        });
    }

    $scope.remindMeLater = function () {
        $http.post('/api/points/set_reminder', {point_id: $scope.pointId, remind_in: $scope.remindIn}).then(function () {
            window.close();
        });
    }

    $scope.tinymceReplyOptions = {
        language: 'he_IL',
        directionality : 'rtl',
        menubar: false,
        statusbar: false,
        plugins: "directionality",
        toolbar: "bold italic underline | alignleft alignright | undo redo | bullist numlist | ltr rtl"
    };

    $scope.sendReply = function() {
        $http({
            method: 'POST',
            url: '/points/' + $scope.point.id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param({message: $scope.replyText,
                            room: $scope.newPointRoom.id })
        }).then(function(response) {
            window.close();
        }, function() {
            notify('שגיאה: התגובה נכשלה');
            $scope.replyText = undefined;
        });
    };

    $scope.closePoint = function(point, e) {
        e.stopPropagation();

        if (confirm('להסיר הודעה?')) {
            point.removing = true;
            $http.delete('/points/' + point.id).then(function() {

            }, function() {
                notify('שגיאה: ההסרה נכשלה');
                point.removing = false;
            });
        }
    };

    $scope.appliedClass = function(point) {
        if ($scope.selectedPoint == point) {
            return 'selected';
        }
        var lastMessage = point.messages[point.messages.length - 1];
        if (lastMessage.read_at == null && lastMessage.person_id != $scope.personId) {
            return 'unread';
        }
    };

    // Make the title blink until the mose is moved on the window.
    var original_title = document.title;
    var blinking_title = '!';
    var intervalId = setInterval(function () {
        document.title = (document.title == blinking_title) ? original_title : blinking_title;
    }, 500);

    $(window).on('mousemove', function () {
        clearInterval(intervalId);
        document.title = original_title;
    });
}]);

app.controller('StudentPointsNotifierCtrl', ['$scope', function($scope) {
    //hideSidebar();

    setInterval(function () {
        $.ajax(
        {
            url: '/api/unread_points/',
            dataType: "json",
            contentType: "application/json",
            success: function(points_ids)
            {
                _(points_ids).each(function (point_id) {
                    window.open('/my_points/' + point_id);
                });
            }
        });
    }, 30 * 1000);

    $(window).on('beforeunload', function() {
        return 'אם תצא מדף זה, לא תקבל התרעה על הודעות חדשות';
    });
}]);

app.controller('StudentAssignmentsSidebarCtrl', ['$scope', '$http','$studentEventService', '$location', function($scope, $http, $studentEventService, $location) {
    $studentEventService.on('selectedArrayChange', function (selectedArrayId) {
        $scope.selectedArrayId = selectedArrayId;

        // reset selected assignment.
        $scope.selectedAssignmentId = -1;
        $scope.setSelectedAssignmentId(-1);

        if (selectedArrayId) {
            $http.get('/api/arrays/' + selectedArrayId + '/assignments').then(function(result) {
                $scope.my_assignments = result.data;
                if (!isInitHashUsed) {
                    isInitHashUsed = true;
                    $scope.selectedAssignmentId = getSelectedFromInitHash().assignment;
                    if ($scope.selectedAssignmentId)
                        $scope.setSelectedAssignmentId($scope.selectedAssignmentId);
                }
            });
        }
        else {
            $scope.my_assignments = undefined;
        }
    });

    $scope.assignmentClick = function (assignment_id) {
        $scope.setSelectedAssignmentId(assignment_id);
    };

    $scope.setSelectedAssignmentId = function (new_id) {
        $location.hash($scope.selectedArrayId + '_' + new_id);
        $scope.selectedAssignmentId = new_id;
        $studentEventService.emit('selectedAssignmentChange', {params: [new_id]});
    };

    $scope.state_to_hebrew = state_to_hebrew;
}]);

app.controller('StudentAssignmentCtrl', ['$scope', '$http','$studentEventService', function($scope, $http, $studentEventService) {
    $scope.state_to_hebrew = state_to_hebrew;

    $scope.markAsRead = function() {
        $http.put("/api/assignment/" + $scope.assignment.id + "/read", {}).then(function(result) {
            // TODO Refresh the assignments list so the assignment won't be bold
            $http.get('/api/assignment/' + $scope.assignment.id).then(function (result) {
                $scope.assignment = result.data;
            });
        });
    }

    $studentEventService.on('selectedAssignmentChange', function (new_id) {
        if (new_id && new_id >= 0) {
            $http.get('/api/assignment/' + new_id).then(function (result) {
                $scope.assignment = result.data;
            });
        }
        else {
            $scope.assignment = null;
        }
    });

    $studentEventService.on('selectedRoomChange', function (room_id){
        $scope.selectedRoomId = room_id;
    });

    $scope.is_pdf = function (url) {
        return /\.pdf$|\.pdf\?/i.test(url);
    };
}]);
