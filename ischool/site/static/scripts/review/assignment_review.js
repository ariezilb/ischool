
$('.js-start-checking a').click(function(ev) {
    toggleAssignmentChecking(ev, true);
});

$('.js-stop-checking a').click(function(ev) {
    toggleAssignmentChecking(ev, false);
});

$('.js-resend-no-review a').click(function(ev) {
    ev.preventDefault();
    var $parent = $(ev.target).parents(".assignment-review");
    var assignmentId = $parent.data("assignment-id");
    $.post("/assignments/" + assignmentId + "/resend-no-review");
    location.reload();
});

function toggleAssignmentChecking(ev, shouldStart) {
    ev.preventDefault();
    var $parent = $(ev.target).parents(".assignment-review");
    var assignmentId = $parent.data("assignment-id");
    var func = shouldStart ? API.assignment.startChecking : API.assignment.stopChecking;
    func(assignmentId).thenCall(function() {
        $parent.find(".js-locked-by").addClass("hide");
        if (shouldStart) {
            $parent.find(".js-start-checking").addClass("hide");
            $parent.find(".js-stop-checking").removeClass("hide");
            $parent.find(".js-send-check").removeClass("hide");
            $parent.find(".js-resend-no-review").removeClass("hide");
        } else {
            $parent.find(".js-stop-checking").addClass("hide");
            $parent.find(".js-start-checking").removeClass("hide");
            $parent.find(".js-send-check").addClass("hide");
            $parent.find(".js-resend-no-review").addClass("hide");
        }
    });
}


var SubmitReviewCtrl = function ($scope, $http) {

    $scope.initScores = function(exerciseId) {
        $http.get("/api/criteria/exercise/" + exerciseId).then(function (result) {
            var criteria = result.data;
            $scope.scores = {};
            $scope.values = {};
            $scope.overStar = {};
            var initCriterion = function(criterion) {
                $scope.scores[criterion.id] = [3, ""];
                $scope.values[criterion.id] = 3;
                $scope.overStar[criterion.id] = null;
            };
            _.each(criteria.taken_from_array, initCriterion);
            _.each(criteria.specific_to_exercise, initCriterion);
        });
    }

    $scope.init = function(assignmentId, exerciseId) {
        $scope.assignmentId = assignmentId;
        $scope.exerciseId = exerciseId;
        $scope.commentForStaff = "";
        $scope.commentForStudent = "";
        $scope.initScores(exerciseId);
        $scope.maxScore = 5;
        $scope.ratingStates = [{stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty'}];

        $scope.hoveringOver = function(value, criterion_id) {
            $scope.overStar[criterion_id] = value;
            $scope.values[criterion_id] = value;
        };
    };

    $scope.submitReview = function(state) {
        $http.post('/assignments/' + $scope.assignmentId + '/submit-review', {
            'state': state,
            'comment_for_staff': this.commentForStaff,
            'comment_for_student': this.commentForStudent,
            'scores': $scope.scores
        }).then(function(result) {
            var file = $("#form input")[0].value;
            if (file) {
                $("#form").submit();
            } else {
                location.reload();
            }
        });
    };
};
