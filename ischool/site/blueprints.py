from flask import Blueprint
from ischool.config import IschoolConfig as config


ischool = Blueprint("ischool", __name__,
        template_folder=config.TEMPLATE_FOLDER,
        static_folder=config.STATIC_FOLDER)
