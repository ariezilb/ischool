# coding=utf-8

"""
Defines convenience shortcuts
"""

from ischool.site.extensions import db


def PkColumn(type=None, primary_key=True, auto_increment=True,
             default=None):
    """
    A shortcut to defining an SQLAlchemy class's primary key column
    :type type: sqlalchemy.types.AbstractType
    :type primary_key: bool
    :type auto_increment: bool
    :rtype : sqlalchemy.schema.Column
    """
    if type is None:
        type = db.Integer
    return db.Column(type, nullable=False, primary_key=primary_key, autoincrement=auto_increment,
                     default=default)


class Model(object):
    """
    Abstract class that forces models to be of InnoDB type (for allowing foreign keys)
    """
    __table_args__ = {'mysql_engine':'InnoDB'}
