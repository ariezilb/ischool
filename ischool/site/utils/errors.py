# coding=utf-8
"""
Define error handling & XHR exceptions
"""

from traceback import format_exc
from flask import jsonify, request
from ischool.site.blueprints import ischool


class XmlHttpRequestError(Exception):
    def __init__(self, message_or_subject, message=None, error_code=0):
        if message is not None:
            subject, message = message_or_subject, message
        else:
            subject, message = u"שגיאה", message_or_subject

        self.subject = subject
        self.message = message
        self.error_code = error_code

XHRError = XmlHttpRequestError


@ischool.errorhandler(XmlHttpRequestError)
def handle_xhr_exceptions(error):
    print format_exc()
    return jsonify({'error': error.error_code, 'msg': error.message, 'subject': error.subject}), 400


@ischool.errorhandler(Exception)
def handle_xhr_arbitrary_errors(error):
    if request.is_xhr:
        # This is probably sent using ischool API, answer with JSON
        print format_exc()
        return jsonify({'error': 500, 'message': error.message, 'subject': u"נזרקה שגיאה מסוג %s"
                                                                       % error.__class__.__name__}), 400
    raise


def custom_error(message, error=400):
    """
    Custom error interface, for friendly & custom error handling (see login.html, user-controllers.js)
    :param message: What happened
    :param error: Just some error code
    :return: Jsonified version of message, along with custom marker
    """
    return jsonify({'error': error, 'message': message, 'custom': True})
