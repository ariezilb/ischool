# Various os/path utils
import os


def get_filename_tuple(file_name):
    """
    Splits the file name into a name and extension (no leading dot).
    @note The extension may be empty.

    @param file_name: str The file name to split
    @return: A tuple which consists of the file name and the extension
    """
    (file_no_ext, ext) = os.path.splitext(file_name)
    if ext:
        # Remove the leading dot
        ext = ext[1:]
    return file_no_ext, ext
