# coding=utf-8
"""
flask_restless' poor implementation wrappers
Mainly for create_api wrapper
"""
from ischool.site.extensions import api_manager
from .auth import validate_permissions


__all__ = ['create_api']


def find_inner_segments(objs, parts):
    """
    Inner function that finds the objects and the last part of an object
    >>> find_inner_segments(array, ['files', 'file_url'])
    >>> (array.files, 'file_url')
    >>> find_inner_segments(array, ['files', 'inner_part', 'inner_attr'])
    >>> (array.files.inner_part, 'inner_attr')
    :param objs: object or list of objects to break into parts
    :param parts: the searched parts
    :return: tuple(parent, 'attr_as_string')
    """
    if objs is None:  # equivalent to a nullified-value in a table
        return []

    part = parts.pop(0)
    if part not in objs:
        return []
    if len(parts) == 0:
        return [(objs, part)]

    results = []
    if not isinstance(objs, list):
        objs = [objs]
    for obj in objs:
        results.extend(find_inner_segments(obj[part], parts))
    return results


PREPROCESSORS_TYPES = ['PATCH_SINGLE', 'PATCH_MANY', 'GET_SINGLE', 'GET_MANY', 'POST', 'DELETE']


def create_api(model, *permissions, **kwargs):
    """
    Wrapper for manager.create_api(...)
    Example:
    >>> create_api(
    >>>       # DB Model
    >>>       Array,
    >>>       # Permissions list
    >>>       Permissions.all_staffs,
    >>>       # flask_restless parameters
    >>>       include_methods=['manager.associated_person',
    >>>                        'files.file_type', 'files.file_name', 'files.file_url'],
    >>>       preprocessors={
    >>>           'POST': [create_array_preprocessor],
    >>>           'DELETE': [remove_array_preprocessor],
    >>>       },
    >>>       methods=['GET', 'POST', 'PUT', 'DELETE'])
    :param model: SQLAlchemy Model to support
    :param permissions: list of .auth.Permissions that are authorized to access this api
    :param validator: Extra function that tries to validate
    :return:
    """
    results_per_page = kwargs.pop("results_per_page", None)
    include_methods = kwargs.pop("include_methods", [])
    preprocessors = kwargs.pop("preprocessors", {})

    # Extra functions to validate
    validator = kwargs.pop("validator", None)

    def clear_methods_data_preprocessor(instance_id=None, data=None, **kw):
        for method in include_methods:
            for obj, attr in find_inner_segments(data, method.split(".")):
                obj.pop(attr)

    def validate_permissions_internal(*args, **kwargs):
        return validate_permissions(permissions, validator, *args, **kwargs)

    for pp_type in PREPROCESSORS_TYPES:
        preprocessors[pp_type] = preprocessors.get(pp_type, [])
        preprocessors[pp_type].insert(0, validate_permissions_internal)

    preprocessors['PATCH_SINGLE'].append(clear_methods_data_preprocessor)

    api_manager.create_api(model,
                       results_per_page=results_per_page,
                       include_methods=include_methods,
                       preprocessors=preprocessors,
                       **kwargs)
