# coding=utf-8
"""
Application URL Converters
Converts URL paths to real objects:
>>> @ischool_route("/groups/<group_instance:group>"):
>>> def some_endpoint(group_instance):
>>>     group_instance = group_instance()
>>>     # Logic ...
Allows to instantiate objects (DB objects, etc.) using flask URL definitions
"""

from flask.globals import g
from werkzeug.routing import BaseConverter, ValidationError
from ischool.site.extensions import db
from ischool.models import (Array, EducationUnit, Group, AbstractPlacement,
                            Person, LessonInstance, InstanceTask, InstanceExercise,
                            Course)
from ischool.models.points.point import Point


extra_converters = dict()

def register_converter(name):
    """
    Convenience decorator for registering url converters
    :param name: the name that will be used, e.g. /groups/<group:group> - name = group
    :return: Decorated function
    """
    def decorator(f):
        extra_converters[name] = f
    return decorator


@register_converter('uuid')
class UuidConverter(BaseConverter):
    regex = '[A-Fa-f0-9]{32}'


class DbObjectConverter(BaseConverter):
    """
    Abstract class for registering DB Object converters.
    Provides lazy instantiation interface and automatic URL building

    Usage:
    >>> @ischool_route("/<some_instance:instance>", ...):
    >>> def some_endpoint(some_instance):
    >>>     # Initialize instance (this is used to avoid caching)
    >>>     some_instance = some_instance()
    >>>     # Now we have a DB model instance, do whatever we want
    >>>     some_instance.do_something()
    """
    regex = '\d+'
    joins = []
    nullable = False

    def __init__(self, map, join=None, nullable=False):
        BaseConverter.__init__(self, map)
        self.nullable = nullable
        if join:
            self.joins = [db.joinedload(prop) for prop in join.split(',')]

    def to_python(self, obj_id):
        # We return a function because the output of converters is cached (and we don't want
        #  to cache SQLAlchemy objects, as they can't go out of session)
        def func(*args, **kwargs):
            obj = self.fetch(int(obj_id))
            if obj is None and not self.nullable:
                raise ValidationError
            return obj
        return func

    def fetch(self, obj_id):
        return None

    def to_url(self, obj):
        if hasattr(obj, 'id'):
            return BaseConverter.to_url(self, "%d" % obj.id)
        elif isinstance(obj, (int, long)):
            return BaseConverter.to_url(self, obj)
        else:
            raise ValidationError("Given item is not a DB object, nor it's an id")


class CourseObjectConverter(DbObjectConverter):
    """
    Automatically assign g.course = obj.get_course() (works on course-specific objects)
    This is a fallback way to assign g.course, if there is no URL value (/courses/<course:course>/...),
        using course_dependent=True with ischool_route
    """
    def to_python(self, obj_id):
        func = DbObjectConverter.to_python(self, obj_id)

        def new_func():
            obj = func()
            g.course = obj.get_course()
            return obj
        return new_func


# Define many course & db converters:
@register_converter('array')
class ArrayConverter(CourseObjectConverter):
    def fetch(self, arr_id):
        return Array.query.get_or_404(arr_id)


@register_converter('edu_unit')
class EducationUnitConverter(CourseObjectConverter):
    def fetch(self, unit_id):
        return EducationUnit.query.get_or_404(unit_id)


@register_converter('group')
class GroupConverter(CourseObjectConverter):
    def fetch(self, group_id):
        return Group.query.get_or_404(group_id)


@register_converter('placement')
class AbstractPlacementConverter(CourseObjectConverter):
    def fetch(self, placement_id):
        return AbstractPlacement.query.get_or_404(placement_id)


@register_converter('person')
class PersonConverter(DbObjectConverter):
    def fetch(self, person_id):
        return Person.query.get_or_404(person_id)


@register_converter('instance')
class InstanceConverter(CourseObjectConverter):
    def fetch(self, arr_id):
        return LessonInstance.query.get_or_404(arr_id)


@register_converter('task')
class InstanceTaskConverter(CourseObjectConverter):
    def fetch(self, task_id):
        return InstanceTask.query.get_or_404(task_id)


@register_converter('instance_exercise')
class InstanceExerciseConverter(DbObjectConverter):
    def fetch(self, inst_ex_id):
        return InstanceExercise.query.get_or_404(inst_ex_id)


@register_converter('point')
class PointConverter(DbObjectConverter):
    def fetch(self, point_id):
        return Point.query.get_or_404(point_id)


@register_converter('course')
class CourseNameConverter(BaseConverter):
    """
    Translates course name parameter to a specific course, note that this won't change g.course!
    """
    nullable = True

    def __init__(self, map, nullable=True):
        BaseConverter.__init__(self, map)
        self.nullable = nullable

    def to_python(self, course_name):
        query = Course.query.filter(Course.eng_name.ilike(course_name))
        if self.nullable:
            course = query.first()
        else:
            course = query.first_or_404()

        # We return a function because the output of converters is cached (and we don't want
        #  to cache SQLAlchemy objects, as they can't go out of session)
        def func():
            return course
        return func

    def to_url(self, course):
        if hasattr(course, 'eng_name'):
            return BaseConverter.to_url(self, course.eng_name.lower())
        elif isinstance(course, str):
            return BaseConverter.to_url(self, course)
        else:
            raise ValidationError("Given item is not a course object, nor it's name")
