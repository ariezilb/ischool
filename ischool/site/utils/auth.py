# coding=utf-8

from functools import wraps
from flask import g, abort
from ischool.site.blueprints import ischool

__author__ = 'user1'


class Permissions(object):
    """
    Enum of permissions to allow/restrict people from pages
    """
    guest = 'GUEST'
    logged_in = 'LOGGED_IN'
    students = 'STUDENTS'
    current_staff = 'CURRENT_STAFF'
    all_staffs = 'ALL_STAFF'
    course_checkers = 'COURSE_CHECKERS'


def is_person_allowed(person, roles):
    """
    Check whether a person is allowed to use a resource according to the allowed permissions list
    :param person: Person to check (see models.Person)
    :param roles: list of roles to check against (see .PermissionsEnum)
    :return: True if any of the roles verifies person, False otherwise
    """
    if not isinstance(roles, (list, tuple)):
        roles = [roles]

    if Permissions.guest in roles and person is None:
        return True

    if Permissions.logged_in in roles and person is not None:
        return True

    # In that stage, we only check logged-on permissions
    if not person:
        return False

    # The person is not from the current course
    if person.main_course_id != g.course.id:
        return False

    for role in roles:
        if role == Permissions.students and person.role == "student":
            return True
        if role == Permissions.course_checkers and person.role == "checker":
            return True
        if role == Permissions.current_staff and person.is_staff():
            return True
        if role == Permissions.all_staffs and person.is_staff():
            return True

    return False


def validate_permissions(permissions, validator=None, *args, **kwargs):
    """
    Check whether the current person is allowed to use current permission set
    :param permissions: list of allowed roles (see .PermissionsEnum)
    :param validator: An extra function to validate instead of roles (e.g., check if current page belongs
    to current student). It is being ORed with the permissions list
    :param args: Original arguments, sent to validator
    :param kwargs: Original arguments, sent to validator
    """
    if validator is None:
        validator = lambda *a, **b: False

    if not is_person_allowed(g.person, permissions) and not validator(*args, **kwargs):
        if g.connected:
            abort(403)
        else:
            abort(401)


def ischool_route(urls, *permitted_roles, **kwargs):
    """
    Creates an application route (Flask.route) with several improvements
    :param urls: One or some url(s) patterns to absorb
    :param course_dependent: bool that indicates whether /courses/<course>/url... should be supported, defaults to False
    :param permitted_roles: list of roles which are allowed to view this page
    :param validator: validation function that can replace permitted roles (an extra role, sort-of)
    :param kwargs: other parameters that should be sent to Flask.route
    :return: Wrapped function with routes
    """
    if not isinstance(urls, list):
        urls = [urls]

    course_dependant = kwargs.pop('course_dependant', False)
    if course_dependant:
        # Add /courses/<course> rule for each URL
        for url in list(urls):
            urls.append("/courses/<course(nullable=False):course>/%s" % url.lstrip("/"))

    # Extra functions to validate
    validator = kwargs.pop('validator', None)

    def inner(f):
        @wraps(f)
        def view(*args, **kwargs):
            validate_permissions(permitted_roles, validator, *args, **kwargs)
            return f(*args, **kwargs)

        # Chain @app.route-s
        for url in urls:
            view = ischool.route(url, **kwargs)(view)

        return view
    return inner
