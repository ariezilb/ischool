# coding=utf-8
"""
Solve encoding issues (mainly JSON)
"""

from flask import json, request, current_app


class SturdyJsonEncoder(json.JSONEncoder):
    """
    A JSON Encoder that tries to jsonify using obj.jsonify(), otherwise calls default implementation
    Also tries to serialize using isoformat()
    """
    def default(self, obj):
        if hasattr(obj, 'jsonify'):
            return obj.jsonify()
        if hasattr(obj, 'isoformat'):
            return obj.isoformat()
        else:
            return json.JSONEncoder.default(self, obj)


def jsonify_string(*args, **kwargs):
    """
    Serializes args & kwargs to a json string
    :return: JSON-ified string
    """
    indent = kwargs.pop('indent', True)

    # Assume args[0], if sent alone, may not have to be a dict
    obj = None
    if len(args) == 1:
        try:
            obj = args[0]
        except:
            pass
    if obj is None:
        obj = dict(*args, **kwargs)

    return json.dumps(obj, cls=SturdyJsonEncoder,
                      indent=None if request.is_xhr else (2 if indent else None))


def jsonify(*args, **kwargs):
    """
    Taken from Flask::jsonify, slightly improved
    """

    return current_app.response_class(jsonify_string(*args, **kwargs), mimetype='application/json')


def get_filters(*filters_list):
    """
    Creates filters for flask_restless search (?q={"filters": <filters>})
    :param filters: List of filter strings to parse
    :return: JSON-ified string of filters
    """
    filters = []
    for fltr in filters_list:
        chunks = fltr.split(" ")
        name, operator, rest = chunks[0], chunks[1], chunks[2:]
        new_filter = {
            'name': name,
            'op': operator
        }
        if len(rest) > 0:
            new_filter['val'] = eval(" ".join(rest))
        filters.append(new_filter)
    return jsonify_string(filters=filters, indent=False)
