# coding=utf-8
"""
Custom jinja filters
"""
import re
import os
from hashlib import md5
from datetime import datetime, date
from flask import url_for
from jinja2 import evalcontextfilter, environmentfilter
from jinja2.filters import make_attrgetter
from ischool.site.blueprints import ischool
from .encoding import jsonify_string
from urlparse import urljoin
from BeautifulSoup import BeautifulSoup, Comment


MIN_DAYS_IN_WEEK = 5

jinja_env_tests = dict()

@ischool.app_template_filter('jsonify')
def jsonify_obj(obj):
    return jsonify_string(obj)


@ischool.app_template_filter('sort_people')
def sort_people(people):
    l = list(people)
    l.sort(key=lambda p: p.name)
    return l


@ischool.app_template_filter('person_short_name')
@evalcontextfilter
def person_short_name(eval_ctx, person, unique_in_group=None):
    if not unique_in_group:
        return person.first_name

    if unique_in_group:
        same_first_name = filter(lambda p: p is not person and p.first_name == person.first_name, unique_in_group)
        if len(same_first_name) == 0:
            return person.first_name

        last_name_letter = person.last_name[:1]
        same_last_name_letter = filter(lambda p: p.last_name[:1] == last_name_letter, same_first_name)
        if len(same_last_name_letter) == 0:
            return '%s %s' % (person.first_name, last_name_letter)
        else:
            return person.name



@ischool.app_template_filter('short_float')
def short_float(num):
    val = str(num).rstrip('0').rstrip('.')
    if val == '':
        return '0'
    return val


@ischool.app_template_filter('instances_of_group')
def sum_inst_hours(instances, group):
    return filter(lambda inst: group in inst.groups, instances)


@ischool.app_template_filter('sum_inst_hours')
def sum_inst_hours(instances):
    from ischool.models.instances import LessonInstance
    return LessonInstance.sum_instances_hours(instances)


@ischool.app_template_filter('sum_inst_suggested_hours')
def sum_inst_suggested_hours(instances):
    from ischool.models.instances import LessonInstance
    return LessonInstance.sum_suggested_hours(instances)


@ischool.app_template_filter('sum_inst_luz_hours')
def sum_inst_luz_hours(instances):
    return sum(map(lambda inst: inst.luz_hours, instances))


@ischool.app_template_filter('dates_days')
def dates_days(dates):
    return map(lambda d: d.day, dates)


@ischool.app_template_filter('get_month_weeknum')
def get_month_weeknum(date, course):
    month_start = datetime(date.year, date.month, 1)
    return course.get_week_num(month_start)


def icon(name):
    return '/static/images/icons/%s.png' % name


@ischool.app_template_filter('scheduled_icon')
def scheduled_icon(exercise):
    if len(exercise.instance_exercise) > 0:
        return icon('check2')
    else:
        return ''


@ischool.app_template_filter('course_specific_icon')
def course_specific_icon(exercise):
    if exercise.course_specific:
        return icon('pig')
    else:
        return ''


@ischool.app_template_filter('submit_type_icon')
def submit_type_icon(exercise):
    if exercise.submit_type == 'files':
        return icon('page_copy')
    elif exercise.submit_type == 'ok':
        return icon('thumb_up')
    elif exercise.submit_type == 'svn':
        return icon('link')
    else:
        return ''


def has_student_copy(exercise):
    return len(filter(lambda f: f.file_type == 'student_copy', exercise.files)) > 0
jinja_env_tests['has_student_copy'] = has_student_copy

def has_student_attachment(exercise):
    return len(filter(lambda f: f.file_type == 'student_attachment', exercise.files)) > 0
jinja_env_tests['has_student_attachment'] = has_student_attachment


def have_review_file(submission):
    if submission.review is not None and submission.review.file_id is not None:
        return True
    else:
        return False

jinja_env_tests['have_review_file'] = have_review_file
def is_pdf(exercise):
    files = filter(lambda f: f.file_type == 'student_copy', exercise.files)
    if len(files) == 0:
        return ''
    file = files[0]
    if file.file_usage.exercise_file[0].file_usage.file_revision.file.type == 'pdf':
        return True
    else:
        return False
jinja_env_tests['is_pdf'] = is_pdf

@ischool.app_template_filter('missing_student_copy')
def missing_student_copy(exercise):
    if not has_student_copy(exercise):
        return icon('warning')
    else:
        return ''


@ischool.app_template_filter('student_copy')
def student_copy(exercise):
    files = filter(lambda f: f.file_type == 'student_copy', exercise.files)
    if len(files) == 0:
        return ''
    file = files[0]
    file_revision = file.file_usage.exercise_file[0].file_usage.file_revision
    if file_revision.file.type == 'pdf':
        return file_url(file, down=False)
    else:
        return file_url(file)
        """
        from flask import send_from_directory
        return send_from_directory(app.config['STORAGE_PATH'], file_revision.id,
                                   attachment_filename=file_revision.file_full_name(),
                                   as_attachment=True)
        """


@ischool.app_template_filter('student_attachment')
def student_attachment(exercise):
    files = filter(lambda f: f.file_type == 'student_attachment', exercise.files)
    if len(files) == 0:
        return ''
    file = files[0]
    file_revision = file.file_usage.exercise_file[0].file_usage.file_revision
    return file_url(file)
    """
    from flask import send_from_directory
    return send_from_directory(app.config['STORAGE_PATH'], file_revision.id,
                               attachment_filename=file_revision.file_full_name(),
                               as_attachment=True)
    """




def supports_exercises(instance):
    return instance.type == 'self-work' or instance.type == 'tamak' or \
           instance.type == 'self-learn' or instance.type == 'targil' or instance.type == 'project'
jinja_env_tests['supports_exercises'] = supports_exercises


@ischool.app_template_filter('sort_assignments')
def sort_assignments(assignments):
    return sorted(assignments, key=lambda x: x.given_time)
    #return sorted(assignments, key=lambda x: x.state_priority)


@ischool.app_template_filter('assignments_arrays')
def assignments_arrays(assignments):
    return sorted(set([ass.exercise.array for ass in assignments]), key=lambda ass: ass.id)


@ischool.app_template_filter('file_url')
def file_url(file_rev, name=None, down=True):
    hasher = md5()
    from ischool.models import FileUsage, ExerciseFile
    if isinstance(file_rev, FileUsage):
        file_rev = file_rev.file_revision
    if isinstance(file_rev, ExerciseFile):
        file_rev = file_rev.file_usage.file_revision

    if not name:
        name = file_rev.file_full_name()

    (file_no_ext, ext) = os.path.splitext(name)
    if not ext:
        ext = ""
    hasher.update(file_no_ext.encode('utf8'))
    hashed_name = hasher.digest().encode('hex')
    full_hashed_name = hashed_name + ext
    if not down:
        return url_for('ischool.file_get', rev_id=file_rev.id, slug=full_hashed_name, _external=True, down=down)
    else:
        return url_for('ischool.file_get', rev_id=file_rev.id, slug=full_hashed_name, _external=True)


@ischool.app_template_filter('sort_items')
@environmentfilter
def sort_items(environment, items, prop):
    expr = make_attrgetter(environment, prop)
    return sorted(items, key=expr)


@ischool.app_template_filter('get_inst_time_desc')
def get_inst_time_desc(inst):
    day_names = [u'א', u'ב', u'ג', u'ד', u'ה', u'ו', u'ש']

    x = u"יום %s', %s שעות"
    return x % (day_names[inst.start_time().isoweekday() % 7], unicode(short_float(inst.luz_hours())))


@ischool.app_template_filter('calculate_number_of_days_in_week')
def calculate_number_of_days_in_week(placements):
    if len(placements) == 0:
        return MIN_DAYS_IN_WEEK
    min_time = min(placements, key=lambda p: p.start_time).start_time
    max_time = max(placements, key=lambda p: p.start_time).start_time

    days = (max_time - min_time).days
    if days < MIN_DAYS_IN_WEEK:
        days = MIN_DAYS_IN_WEEK
    return days

@ischool.app_template_filter('get_week_start_time')
def get_week_start_time(course, week_num):
    start_time, _ = course.get_week_time(week_num)
    return start_time


@ischool.app_template_filter('strftime')
def strftime(time, sformat):
    return time.strftime(sformat)


@ischool.app_template_filter('ticks')
def ticks(time):
    """
    Equivalent to DateTime.Ticks in .NET
    :param time datetime
    """
    if isinstance(time, datetime):
        time = time.date()
    return long((time - date(1, 1, 1)).total_seconds() * 1000 ** 2)


def hebrew_time_difference(time, timenow):
    diff = abs(timenow - time)
    hours = diff.seconds / (60 * 60)
    if diff.days > 365:
        return u'יותר משנה'
    elif diff.days == 365:
        return u'שנה בדיוק'
    elif diff.days > 90:
        return u'יותר מ-3 חודשים'
    elif diff.days > 60:
        return u'יותר מחודשיים'
    elif diff.days > 30:
        return u'יותר מחודש'
    elif diff.days == 30:
        return u'חודש בדיוק'
    elif diff.days > 27:
        return u'כחודש'
    elif diff.days > 20:
        return u'כ-3 שבועות'
    elif diff.days > 13:
        return u'כשבועיים'
    elif diff.days > 7:
        return u'יותר משבוע'
    elif diff.days == 7:
        return u'בדיוק שבוע'
    elif diff.days > 2:
        return str(diff.days) + u' ימים'
    elif diff.days == 2:
        return u'יומיים'
    elif diff.days == 1:
        return u'יום'
    elif hours > 2:
        return str(hours) + u' שעות'
    elif hours == 2:
        return u'שעתיים'
    elif hours == 1:
        return u'שעה'
    elif diff.seconds >= 180:
        return str(diff.seconds / 60) + u' דקות'
    elif diff.seconds > 120:
        return u'שתי דקות'
    elif diff.seconds > 60:
        return u'דקה'
    elif diff.seconds > 5:
        return u'פחות מדקה'
    else:
        return u'רגע'

@ischool.app_template_filter('moments_time')
def moments_time(time):
    """
    Returns a short Hebrew description of when a given date has happened
    :param time datetime
    """
    if not isinstance(time, datetime):
        return u'לא ידוע'
    now = datetime.now()
    if time < now:
        return u'לפני ' + hebrew_time_difference(time, now)
    elif time > now:
        return u'בעוד ' + hebrew_time_difference(time, now)
    else:
        return u'הרגע'


@ischool.app_template_filter('personalize_message')
def personalize_message(person, male_message, female_messages):
    from ischool.models import Role
    if isinstance(person, Role):
        # Catchy!
        person = person.person

    if not person or person.gender == 'male':
        return male_message
    else:
        return female_messages


@ischool.app_template_filter('is_hebrew')
def is_hebrew(text):
    """
    Determines if the given text is in Hebrew. Returns true if more than
    half the text is in Hebrew
    :param text The text
    """
    half = len(text) / 2
    hebrew_chars = 0

    for c in text:
        if ord(c) >= ord(u'א'):
            hebrew_chars += 1
        if hebrew_chars > half:
            return True

    return False


@ischool.app_template_filter('markup')
def markup(text):
    """
    Returns an HTML marked-up copy of a given text, where \n gets converted
    to <br />, etc...
    :param text The text
    """
    return text.replace("\n", "<br />")

@ischool.app_template_filter('sanitize_html')
def sanitize_html(text):
    """
    Simple function that strips unneeded html tags, like scripts, divs and so on..
    @param text: The text
    """
    rjs = r'[\s]*(&#x.{1,7})?'.join(list('javascript:'))
    rvb = r'[\s]*(&#x.{1,7})?'.join(list('vbscript:'))
    re_scripts = re.compile('(%s)|(%s)' % (rjs, rvb), re.IGNORECASE)
    validTags = 'p i strong b u a h1 h2 h3 pre br img span'.split()
    validAttrs = 'href src width height'.split()
    urlAttrs = 'href src'.split() # Attributes which should have a URL
    soup = BeautifulSoup(text)
    for comment in soup.findAll(text=lambda text: isinstance(text, Comment)):
        # Get rid of comments
        comment.extract()
    for tag in soup.findAll(True):
        if tag.name not in validTags:
            tag.hidden = True
        attrs = tag.attrs
        tag.attrs = []
        for attr, val in attrs:
            if attr in validAttrs:
                val = re_scripts.sub('', val) # Remove scripts (vbs & js)
                if attr in urlAttrs:
                    val = urljoin(base_url, val) # Calculate the absolute url
                tag.attrs.append((attr, val))

    return soup.renderContents().decode('utf8')

@ischool.app_template_filter('sort_by_priority')
def sort_by_priority(l, key='priority'):
    l = list(l)
    l.sort(key=lambda p: int(getattr(p, key)))
    return l
