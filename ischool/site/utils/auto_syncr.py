import json
from flask import request

def max_or_default(iterable, default):
    try:
        return max(max(iterable), default)
    except ValueError:
        return default

def request_args_or_default(key, default):
    if key in request.args:
        return json.loads(request.args[key])
    return default
