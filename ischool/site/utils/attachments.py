# coding=utf-8
"""

"""
from functools import wraps
from flask import request
from flask.wrappers import Response

__author__ = 'user1'
__all__ = ['CONTENT_TYPES', 'adapt_response']


CONTENT_TYPES = {
    'luzx': 'application/luz-xml',
    'xml': 'application/xml',
}


def get_extension(filename):
    """
    Returns file extension
    :param filename: File name without slashes
    :return: Extension
    """
    if "." not in filename:
        return ""
    return filename.rsplit(".")[-1].lower()


def get_filename(filepath):
    """
    Returns only file name (without directory)
    IMPORTANT: Works on UNIX-based filesystems
    :param filepath: Full file path
    :return: File name
    """
    return filepath.rsplit("/")[-1]


def adapt_response(mimetype=None, headers=None, attachment=False):
    """
    Decorator that returns an appropriate response with mimetype,
        and adds content-disposition parameter if needed
    :param mimetype: Content-type to send, if none is given tries to resolve from CONTENT_TYPES list
    :param headers: Extra headers to send
    :param attachment: whether the file should be an attachment
    :return: Decorated function that returns a fixed flask.Response
    """
    def decorator(f):
        @wraps(f)
        def func(*args, **kwargs):
            body = f(*args, **kwargs)
            extension = get_extension(request.path)

            # Copy for local modification
            mime_type = None

            if mimetype is None:
                if extension not in CONTENT_TYPES:
                    mime_type = None
                else:
                    mime_type = CONTENT_TYPES[extension]
            else:
                mime_type = mimetype

            if headers is not None:
                sent_headers = headers.copy()
            else:
                sent_headers = {}
            if attachment:
                sent_headers['Content-disposition'] = "attachment; filename=" + get_filename(request.path)

            return Response(body, mimetype=mime_type, headers=sent_headers)

        return func
    return decorator
