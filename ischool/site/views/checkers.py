# coding=utf-8

from flask import render_template, jsonify, g, abort
from ischool.site.utils.auth import ischool_route, Permissions
from ischool.models.people import Person
from ischool.models.exercise.exercise import Exercise
from ischool.models.exercise.assignment import Assignment
from ischool.models.exercise.checkers_assignments import CheckersAssignments
from itertools import groupby
import json

@ischool_route('/api/students', Permissions.course_checkers)
def get_students_list():
    students = Person.query.filter(Person.role == "student")
    dicts = []
    for s in students:
        obj = {
            "id": s.id,
            "course_number": s.course_number
        }
        dicts.append(obj)

    return json.dumps(dicts)


@ischool_route('/api/get_checker_exercises', Permissions.course_checkers)
def get_checker_exe_list():
    checker_id = g.person.id
    exercises_list = [ass.exercise_id for ass in CheckersAssignments.query.filter(CheckersAssignments.checker_id == checker_id)]
    exercises = Exercise.query.filter(Exercise.id.in_(exercises_list))
    dicts = []
    for s in exercises:
        obj = {
            "id": s.id,
            "name": s.name,
            "array_id": s.array_id
        }
        dicts.append(obj)

    return json.dumps(dicts)

@ischool_route('/api/assignments_checker', Permissions.course_checkers)
def get_checker_assignments():
    checker_id = g.person.id
    exercise_ids = [ass.exercise_id for ass in CheckersAssignments.query.filter(CheckersAssignments.checker_id == checker_id)]

    assignments = Assignment.query.filter(Assignment.exercise_id.in_(exercise_ids)).all()

    # Group the result by student and then by exercise.
    res = {}
    for a in assignments:
        if a.exercise_id not in res:
            res[a.exercise_id] = {}

        res[a.exercise_id][a.person_id] = a.jsonify()

    return jsonify(res)
