# coding=utf-8

from ischool.config import IschoolConfig as config
from ischool.site.extensions import db
from flask import g, render_template, redirect, url_for, jsonify, request, abort
from ischool.site.utils.flask_restless import create_api
from ischool.site.utils.auth import ischool_route, Permissions
from ischool.models import Exercise, ExerciseFile, File, FileUsage, \
    InstanceExercise, LessonInstance, CheckersAssignments, Person
import os
import json
from hashlib import md5
from ischool.models.arrays import Array
from ischool.models.medida.criteria import Criterion
from sqlalchemy import func


@ischool_route('/arrays/<array:arr>/exercise/plan', Permissions.all_staffs)
def array_exercise_plan_view(arr):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    arr = arr()

    return render_template('array-exercise-plan.html', array=arr)


@ischool_route('/arrays/<array:arr>/exercise/schedule', Permissions.all_staffs, methods=['GET'])
def array_exercise_schedule_view(arr):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    arr = arr()

    return render_template('array-exercise-schedule.html', array=arr)


# TODO: RESTLESS: Remove this (replace by PUT method)
@ischool_route('/arrays/<array:arr>/exercise/<int:ex_id>/reorder', Permissions.all_staffs, methods=['POST'])
def array_exercise_schedule_reorder(arr, ex_id):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    arr = arr()

    instance_exercise = InstanceExercise.query.get(ex_id)
    current_order = instance_exercise.order
    modifier = int(request.form.get('modifier'))

    res = filter(lambda x: x.order == current_order + modifier,
                 instance_exercise.instance.instance_exercises)
    if len(res) == 0:
        return
    other = res[0]

    other.order = current_order
    instance_exercise.order = current_order + modifier

    db.session.commit()

    return jsonify(otherId=other.id)


# TODO: RESTLESS: Remove this (replace by DELETE method)
@ischool_route('/exercises/instances/<instance_exercise:instance_exercise>',
               Permissions.all_staffs,
               methods=['DELETE'])
def array_exercise_schedule_remove(instance_exercise):
    instance_exercise = instance_exercise()
    current_order = instance_exercise.order
    for i_exercise in filter(lambda x: x.order > current_order,
                             instance_exercise.instance.instance_exercises):
        i_exercise.order -= 1

    db.session.delete(instance_exercise)
    db.session.commit()

    return jsonify(removed_id=instance_exercise.id)


# TODO: RESTLESS: Remove this (replace by POST? method)
@ischool_route('/arrays/<array:arr>/exercise/<int:inst_id>/<int:ex_id>/schedule', Permissions.all_staffs,
               methods=['POST'])
def array_exercise_schedule(arr, inst_id, ex_id):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    arr = arr()

    exercise = Exercise.query.get(ex_id)
    instance = LessonInstance.query.get(inst_id)
    order = len(instance.instance_exercises) + 1
    ix = InstanceExercise(instance=instance, order=order)
    exercise.instance_exercise.append(ix)
    db.session.add(ix)
    db.session.commit()

    return jsonify({
        "id": ix.id,
        "exercise_id": ix.exercise_id,
        "instance_id": ix.instance_id,
        "is_filler": ix.is_filler,
        "order": ix.order,
    })

@ischool_route('/arrays/<array:arr>/instance_exercise/<int:ie_id>', Permissions.all_staffs,
               methods=['PUT'])
def array_instance_exercise_update(arr, ie_id):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    ix = InstanceExercise.query.get(ie_id)

    data = json.loads(request.data)

    ix.is_filler = data.get("is_filler")
    ix.order = data.get("order")

    db.session.add(ix)
    db.session.commit()

    return jsonify({
        "id": ix.id,
        "exercise_id": ix.exercise_id,
        "instance_id": ix.instance_id,
        "is_filler": ix.is_filler,
        "order": ix.order,
    })


@ischool_route('/api/exercise',
               Permissions.all_staffs,
               methods=['POST'])
def array_exercise_create():
    data = json.loads(request.data)

    arr = Array.query.get(data['array_id'])
    ex = Exercise(name=data['name'],
                  course_specific=data['course_specific'],
                  submit_type=data['submit_type'])
    arr.exercises.append(ex)
    db.session.add(ex)

    selected_array_criteria = data['selected_array_criteria']
    if selected_array_criteria:
        for id in selected_array_criteria:
            criterion = Criterion.query.get(id)
            if selected_array_criteria[id]:
                criterion.exercises.append(ex)
    db.session.commit()
    return json.dumps({"exercise_id":ex.id})

@ischool_route('/api/exercise/<int:exercise_id>',
               Permissions.all_staffs,
               methods=['PUT'])
def exercise_edit(exercise_id):
    data = json.loads(request.data)
    ex = Exercise.query.get(exercise_id)
    ex.name = data['name']
    ex.submit_type = data['submit_type']
    ex.course_specific = data['course_specific']

    selected_array_criteria = data['selected_array_criteria']
    if selected_array_criteria:
        for id in selected_array_criteria:
            criterion = Criterion.query.get(id)
            if selected_array_criteria[id] and not ex in criterion.exercises:
                criterion.exercises.append(ex)
            elif not selected_array_criteria[id] and ex in criterion.exercises:
                criterion.exercises.remove(ex)

    db.session.commit()
    return json.dumps({"exercise_id":ex.id})

@ischool_route('/exercise/delete/<int:exercise_id>',
               Permissions.all_staffs,
               methods=['DELETE'])
def exercise_delete(exercise_id):
    ex = Exercise.query.get(exercise_id)
    db.session.delete(ex)
    db.session.commit()
    return json.dumps({"exercise_id":ex.id})

@ischool_route('/arrays/<array:arr>/exercise/<int:ex_id>', Permissions.all_staffs, methods=['POST'])
def array_exercise_edit(arr, ex_id):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    arr = arr()
    exercise = Exercise.query.get(ex_id)
    exercise.name = request.form.get('name')
    db.session.commit()
    return json.dumps({'exercise_id':ex_id, 'exercise_name':exercise.name})

@ischool_route('/arrays/<array:arr>/exercise/<int:ex_id>', Permissions.all_staffs, methods=['GET'])
def array_exercise_view(arr, ex_id):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    arr = arr()

    exercise = Exercise.query.get(ex_id)
    return render_template('array-exercise.html', array=arr, exercise=exercise)


# TODO: RESTLESS: Remove this (replace by PUT method)
@ischool_route('/arrays/<array:arr>/exercise/<int:ex_id>/files/<int:file_id>',
               Permissions.all_staffs,
               methods=['POST'])
def array_exercise_delete_file(arr, ex_id, file_id):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    arr = arr()

    e_file = ExerciseFile.query.get(file_id)
    db.session.delete(e_file)
    db.session.commit()
    return redirect(url_for('ischool.array_exercise_plan_view', arr=arr))


@ischool_route('/arrays/<array:arr>/exercise/<int:ex_id>/upload/<file_type>/<filename>', Permissions.all_staffs, methods=['PUT'])
def array_exercise_upload_file(arr, ex_id, file_type, filename):
    hasher = md5()
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    arr = arr()
    exercise = Exercise.query.get(ex_id)

    file = request.files['file']
    filename = file.filename
    (file_no_ext, ext) = os.path.splitext(filename)

    if not ext:
        ext = ""

    hasher.update(file_no_ext.encode('utf8'))
    hashed_file_no_ext = hasher.digest().encode('hex')
    full_hashed_name = hashed_file_no_ext + ext
    full_path = os.path.join(config.UPLOAD_FOLDER, full_hashed_name)
    file.save(full_path)
    (file_no_ext, ext) = os.path.splitext(filename)

    if ext:
        # Remove the leading dot
        ext = ext[1:]
    e_file = ExerciseFile(file_type=file_type)

    #TODO: Why should the array manager be the file uploader? Why not the actual uploader?
    f = File(file_no_ext, ext, exercise.array.manager, full_path)
    e_file.file_usage = FileUsage(f.last_revision)

    exercise.files.append(e_file)

    db.session.add(f)
    db.session.add(e_file)
    db.session.add(exercise)
    db.session.commit()

    return jsonify(exercise_id=exercise.id ,id=e_file.id, file_usage_id=e_file.file_usage.id, file_link=e_file.file_link, file_name=filename,
                   file_type=e_file.file_type)


@ischool_route('/api/checkers_assignments/<int:exercise_id>/<int:student_id>/<int:checker_id>',
                Permissions.all_staffs,
                methods=['DELETE', 'PUT'])
def checker_assignment(exercise_id, student_id, checker_id):
    if request.method == 'DELETE':
        CheckersAssignments.query.filter(CheckersAssignments.exercise_id == exercise_id)      \
                                        .filter(CheckersAssignments.student_id == student_id) \
                                        .filter(CheckersAssignments.checker_id == checker_id) \
                                        .delete()
    else:
        if CheckersAssignments.query.filter(CheckersAssignments.exercise_id == exercise_id,  \
                                            CheckersAssignments.checker_id == checker_id,    \
                                            CheckersAssignments.student_id == student_id)    \
                                            .count() == 0:
            db.session.add(CheckersAssignments(exercise_id=exercise_id,
                                               checker_id=checker_id,
                                               student_id=student_id))


    db.session.commit()
    return ''

@ischool_route(['/api/checkers_assignments/<int:array_id>/<int:last_checker_assignment_id>',
    '/api/checkers_assignments/<int:array_id>'],
    Permissions.all_staffs)
def get_checkers_assignments_by_array(array_id, last_checker_assignment_id = -1):
    # If there is no new checker_assignment, return ''
    max_checker_assignment_id = CheckersAssignments.query.with_entities(func.max(CheckersAssignments.id)).first()[0]
    if max_checker_assignment_id == last_checker_assignment_id:
        return jsonify({'last_checker_assignment_id': max_checker_assignment_id})

    checkers = CheckersAssignments.query.join(Exercise).filter(Exercise.array_id == array_id).all()

    return jsonify({'last_checker_assignment_id': max_checker_assignment_id,
        'checker_assignments': [{
            "id": c.id,
            "exercise_id": c.exercise_id,
            "checker_id": c.checker_id,
            "student_id": c.student_id
        } for c in checkers]})
