import re
import dateutil.parser
from urlparse import urljoin
from werkzeug.contrib.atom import AtomFeed
from ischool.site.extensions import db
from flask import render_template, g, request, jsonify
from ischool.site.utils.auth import ischool_route, Permissions
from ischool.models import twitter, people
import json

@ischool_route(['/twitter/',
    '/twitter/twitter'],
    Permissions.all_staffs, course_dependant=True)
def twitter_home_page():
    return render_template('twitter.html', course=g.course, filters="")


@ischool_route('/twitter/twitter/filtered/<string:filters>', Permissions.all_staffs, course_dependant=True)
def twitter_home_page_filtered(filters = ""):
    return render_template('twitter.html', course=g.course, filters=filters)


@ischool_route('/twitter/hashtags', Permissions.all_staffs)
def twitter_hash_tags():
    return render_template('twitter.html', course=g.course, tweets="")


@ischool_route('/twitter/tweet', Permissions.all_staffs, methods=['POST'])
def create_tweet():
    message = request.json['message']
    mentions = request.json['mentions']
    tweet = twitter.Tweet(message, mentions, g.person.id)
    return jsonify(twitter.format_tweets_for_client([tweet])[0])

@ischool_route('/twitter/last/<int:tweet_count>', Permissions.all_staffs)
def twitter_last(tweet_count):
    return jsonify(tweets=twitter.get_last_tweets(tweet_count))

@ischool_route('/twitter/initialize/<int:tweet_count>', Permissions.all_staffs)
def twitter_initialize(tweet_count):
    return jsonify(tweets=twitter.get_last_tweets(tweet_count), tags=twitter.get_all_tags())

@ischool_route(['/twitter/pull_since/<int:tweet_id>',
    '/twitter/tweets_since/<int:tweet_id>/'],
    Permissions.all_staffs)
def twitter_pull_since(tweet_id):
    return jsonify(tweets=twitter.get_tweets_since(tweet_id))

@ischool_route('/api/people/begins_with/<string:name_start>', Permissions.all_staffs)
def get_people_autocomplete(name_start):
    return jsonify(people=people.get_people_by_start(name_start))

@ischool_route('/api/hashtags/begins_with/<string:hashtag_start>', Permissions.all_staffs)
def get_hashtags_autocomplete_by_starts_with(hashtag_start):
    return jsonify(hashtags=twitter.get_hashtags_by_start(hashtag_start))

@ischool_route('/api/hashtags/contains/<string:hashtag_contains>', Permissions.all_staffs)
def get_hashtags_autocomplete_by_contains(hashtag_contains):
    return jsonify(hashtags=twitter.get_hashtags_by_contains(hashtag_contains))

def parse_filter(filter):
    hashtag_ids, people_ids, filterHandled = filter.split("@")
    hashtag_ids = hashtag_ids.split(",")
    hashtag_ids.remove("")

    people_ids = people_ids.split(",")
    people_ids.remove("")

    filterHandled = filterHandled == 'True'
    return [int(id) for id in hashtag_ids], [int(id) for id in people_ids], filterHandled

def get_tweets_by_filter(filter):
    hashtag_ids, people_ids, filterHandled = parse_filter(filter)
    return twitter.get_tweets_filtered(hashtag_ids, people_ids, filterHandled)

@ischool_route('/twitter/pull_filtered/<string:filter>', Permissions.all_staffs)
def get_json_tweets_by_filters(filter):
    return jsonify(tweets=get_tweets_by_filter(filter))

@ischool_route('/twitter/load_all/', Permissions.all_staffs)
def get_all_tweets():
    return jsonify(tweets=twitter.get_all_tweets())

@ischool_route('/twitter/load_more/<int:last_id>/<int:load_count>/', Permissions.all_staffs)
def get_more_tweets(last_id, load_count):
    return jsonify(tweets=twitter.get_tweets_between(last_id - load_count, last_id))


@ischool_route(['/api/tags_cloud',
    '/api/tags_cloud/<string:filter>'],
    Permissions.all_staffs)
def get_hashtag_cloud(filter = '@@False'): #default-filter: not filtered.
    hashtag_ids, people_ids, filterHandled = parse_filter(filter)
    return json.dumps(twitter.get_hashtag_cloud(hashtag_ids, people_ids, filterHandled))

@ischool_route('/api/tweets/handled/<int:tweet_id>/<string:handled>', Permissions.all_staffs)
def set_tweet_state(tweet_id, handled):
    tweet = twitter.Tweet.get_by_id(tweet_id)
    tweet.handled = True if handled == "True" else False
    db.session.add(tweet)
    db.session.commit()
    return jsonify(message="success")

@ischool_route('/api/tweets/<int:tweet_id>', Permissions.all_staffs, methods=['DELETE'])
def delete_tweet(tweet_id):
    tweet = twitter.Tweet.get_by_id(tweet_id)
    db.session.delete(tweet)
    db.session.commit()
    return jsonify(message="success")

def make_external(url):
    return urljoin(request.url_root, url)

@ischool_route('/api/rss/tweets/<string:filter>', Permissions.all_staffs)
def get_tweets_rss(filter):
    feed = AtomFeed('Tweets',
                    feed_url=request.url, url=request.url_root)
    tweets = get_tweets_by_filter(filter)
    for tweet in tweets:
        tweet_text = parse_tweet(tweet)
        feed.add(unicode(tweet_text),
                 id=tweet['id'],
                 content_type='html',
                 url=make_external(str(tweet['id'])),
                 author=tweet['poster'],
                 updated=dateutil.parser.parse(tweet['timestamp']))
    return feed.get_response()

def parse_tweet(tweet):
    content = tweet['content']
    people = tweet['people']
    hashtags = tweet['hashtags']
    mentions = re.findall("@\[(.*?)]", content)
    for mention in mentions:
        type, id = mention.split(":")
        if type == "hashtag":
            new_text = "#" + find_match(hashtags, id)
        elif type == "person":
            new_text = "@" + find_match(people, id)
        content = content.replace("@[" + mention + "]", new_text)
    return content

def find_match(match_list, id):
    # We assume that match_list has ["id"] and ["name"]
    for match in match_list:
        if match["id"] == str(id):
            return match["name"]
    return None
