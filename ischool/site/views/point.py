# coding=utf-8
import json

from flask import render_template, g, request, abort, Markup
from sqlalchemy.sql.expression import or_
from ischool.site.extensions import db
from ischool.models import Point, Message, Person
from ischool.site.utils.jinja_filters import is_hebrew, moments_time, markup, sanitize_html
from ischool.site.utils.flask_restless import create_api
from ischool.site.utils.auth import ischool_route, Permissions
from ischool.site.utils.encoding import jsonify
import datetime
from ischool.models.exercise.assignment import Assignment
from ischool.models.exercise.exercise import Exercise


@ischool_route('/my_points', Permissions.students)
def student_points_view():
    return render_template('point/student_points.html', course=g.course, assignments=g.person.assignments)

@ischool_route('/my_points/<int:point_id>', Permissions.students)
def student_point_popup_view(point_id):
    return render_template('point/student_point_popup.html', course=g.course, point_id=point_id, person=g.person)

#@ischool_route('/points_notifier', Permissions.students)
#def student_point_notifier_view():
#    return render_template('point/student_points_notifier.html', course=g.course, person=g.person)


@ischool_route(['/courses/<course:course>/points/manage',
    '/points/manage'],
    Permissions.all_staffs)
def manage_points():
    if not g.person.is_staff(g.course):
        abort(401)
    #TODO: Get only the current course's points (currently a person doesn't have a main_course)
    all_points = Point.query.all()#.join(Point.person).filter(Person.main_course == g.course).all()
    pending_points = filter(lambda p: p.staff_status in ['open', 'handling'], all_points)
    closed_points = filter(lambda p: p.staff_status in ['closed'], all_points)
    return render_template('point/manage.html', active_tab='pending', pending_points=pending_points,
                           closed_points=closed_points, points_count=len(all_points))

create_api(Point,
           Permissions.all_staffs,
           exclude_columns=['_staff_status', 'assignment.version'],
           methods=['GET', 'POST', 'PUT', 'DELETE'],
           include_methods=['exercise_name', 'array_id'],
           results_per_page=10)

def pre_get_many_points_processor(result=None, **kw):
    if 'objects' in result:
        result['objects'] = [p for p in result['objects'] if p['person_id'] == g.person.id]

create_api(Point,
           Permissions.students,
           collection_name='student_points',
           exclude_columns=['_staff_status', 'staff_status'],
           methods=['GET'],
           postprocessors={
               'GET_MANY': [pre_get_many_points_processor]
           })

@ischool_route('/api/points/<point:point>/messages', Permissions.all_staffs, methods=['GET'])
def get_point_messages(point):
    point = point()
    return jsonify(point.messages)

@ischool_route('/api/points/answered_points_count', Permissions.students, methods=['GET'])
def get_answered_points_count():
    current_person = g.person
    return jsonify(count=Point.query
        .filter_by(_staff_status='closed', person=current_person)
        .filter(Point.type != 'bathroom')
        .join(Message)
        .filter(Message.read_at == None)
        .count()
    )

@ischool_route('/points/bathroom_state', Permissions.students, methods=['GET'])
def get_bathroom_state():
    current_person = g.person
    found_point_id = 0
    returned_state = "enable"
    open_points = Point.query.filter_by(_staff_status='open', type='bathroom', person=current_person).all()
    approved_points = Point.query.filter_by(_staff_status='approved', type='bathroom', person=current_person).all()
    if len(open_points) > 0:
        returned_state = "disable"
    elif len(approved_points) > 0:
        returned_state = "can_go"
        found_point_id = approved_points[0].id
    return jsonify(state=returned_state, point_id=found_point_id)

@ischool_route('/points/manage/closed',  Permissions.all_staffs)
def manage_closed_points():
    #TODO: Get only the current course's points (currently a person doesn't have a main_course)
    all_points = Point.query.all()#join(Point.person).filter(Person.main_course == g.course)
    pending_points = filter(lambda p: p.staff_status in ['open', 'handling'], all_points)
    closed_points = filter(lambda p: p.staff_status in ['closed'], all_points)
    return render_template('point/manage.html', active_tab='closed', closed_points=closed_points,
                           pending_points=pending_points, points_count=len(all_points))

@ischool_route('/points/student/<int:student_id>', Permissions.all_staffs)
def all_student_points(student_id):
    student = Person.query.filter_by(id=student_id).first()
    points = Point.query.filter_by(person_id = student_id).all()
    points = [p for p in points if len(p.messages) > 1 or not p.messages[0].text.startswith(u"קיבלת תרגיל חדש במערך")]
    return render_template('all_points.html', points=points, student=student)

@ischool_route('/points/manage/saved',  Permissions.all_staffs)
def manage_saved_points():
    #TODO: Get only the current course's points (currently a person doesn't have a main_course)
    all_points = Point.query.all()#join(Point.person).filter(Person.main_course == g.course)
    pending_points = filter(lambda p: p.staff_status in ['open', 'handling'], all_points)
    closed_points = filter(lambda p: p.staff_status in ['closed'], all_points)
    saved_points = filter(lambda p: p.staff_status in ['saved'], all_points)
    return render_template('point/manage.html', active_tab='saved', saved_points=saved_points,
                           pending_points=pending_points, closed_points=closed_points, points_count=len(all_points))

@ischool_route('/points', Permissions.all_staffs, Permissions.students, methods=['POST'])
def create_point():
    current_person = g.person
    type = request.json['type']
    messages = request.json['messages']
    assignment_id = request.json.get('assignment_id')
    room_id = request.json.get('room_id')
    if request.json.get('person_id') and current_person.is_staff(g.course):
        point_person = Person.query.get_or_404(request.json.get('person_id'))
    else:
        point_person = current_person
    point = Point(person=point_person, type=type, assignment_id=assignment_id, room_id=room_id)
    if messages:
        msg = Message(person=current_person, point=point, text=messages[0]['text'])
        # msg = Message(person=current_person, point=point, text=sanitize_html(messages[0]['text']))
        db.session.add(msg)

    if current_person.is_staff(g.course) and request.json.get('staff_status'):
        point.staff_status = request.json.get('staff_status')

    db.session.add(point)
    db.session.commit()
    return jsonify(point=json_point(point, point_person))

@ischool_route('/api/points/new_staff_point', Permissions.all_staffs, methods=['POST'])
def new_stuff_point():
    type = request.json['type']
    message_text = request.json['message_text']
    room_id = request.json.get('room_id')
    student_ids = request.json['student_ids']

    for student_id in student_ids:
        point = Point(person_id=student_id, type=type, room_id=room_id, staff_status='closed')
        msg = Message(person=g.person, point=point, text=message_text)
        db.session.add(point)
        db.session.add(msg)

    db.session.commit()
    return ''


@ischool_route('/points/<int:point_id>', Permissions.all_staffs, Permissions.students, methods=['POST'])
def update_point(point_id):
    current_person = g.person
    point = Point.query.get_or_404(point_id)
    if not current_person.is_staff(g.course) and point.person_id != current_person.id:
        abort(401)

    #message = sanitize_html(request.form['message'])
    message = request.form['message']
    if message:
        msg = Message(person=current_person, point=point, text=message)
        db.session.add(msg)

    if point.person_id == current_person.id:
        if point.type == 'bathroom':
            point.staff_status = 'closed'
        else:
            point.staff_status = 'open'

        if 'room' in request.form:
            point.room_id = request.form['room']

        for m in point.messages:
            if m.read_at is None:
                m.mark_read()

    elif current_person.is_staff(g.course) and point.type == 'bathroom':
        point.staff_status = 'approved'

    db.session.commit()

    return jsonify(point=json_point(point, current_person))


@ischool_route('/points/<point:point>/message', Permissions.all_staffs, methods=['POST'])
def send_message(point):
    current_person = g.person
    point = point()

    #message = sanitize_html(request.form['message'])
    msg = Message(person=current_person, point=point, text=request.form['text'])
    db.session.add(msg)
    db.session.commit()

    return jsonify(msg)


@ischool_route('/points/mark_last_message_read', Permissions.students, methods=['POST'])
def mark_last_message_read():
    current_person = g.person
    point_id = request.json['point_id']
    point = Point.query.get_or_404(point_id)
    if point.person_id != current_person.id:
        abort(401)
    last_message = point.last_message()
    last_message.mark_read()
    db.session.commit()
    return jsonify(read_at = last_message.read_at.isoformat())

@ischool_route('/points/<int:point_id>', Permissions.all_staffs, Permissions.students)
def get_point(point_id):
    current_person = g.person
    point = Point.query.get_or_404(point_id)
    if not current_person.is_staff(g.course) and point.person.id != current_person.id:
            abort(401)

    return jsonify(point=json_point(point, current_person))

@ischool_route('/points/<int:point_id>', Permissions.all_staffs, Permissions.students, methods=['DELETE'])
def delete_point(point_id):
    current_person = g.person
    point = Point.query.get_or_404(point_id)
    if point.person == current_person:
        point.student_status = 'hidden'
        db.session.commit()

    return jsonify(points=json_point(point, current_person))


def json_point(point, person):
    return {
        'id': point.id,
        'type': point.type,
        'title': point.title(),
        'messages': map(json_message, point.messages),
        'student_status': point.student_status,
        'has_unread_messages': len(point.unread_messages_for_person(person)) > 0,
    }


def json_message(message):
    return {
        'id': message.id,
        'created_at': moments_time(message.created_at),
        'read_at': moments_time(message.read_at) if message.read_at is not None else None,
        'text': markup(message.text),
        'is_hebrew': is_hebrew(message.text)
    }

@ischool_route('/latest_points/<string:staff_status>/<int:amount>', Permissions.all_staffs)
def get_latest_points_ids(staff_status, amount):
    """
    Finds the ids of the points which were responded lately.
    Also filters according to the room id and the oldest message id that was already loaded
    (finds the next newest points).
    """
    query = Point.query.filter(create_staff_status_filter(staff_status))
    if 'room_id' in request.args:
        query = query.filter(Point.room_id == request.args['room_id'])
    query = query.join(Point.messages)
    if 'oldest_message_id' in request.args:
        print request.args['oldest_message_id']
        query = query.filter(Message.id < request.args['oldest_message_id'])
    query = query.order_by(Message.id.desc()).limit(amount)
    points = query.all()
    return json.dumps([point.id for point in points])

@ischool_route('/api/newer_points_student/<int:newest_message_id>', Permissions.students)
def get_newer_points_student(newest_message_id):
    """
    Finds the ids of the points which were responded after the creation of the message with the given message id.
    """
    query = Point.query.filter(Point.person_id == g.person.id)
    query = query.join(Point.messages)
    query = query.filter(Message.id > newest_message_id)
    query = query.order_by(Message.id.desc())
    points = query.all()
    return json.dumps([point.id for point in points])

@ischool_route('/newer_points/<string:staff_status>/<int:newest_message_id>', Permissions.all_staffs)
def get_newer_points(staff_status, newest_message_id):
    """
    Finds the ids of points which were responded after the creation of the message with the given message id.
    """
    query = Point.query.filter(create_staff_status_filter(staff_status))
    query = query.join(Point.messages)
    query = query.filter(Message.id > newest_message_id)
    query = query.order_by(Message.id.desc())
    points = query.all()
    return json.dumps([point.id for point in points])

def create_staff_status_filter(staff_status):
    """
    Create sqlalchemy clause to filter the points by their staff status.
    """
    if staff_status == 'pending':
        return or_(Point.staff_status == 'open', Point.staff_status == 'handling')
    elif staff_status == 'saved':
        return Point.staff_status == 'saved'
    elif staff_status == 'closed':
        return Point.staff_status == 'closed'


def new_json_point(point):
    d = {
        'id': point.id,
        'type': point.type,
        'messages': map(new_json_message, point.messages),
        'student_status': point.student_status
    }

    if point.assignment is not None:
        d['assignment'] = {'id': point.assignment.exercise.id,
            'name': point.assignment.exercise.name}
    return d

def new_json_message(message):
    return {
        'id': message.id,
        'created_at': message.created_at.isoformat(),
        'read_at': message.read_at.isoformat() if message.read_at is not None else None,
        'text': markup(message.text),
    }

@ischool_route('/api/unread_points/', Permissions.students, methods=['GET'])
def get_unread_points():
    active_points_query = Point.query.filter_by(person_id = g.person.id, student_status = 'active')
    unread_points = active_points_query.join(Point.messages)  \
                    .filter(Message.read_at == None)          \
                    .filter(Message.person_id != g.person.id) \
                    .filter(Message.remind_in < datetime.datetime.now()) \
                    .all()
    return json.dumps([p.id for p in unread_points])

@ischool_route('/api/points/<int:point_id>', Permissions.all_staffs, Permissions.students)
def new_get_point(point_id):
    current_person = g.person
    point = Point.query.get_or_404(point_id)
    if not current_person.is_staff(g.course) and point.person.id != g.person.id:
            abort(401)
    for message in point.messages:
        if message.read_at is None:
            message.read_at = datetime.datetime.now()
    db.session.commit()
    return json.dumps(new_json_point(point))

@ischool_route('/api/points/mark_point_read', Permissions.students, methods=['POST'])
def mark_point_read():
    current_person = g.person
    point_id = request.json['point_id']
    point = Point.query.get_or_404(point_id)
    if point.person_id != current_person.id:
        abort(401)
    for message in point.messages:
        if message.read_at is None:
            message.mark_read()

    db.session.commit()
    return ''

@ischool_route('/api/points/set_reminder', Permissions.students, methods=['POST'])
def set_reminder():
    data = json.loads(request.data)
    point_id = data['point_id']
    remind_in = data['remind_in']
    point = Point.query.get_or_404(point_id)
    message = point.last_message()
    message.read_at = None
    message.remind_in = datetime.datetime.now() +\
                        datetime.timedelta(seconds=int(remind_in))
    db.session.commit()
    return ""

@ischool_route('/api/points/paginated/<string:array_id>/<int:page>', Permissions.students)
def get_paginated_points(array_id, page):
    array_id = int(array_id)
    points_query = Point.query.filter(Point.person_id == g.person.id)
    if array_id != -1:
        points_query = points_query.join(Point.assignment).join(Assignment.exercise).filter(Exercise.array_id == array_id)
    points_query = points_query.join(Point.messages)
    points_query = points_query.order_by(Message.created_at.desc())

    points = points_query.paginate(page, 10, False).items
    return json.dumps([new_json_point(point) for point in points])
