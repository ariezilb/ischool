# coding=utf-8
from . import user_management
from flask import render_template, session, url_for, redirect, g
from flask.globals import request
from ischool.site.blueprints import ischool
from ischool.site.utils.auth import ischool_route, Permissions, is_person_allowed
from ischool.site.utils.encoding import jsonify
from ischool.models.arrays import Array
from ischool.models.exercise.exercise import Exercise
from ischool.models.people import Person


@ischool_route('/', Permissions.logged_in, Permissions.guest)
def default_view():
    if is_person_allowed(g.person, Permissions.logged_in):
        return redirect(url_for('ischool.course_view', course=g.course))
    return redirect(url_for('ischool.login_view'))


@ischool_route('/tracks/', Permissions.all_staffs, course_dependant=True)
def tracks():
    pass


@ischool_route('/tracks/<track_name>', Permissions.all_staffs, course_dependant=True)
def track_view(track_name):
    pass


@ischool_route('/api/search/<string:query>', Permissions.all_staffs, course_dependant=True)
def omni_search(query):
    result = {}

    if request.args.get('people', 'false') == 'true':
        people = Person.query.filter(Person.name.like(u"{0}%".format(query)) |
                                     Person.name.like(u"% {0}%".format(query)) |
                                     Person.course_number.like(u"{0}".format(query))).limit(5).all()
        result['people'] = people

    if request.args.get('arrays', 'false') == 'true':
        arrays = Array.query.filter(Array.name.like(u"{0}%".format(query)) |
                                    Array.name.like(u"% {0}%".format(query))).limit(5).all()
        result['arrays'] = arrays

    if request.args.get('exercises', 'false') == 'true':
        exercises = Exercise.query.filter(Exercise.name.like(u"{0}%".format(query)) |
                                          Exercise.name.like(u"% {0}%".format(query))).limit(5).all()
        result['exercises'] = exercises

    return jsonify(result)


def error_view(http_code, error_title, error_desc):
    if not http_code:
        http_code = 500

    return render_template('errors.html', error_title=error_title, http_code=http_code), \
           http_code

@ischool.errorhandler(404)
def page_not_found(err):
    return error_view(404, u'העמוד המבוקש אינו קיים.', err)


@ischool.errorhandler(403)
def access_denied(err):
    return error_view(403, u'העמוד המבוקש אסור לצפיה.', err)


@ischool.errorhandler(401)
def unauthorized(err):
    return error_view(401, u'אין לך הרשאות לצפות בעמוד זה.', err)
