# coding=utf-8
from flask import render_template, g, redirect
from ischool.site.utils.auth import ischool_route, is_person_allowed, Permissions
from ischool.site.utils.encoding import get_filters
from ischool.site.utils.flask_restless import create_api
from ischool.models import Course


create_api(Course,
           Permissions.all_staffs,
           results_per_page=None,
           methods=['GET', 'PUT', 'DELETE', 'POST'])


@ischool_route('/courses', Permissions.all_staffs)
def courses_view():
    return render_template('courses.html')


@ischool_route('/api/courses/mine', Permissions.all_staffs)
def get_course():
    return redirect("/api/courses?q=%s" % get_filters("id == %d" % g.course.id))


@ischool_route('/courses/<course(nullable=False):course>/', Permissions.all_staffs, Permissions.course_checkers,
                Permissions.students)
def course_view():
    if is_person_allowed(g.person, Permissions.all_staffs):
        return render_template('course_overview.html', course=g.course)
    elif is_person_allowed(g.person, Permissions.course_checkers) and g.person.role == 'checker':
        return render_template('checkers_view.html', course=g.course)
    else:
        return render_template('assignment.html', course=g.course)
