# coding=utf-8
import json
from flask.templating import render_template
from ischool.site.utils.auth import ischool_route, Permissions
from ischool.site.utils.flask_restless import create_api

from ischool.models import Group
from ischool.models.arrays import Array
from ischool.models.people import Person

create_api(Group,
           Permissions.all_staffs,
           methods=['GET', 'POST', 'PUT', 'DELETE'])


@ischool_route('/groups/<int:group_id>', Permissions.all_staffs)
def group_view(group_id):
    group = Group.query.get_or_404(group_id)
    return render_template('group.html', group=group)

@ischool_route("/api/groups/all-students/<int:group_id>", Permissions.all_staffs)
def all_group_students(group_id):
    group = Group.query.get_or_404(group_id)
    members_list = []
    for member in group.members:
        members_list.append({
            "id": member.id,
            "name": member.name
        })
    return json.dumps(members_list)

@ischool_route("/api/groups/all-groups/<int:array_id>", Permissions.all_staffs)
def groups_by_array(array_id):
    array = Array.query.get_or_404(array_id)
    groups = array.groups
    groups_list = []
    for group in groups:
        groups_list.append({
            "name": group.name,
            "id": group.id,
            "students": [student.id for student in group.members]
        })
    return json.dumps(groups_list)
