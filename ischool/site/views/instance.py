# coding=utf-8
from datetime import datetime
import os

from flask import request, jsonify, url_for, g, abort

from ischool.config import IschoolConfig as config
from ischool.site.extensions import db
from ischool.site.utils.jinja_filters import file_url
from ischool.site.utils.auth import ischool_route, Permissions
from ischool.site.utils.flask_restless import create_api
from ischool.site.utils.path import get_filename_tuple
from ischool.models import LessonInstance, File, FileUsage
from ischool.models.instances import InstanceTask
from ischool.models.serialization import jsonify_object


__author__ = 'user1,user4'


create_api(LessonInstance,
           Permissions.all_staffs,
           include_methods=['hebrew_type', 'luz_hours',
                            'weeknum', 'start_time',
                            'placements.is_needs_verifying',
                            'placements.is_all_verified',
                            'run_files.file_name',
                            'run_files.file_type',
                            'run_files.file_full_name',
                            'run_files.file_url',
                            'fu_files.file_name',
                            'fu_files.file_type',
                            'fu_files.file_full_name',
                            'fu_files.file_url',
                            ],
           methods=['GET', 'POST', 'PUT', 'DELETE'])


create_api(InstanceTask,
           Permissions.all_staffs,
           collection_name='tasks',
           methods=['GET', 'POST', 'DELETE'])


def upload_file_and_create_file_usage(filename, request_file):
    """
    Uploads a file from http request, creates the matching FileUsage and adds the file and file usage to the db session.
    Returns the created File and FileUsage.
    """
    #TODO: filename = secure_filename(file.filename) -- Test it before using it to see if hebrew chars are allowed
    filepath = os.path.join(config.UPLOAD_FOLDER, filename)
    #TODO: These both don't work - why Werkzeug why??! --User2 22.10.13
    f = request.files['file']
    if f:
        f.save(filepath)

    (file_no_ext, ext) = get_filename_tuple(filename)
    f = File(file_no_ext, ext, g.person, filepath)
    file_usage = FileUsage(f.last_revision)
    db.session.add(f)
    db.session.add(file_usage)
    return f, file_usage


@ischool_route('/instances/<int:instance_id>/files/<filename>', Permissions.all_staffs,
               methods=['PUT'])
def upload_inst_file(instance_id, filename):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    inst = LessonInstance.query.get_or_404(instance_id)
    f, file_usage = upload_file_and_create_file_usage(filename, request.files['file'])
    inst.run_files.append(file_usage)
    db.session.commit()

    return jsonify(id=f.id, file_url=file_url(f.last_revision), file_full_name=filename,
                   file_type=file_usage.file_type())


@ischool_route('/instances/<int:instance_id>/files/<int:file_id>', Permissions.all_staffs,
               methods=['DELETE'])
def delete_inst_file(instance_id, file_id):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    inst = LessonInstance.query.get_or_404(instance_id)
    file_usage = FileUsage.query.get_or_404(file_id)
    file_usage.inst_run_id = None
    delete_file_usage_if_unused(file_usage)
    db.session.commit()

    return "{}"

@ischool_route('/instances/<int:instance_id>/fus/<filename>', Permissions.all_staffs,
               methods=['PUT'])
def upload_inst_fu(instance_id, filename):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    inst = LessonInstance.query.get_or_404(instance_id)
    f, file_usage = upload_file_and_create_file_usage(filename, request.files['file'])
    inst.fu_files.append(file_usage)
    db.session.commit()

    return jsonify(id=f.id, file_url=file_url(f.last_revision), file_full_name=filename,
                   file_type=file_usage.file_type())


@ischool_route('/instances/<int:instance_id>/fus/<int:fu_id>', Permissions.all_staffs,
               methods=['DELETE'])
def delete_inst_fu(instance_id, fu_id):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    inst = LessonInstance.query.get_or_404(instance_id)
    file_usage = FileUsage.query.get_or_404(fu_id)
    file_usage.inst_fu_id = None
    delete_file_usage_if_unused(file_usage)
    db.session.commit()

    return "{}"


def delete_file_usage_if_unused(file_usage):
    """
    If no instance uses the FileUsage as a run file or a follow up file the FileUsage should be deleted.
    """
    if not file_usage.inst_fu_id and not file_usage.inst_run_id:
        db.session.delete(file_usage)


### Replaced by POST method
# @ischool_route('/instances/<int:instance_id>/tasks', Permissions.all_staffs, methods=['PUT'])
# def new_task(instance_id):
#     if not g.person.is_staff(g.course):
#         # Protect from tricksy hobbitses
#         abort(404)
#
#     data = request.json
#
#     inst = LessonInstance.query.get(instance_id)
#
#     text = data['text'] if 'text' in data else u''
#
#     task = InstanceTask(text, datetime.now())
#     task.instance_id = inst.id
#
#     db.session.add(task)
#     db.session.commit()
#
#     updates = [jsonify_object(task, 'text', task.text),
#                jsonify_object(task, 'is_done', task.is_done),
#                jsonify_object(inst, 'unfinished-tasks', len(inst.get_unfinished_tasks())),
#                ]
#     return jsonify(id=task.id, url=url_for('ischool.array_run_view', arr=task.instance.array),
#                    updates=updates)
