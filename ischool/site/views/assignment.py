# coding=utf-8
from datetime import datetime
import json
import os
from flask import render_template, g, redirect, url_for, request, jsonify, send_from_directory, abort


from ischool.config import IschoolConfig as config
from ischool.site.extensions import db
from ischool.site.utils.auth import ischool_route, Permissions
from ischool.site.utils.path import get_filename_tuple
from ischool.site.utils.jinja_filters import student_copy, student_attachment
from ischool.models import File, FileUsage
from ischool.models.exercise.assignment import Assignment
from ischool.models.exercise.exercise import Exercise
from ischool.site.utils.flask_restless import create_api
from ischool.models.exercise.submission import *
from ischool.models.exercise.review import Review
from ischool.models.exercise.submission_lock import SubmissionLock
from ischool.models.medida.scores import SubmissionScore
from ischool.models import Person, Point, Message, Array

def is_my_submission(submission_id, *args, **kwargs):
    submission = FileSubmission.query.get_or_404(submission_id)
    return submission.assignment.person.id == g.person.id


def is_my_assignment(assignment_id, *args, **kwargs):
    assignment = Assignment.query.get_or_404(assignment_id)
    return assignment in g.person.assignments

# Student assignment view page
@ischool_route('/arrays/<int:array_id>/assignments/<int:assignment_id>',
               Permissions.all_staffs, validator=is_my_assignment)
def assignment_view(array_id, assignment_id):
    assignment = Assignment.query.get_or_404(assignment_id)
    if not g.person.is_staff(g.course) and assignment not in g.person.assignments:
        # Protect from tricksy hobbitses
        abort(404)

    return render_template('assignment.html',
                           course=g.course,
                           assignments=g.person.assignments,
                           assignment=assignment,
                           array_id=array_id)

# Student assignment submit
@ischool_route('/arrays/<int:array_id>/assignments/<int:assignment_id>/submit',
               Permissions.all_staffs, validator=is_my_assignment,
               methods=['POST'])
def submit_assignment(array_id, assignment_id):
    assignment = Assignment.query.get(assignment_id)
    # TODO: add check logic to PermissionsEnum somehow
    if not g.person.is_staff(g.course) and assignment not in g.person.assignments:
        # Protect from tricksy hobbitses
        abort(404)

    # Prevent double submission
    if assignment.state in ('submitted', 'done'):
        return redirect(url_for('ischool.assignment_view', array_id=array_id, assignment_id=assignment_id))

    if assignment.exercise.submit_type == 'files':
        file = request.files['file']
        if file is None:
            raise ValueError('No files were sent.')

        #filename = file.filename
        import datetime
        filename = "submisson_of_ex" + str(assignment.exercise.id) + "_student" + str(g.person.course_number) + "_assignment" + \
                   str(assignment.id) + "_in_" + str(datetime.datetime.now().strftime("%I:%M%B%d")) + "." + \
                   file.filename.split('.')[-1]
        full_path = os.path.join(config.UPLOAD_FOLDER, filename)
        file.save(full_path)
        (file_no_ext, ext) = get_filename_tuple(filename)
        f = File(file_no_ext, ext, g.person, full_path)

        assignment.state = 'submitted'

        submission = FileSubmission()

        submission.file_usage = FileUsage(f.last_revision)
        submission.assignment = assignment

        db.session.add(f)
        db.session.add(submission)
    elif assignment.exercise.submit_type == 'ok':
        assignment.state = 'submitted'

        submission = Submission()
        submission.assignment = assignment

        db.session.add(submission)
    elif assignment.exercise.submit_type == 'svn':
        svn_path = request.form['svn']
        assignment.state = 'submitted'

        submission = SvnPathSubmission()
        submission.svn_path = svn_path
        submission.assignment = assignment

        db.session.add(submission)
    else:
        raise ValueError("Unknown handler for exercise type '%s'." % assignment.exercise.submit_type)

    db.session.commit()

    return redirect(url_for('ischool.assignment_view', array_id=array_id, assignment_id=assignment_id))

# Create an automated point when student finishes an exercise and submits it.
def submit_assignment_point(assignment, room_id):
    point_person = Person.query.get_or_404(assignment.person_id)
    point = Point(person=point_person, type='exercise', assignment_id=assignment.id, student_status='hidden', room_id=room_id)
    msg = Message(person=point_person, point=point, text="סיימתי")
    db.session.add(point)
    db.session.add(msg)
    db.session.commit()

@ischool_route('/submissions/<int:submission_id>', Permissions.all_staffs, Permissions.course_checkers,
               validator=is_my_submission, methods=['GET'])
def download_submission_file(submission_id):
    submission = FileSubmission.query.get_or_404(submission_id)
    # We encode the filename to ascii, ignoring any non-ascii characters
    # This is done because werkzeug doesn't do hebrew files well
    # It is ok because we only save the original file extension.
    # If it's hebrew, we don't care anyway.
    return send_from_directory(config.STORAGE_PATH, submission.file_usage.file_revision_id,
                               attachment_filename=submission.file_usage.file_revision.file_full_name().encode('ascii', 'ignore'),
                               as_attachment=True)

@ischool_route('/submissions/review/<int:submission_id>', Permissions.all_staffs, Permissions.course_checkers,
               validator=is_my_submission, methods=['GET'])
def download_submission_review_file(submission_id):
    submission = Submission.query.get_or_404(submission_id)

    return send_from_directory(config.STORAGE_PATH, submission.review.file.file_revision_id,
                               attachment_filename=submission.review.file.file_revision.file_full_name(),
                               as_attachment=True)

@ischool_route('/submissions/svn/<int:submission_id>', Permissions.all_staffs, Permissions.course_checkers,
               validator=is_my_submission, methods=['GET'])
def get_submission_svn(submission_id):
    submission = SvnPathSubmission.query.get_or_404(submission_id)
    svn_path = submission.svn_path
    return redirect(svn_path)

@ischool_route('/arrays/<array:arr>/assignments/<int:assignment_id>/review',
               Permissions.all_staffs, Permissions.course_checkers)
def assignment_review_view(arr, assignment_id):
    assignment = Assignment.query.get_or_404(assignment_id)
    arr = arr()
    is_checker = g.person.is_checker()
    return render_template('assignment_review.html', array=arr, assignment=assignment, checker=is_checker)

@ischool_route('/assignments/<int:assignment_id>/start-checking',
               Permissions.current_staff, Permissions.course_checkers, methods=['POST'])
def start_checking_assignment(assignment_id):
    assignment = Assignment.query.get_or_404(assignment_id)
    assert assignment.state != 'in_progress', 'Assignment is still in progress'
    assignment.state = 'checking'
    assignment.last_submission().lock = SubmissionLock(locker_id = g.person.id)

    db.session.add(assignment.last_submission().lock)
    db.session.commit()
    return jsonify({})


@ischool_route('/assignments/<int:assignment_id>/stop-checking',
               Permissions.current_staff, Permissions.course_checkers, methods=['POST'])
def stop_checking_assignment(assignment_id):
    assignment = Assignment.query.get_or_404(assignment_id)
    assert assignment.state == 'checking', 'Assignment is not in "checking" state'

    assignment.state = 'submitted'
    assignment.last_submission().lock = None

    db.session.commit()
    return jsonify({})


@ischool_route('/assignments/<int:assignment_id>/submit-review',
               Permissions.all_staffs, Permissions.course_checkers, methods=['POST'])
def submit_assignment_review(assignment_id):
    assignment = Assignment.query.get_or_404(assignment_id)
    assignment.is_read = False
    last_submission = assignment.submissions[-1]

    data = json.loads(request.data)
    assignment.state = data['state']
    assignment.last_submission().lock = None
    last_submission.review = Review(
                                reviewer_id = g.person.id,
                                comment_for_staff = data['comment_for_staff'],
                                comment_for_student = data['comment_for_student']
                              )

    for score in last_submission.scores:
        db.session.delete(score)

    scores = data['scores']
    for criterion_id in data['scores']:
        score = SubmissionScore()
        score.score = scores[criterion_id][0]
        score.comment = scores[criterion_id][1]
        score.criterion_id = criterion_id
        db.session.add(score)
        score.submission = last_submission

    db.session.commit()

    return redirect(url_for('ischool.assignment_review_view', arr=assignment.exercise.array, assignment_id=assignment_id))
    #return jsonify({'url': '/assignments/' + str(assignment_id) + '/review'})

@ischool_route('/assignments/<int:assignment_id>/resend-no-review',
               Permissions.all_staffs, Permissions.course_checkers, methods=['POST'])
def resend_without_review(assignment_id):
    assignment = Assignment.query.get_or_404(assignment_id)
    assignment.is_read = False
    assignment.state = Assignment.RESEND
    assignment.last_submission().review = Review(reviewer_id = g.person.id, comment_for_student=u"<ללא הערות>")
    assignment.last_submission().lock = None

    db.session.commit()
    return redirect(url_for('ischool.assignment_review_view', arr=assignment.exercise.array, assignment_id=assignment_id))

@ischool_route('/assignments/<int:assignment_id>/submit-review-file',
               Permissions.all_staffs, Permissions.course_checkers, validator=is_my_assignment,
               methods=['POST'])
def submit_assignment_review_file(assignment_id):
    assignment = Assignment.query.get(assignment_id)
    last_submission = assignment.submissions[-1]

    if assignment.exercise.submit_type == 'files':
        file = request.files['file']
        if file is None:
            raise ValueError('No files were sent.')

        import datetime
        filename = "review_of_ex" + str(assignment.exercise.id) + "_student" + str(g.person.course_number) + "_assignment" + \
                   str(assignment.id) + "_in_" + str(datetime.datetime.now().strftime("%I:%M%B%d")) + "." + \
                   file.filename.split('.')[-1]
        full_path = os.path.join(config.UPLOAD_FOLDER, filename)
        file.save(full_path)
        f = File('.'.join(filename.split('.')[:-1]), filename.split('.')[-1], g.person, full_path)

        f_usage = FileUsage(f.last_revision)

        db.session.add(f)
        db.session.add(f_usage)
        db.session.commit()

        last_submission.review.file_id = f_usage.id
        db.session.add(last_submission)

        db.session.commit()
        return redirect(url_for('ischool.assignment_review_view', arr=assignment.exercise.array, assignment_id=assignment_id))

def get_many_assignments_postprocessor(result=None, **kw):
    result['objects'] = [e for e in result['objects'] if e['person_id'] == g.person.id]

#create_api(Assignment,
#           Permissions.students,
#           postprocessors={
#               'GET_MANY': [get_many_assignments_postprocessor],
#           },
#           exclude_columns=['version'],
#           collection_name='student_assignments',
#           methods=['GET'])

@ischool_route('/api/assignments/arrays',
               Permissions.students,
               methods=['GET'])
def get_assignments_arrays():
    arrays = Array.query.select_from(Assignment)                  \
                  .filter(Assignment.person_id == g.person.id)    \
                  .join(Assignment.exercise)                      \
                  .join(Exercise.array)                           \
                  .all()
    return json.dumps([{'id': arr.id, 'name': arr.name} for arr in arrays])

@ischool_route('/api/arrays/<int:array_id>/assignments',
               Permissions.students,
               methods=['GET'])
def get_assignments_of_array(array_id):
    assignments = Assignment.query                                \
                  .filter(Assignment.person_id == g.person.id)    \
                  .join(Assignment.exercise)                      \
                  .filter(Exercise.array_id == array_id)          \
                  .all()
    return json.dumps([
        {
            'id': ass.id,
            'exercise_id': ass.exercise.id,
            'name': ass.exercise.name,
            'state': ass.state_for_student,
            'is_read': ass.is_read
        } for ass in assignments])

def assignment_to_json(ass):
      return {'id': ass.id,
              'name': ass.exercise.name,
              'state': ass.state_for_student,
              'is_read': ass.is_read,
              'submit_type': ass.exercise.submit_type,
              'submissions': [submission_to_json(sub, ass.exercise.submit_type) for sub in ass.submissions],
              'file': student_copy(ass.exercise),
              'attachment': student_attachment(ass.exercise),
              'submit_url': url_for('ischool.submit_assignment', assignment_id=ass.id, array_id=ass.exercise.array_id)
              }

def submission_to_json(sub, submit_type):
    res = {'created_at': sub.created_at.isoformat(),
           'comments': sub.review.comment_for_student if sub.review else '',
           'url': '',
           'review_file': url_for('ischool.download_submission_review_file', submission_id=sub.id) if sub.review is not None and sub.review.file_id is not None else None,
           }
    if submit_type == 'files':
        res['url'] = url_for('ischool.download_submission_file', submission_id=sub.id)
    elif submit_type == 'svn':
        res['url'] = url_for('ischool.get_submission_svn', submission_id=sub.id)

    return res

@ischool_route('/api/assignment/<int:assignment_id>',
               Permissions.students,
               methods=['GET'])
def get_assignment(assignment_id):
    ass = Assignment.query.get_or_404(assignment_id)
    if ass.person_id != g.person.id:
        abort(404)
    return json.dumps(assignment_to_json(ass))

@ischool_route('/api/assignment/<int:assignment_id>/read',
               Permissions.students,
               methods=['PUT'])
def mark_assignment_as_read(assignment_id):
    ass = Assignment.query.get_or_404(assignment_id)
    if ass.person_id != g.person.id:
        abort(404)
    ass.is_read = True
    db.session.commit()
    return json.dumps({'id': assignment_id})
