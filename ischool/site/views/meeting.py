__author__ = 'aviad'
from ischool.models import Meeting
from flask import render_template, send_file, url_for, redirect, g, abort, request
from ischool.site.extensions import db
from datetime import datetime
from datetime import date
import json
from ischool.site.utils.auth import ischool_route, Permissions
from ischool.models.people import Person

@ischool_route("/api/meetings/student/<int:student_id>", Permissions.all_staffs)
def get_students_meetings(student_id):
    meetings = Meeting.query.filter(Meeting.student_id == student_id)
    meetings_list = []
    for meeting in meetings:
        meetings_list.append({
            'id': meeting.id,
            'title': meeting.title,
            'last_updated': str(meeting.last_updated),
            'scheduled_time': meeting.scheduled_time.strftime("%Y-%m-%d"),
            'agenda': meeting.agenda,
            'summary': meeting.summary,
            'short_summary': meeting.short_summary,
            'student_id': meeting.student_id,
            'staff_id': meeting.staff_id,
            'staff_name': meeting.staff.name,
            'student_name': meeting.student.name
        })
    return json.dumps(meetings_list)

@ischool_route("/api/meetings/<int:meeting_id>", Permissions.all_staffs)
def get_meeting(meeting_id):
    meetings = Meeting.query.filter(Meeting.id == meeting_id)
    meeting = meetings[0];
    meeting_dict = {
        'id': meeting.id,
        'title': meeting.title,
        'last_updated': str(meeting.last_updated),
        'scheduled_time': meeting.scheduled_time.strftime("%Y-%m-%d"),
        'agenda': meeting.agenda,
        'summary': meeting.summary,
        'short_summary': meeting.short_summary,
        'student_id': meeting.student_id,
        'staff_id': meeting.staff_id,
        'staff_name': meeting.staff.name,
        'student_name': meeting.student.name
    }
    return json.dumps(meeting_dict)

@ischool_route('/api/meetings/student/<int:student_id>', Permissions.all_staffs, methods=['POST'])
def add_meeting_to_student(student_id):
    """
    Adds a meeting to the a student.
    """
    student = Person.query.get(student_id)
    data = json.loads(request.data)

    meeting_title = data['meeting_title']
    meeting_agenda = data['meeting_agenda']
    meeting_summary = data['meeting_summary']
    meeting_short_summary = data['meeting_short_summary']
    meeting_scheduled_time = data['meeting_scheduled_time']

    meeting = Meeting()
    meeting.student_id = student_id
    meeting.title = meeting_title
    meeting.agenda = meeting_agenda
    meeting.summary = meeting_summary
    meeting.short_summary = meeting_short_summary
    meeting.scheduled_time = meeting_scheduled_time
    meeting.last_updated = datetime.now()
    meeting.staff_id = g.person.id

    db.session.add(meeting)
    db.session.commit()

    response = {'id': meeting.id}
    return json.dumps(response)


@ischool_route('/api/meetings/<int:meeting_id>', Permissions.all_staffs, methods=['PUT'])
def update_meeting(meeting_id):

    #Updates a given meeting.

    meeting = Meeting.query.get(meeting_id)
    data = json.loads(request.data)

    meeting_title = data['meeting_title']
    meeting_agenda = data['meeting_agenda']
    meeting_summary = data['meeting_summary']
    meeting_short_summary = data['meeting_short_summary']
    meeting_scheduled_time = data['meeting_scheduled_time']

    meeting.title = meeting_title
    meeting.agenda = meeting_agenda
    meeting.summary = meeting_summary
    meeting.short_summary = meeting_short_summary
    meeting.scheduled_time = meeting_scheduled_time
    meeting.last_updated = datetime.now()
    db.session.commit()

    response = {'id': meeting.id}
    return json.dumps(response)

@ischool_route('/api/meetings/<int:meeting_id>', Permissions.all_staffs, methods=['DELETE'])
def delete_meeting(meeting_id):
    meeting = Meeting.query.get(meeting_id)
    db.session.delete(meeting)
    db.session.commit()
    return ""
