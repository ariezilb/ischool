#coding=utf-8
from werkzeug.datastructures import Headers
from flask import abort, Response, request, safe_join, url_for, send_from_directory
import os
import uuid
from ischool.config import IschoolConfig as config
from ischool.site.blueprints import ischool
from ischool.models import FileRevision


# TODO: Add permissions


@ischool.route('/files/', methods=['OPTIONS'])
@ischool.route('/files/<uuid:rev_id>-<slug>', methods=['OPTIONS'])
def file_options(rev_id=None, slug=None):
    """
    This returns a WebDAV OPTIONS response
    """
    hlist = [('Content-Type', 'text/html'),
             ('Content-Length', '0'),
             ('DASL', '<DAV:sql>'),
             ('DAV', '1,2'),
             ('Server', 'DAV/2'),
             ('MS-Author-Via', 'DAV'),
             ('Public', 'OPTIONS, TRACE, GET, HEAD, DELETE, PUT, COPY, MOVE, '
                        'PROPFIND, PROPPATCH, SEARCH, LOCK, UNLOCK'),
             ('Allow', 'OPTIONS, TRACE, GET, HEAD, DELETE, PUT, COPY, MOVE, '
                       'PROPFIND, PROPPATCH, SEARCH, LOCK, UNLOCK')]
    return Response(headers=Headers.linked(hlist))


@ischool.route('/files/<uuid:rev_id>-<slug>', methods=['GET', 'HEAD'])
def file_get(rev_id, slug):
    """
    Returns a file revision's data as attachment
    """
    rev = FileRevision.query.get_or_404(rev_id)

    # I don't put the filename for the attachment because headers usually don't
    # support unicode (and the file name is likely to contain these).
    # The client will hopefully degrade to the the filename from the path
    as_attachment = request.args.get('down', True) != 'False'
    if as_attachment:
        return send_from_directory(config.STORAGE_PATH, rev.get_path(),
                                   as_attachment=as_attachment, attachment_filename='')
    else:
        response = send_from_directory(config.STORAGE_PATH, rev.get_path(), mimetype='application/pdf')
        response.headers.add('Content-Disposition', 'inline')
        response.headers.add('Content-Transfer-Encoding', 'binary')
        return response


@ischool.route('/files/<uuid:rev_id>-<slug>', methods=['PUT'])
def file_put(rev_id, slug):
    rev = FileRevision.query.get(rev_id)
    if not rev:
        print "file not found"
        abort(404)

    filepath = safe_join(config.STORAGE_PATH, rev.get_path())
    filedir = os.path.dirname(filepath)
    if not os.path.exists(filedir):
        os.makedirs(filedir)

    with open(filepath, 'wb') as f:
        f.write(request.stream.read())

    return Response('200 OK')


@ischool.route('/files/<uuid:rev_id>-<slug>', methods=['LOCK'])
def lock(rev_id, slug):
    token = str(uuid.uuid1())
    username = 'flora'
    xml = '''<?xml version="1.0" encoding="utf-8" ?>
    <D:prop xmlns:D="DAV:">
        <D:lockdiscovery>
            <D:activelock>
                <D:lockscope>
                    <D:exclusive/>
                </D:lockscope>
                <D:locktype>
                    <D:write/>
                </D:locktype>
                <D:depth>0</D:depth>
                <D:timeout>Second-179</D:timeout>
                <D:owner>%s</D:owner>
                <D:locktoken>
                    <D:href>opaquelocktoken:%s</D:href>
                </D:locktoken>
            </D:activelock>
        </D:lockdiscovery>
    </D:prop>''' % (username, token)

    hlist = [('Content-Type', 'text/xml'),
             ('Lock-Token', '<urn:uuid:%s>' % token)]

    return Response(xml, headers=Headers.linked(hlist))
    #, status = '423 Locked'


@ischool.route('/files/<uuid:rev_id>-<slug>', methods=['UNLOCK'])
def file_unlock(rev_id, slug):
    return Response(status='204 No Content')


@ischool.route('/files/<uuid:rev_id>-<slug>', methods=['PROPFIND'])
def file_propfind(rev_id, slug):
    rev = FileRevision.query.get(rev_id)

    xml = '''<?xml version="1.0" encoding="UTF-8"?>
    <D:multistatus xmlns:D="DAV:">
        <D:response>
            <D:href>%s</D:href>
            <D:propstat>
                <D:prop/>
                <D:status>HTTP/1.1 200 OK</D:status>
            </D:propstat>
            <D:propstat>
                <D:prop>
                    <D:isreadonly/>
                </D:prop>
                <D:status>HTTP/1.1 404 Not Found</D:status>
            </D:propstat>
        </D:response>
    </D:multistatus>''' % url_for('ischool.file_propfind', rev_id=rev_id, slug=rev.file_name())

    hlist = [('Content-Type', 'text/xml')]
    return Response(xml, headers=Headers.linked(hlist))
