# coding=utf-8

from flask import render_template, request, g, json
from ischool.site.extensions import db
from ischool.site.utils.auth import ischool_route, Permissions
from ischool.site.utils.flask_restless import create_api
from ischool.models import Array, Group, EducationUnit, people, LessonInstance
import json
from ischool.models.people import Person


def remove_unit_preprocessor(instance_id=None, **kw):
    # FIXME: order of instances (orphans, at least) gets messy upon deletion
    unit = EducationUnit.query.get_or_404(instance_id)
    arr = unit.array

    # Fix orders of later units
    later_units = filter(lambda other: other.order > unit.order, arr.units)
    for later_unit in later_units:
        later_unit._order -= 1

    # Fix orders of orphan instances
    orphan_instances = arr.orphan_instances
    if len(orphan_instances) > 0:
        last_order = max(orphan_instances, key=lambda inst: inst.order).order
    else:
        last_order = 0

    for i, inst in enumerate(unit.instances):
        inst._order = last_order + (i + 1)
        # We don't want to remove the instances recursively, so we'll disconnect them from the unit
        unit.instances.remove(inst)


def create_array_preprocessor(data=None, **kw):
    # Add default group
    default_group = Group(u'כל העולם')
    data['groups'] = [default_group]


def remove_array_preprocessor(instance_id=None, **kw):
    # TODO: implement
    raise NotImplementedError("Deep-delete needed (exercises, etc.)")


create_api(Array,
           Permissions.all_staffs,
           include_methods=['manager.associated_person',
                            'files.file_type', 'files.file_name', 'files.file_url'],
           exclude_columns=['_order'],
           preprocessors={
               'POST': [create_array_preprocessor],
               'DELETE': [remove_array_preprocessor],
           },
           methods=['GET', 'POST', 'PUT', 'DELETE'])
create_api(EducationUnit,
           Permissions.all_staffs,
           collection_name='units',
           exclude_columns=['_order'],
           preprocessors={
               'DELETE': [remove_unit_preprocessor],
           },
           methods=['GET', 'POST', 'PUT', 'DELETE'])

@ischool_route('/arrays/', Permissions.all_staffs, course_dependant=True)
def arrays_view():
    return render_template('arrays.html', course=g.course)


# TODO: Use the following piece of code when restoring the bizur & placements logic
"""
# TODO: RESTLESS: Remove this (replace by POST method)
@app.route('/placements/<placement:placement>/bizur/<person:person>', methods=['DELETE'])
def remove_bizur(placement, person):
    placement = placement()
    person = person()
    bizur = placement.get_bizur_by_person_id(person.id)

    placement.bizurs.remove(bizur)
    db.session.delete(bizur)
    db.session.commit()

    return jsonify({})


# TODO: RESTLESS: Remove this (replace by PUT method)
@app.route('/placements/<placement:placement>/bizur/<person:person>/set-verification', methods=['POST'])
def set_bizur_verification(placement, person):
    placement = placement()
    person = person()
    bizur = placement.get_bizur_by_person_id(person.id)

    bizur.is_verified = request.json['value']

    db.session.commit()

    return jsonify({'id': person.id,
                    'is_verified': jsonify_object(placement, 'is_verified', request.json['value']),
                    'is-all-verified': jsonify_object(placement, 'is-all-verified',
                                                      placement.instance.is_all_placements_verified),
                    })


# TODO: RESTLESS: Remove this (replace by DELETE method)
@app.route('/placements/<placement:placement>/bizur', methods=['PUT'])
def remove_placement(placement):
    data = request.json
    placement = placement()

    person = Person.query.filter((Person.first_name + ' ' + Person.last_name).startswith(data['name'])).first()
    if person is None:
        raise XHRError(u"לא נמצא אדם שמתחיל ב%s." % data['name'])

    if len(placement.get_bizurs_by_person_id(person.id)) > 0:
        raise XHRError(u"%s כבר מבוזר למופע." % data['name'])

    new_bizur = Bizur(person, placement)
    placement.bizurs.append(new_bizur)

    db.session.commit()
    return jsonify({})
"""


@ischool_route('/arrays/<array:arr>', Permissions.all_staffs)
def array_default_view(arr):
    preferred_view = request.cookies.get('array_preferred_view', '')
    if preferred_view == 'plan':
        return array_plan_view(arr)
    elif preferred_view == 'run':
        return array_run_view(arr)
    else:
        return array_plan_view(arr)


def group_of_array(group, arr):
    if group:
        group = group()

    if group in arr.groups:
        return group

    if len(arr.groups) > 0:
        return arr.groups[0]
    else:
        return None

@ischool_route(['/arrays/<array(join="manager,instances"):arr>/plan',
    '/arrays/<array(join="manager,instances"):arr>/plan/groups/<group:group>'],
    Permissions.all_staffs)
def array_plan_view(arr, group=None):
    arr = arr()
    group = group_of_array(group, arr)
    units = sorted(arr.units, key=lambda u: u.order)

    return render_template('array-plan.html', array=arr, group=group, units=units, people_list=people.get_staff_dropdown())

@ischool_route(['/arrays/<array(join="manager,instances"):arr>/run',
    '/arrays/<array(join="manager,instances"):arr>/run/groups/<group:group>'],
    Permissions.all_staffs)
def array_run_view(arr, group=None):
    arr = arr()
    group = group_of_array(group, arr)

    return render_template('array-run.html', array=arr, group=group, people_list=people.get_staff_dropdown())


@ischool_route('/arrays/<array(join="manager,instances"):arr>/set_manager/<int:new_manager_id>', Permissions.all_staffs)
def array_set_manager(arr, new_manager_id):
    arr = arr()
    arr.manager_id = new_manager_id

    return json.dumps(new_manager_id)

@ischool_route('/api/arrays/all-students/<int:array_id>', Permissions.all_staffs)
def all_students(array_id):
    """
    Students mapped to their group (or no group at all) in the given array.
    """
    array = Array.query.get_or_404(array_id)
    student_group_mapping = {}
    for student in Person.query.filter(Person.role == 'student'):
        student_group_mapping[student.id] = {'name': student.name, 'groups': []}

    for group in array.groups:
        for member in group.members:
            student_group_mapping[member.id]['groups'].append({'id': group.id, 'name': group.name})

    students = [{'id': id, 'name': student_group_mapping[id]['name'], 'groups': student_group_mapping[id]['groups']}
                for id in student_group_mapping]

    return json.dumps(students)

@ischool_route('/api/arrays', Permissions.all_staffs)
def all_arrays():
    arrays = Array.query.all()

    return json.dumps([{'id': arr.id,
                        'name': arr.name,
                        'track_id': arr.track_id,
                        'is_archived': arr.is_archived,
                        }
                for arr in arrays])

@ischool_route('/api/2.0/arrays/<int:array_id>/instances', Permissions.all_staffs)
def instances_for_array(array_id):
    """
    This function is written because the create_api utility
    is ridiculously inefficient and causes thousands of db
    requests every time the route is normally called

    It returns all of the information needed by the call to the
    function in the matrix controller.
    """
    instances = LessonInstance.query.filter_by(array_id=array_id).join(EducationUnit)
    return json.dumps([
        {
            'unit': {
                'array_id': instance.unit.array_id,
                '_order': instance.unit._order,
                'name': instance.unit.name,
                'id': instance.unit.id,
                'group_id': instance.unit.group_id,
                'order': instance.unit.order,
                },
            'instance_exercises': [{
                'instance_id': ins_ex.instance_id,
                'exercise_id': ins_ex.exercise_id,
                'is_filler': ins_ex.is_filler,
                'id': ins_ex.id,
                'order': ins_ex.order,
                } for ins_ex in instance.instance_exercises],
            'unit_id': instance.unit_id,
            'name': instance.name,
            'id': instance.id,
        }
        for instance in instances])

@ischool_route('/api/arrays/is_archived', Permissions.all_staffs, methods=['PUT'])
def set_is_archived():
    params = request.json
    arr = Array.query.get_or_404(params['arr_id'])

    arr.is_archived = params['is_archived']

    db.session.commit()
    return ''
