# coding=utf-8
from flask import json

from ischool.site.utils.auth import Permissions, ischool_route
from ischool.models.tracks import Track


@ischool_route('/api/tracks', Permissions.all_staffs, methods=['GET'])
def get_tracks():
    tracks = Track.query.all()
    return json.dumps([{'name': t.name, 'id': t.id} for t in tracks])

@ischool_route('/api/tracks/<int:track>', Permissions.all_staffs, methods=['GET'])
def get_track(track):
    track = Track.query.get(track)
    return json.dumps({'name': track.name, 'id': track.id})
