import json
from flask.globals import request
from ischool.site.extensions import db
from ischool.site.utils.auth import Permissions, ischool_route
from ischool.models.arrays import Array
from ischool.models.exercise.exercise import Exercise
from ischool.models.medida.criteria import Criterion
from ischool.models.people import Person


@ischool_route('/api/criteria/array/<int:array_id>', Permissions.all_staffs, Permissions.course_checkers, methods=['GET'])
def get_array_criteria(array_id):
    """
    Returns all the criteria of a given array.
    """
    array = Array.query.get(array_id)
    criteria = [translate_criterion_to_dict(criterion) for criterion in array.criteria]
    return json.dumps(criteria)


@ischool_route('/api/criteria/exercise/<int:exercise_id>', Permissions.all_staffs, Permissions.course_checkers, methods=['GET'])
def get_exercise_criteria(exercise_id):
    """
    Returns all the criteria of a given array.
    """
    exercise = Exercise.query.get(exercise_id)
    specific_to_exercise = [criterion for criterion in exercise.criteria if not criterion.array_id]
    taken_from_array = [criterion for criterion in exercise.criteria if criterion.array_id]
    not_taken_from_array = [criterion for criterion in exercise.array.criteria if criterion not in taken_from_array]

    specific_to_exercise = [translate_criterion_to_dict(criterion) for criterion in specific_to_exercise]
    taken_from_array = [translate_criterion_to_dict(criterion) for criterion in taken_from_array]
    not_taken_from_array = [translate_criterion_to_dict(criterion) for criterion in not_taken_from_array]
    returned_criteria = {'specific_to_exercise': specific_to_exercise, 'taken_from_array': taken_from_array,
                         'not_taken_from_array': not_taken_from_array}
    return json.dumps(returned_criteria)

def translate_criterion_to_dict(criterion):
    """
    Translates a Criterion object to a dictionary
    Used for json purposes.
    """
    return {'id': criterion.id, 'name': criterion.name,
            'low_score_description': criterion.low_score_description,
            'middle_score_description': criterion.middle_score_description,
            'high_score_description': criterion.high_score_description}

@ischool_route('/api/criteria/array/<int:array_id>', Permissions.all_staffs, methods=['POST'])
def add_criterion_to_array(array_id):
    """
    Adds a criterion to the given array.
    """
    array = Array.query.get(array_id)
    data = json.loads(request.data)

    criterion_name = data['criterion_name']
    low_score_description = data['low_score_description']
    middle_score_description = data['middle_score_description']
    high_score_description = data['high_score_description']

    criterion = Criterion()
    criterion.array = array
    criterion.name = criterion_name
    criterion.low_score_description = low_score_description
    criterion.middle_score_description = middle_score_description
    criterion.high_score_description = high_score_description

    db.session.add(criterion)
    db.session.commit()

    response = {'id': criterion.id}
    return json.dumps(response)

@ischool_route('/api/criteria/exercise/<int:exercise_id>', Permissions.all_staffs, methods=['POST'])
def add_criterion_specific_to_exercise(exercise_id):
    """
    Adds a criterion to the given array.
    """
    exercise = Exercise.query.get(exercise_id)
    data = json.loads(request.data)

    criterion_name = data['criterion_name']
    low_score_description = data['low_score_description']
    middle_score_description = data['middle_score_description']
    high_score_description = data['high_score_description']

    criterion = Criterion()
    criterion.name = criterion_name
    criterion.low_score_description = low_score_description
    criterion.middle_score_description = middle_score_description
    criterion.high_score_description = high_score_description
    db.session.add(criterion)

    criterion.exercises.append(exercise)
    db.session.commit()

    response = {'id': criterion.id}
    return json.dumps(response)


@ischool_route('/api/criteria/<int:criterion_id>', Permissions.all_staffs, methods=['PUT'])
def update_criterion(criterion_id):
    """
    Updates a given criterion.
    """
    criterion = Criterion.query.get(criterion_id)
    data = json.loads(request.data)

    criterion_name = data['criterion_name']
    low_score_description = data['low_score_description']
    middle_score_description = data['middle_score_description']
    high_score_description = data['high_score_description']

    criterion.name = criterion_name
    criterion.low_score_description = low_score_description
    criterion.middle_score_description = middle_score_description
    criterion.high_score_description = high_score_description

    db.session.commit()

    response = {'id': criterion.id}
    return json.dumps(response)


@ischool_route('/api/criteria/<int:criterion_id>', Permissions.all_staffs, methods=['DELETE'])
def delete_criterion(criterion_id):
    """
    Deletes a given criterion.
    Pay attention:
    """
    criterion = Criterion.query.get(criterion_id)
    db.session.delete(criterion)
    db.session.commit()
    return ""
