# coding=utf-8

from flask import render_template, jsonify, g, abort, request
from ischool.site.extensions import db
from ischool.site.utils.auth import ischool_route, Permissions
from ischool.site.utils.auto_syncr import max_or_default, request_args_or_default
from ischool.models.arrays import Array
from ischool.models.groups import group_members_table
from ischool.models.units import EducationUnit
from ischool.models.people import Person, get_staff_dropdown
from ischool.models.exercise import Exercise, Assignment, Submission, Review, CheckersArrays, SubmissionLock
from ischool.models import Person, Point, Message, LessonInstance, InstanceExercise, Group
from sqlalchemy import text, func
from itertools import groupby

@ischool_route('/arrays/<array:arr>/matrix', Permissions.all_staffs)
def matrix_view(arr):
    arr = arr()

    instances = [instance_json(x) for x in
            LessonInstance.query.filter_by(array_id=arr.id).join(InstanceExercise).all()]
    groups = map(group_json, arr.groups)

    exercise_ids = [x['id'] for x in flatten([x['exercises'] for x in instances])]
    person_ids = [x.id for x in arr.groups[0].members]

    assignments = list(Assignment.query.filter(Assignment.exercise_id.in_(exercise_ids)).filter(
        Assignment.person_id.in_(person_ids)))
    assignments_by_keys = dict(((x.exercise_id, x.person_id), x) for x in assignments)
    assignments_by_id = dict((x.id, assignment_json(x)) for x in assignments)
    return render_template('matrix/matrix.html',
                           array=arr,
                           groups=arr.groups,
                           instances=instances,
                           assignments=assignments_by_keys,
                           assignments_by_id=assignments_by_id,
                           people_list=get_staff_dropdown())

@ischool_route('/people/<int:person_id>/megamatrix', Permissions.all_staffs)
def mega_matrix_view(person_id):
    person = Person.query.get_or_404(person_id)

    if person.role != 'student':
        #valid only for students
        abort(403)

    return render_template('matrix/mega-matrix.html', person=person)

# TODO: remove
@ischool_route(['/api/assignments/<int:array_id>/<int:last_assignment_id>/<int:last_submission_id>',
    '/api/assignments/<int:array_id>'],
    Permissions.all_staffs)
def get_assignments(array_id, last_assignment_id = -1, last_submission_id = -1):
    # If there is no new assignment or submission in the requested array, return the ids of the lasts.

    # NOTE: This query is writen in raw sql. because sqlalchemy insists not to allow 'right join'. but I realy need it in order to make it in single afficient query.
    max_assignment_id, max_submission_id = db.session.execute(text('select max(ass.id), max(sub.id) from submissions sub right join assignments ass on sub.assignment_id = ass.id join exercises ex on ass.exercise_id = ex.id where ex.array_id = %d;' % array_id)).first()

    if max_assignment_id == last_assignment_id and max_submission_id == last_submission_id:
        return jsonify({'last_assignment_id': max_assignment_id, 'last_submission_id': max_submission_id})

    assignments = Assignment.query.join(Exercise).filter(Exercise.array_id == array_id).all()

    # Group the result by student and then by exercise.
    res = {}
    for a in assignments:
        if a.exercise_id not in res:
            res[a.exercise_id] = {}

        res[a.exercise_id][a.person_id] = a.jsonify()

    return jsonify({'assignment': res, 'last_assignment_id': max_assignment_id, 'last_submission_id': max_submission_id})

@ischool_route('/api/assignments/<int:array_id>/of/<int:group_id>/auto_sync', Permissions.all_staffs)
def get_new_assignments(array_id, group_id):
    since_ids = request_args_or_default('since_ids', {'assignment': 0, 'submission': 0, 'review': 0, 'submission_lock': 0})

    # Get all the assignment of students in this group that are newer then since_ids['assignment']
    #   or contains submission that is newer then since_ids['submission']
    #   or contains review that is newer then since_ids['review']
    #   or contains submission_lock that is newer then since_ids['submission_lock']
    new_assignments = Assignment.query.distinct()                                                                           \
                            .join(Exercise).filter(Exercise.array_id == array_id)                                           \
                            .join(group_members_table, group_members_table.c.person_id == Assignment.person_id)             \
                            .filter(group_members_table.c.group_id == group_id)                                             \
                            .outerjoin(Submission.__table__)                                                                \
                            .outerjoin(SubmissionLock.__table__)                                                            \
                            .outerjoin(Review.__table__)                                                                    \
                            .filter((Assignment.id > since_ids['assignment'])                                               \
                                  | (Submission.id > since_ids['submission'])                                               \
                                  | (SubmissionLock.id > since_ids['submission_lock'])                                      \
                                  | (Review.id > since_ids['review']))                                                      \
                            .all()

    if len(new_assignments) == 0:
        return '' # There is no new data.

    ids = [a.id for a in new_assignments]

    # Because the fields review and submission_lock were joined with
    # outer join, they are not present in our selected objects and
    # each access to them results in a new query. It is way more
    # efficient to run just one more query in order to find the
    # appropriate maximums
    max_review = db.session.query(func.max(Review.id)).join(Submission).join(Assignment).filter(Assignment.id.in_(ids)).first()[0]
    if max_review is None:
        max_review = since_ids['review']
    max_review = max(max_review, since_ids['review'])

    max_lock = db.session.query(func.max(SubmissionLock.id)).join(Submission).join(Assignment).filter(Assignment.id.in_(ids)).first()[0]
    if max_lock is None:
        max_lock = since_ids['submission_lock']
    max_lock = max(max_lock, since_ids['submission_lock'])

    return jsonify(
        new_data = {
            'assignments': [{
                'id': ass.id,
                'state': ass.state,
                'exercise_id': ass.exercise.id,
                'person_id': ass.person_id,
                'version': ass.version,
            } for ass in new_assignments],
        },
        newest_ids = {
            'assignment': max_or_default((ass.id for ass in new_assignments), since_ids['assignment']),
            'submission': max_or_default((sub.id for ass in new_assignments for sub in ass.submissions), since_ids['submission']),
            'submission_lock': max_lock,
            'review': max_review
        }
    )

@ischool_route('/api/checkers_in_array/<int:array_id>', Permissions.all_staffs)
def get_checkers_in_array(array_id):
    return jsonify(checkers=[c.checker_id for c in CheckersArrays.query.filter(CheckersArrays.array_id == array_id)])

@ischool_route('/api/checker/<int:checker_id>/in_array/<int:array_id>', Permissions.all_staffs, methods = ['PUT', 'DELETE'])
def add_checker_to_array(checker_id, array_id):
    if request.method == 'DELETE':
        CheckersArrays.query.filter(CheckersArrays.checker_id == checker_id).filter(CheckersArrays.array_id == array_id).delete()
    else:
        if CheckersArrays.query.filter(CheckersArrays.checker_id == checker_id).filter(CheckersArrays.array_id == array_id).count() == 0:
            db.session.add(CheckersArrays(checker_id=checker_id, array_id=array_id))
        else:
            abort(500)

    db.session.commit()
    return ''

def create_assignment(exercise_id, member_id):
    assignment = Assignment()
    assignment.exercise = Exercise.query.get(exercise_id)
    assignment.person = Person.query.get(member_id)
    return assignment

@ischool_route('/exercises/<int:exercise_id>/member/<int:member_id>',
               Permissions.all_staffs,
               methods=['PUT'])
def create_new_assignment(exercise_id, member_id):
    assignment = create_assignment(exercise_id, member_id)

    db.session.add(assignment)
    db.session.commit()

    new_assignments_points([assignment])

    return jsonify({
        'id': assignment.id,
        'person_id': assignment.person_id,
        'exercise_id': assignment.exercise_id,
        'state': assignment.state,
        'version': assignment.version
    })

@ischool_route('/exercises/<int:exercise_id>/members/<member_ids>',
               Permissions.all_staffs,
               methods=['PUT'])
def create_new_assigments(exercise_id, member_ids):
    member_ids = member_ids.split(",")
    results = []
    for member_id in member_ids:
        assignment = create_assignment(exercise_id, member_id)
        db.session.add(assignment)
        results.append(assignment)
    db.session.commit()

    return jsonify({'results': [{
        'id': assignment.id,
        'person_id': assignment.person_id,
        'exercise_id': assignment.exercise_id,
        'state': assignment.state,
        'version': assignment.version
    } for assignment in results]})

@ischool_route('/assignments/<int:assignment_id>',
               Permissions.all_staffs,
               methods=['DELETE'])
def delete_assignment(assignment_id):
    assignment = Assignment.query.get(assignment_id)
    delete_assignment_point(assignment)
    db.session.delete(assignment)
    db.session.commit()

    return jsonify({})

def instance_json(instance):
    return {
        'id': instance.id,
        'name': instance.name,
        'exercises': [exercise_json(x.exercise) for x in instance.instance_exercises],
    }

def exercise_json(exercise):
    return {
        'id': exercise.id,
        'name': exercise.name,
        'array_id': exercise.array_id
    }

def group_json(group):
    return {
        'id': group.id,
        'name': group.name,
        'members': map(person_json, group.members)
    }

def person_json(person):
    return {
        'id': person.id,
        'name': person.name
    }

def assignment_json(assignment):
    return {
        'id': assignment.id,
        'state': assignment.state,
        'version': assignment.version
    }

def supports_exercises(instance):
    return instance.type == 'self-work' or instance.type == 'tamak' or \
        instance.type == 'self-learn' or instance.type == 'targil' or instance.type == 'project'

def id_for(obj):
    return obj.id

# TODO: uncompleted (should flat [a, [b, [...]]] as well)
def flatten(l):
    res = []
    for x in l:
        res += x
    return res

# Create an automated point when students get new exercises.
# TODO: Connect it to classroom. Now the point sent without any.
def new_assignments_points(assignment_list):
    for assignment in assignment_list:
        point = Point(person=assignment.person, type='exercise', assignment_id=assignment.id, student_status='active', staff_status='closed')
        msg_text = u"קיבלת תרגיל חדש במערך %s: %s"
        msg_text = msg_text % (assignment.exercise.array.name, assignment.exercise.name)
        msg = Message(person=assignment.person, point=point, text=msg_text)
        db.session.add(point)
        db.session.add(msg)
    db.session.commit()

# Deletes the points connected to a relevant assignment.
def delete_assignment_point(assignment):
    relevant_points = Point.query.filter(Point.assignment_id == assignment.id).all()
    for p in relevant_points:
        db.session.delete(p)
    db.session.commit()

@ischool_route('/people/<int:person_id>/scores', Permissions.all_staffs)
def person_scores(person_id):

    if not g.person.is_staff(g.course) and g.person.id != person_id:
        # Protect from tricksy hobbitses
        abort(403)
    if Person.query.get_or_404(person_id).role != 'student':
        #valid only for students
        abort(403)

    person = Person.query.get_or_404(person_id)

    return render_template('person-scores.html',
                           person=person)

def create_exercise_scores_dict(array_id, person):
    """
    Creates a dictionary of exercise to submissions with scores of a given person in a given array.
    """
    exercise_scores_dict = {}
    for assignment in person.assignments:
        if assignment.exercise.array_id == array_id:
            for submission in assignment.submissions:
                if submission.review: # If the submission was reviewed
                    if assignment.exercise not in exercise_scores_dict:
                        exercise_scores_dict[assignment.exercise] = {'assignment': assignment, 'scores': []}
                    scores = {'submission': submission,
                              'array_scores': {},
                              'exercise_scores': [],
                              'reviewer': submission.review.reviewer,
                              'staff_comments': submission.review.comment_for_staff}
                    for score in submission.scores:
                        if score.criterion.array_id:
                            scores['array_scores'][score.criterion_id] = score
                        else:
                            scores['exercise_scores'].append(score)
                    array_scores_in_numbers = [score.score for score in scores['array_scores'].values()]
                    if array_scores_in_numbers:
                        scores['avg'] = sum(array_scores_in_numbers)/float(len(array_scores_in_numbers))
                    exercise_scores_dict[assignment.exercise]['scores'].append(scores)
    return exercise_scores_dict

def create_exercise_unit_mapping(array, person):
    """
    Creates a mapping of educational units to its exercises (of a given person in a given array).
    """
    group = None
    for array_group in array.groups:
        if person in array_group.members:
            group = array_group

    instances_with_exercises = []
    for unit in group.units:
        instances_with_exercises += [instance for instance in unit.instances if instance.instance_exercises]

    units = {}
    for instance in instances_with_exercises:
        if instance.unit not in units:
            units[instance.unit] = []
        for instance_exercise in instance.instance_exercises:
            units[instance.unit].append(instance_exercise.exercise)
    return units

def calculate_exercise_scores_averages(exercise_scores):
    """
    Calculates the scores averages of an exercise.
    """
    array_criteria_scores = {}
    for submission in exercise_scores['scores']:
        for criterion_id in submission['array_scores']:
            if criterion_id not in array_criteria_scores:
                array_criteria_scores[criterion_id] = []
            array_criteria_scores[criterion_id].append(submission['array_scores'][criterion_id].score)
    avgs = {'per_criterion':{}}
    for criterion_id in array_criteria_scores:
        avgs['per_criterion'][criterion_id] = round(sum(array_criteria_scores[criterion_id]) / float(len(array_criteria_scores[criterion_id])), 1)
    if avgs['per_criterion']:
        avgs['total'] = round(sum(avgs['per_criterion'].values()) / float(len(avgs['per_criterion'].values())), 1)
    return avgs

def calculate_unit_scores_averages(unit_scores):
    """
    Calculates the scores averages of an educational unit.
    """
    unit_criteria_exercises_averages = {}
    for exercise_scores in unit_scores:
        for criterion_id in exercise_scores['avgs']['per_criterion']:
            if criterion_id not in unit_criteria_exercises_averages:
                unit_criteria_exercises_averages[criterion_id] = []
            unit_criteria_exercises_averages[criterion_id].append(exercise_scores['avgs']['per_criterion'][criterion_id])
    avgs = {'per_criterion':{}}
    for criterion_id in unit_criteria_exercises_averages:
        avgs['per_criterion'][criterion_id] = round(sum(unit_criteria_exercises_averages[criterion_id]) / float(len(unit_criteria_exercises_averages[criterion_id])), 1)
    if avgs['per_criterion']:
        avgs['total'] = round(sum(avgs['per_criterion'].values()) / float(len(avgs['per_criterion'].values())), 1)
    return avgs

def calculate_total_exercises_scores_averages(units_scores):
    """
    Calculates the averages of all of the exercises scores.
    """
    criteria_averages = {}
    for unit_scores in units_scores:
        for exercise_scores in unit_scores['scores']:
            for criterion_id in exercise_scores['avgs']['per_criterion']:
                if criterion_id not in criteria_averages:
                    criteria_averages[criterion_id] = []
                criteria_averages[criterion_id].append(exercise_scores['avgs']['per_criterion'][criterion_id])
    avgs = {'per_criterion':{}}
    for criterion_id in criteria_averages:
        avgs['per_criterion'][criterion_id] = round(sum(criteria_averages[criterion_id]) / float(len(criteria_averages[criterion_id])), 1)
    if avgs['per_criterion']:
        avgs['total'] = round(sum(avgs['per_criterion'].values()) / float(len(avgs['per_criterion'].values())), 1)
    return avgs

@ischool_route('/people/<int:person_id>/scores/array/<int:array_id>', Permissions.all_staffs)
def person_array_scores(person_id, array_id):

    if not g.person.is_staff(g.course) and g.person.id != person_id:
        # Protect from tricksy hobbitses
        abort(403)
    if Person.query.get_or_404(person_id).role != 'student':
        #valid only for students
        abort(403)

    person = Person.query.get_or_404(person_id)
    array = Array.query.get_or_404(array_id)

    exercises_scores_dict = create_exercise_scores_dict(array_id, person)
    exercises_units_dict = create_exercise_unit_mapping(array, person)

    # unit --> exercises --> scores
    units_scores = []
    for unit in sorted(exercises_units_dict, key=lambda unit: unit.order):
        unit_scores = []
        for exercise in exercises_units_dict[unit]:
            if exercise in exercises_scores_dict:
                exercise_scores = {'scores': exercises_scores_dict[exercise]['scores'],
                                   'exercise': exercise,
                                   'assignment': exercises_scores_dict[exercise]['assignment']}
                exercise_scores['avgs'] = calculate_exercise_scores_averages(exercise_scores)
                unit_scores.append(exercise_scores)
        units_scores.append({'unit': unit, 'scores': unit_scores, 'avgs': calculate_unit_scores_averages(unit_scores)})
    avgs = calculate_total_exercises_scores_averages(units_scores)

    return render_template('person-array-scores.html',
                           person=person,
                           scores_dict=exercises_scores_dict,
                           units_scores=units_scores,
                           avgs=avgs,
                           array=array)
