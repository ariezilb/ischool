# coding=utf-8

from flask import g
from ischool.models import Room
from ischool.site.utils.flask_restless import create_api
from ischool.site.utils.auth import Permissions

def pre_get_many_rooms_processor(result=None, **kw):
    result['objects'] = [room for room in result['objects'] if room['course_id'] == g.course.id]
    result['num_results'] = len(result['objects'])

create_api(Room,
        Permissions.all_staffs,
        Permissions.students,
        exclude_columns='placements',
        postprocessors={
            'GET_MANY': [pre_get_many_rooms_processor]
        },
        methods=['GET'])
