# coding=utf-8
from flask import request, render_template, session, redirect, url_for
from ischool.site.utils.auth import ischool_route, Permissions
from ischool.site.utils.encoding import jsonify
from ischool.site.utils.errors import custom_error
from ischool.models.users import User, IncorrectUsernameError, IncorrectPasswordError


@ischool_route('/login', Permissions.guest, methods=['GET'])
def login_view():
    return render_template('login.html')


# TODO: Implement as create_api
@ischool_route('/api/login', Permissions.guest, methods=['POST'])
def perform_login():
    try:
        if 'username' not in request.json:
            raise IncorrectUsernameError(u"שם משתמש לא נשלח")
        if 'password' not in request.json:
            raise IncorrectPasswordError(u"סיסמה לא נשלחה")
        if len(request.json['username']) == 0:
            raise IncorrectUsernameError(u"שם המשתמש לא יכול להיות ריק")
        if len(request.json['password']) == 0:
            raise IncorrectPasswordError(u"הסיסמה לא יכול להיות ריקה")

        user = User.connect(request.json['username'], request.json['password'])
    except (IncorrectUsernameError, IncorrectPasswordError), e:
        return custom_error(e.msg), 400

    session['uid'] = user.person_id
    return jsonify(success=True)

@ischool_route('/api/changePassword', Permissions.logged_in, methods=['POST'])
def perform_change_password():
    try:
        if 'username' not in request.json:
            raise IncorrectUsernameError(u"שם משתמש לא נשלח")
        if 'password' not in request.json:
            raise IncorrectPasswordError(u"סיסמה לא נשלחה")
        if 'newPassword' not in request.json:
            raise IncorrectPasswordError(u"סיסמה חדשה לא נשלחה")
        if len(request.json['username']) == 0:
            raise IncorrectUsernameError(u"שם המשתמש לא יכול להיות ריק")
        if len(request.json['password']) == 0:
            raise IncorrectPasswordError(u"הסיסמה לא יכול להיות ריקה")
        if len(request.json['newPassword']) == 0:
            raise IncorrectPasswordError(u"הסיסמה לא יכול להיות ריקה")

        n = request.json['newPassword']
        user = User.connect(request.json['username'], request.json['password'])
        user.change_password(n)
    except (IncorrectUsernameError, IncorrectPasswordError), e:
        return custom_error(e.msg), 400

    session['uid'] = user.person_id
    return jsonify(success=True)


@ischool_route('/logout', Permissions.logged_in, methods=['GET'])
def logout_view():
    session['uid'] = None
    del session['uid']
    return redirect(url_for('ischool.login_view'))


@ischool_route('/changePassword', Permissions.logged_in)
def change_password_view():
    return render_template('change-password.html')

@ischool_route('/api/getUserName/<int:id>', Permissions.logged_in, methods=['GET'])
def get_user(id):
    u = User.query.filter(User.person_id == id).first()
    return u.username
