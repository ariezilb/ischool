# coding=utf-8
import os
from flask import render_template, send_file, url_for, redirect, g, abort, request
from ischool.config import IschoolConfig as config
from ischool.site.extensions import db
from ischool.site.utils.auth import ischool_route, Permissions
from ischool.site.utils.encoding import jsonify, jsonify_string
from ischool.site.utils.flask_restless import create_api
from ischool.models import Person, Course, ContactDetail
from ischool.models.users import User
from sqlalchemy import or_
#from matrix_views import *
import json

create_api(Person,
           Permissions.all_staffs,
           exclude_columns=['user.password', 'user.password_hash', 'user.password_salt', 'points', 'assignments', 'messages', 'main_course', 'tweets'],
           include_methods=['hebrew_gender', 'contact_details.hebrew_type', 'name_abbreviation'],
           methods=['GET', 'POST', 'PUT', 'DELETE'])

create_api(ContactDetail,
           Permissions.all_staffs,
           include_methods=['contact_details.hebrew_type'],
           methods=['DELETE'])

create_api(User,
           Permissions.all_staffs,
           methods=['DELETE'])


@ischool_route(['/courses/<course(nullable=False):course>/people/',
    '/people/'],
    Permissions.all_staffs)
def person_list_view():
    return render_template('people.html')


@ischool_route('/api/people/teachers-of-array/<array:array>', Permissions.all_staffs)
def get_teachers_of_array(array):
    # TODO: Implement using has/any relation (restless ?q=)
    # Return all teachers of specific array.
    raise NotImplementedError()

@ischool_route('/people/<int:person_id>', Permissions.all_staffs)
def person_view(person_id):
    person = Person.query.get_or_404(person_id)
    courses = db.session.query(Course).all()
    return render_template('person.html', person=person, courses=courses)

@ischool_route('/people/<int:person_id>/wall', Permissions.all_staffs)
def person_wall(person_id):
    if Person.query.get_or_404(person_id).role != 'student':
        #valid only for students
        abort(403)

    person = Person.query.get_or_404(person_id)
    return render_template('person-wall.html', person=person)


@ischool_route('/people/<int:person_id>/groups', Permissions.all_staffs)
def person_groups(person_id):
    if Person.query.get_or_404(person_id).role != 'student':
        #valid only for students
        abort(403)

    person = Person.query.get_or_404(person_id)
    return render_template('person-groups.html', person=person)


@ischool_route('/people/<int:person_id>/meetings', Permissions.all_staffs)
def person_meetings(person_id):
    if Person.query.get_or_404(person_id).role != 'student':
        #valid only for students
        abort(403)

    person = Person.query.get_or_404(person_id)
    return render_template('person-meetings.html', person=person)


@ischool_route('/people/<int:person_id>/picture', Permissions.all_staffs)
def person_picture(person_id):
    filepath = os.path.join(config.STORAGE_PATH, 'people_pictures', str(person_id))
    if os.path.exists(filepath):
        return send_file(filepath)
    else:
        return redirect(url_for('ischool.static', filename='images/default-person-picture.png'))


@ischool_route('/api/people/role/<string:role>', Permissions.all_staffs)
def people_by_role(role):
    people = Person.query.filter(Person.role == role).all()
    return jsonify(people)

@ischool_route('/api/2.0/people', Permissions.all_staffs)
def get_people():
    """
    Return minimum amount of data needed to work with people.
    Saves on hundreds of extra queries that happen on
    /api/people because create_api is terrible
    """
    people = Person.query.all()
    return jsonify([{'id': p.id, 'name': p.name, 'role': p.role, 'gender': p.gender} for p in people])

@ischool_route('/api/people/all_staff', Permissions.all_staffs)
def all_staff():
    people = Person.query.filter(or_(Person.role == 'staff', Person.role == 'track_leader', Person.role == 'course_chief')).order_by(Person.name).all()
    return jsonify(people)

@ischool_route('/api/people/all', Permissions.all_staffs)
def all_people():
    people = Person.query.all()
    return jsonify(people)

@ischool_route('/api/people/<int:person_id>/user', Permissions.all_staffs, methods=['put'])
def create_user(person_id):
    person = Person.query.get_or_404(person_id)
    data = json.loads(request.data)
    if person.user is None: # If there is no user already, create new user.
        person.user = User(username=data['username'], password=data['password'])
    else: # If user already exist, update it.
        person.user.username = data['username']
        if 'password' in data:
            person.user.password = data['password']
    db.session.commit()
    return jsonify(person.user)


@ischool_route('/api/people/<person:person>/groups', Permissions.all_staffs)
def api_person_groups(person):
    person = person()
    groups = [group.jsonify() for group in person.groups]
    for i in xrange(len(groups)):
        groups[i]['array'] = person.groups[i].array
    return jsonify(groups)


@ischool_route('/api/people/<person:person>/assignments', Permissions.all_staffs)
def api_person_assignments(person):
    person = person()
    sorted_assignments = list(reversed(sorted(person.assignments, compare_assignments)))
    assignments = [assignment.jsonify() for assignment in sorted_assignments]
    for i in xrange(len(sorted_assignments)):
        assignments[i]['exercise'] = sorted_assignments[i].exercise
    return jsonify(assignments)


def compare_assignments(a1, a2):
    a1_time = a1.last_change_time()
    a2_time = a2.last_change_time()

    if a1_time is None:
        return 1
    if a2_time is None:
        return -1

    return cmp(a1_time, a2_time)
