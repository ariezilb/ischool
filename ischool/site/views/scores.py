# coding=utf-8
import json
from ischool.site.utils.auth import ischool_route, Permissions
from ischool.models import Person, Group, Array, Assignment, Submission, SubmissionScore, Exercise, Criterion
from ischool.models.groups import group_members_table
from sqlalchemy import func
from itertools import groupby

@ischool_route('/api/arrays_of_student/<int:student_id>', Permissions.all_staffs, methods=['GET'])
def get_arrays_of_student(student_id):
    """
    Returns dictionary mapping from array.id to the array.name for each array that the given student is study.
    """
    arrays = Array.query.select_from(Assignment)                  \
                  .filter(Assignment.person_id == student_id)     \
                  .join(Assignment.exercise)                      \
                  .join(Exercise.array)                           \
                  .all()
    return json.dumps(dict([(arr.id, arr.name) for arr in arrays]))

@ischool_route('/api/scores_statistics/<int:student_id>/<string:compare_to>/<string:which_score_to_use>/<string:score_src_type>/<int:score_src_id>', Permissions.all_staffs, methods=['GET'])
def get_scores_statistics(student_id, compare_to, which_score_to_use, score_src_type, score_src_id):
    """

    @param student_id The id of the student to get statistics about.
    @type int

    @param compare_to 'compare_to_group' / 'compare_to_all'.
    @type string

    @param which_score_to_use 'only_the_last_submission' / 'only_the_first_submission' / 'only_the_dones' / 'all_the_scores'
    @type string

    @param score_src_type 'exercise' / 'unit' / 'array'
    @type string

    @param score_src_id The id of the exercise of unit or array (depends on score_src_type)
    @type int

    """
    assert compare_to == 'compare_to_group' or compare_to == 'compare_to_all'
    assert which_score_to_use == 'only_the_last_submission' or which_score_to_use == 'only_the_first_submission' or which_score_to_use == 'only_the_dones' or which_score_to_use == 'all_the_scores'
    assert score_src_type == 'exercise' or score_src_type == 'unit' or score_src_type == 'array'

    assert score_src_type != 'unit', 'sorry, query by score_src_type = "unit" is not implemented yet.'

    group = None

    # Query SubmissionScores from Assignments
    scores = SubmissionScore.query.select_from(Assignment)

    # Filter to the student group only
    if compare_to == 'compare_to_group':
        # Get the array id.
        if score_src_type == 'array':
            array_id = score_src_id
        elif score_src_type == 'exercise':
            array_id = Exercise.query.get(score_src_id).array_id
        elif score_src_type == 'unit':
            array_id = 1 # TODO: implement

        # Get the group of the student
        group = Group.query.filter_by(array_id = array_id)                  \
                           .join(group_members_table)                           \
                           .filter(group_members_table.c.person_id == student_id) \
                           .first()

        # Filter the assignment by the members of the group.
        scores = scores.join(group_members_table, group_members_table.c.person_id == Assignment.person_id)
        scores = scores.filter(group_members_table.c.group_id == group.id)

    # If only_the_dones, filter by DONE state.
    if which_score_to_use == 'only_the_dones':
        scores = scores.filter(Assignment.state == Assignment.DONE)

    # Filter the Assignments by requested score_src.
    if score_src_type == 'array':
        scores = scores.join(Assignment.exercise).filter(Exercise.array_id == score_src_id)
    elif score_src_type == 'exercise':
        scores = scores.filter(Assignment.exercise_id == score_src_id)
    elif score_src_type == 'unit':
        pass # TODO: implement

    # Join with the Submissions.
    scores = scores.join(Assignment.submissions)

    # Filter only_the_last_submission.
    # In case of only_the_dones, we also want to filter by only_the_last_submission.
    if which_score_to_use == 'only_the_last_submission' or which_score_to_use == 'only_the_dones':
        last_sub_id = Submission.query.session.query(func.max(Submission.id).label('id')).group_by(Submission.assignment_id).subquery()
        scores = scores.join(last_sub_id, last_sub_id.c.id == Submission.id)
    elif which_score_to_use == 'only_the_first_submission':
        last_sub_id = Submission.query.session.query(func.min(Submission.id).label('id')).group_by(Submission.assignment_id).subquery()
        scores = scores.join(last_sub_id, last_sub_id.c.id == Submission.id)

    # Join with the SubmissionScores.
    scores = scores.join(Submission.scores)

    # Fetch
    scores = list(scores.values(SubmissionScore.score, SubmissionScore.criterion_id, Assignment.person_id, Assignment.exercise_id))

    # Sort the scores by criteria
    scores = sorted(scores, key=get_criterion_id) # Sort before use group by. For more information, see http://docs.python.org/2/library/itertools.html#itertools.groupby

    general_statistics = {
       'student_statistics': calc_statistics(filter(lambda s: get_person_id(s) == student_id, scores), minimal_statistics = True),
       'compare_to_statistics': calc_statistics(scores),
    }

    res = {'num of scores': len(scores)}

    if group is not None:
        res['group'] = {'id': group.id, 'name': group.name}

    # If the src_type is 'array', add to res the statistics for each exercise too.
    if score_src_type == 'array':
        scores = sorted(scores, key=get_exercise_id) # Sort before use group by. For more information, see http://docs.python.org/2/library/itertools.html#itertools.groupby
        # NOTE: sorted does stable sorting algorithm (http://stackoverflow.com/questions/1915376/is-pythons-sorted-function-guaranteed-to-be-stable)
        #       So it guaranteed as that aldo we now sorting by exercise_id, for each exercise_id the scores are still sorted by criterion_id as it was before.
        #       It is importent for the groupby in calc_statistics :)

        # Prepare the statistics for each exercise.
        res_exercises = {}
        for exercise_id, exercise_scores in groupby(scores, get_exercise_id):
            exercise_scores = list(exercise_scores)
            exercise_scores_of_student = filter(lambda s: get_person_id(s) == student_id, exercise_scores)

            # If there is any score for the student in the current exercise
            if len(exercise_scores_of_student) > 0:
                res_exercises[exercise_id] = {
                    'student_statistics': calc_statistics(exercise_scores_of_student, minimal_statistics=True),
                    'compare_to_statistics': calc_statistics(exercise_scores)
                }

        # Fetch the names of all the relevante exercises.
        for exercise_id, exercise_name in Exercise.query.filter(Exercise.id.in_(res_exercises.keys())).values(Exercise.id, Exercise.name):
            res_exercises[exercise_id]['name'] = exercise_name

        res_exercises['all'] = general_statistics
        res_exercises['all']['name'] = u'כלל התרגילים'

        res['exercises'] = res_exercises
    else:
        res['statistics'] = general_statistics


    criteria = Criterion.query.filter(Criterion.id.in_(general_statistics['compare_to_statistics'].keys())).all()
    res['criteria'] = dict([(criterion.id, {
                                'name': criterion.name,
                                'description': {
                                    'low': criterion.low_score_description,
                                    'middle': criterion.middle_score_description,
                                    'height': criterion.high_score_description
                                }
                            }) for criterion in criteria])

    return json.dumps(res)

get_score = lambda s: s[0]
get_criterion_id = lambda s: s[1]
get_person_id = lambda s: s[2]
get_exercise_id = lambda s: s[3]

def calc_statistics(scores, minimal_statistics = False):
    return dict([(criterion_id, calc_single_statistics(scores, minimal_statistics)) for criterion_id, scores in groupby(scores, get_criterion_id)])

def calc_single_statistics(scores, minimal_statistics):
    avg = lambda s: (10*sum(s) / len(s)) / 10.0 # Avarage with 1 digit precision

    # Get the actual scores
    scores = map(get_score, scores)

    if len(scores) >= 4 and not minimal_statistics:
        # Sort before calc quarters.
        scores = sorted(scores)

        return {#'max': scores[-1],
                #'min': scores[0],
                #'avg': avg(scores),
                'q1_avg': avg(scores[ : len(scores)/4]),
                'q2_avg': avg(scores[len(scores)/4 : 2 * len(scores)/4]),
                'q3_avg': avg(scores[2 * len(scores)/4 : 3 * len(scores)/4]),
                'q4_avg': avg(scores[3 * len(scores)/4 : ]),
                #'q1': scores[len(scores)/4],
                #'q2': scores[2 * len(scores)/4],
                #'q3': scores[3 * len(scores)/4],
                }
    else:
        return {'avg': avg(scores)}
