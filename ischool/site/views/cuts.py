# coding=utf-8

from ischool.site.extensions import db
from flask import g, render_template, redirect, url_for, jsonify, request, abort
from ischool.site.utils.auth import ischool_route, Permissions
from ischool.site.utils.auto_syncr import max_or_default, request_args_or_default
from ischool.site.utils.flask_restless import create_api
from sqlalchemy import func
from ischool.models.cuts import SmallGroup, Phase, Cut
from ischool.models.cuts.small_groups import small_group_members_table
from ischool.models import Assignment, Submission, Exercise, InstanceExercise, Room
import json, datetime
import random

@ischool_route('/cuts/<instance:inst>/plan', Permissions.all_staffs, methods=['GET'])
def cuts_plan_view(inst):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    inst = inst()

    return render_template('cuts/cuts-plan.html', instance_id=inst.id, array=inst.array)

@ischool_route('/cuts/<instance:inst>/run', Permissions.all_staffs, methods=['GET'])
def cuts_run_view(inst):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    inst = inst()

    return render_template('cuts/cuts-run.html', instance_id=inst.id, array=inst.array)

@ischool_route('/api/cuts/<instance:inst>/rooms', Permissions.all_staffs, methods=['GET'])
def rooms_for_cut(inst):
    inst = inst()
    rooms = Room.query.join(SmallGroup).filter(SmallGroup.instance_id == inst.id).all()

    return jsonify(rooms=[{
        'id': r.id,
        'name': r.name,
        'eng_name': r.eng_name,
        } for r in rooms])

create_api(SmallGroup,
           Permissions.all_staffs,
           exclude_columns=['cuts'],
           methods=['GET', 'POST', 'PUT', 'DELETE'])

create_api(Phase,
           Permissions.all_staffs,
           methods=['GET', 'POST', 'PUT', 'DELETE'])

@ischool_route('/api/cuts/<instance:inst>/members', Permissions.all_staffs, methods=['GET'])
def get_all_members(inst):
    inst = inst()

    return jsonify(members = [{
                              'id': member.id,
                              'course_number': member.course_number,
                              'name': member.name}
                              for member in inst.unit.group.members])

def ass_to_json(ass):
    return {
    'id': ass.id,
    'state': ass.state,
    'exercise': {'id': ass.exercise.id, 'name': ass.exercise.name},
    'person_id': ass.person_id,
    'version': ass.version,
    'given_time': ass.given_time.isoformat(),
    'submissions': [sub_to_json(sub) for sub in ass.submissions],
    }

def sub_to_json(sub):
    return {
    'id': sub.id,
    'created_at': sub.created_at.isoformat(),
    'reviewed_at': sub.review.reviewed_at.isoformat() if sub.review else None,
    'reviewer': {'name': sub.review.reviewer.name, 'id': sub.review.reviewer.id} if sub.review else None,
    }

def cut_to_json(cut):
    return {
    'small_group_id': cut.small_group_id,
    'timestamp': cut.timestamp.isoformat(),
    'cutter': {'name': cut.cutter.name, 'id': cut.cutter.id},
    'phases': [phase.id for phase in cut.phases],
    'comment': cut.comment,
    'id': cut.id,
    }

@ischool_route('/api/cuts/<int:inst_id>/room/<int:room_id>/auto_sync', Permissions.all_staffs, methods=['GET'])
def get_cuts(inst_id, room_id):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    since_ids = request_args_or_default('since_ids', {'cut': 0, 'assignment': 0, 'submission': 0})

    new_cuts = Cut.query \
        .filter(Cut.id > since_ids['cut']) \
        .join(Cut.small_group) \
        .filter(SmallGroup.room_id == room_id) \
        .filter(SmallGroup.instance_id == inst_id) \
        .all()

    students_in_class = SmallGroup.query.session.query(small_group_members_table) \
        .join(SmallGroup) \
        .filter(SmallGroup.room_id == room_id) \
        .filter(SmallGroup.instance_id == inst_id) \
        .subquery()

    # Get all the assignment of student in this cut-class that are newer then since_ids['assignment'] or contains submission that is newer then since_ids['submission'],
    new_assignments = Assignment.query \
        .join(Exercise).join(Exercise.instance_exercise).filter(InstanceExercise.instance_id == inst_id) \
        .join(students_in_class, students_in_class.c.person_id == Assignment.person_id) \
        .outerjoin(Assignment.submissions) \
        .filter((Assignment.id > since_ids['assignment']) | (Submission.id > since_ids['submission'])) \
        .all()

    if len(new_assignments) == 0 and len(new_cuts) == 0:
        return '' # There is no new data.

    return jsonify(
        new_data = {
        'cuts': [cut_to_json(cut) for cut in new_cuts],
        'assignments': [ass_to_json(ass) for ass in new_assignments],
        },
        newest_ids = {
        'cut': max_or_default((cut.id for cut in new_cuts), since_ids['cut']),
        'assignment': max_or_default((ass.id for ass in new_assignments), since_ids['assignment']),
        'submission': max_or_default((sub.id for ass in new_assignments for sub in ass.submissions), since_ids['submission']),
        }
    )

@ischool_route('/api/cuts/small_group/<int:sg_id>', Permissions.all_staffs, methods=['POST'])
def save_new_cut(sg_id):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    sg = SmallGroup.query.get_or_404(sg_id)

    data = json.loads(request.data)

    data['timestamp'] = datetime.datetime.fromtimestamp(data['timestamp_serialized'])

    # Create new cut.
    new_cut = Cut(cutter_id=data['cutter'], comment=data['comment'], timestamp=data['timestamp'])

    # Add cuts-phases data
    new_cut.phases = filter(lambda phase: data['phases'][str(phase.id)], sg.instance.phases)

    sg.cuts.append(new_cut)

    db.session.commit()

    return jsonify(new_cut = cut_to_json(new_cut))

@ischool_route('/api/instances/<instance:inst>/exercises', Permissions.all_staffs, methods=['GET'])
def get_exercises(inst):
    if not g.person.is_staff(g.course):
        # Protect from tricksy hobbitses
        abort(404)

    inst = inst()

    return jsonify(exercises = [{
                                'name': ie.exercise.name,
                                'id': ie.exercise.id
                                } for ie in inst.instance_exercises])

@ischool_route('/api/cuts/<instance:inst>/randomize', Permissions.all_staffs, methods=['POST'])
def randomize_small_groups(inst):
    """
    Make random groups of students
    If there are extra students, one of the
    groups will be smaller
    """
    inst = inst()
    num_of_people = request.json['num_of_people']
    rooms = request.json['rooms']
    group = inst.unit.group.members

    assert len(rooms) > 0
    current_room = 0
    random.shuffle(group)

    # Delete existing groups
    SmallGroup.query.filter_by(instance=inst).delete()

    for i in xrange(0, len(group), num_of_people):
        current = SmallGroup()
        current.instance_id = inst.id
        current.members = group[i:i + num_of_people]
        current.room_id = rooms[current_room]
        current_room = (current_room + 1) % len(rooms)
        db.session.add(current)

    db.session.commit()
    return ""
