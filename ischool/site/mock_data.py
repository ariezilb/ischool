# coding=utf-8
from flask.globals import g
from ischool.models.medida.criteria import Criterion
from ischool.models.rooms import Room
from ischool.models.tracks import Track
from ischool.models.users import User
from datetime import datetime
from datetime import date

from datetime import datetime
from ischool.site.extensions import db
from ischool.models import Person, ContactDetail, Course, Array, Group, EducationUnit, LessonInstance, PlannedPlacement,\
        FileUsage, Exercise, InstanceExercise, ExerciseFile, Point, Assignment, Submission, SubmissionScore, \
        Message, RealPlacement, Bizur, InstanceTask, File, CheckersAssignments, CheckersArrays, Meeting, \
        Phase, SmallGroup, Cut, CutScore, Review

def create_course(name, eng_name, number_of_students, number_of_points, number_of_groups, num_of_exercises):

    course = Course(name, eng_name, 15, datetime(2012, 11, 1))
    db.session.add(course)
    db.session.commit()

    create_staff(course)
    create_rooms(course)
    create_tracks(course)
    specific_groups, other_groups = create_array_groups(number_of_groups)
    u99, course_students = create_students(course, specific_groups, other_groups, number_of_students)
    _, array1 = create_array('מערך 1', course, specific_groups + other_groups, num_of_exercises, 30, course.tracks[0])
    specific_groups, other_groups = create_array_groups(number_of_groups)

    for id, s in enumerate(course_students):
        group_specific1 = specific_groups[0]
        group_all = specific_groups[1]

        group_all.members.append(s)

        # We don't want all of the students to be in the specific group
        if id < 30:
            group_specific1.members.append(s)

        # # Add students to the other groups
        # other_groups[id % len(other_groups)].members.append(s)

    create_array('מערך 2', course, specific_groups + other_groups, num_of_exercises, 6, course.tracks[0])
    create_points(course_students, number_of_points)
    create_meetings(u99)

    create_cuts(array1)

    db.session.commit()

    return course

#@app.route('/ready')
def ready():
    """
    Add all mock data
    """
    db.create_all()

    # Create minimal course
    course1 = create_course(u'קורס1', u'course1',
        number_of_students = 100,
        number_of_points = 1,
        number_of_groups = 2,
        num_of_exercises = 6)

    # Create big course.
    #course1 = create_course(u'קורס1', u'course1',
    #    number_of_students = 100,
    #    number_of_points = 20,
    #    number_of_groups = 2,
    #    num_of_exercises = 20)

    return 'It WORKS! finally!'

def create_cuts(array):
    instance = array.instances[-1]

    instance.phases = [Phase(name=u'שלב 1', description='זהו השלב הראשון', order=1),  \
                       Phase(name=u'שלב 2', description='זהו השלב השני', order=2),    \
                       Phase(name=u'שלב אחרון', description='זהו השלב האחרון ודי', order=3)]

    students = instance.unit.group.members
    for s1, s2 in zip(students[::2], students[1::2]):
        instance.small_groups.append(SmallGroup(members=[s1, s2], room=instance.array.course.rooms[0]))

    for i, sg in enumerate(instance.small_groups):
        c = Cut(cutter=instance.array.manager, comment=u'חתכתי אותם!! מואהאהאהאה')
        sg.cuts.append(c)

        # Add cuts-phases data
        c.phases = instance.phases[:i % (len(instance.phases) + 1)]

        # Add cuts-scores data
        criteria = set()
        for ex in [ie.exercise for ie in instance.instance_exercises]:
            criteria = criteria.union(ex.criteria)

        c.scores = [CutScore(criterion=criterion, score=i%5, comment=u'סתם רנדום', person=sg.members[i % len(sg.members)]) for criterion in criteria]

    db.session.commit()

def create_array_groups(num_of_groups):
    # Createing 2 default groups
    beginners_group = Group(u'מתחילים')
    everybody_group = Group(u'כולם')
    specific_groups = [beginners_group, everybody_group]

    # Create the other groups
    other_groups = [Group(u'קבוצה ' + str(i+1)) for i in xrange(num_of_groups)]

    # Add the groups to the DB.
    for group in other_groups:
        db.session.add(group)

    return specific_groups, other_groups

def create_array(name, course, groups, num_of_exercises, num_of_submissions, track):

    segel = Person.get_by_name(u'איש סגל')
    array = Array(name, segel.id)
    array.track_id = track.id
    course.arrays.append(array)

    create_criteria(array)
    create_array_instances(array, groups[0], num_of_exercises, num_of_submissions)

    for group in groups:
        array.groups.append(group)

    db.session.add(array)

    return groups, array

def create_exercise(instance, name):
    ex = Exercise(name=name, submit_type='ok')
    ex.array = instance.array
    ex.instance_exercise.append(InstanceExercise(instance=instance, order=(len(instance.instance_exercises) + 1)))
    for criterion in ex.array.criteria:
        criterion.exercises.append(ex)
    #student_file = ExerciseFile(file_type='student_copy')

    #from shutil import copy
    #f = File('סיכום המערך', 'docx', instance.array.manager.person, r'/path/to/file')
    #db.session.add(f)
    #student_file.file_usage = FileUsage(f.last_revision)

    #ex.files.append(student_file)

    db.session.add(ex)
    return ex

def create_staff(course):
    """
    Add mock staff
    """

    user = Person(u'משה אופניק', 'male', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'052-2222222'))
    user.contact_details.append(ContactDetail(u'home_phone', u'09-2222222'))
    user.role='course_chief'
    user.main_course_id=course.id
    user.user = User(u'chief')
    db.session.add(user)

    user = Person(u'גיא שמוץ', 'male', u'reserve')
    user.contact_details.append(ContactDetail(u'home_phone', u'09-1111111'))
    user.contact_details.append(ContactDetail(u'cellphone', u'054-1111111'))
    user.role='track_leader'
    user.main_course_id=course.id
    user.user = User(u'guy')
    db.session.add(user)

    user = Person(u'יניב הגבר', 'male', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'050-2323231'))
    user.contact_details.append(ContactDetail(u'home_phone', u'09-2323231'))
    user.role='track_leader'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'תומר הגבר', 'male', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'052-1212121'))
    user.contact_details.append(ContactDetail(u'home_phone', u'03-1212121'))
    user.role='track_leader'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'גיא בלאט', 'male', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'054-5555555'))
    user.contact_details.append(ContactDetail(u'home_phone', u'077-5555555'))
    user.role='track_leader'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'רוני מזאת', 'female', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'052-4343433'))
    user.contact_details.append(ContactDetail(u'home_phone', u'04-4343433'))
    user.role='staff'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'ירון לונון', 'male', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'054-9999999'))
    user.contact_details.append(ContactDetail(u'home_phone', u'09-9999999'))
    user.role='staff'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'אורי מויש', 'male', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'054-41414141'))
    user.contact_details.append(ContactDetail(u'home_phone', u'03-41414141'))
    user.role='staff'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'איש סגל', 'male', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'054-1111111'))
    user.contact_details.append(ContactDetail(u'home_phone', u'03-1111111'))
    user.user = User(u'segel', password=u'1234')
    user.role='staff'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'לי דדון', 'male', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'052-7979797'))
    user.contact_details.append(ContactDetail(u'home_phone', u'04-7979797'))
    user.role='staff'
    user.main_course_id=course.id
    user.user = User(u'li')
    db.session.add(user)

    user = Person(u'רון שריף', 'female', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'052-1133113'))
    user.contact_details.append(ContactDetail(u'home_phone', u'03-1133113'))
    user.role='staff'
    user.main_course_id=course.id
    user.user = User(u'ron')
    db.session.add(user)

    user = Person(u'רבקה קובינפלד', 'male', u'conscript')
    user.contact_details.append(ContactDetail(u'cellphone', u'052-1122331'))
    user.contact_details.append(ContactDetail(u'home_phone', u'03-1122331'))
    user.role='staff'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'ליבי עעעכ', 'female', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'052-1122331'))
    user.contact_details.append(ContactDetail(u'home_phone', u'03-2211223'))
    user.role='staff'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'מור אבוגוש', 'male', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'054-4433445'))
    user.contact_details.append(ContactDetail(u'home_phone', u'04-3344334'))
    user.role='staff'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'ניר שלהלהלה', 'male', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'052-887788'))
    user.contact_details.append(ContactDetail(u'home_phone', u'03-887788'))
    user.role='staff'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'אמנון אמנון', 'female', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'054-2233112'))
    user.contact_details.append(ContactDetail(u'home_phone', u'09-2233112'))
    user.role='staff'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'תור הפטיש', 'female', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'054-1122331'))
    user.contact_details.append(ContactDetail(u'home_phone', u'08-1122331'))
    user.role='staff'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'סופר מן', 'female', u'reserve')
    user.contact_details.append(ContactDetail(u'cellphone', u'050-4433443'))
    user.contact_details.append(ContactDetail(u'home_phone', u'08-4433443'))
    user.role='staff'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'בט מן', 'female', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'054-1122331'))
    user.contact_details.append(ContactDetail(u'home_phone', u'03-1122331'))
    user.role='staff'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'ספיידרמן', 'female', u'keva')
    user.contact_details.append(ContactDetail(u'cellphone', u'050-0099009'))
    user.contact_details.append(ContactDetail(u'home_phone', u'08-0099009'))
    user.role='staff'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'רובין בוד', 'female', u'reserve')
    user.contact_details.append(ContactDetail(u'cellphone', u'054-1111222'))
    user.contact_details.append(ContactDetail(u'home_phone', u'08-1111222'))
    user.role='staff'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'ברזיל איש', 'female', u'conscript')
    user.contact_details.append(ContactDetail(u'cellphone', u'050-2222333'))
    user.contact_details.append(ContactDetail(u'home_phone', u'08-2222233'))
    user.role='staff'
    user.main_course_id=course.id
    user.user = User(u'iron', password=u'1234')
    db.session.add(user)

    user = Person(u'שמעון - הסעות', 'male', u'other')
    user.contact_details.append(ContactDetail(u'cellphone', u'052-1111122'))
    db.session.add(user)

    user = Person(u'ברוך אללה', 'male', u'other')
    user.contact_details.append(ContactDetail(u'cellphone', u'054-3333221'))
    user.contact_details.append(ContactDetail(u'extension', u'1'))
    db.session.add(user)

    user = Person(u'רמי שמטוב', 'male', u'other')
    user.contact_details.append(ContactDetail(u'cellphone', u'052-6666777'))
    user.contact_details.append(ContactDetail(u'extension', u'656'))
    db.session.add(user)

    user = Person(u'חדר סגל', 'male', u'other')
    user.contact_details.append(ContactDetail(u'green_phone', u'03-9999888'))
    user.contact_details.append(ContactDetail(u'matkali', u'033333'))
    user.contact_details.append(ContactDetail(u'extension', u'000'))
    db.session.add(user)

    user = Person(u'בודק מנומס', 'male', u'conscript')
    user.role='checker'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'בודק אדיב', 'male', u'conscript')
    user.role='checker'
    user.main_course_id=course.id
    db.session.add(user)

    user = Person(u'בודק חביב', 'male', u'conscript')
    user.role='checker'
    user.main_course_id=course.id
    db.session.add(user)

    db.session.commit()

def create_students(course, specific_groups, other_groups, number_of_students):
    students = [[u'רן רני', 'male'], [u'שי מורן', 'male'],
                [u'רומי בלהבלה', 'female'],
                [u'בוריס פלוטשקי', 'male']]

    for i in (xrange(number_of_students-4)):
        students.append([u'חניך ' + str(i+5), 'male'])

    people_list = []

    group_specific1 = specific_groups[0]
    group_all = specific_groups[1]

    user = None
    for student_id, student in enumerate(students):
        user = Person(student[0], student[1], u'conscript', course.id)
        user.course_number = student_id + 1
        user.role = "student"
        db.session.add(user)
        people_list.append(user)
        group_all.members.append(user)

        # We don't want all of the students to be in the specific group
        if student_id < 30:
            group_specific1.members.append(user)

        # Add students to the other groups
        other_groups[student_id % len(other_groups)].members.append(user)

    # Last user is 'u99', set his password
    u99 = people_list[3]
    u99.user = User(u'u99', password=u'1234')

    db.session.commit()

    return u99, people_list

def create_rooms(course):
    for i in xrange(1, 20):
        room = Room(u'כיתה %d' % i, u'class %d' % i)
        course.rooms.append(room)
        db.session.add(room)

def create_tracks(course):
    track = Track(u"מגמה1", u'megama1')
    course.tracks.append(track)
    db.session.add(track)

    track = Track(u"מגמה2", u'megama2')
    course.tracks.append(track)
    db.session.add(track)

    track = Track(u"מגמה3", u'megama3')
    course.tracks.append(track)
    db.session.add(track)

    track = Track(u"מגמה4", u'megama4')
    course.tracks.append(track)
    db.session.add(track)

def create_array_instances(array, group, num_of_exercises, num_of_submissions):
    """
    Add mock lesson instances
    :type array: Array
    :type group: Group
    """
    unit1 = EducationUnit(u'יח"ל 1 - היחידה שלי', array=array, order=1, group=group)
    unit2 = EducationUnit(u'יח"ל 2 - דבירם מתקדמים', array=array, order=2, group=group)

    array.units.append(unit1)
    array.units.append(unit2)
    db.session.add(unit1)
    db.session.add(unit2)

    # Make sure that all of the units and arrays are committed before
    # creating any instances
    db.session.commit()

    inst = LessonInstance(u'תמ"כ Hangman', u'tamak', 3, 3, array, unit1)
    inst.tasks.append(InstanceTask(u'לבדוק שהכל קיים', datetime(2012, 02, 3), datetime(2013, 03, 5)))
    inst.placements.append(RealPlacement(datetime(2013, 3, 5, 10, 0, 0), 3,
                                         bizur_people=[Person.get_by_name(u'איש סגל')]))

    inst = LessonInstance(u'סיכום תמ"כ', u'cut', 1.5, 1.5, array, unit1)
    inst.tasks.append(InstanceTask(u'לבדוק שהכל קיים', datetime(2012, 02, 3), datetime(2013, 03, 4)))
    inst.tasks.append(InstanceTask(u'להשיג סכין לחיתוך', datetime(2012, 02, 3)))
    inst.tasks.append(InstanceTask(u'להעביר את הזמן', datetime(2012, 02, 3)))
    inst.tasks.append(InstanceTask(u'להכין template לfollowup', datetime(2012, 02, 3)))

    #placement = PlannedPlacement(week=10, bizur_people=[Person.get_by_name(u'איש סגל')])
    #inst.placements.append(placement)

    inst = LessonInstance(u'סינטקס בסיסי', u'lesson', 2, 2, array, unit1)
    inst.tasks.append(InstanceTask(u'לבדוק שהכל קיים', datetime(2012, 02, 3), datetime(2013, 03, 5)))
    inst.tasks.append(InstanceTask(u'לבדוק שיש template לfollowup', datetime(2012, 12, 3), datetime(2013, 03, 5)))
    inst.tasks.append(InstanceTask(u'להעביר את הזמן', datetime(2012, 11, 5)))
    inst.tasks.append(InstanceTask(u'להעביר עוד זמן', datetime(2012, 9, 7)))
    inst.tasks.append(InstanceTask(u'להעביר עוד קצת זמן', datetime(2012, 06, 7)))

    inst = LessonInstance(u'דברים בסיסיים', u'self-work', 5, 5, array, unit1)
    #inst.placements.append(PlannedPlacement(week=11, bizur_people=[Person.get_by_name(u'איש סגל')]))

    inst = LessonInstance(u'Cool lesson', u'lesson', 2, 2, array, unit1)
    #placement = PlannedPlacement(week=11, bizur_people=[Person.get_by_name(u'איש סגל')])
    #inst.placements.append(placement)

    inst = LessonInstance(u'כלים', u'lesson', 0.5, 0.5, array, unit1)
    inst.tasks.append(InstanceTask(u'להשיג צלחת לשיעור כלים', datetime(2012, 02, 13)))
    inst.tasks.append(InstanceTask(u'להשיג סכין לשיעור כלים', datetime(2012, 02, 13)))
    inst.tasks.append(InstanceTask(u'להשיג מזלג לשיעור כלים', datetime(2012, 02, 13), datetime(2012, 02, 14)))
    inst.placements.append(RealPlacement(datetime(2013, 4, 3, 14), 2,
                                         bizur_people=[Person.get_by_name(u'איש סגל')]))

    inst = LessonInstance(u'עוד שיעור', u'self-work', 9.5, 11, array, unit1)
    inst.tasks.append(InstanceTask(u'משימה סגורה', datetime(2012, 02, 13), datetime(2013, 1, 1)))
    inst.tasks.append(InstanceTask(u'עוד משימה סגורה', datetime(2012, 02, 13), datetime(2013, 1, 2)))
    inst.placements.append(RealPlacement(datetime(2013, 4, 12, 16), 1,
                                         bizur_people=[Person.get_by_name(u'איש סגל')]))

    inst = LessonInstance(u'סוף סוף שיעור שווה', u'lesson', 2, 2, array, unit2)
    inst.tasks.append(InstanceTask(u'משימה סגורה', datetime(2012, 10, 13), datetime(2013, 1, 1)))

    inst = LessonInstance(u'מתקדם', u'self-work', 7, 7, array, unit2)
    inst.tasks.append(InstanceTask(u'משימה סגורה', datetime(2012, 10, 13)))
    inst.tasks.append(InstanceTask(u'משימה פתוחה', datetime(2012, 10, 14)))
    inst.tasks.append(InstanceTask(u'משימה בלתי אפשרית', datetime(2012, 10, 15)))
    inst.tasks.append(InstanceTask(u'טוסטר משולשים', datetime(2012, 10, 16)))
    inst.tasks.append(InstanceTask(u'תיק פתוח', datetime(2012, 10, 17)))
    inst.tasks.append(InstanceTask(u'תיק מנהלים', datetime(2012, 10, 18)))
    inst.tasks.append(InstanceTask(u'תיק ססגוני', datetime(2012, 10, 19)))
    #inst.placements.append(PlannedPlacement(bizur_people=[Person.get_by_name(u'איש סגל')]))

    inst = LessonInstance(u'טוסטרים', u'lesson', 1, 1, array, unit2)
    inst.tasks.append(InstanceTask(u'לקנות טוסטר', datetime(2012, 10, 16)))

    inst = LessonInstance(u'איך לעשות שטיפה', u'lesson', 1, 1, array, unit2)

    inst = LessonInstance(u'טוסטרים', u'self-work', 2.5, 2.5, array, unit2)
    inst.tasks.append(InstanceTask(u'משימה פתוחה', datetime(2012, 10, 13), datetime(2011, 4, 5)))

    inst = LessonInstance(u'איך לעשות שטיפה', u'self-work', 5, 5, array, unit2)
    #inst.placements.append(PlannedPlacement(bizur_people=[Person.get_by_name(u'איש סגל')]))

    inst = LessonInstance(u'Things that go boom, Boost', u'lesson', 1, 1, array, unit2)
    inst.tasks.append(InstanceTask(u'משימה סגורה', datetime(2012, 10, 13), datetime(2013, 1, 1)))

    inst = LessonInstance(u'how to clean your toster', u'lesson', 1, 1, array, unit2)

    inst = LessonInstance(u'Things that go boom, Boost', u'self-work', 2.5, 2.5, array, unit2)

    inst = LessonInstance(u'Things that go boom', u'self-work', 2, 2, array, unit2)

    ex = create_exercise(inst, 'תרגיל מגניב')
    create_criteria_to_exercise(ex)
    ex2 = create_exercise(inst, 'תרגיל רגיל')
    checkers = Person.query.filter(Person.role == 'checker').all()
    students = Person.query.filter(Person.role == 'student').all()

    # Create alot of exercises
    exercises = [ex, ex2] + [create_exercise(inst, 'תרגיל' + str(i+1)) for i in xrange(num_of_exercises)]


    seed = 0
    # For the first num_of_submissions students:
    for i, student in zip(range(num_of_submissions), students):
        j = i + 1
        # Open all the exercise
        for ex in exercises:
            if j > 0:
                assignment = Assignment(person = student, exercise = ex)
                if j % 3 == 0:
                    assignment.state = Assignment.DONE
                else:
                    assignment.state = Assignment.RESEND
                db.session.add(assignment)

                # Submit j solutions to the assignment
                for _ in xrange(j):
                    submission = Submission()
                    submission.review = Review(reviewer = checkers[0], comment_for_staff = "comment for staff", comment_for_student = "comment for student")
                    submission.assignment = assignment
                    db.session.add(submission)

                    # create scores for the submission
                    for criterion in ex.criteria:
                        score = SubmissionScore(score = (j + i*2 + seed) % 5 + 1, comment = 'mock score', submission = submission, criterion = criterion)
                        db.session.add(score)
                        seed += 2

                j -= 1


    db.session.add(CheckersAssignments(exercise=ex, checker=checkers[0], student=students[0]))
    db.session.add(CheckersAssignments(exercise=ex, checker=checkers[1], student=students[0]))
    db.session.add(CheckersAssignments(exercise=ex, checker=checkers[1], student=students[1]))
    db.session.add(CheckersAssignments(exercise=ex2, checker=checkers[2], student=students[2]))

    db.session.add(CheckersArrays(array=array, checker=checkers[0]))
    db.session.add(CheckersArrays(array=array, checker=checkers[1]))

def create_points(students, number_of_points):

    for i in xrange(number_of_points):
        p = Point(person=students[i % len(students)])
        m = Message(text='My first text!!!!!', point=p, person=students[i % len(students)])

        db.session.add(p)
        db.session.add(m)

    db.session.commit()

def create_criteria(array):

    for i in xrange(1, 4):
        thoroughness = Criterion()
        thoroughness.array = array
        thoroughness.name = "יסודיות%d" % i
        thoroughness.low_score_description = "מאוד לא יסודי"
        thoroughness.middle_score_description = "יסודי בערך"
        thoroughness.high_score_description = "יסודי מאוד! איזה גבר"

        creativeness = Criterion()
        creativeness.array = array
        creativeness.name = "יצירתיות%d" % i
        creativeness.low_score_description = "מאוד לא יצירתי"
        creativeness.middle_score_description = "יצירתי בערך"
        creativeness.high_score_description = "יצירתי מאוד! איזה גבר"

        db.session.add(thoroughness)
        db.session.add(creativeness)

def create_criteria_to_exercise(exercise):
    coolness = Criterion()
    coolness.name = "מגניבות"
    coolness.low_score_description = "לא מגניב בכלל"
    coolness.middle_score_description = "חצי מגניב"
    coolness.high_score_description = "מגניב בטירוף"
    db.session.add(coolness)
    coolness.exercises.append(exercise)

def create_meetings(student):
    person = Person.query.filter(Person.name == u"בוריס פלוטשקי").first()
    meeting = Meeting()
    meeting.title = "שיחה ראשונה עם בוריס"
    # Should be according to http://www.w3.org/TR/html-markup/input.date.html
    meeting.scheduled_time = (datetime.now()).strftime("%Y-%m-%d")
    meeting.agenda = "א) לראות מה שלומו\nב) לספר לו שאני אוהב אותו"
    meeting.summary = "דיברנו על שלומו, ושלומו בסדר. אמרתי לו שאני אוהב אותו והוא קצת הופתע."
    meeting.short_summary = "צריך לפתח אצל החניך פתיחות לאהבה"
    meeting.student_id = student.id
    meeting.staff_id = '9'
    db.session.add(meeting)
