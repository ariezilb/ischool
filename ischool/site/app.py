import logging
from flask import Flask, abort, g, session
from ischool.config import IschoolConfig
from ischool.models import Person, User, current_locals
from .blueprints import ischool
from .extensions import db, api_manager
from .utils.converters import extra_converters
from .utils.jinja_filters import jinja_env_tests

DEFAULT_BLUEPRINTS = (
    ischool,
)

def create_app(config=None, app_name=None, blueprints=None):
    """Create a Flask app."""

    if config is None:
        config = IschoolConfig
    if app_name is None:
        app_name = config.PROJECT
    if blueprints is None:
        blueprints = DEFAULT_BLUEPRINTS


    # Serve static files through the blueprint and not
    # the main app
    app = Flask(app_name, static_url_path="")
    configure_app(app, config)
    configure_hooks(app)
    configure_extensions(app)
    configure_blueprints(app, blueprints)

    return app

def configure_app(app, config=None):
    if config:
        app.config.from_object(config)
        default_logger = logging.getLogger("ischool-logger")
        for handler in default_logger.handlers:
            app.logger.addHandler(handler)

def configure_hooks(app):

    # This actually supposed to be @app.before_request, but it should precede assign_main_course routine
    @app.url_value_preprocessor
    def set_person(endpoint, values):
        """
        Validates authorization and assigns g.person = models.Person and g.connected = True/False
        :param endpoint: Current endpoint (view function name)
        :param values: useless here
        """
        g.connected = False
        g.person = None
        g.course = None

        # Is there any session?
        if 'uid' not in session:
            # Here we could have set a later redirection to login_view. Instead, we trust our permissions system
            # and just stop the person lookup.
            return
        else:
            uid = session['uid']

        person = Person.query \
            .join(Person.user) \
            .filter(User.person_id == uid) \
            .options(db.joinedload('main_course')).first()

        if person is None:
            raise ValueError("invalid uid (%d)" % uid)

        g.person = person

        if not person:
            raise ValueError('User #%d is not authorized!' % int(uid))
        g.connected = True


    @app.url_value_preprocessor
    def assign_main_course(endpoint, values):
        """
        Assigns main course object if course is sent by URL.
        Falls to g.person's main course if not found, or None if both not found

        There are 3 ways to set the main course:
        * by URL - /courses/<course:course>/...
        * by object - /groups/<group:group>/... - make sure to register a CourseObjectConverter converter
            (see @utils.converters)
        * by g.person.main_course
        If none found, g.course is None
        :param endpoint: Current endpoint (view function name)
        :param values: List of URL values, e.g. /courses/<course:course>/<id:int> => ['course', 'id']
        """
        if values and 'course' in values:
            course = values.pop('course')
            g.course = course()
            return

        if g.person:
            if not g.person.role:
                print 'Person with no main role!'
                abort(403)
            else:
                g.course = g.person.main_course


    @app.before_first_request
    def set_models_locals():
        """
        Sets m.ModelName in jinja globals (useful sometimes)
        """
        app.jinja_env.globals['m'] = current_locals


def configure_blueprints(app, blueprints):
    import ischool.site.views
    for blueprint in blueprints:
        app.register_blueprint(blueprint)

def configure_extensions(app):
    db.init_app(app)
    db.app = app

    api_manager.init_app(app, flask_sqlalchemy_db=db)
    app.url_map.converters.update(extra_converters)
    app.jinja_env.tests.update(jinja_env_tests)
